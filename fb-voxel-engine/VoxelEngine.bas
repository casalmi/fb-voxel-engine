#ifndef MAIN
#define MAIN

#include once "VoxelEngine.bi"

const WINDOW_WIDTH  as integer = 1920
const WINDOW_HEIGHT as integer = 1080

const CAMERA_FOV        as double = 60.0
const CAMERA_RATIO      as double = (WINDOW_WIDTH*1.0)/(WINDOW_HEIGHT*1.0)
const CAMERA_NEAR_PLANE as double = .01
const CAMERA_FAR_PLANE  as double = 20000

''''''''''''''''''''''''''''''Context Initializers'''''''''''''''''''''''''''''
'So, in order to initalize any function grabs (opengl 3.* ones),
'you need to have an active context running...
'*Which in turn effectively removes "print" from the viable functions list*
glScreen(WINDOW_WIDTH, WINDOW_HEIGHT, 32, 32, , 0)

InitOpenGLExt()

'''''''''''''''''''''''''''''''''''OpenCL INIT'''''''''''''''''''''''''''''''''

dprint("Init OpenCL")
OpenCL.initOpenCL("OpenCL/VoxelKernels.cl", "-cl-std=CL1.1 -I OpenCL/")

dprint("    -Init OpenCL kernels")
NoiseKernel.KernelHandle            = OpenCL.initOpenCLKernel("terrainNoise")
'GetGradientKernel.KernelHandle      = OpenCL.initOpenCLKernel("getGradients3D")
MCSortVoxelsKernel.KernelHandle     = OpenCL.initOpenCLKernel("MCSortVoxels")
'MCExtractMeshKernel.KernelHandle    = OpenCL.initOpenCLKernel("MCExtractMesh")
DCSortEdgesKernel.KernelHandle      = OpenCL.initOpenCLKernel("DCSortEdges")
DCGetHermitData.KernelHandle        = OpenCL.initOpenCLKernel("DCGetHermiteData")
dprint("Done initilizing OpenCL")

''''''''''''''''''''''''''''''''''Global Data''''''''''''''''''''''''''''''''''

'-Set the camera stuff up.
dim shared Cam as CameraType = CameraType(CAMERA_FOV, CAMERA_RATIO, CAMERA_NEAR_PLANE, CAMERA_FAR_PLANE)

'Cam.SetPosition(10, 10, -10)
Cam.SetPosition(0,0,0)
Cam.SetSensitivity(0.1)

Cam.InitMouse(WINDOW_WIDTH, WINDOW_HEIGHT)

dim shared testVoxelInstance as VoxelInstance3DType ptr
testVoxelInstance = new VoxelInstance3DType(64, 32)
if testVoxelInstance->GenerateInstance(0) = 0 then
    dprint("nothing generated")
end if
dprint("Voxel instance = ";testVoxelInstance->Octree->Children)

dim testVoxelChunk as VoxelChunkType ptr
testVoxelChunk = new VoxelChunkType(.25, 32, Vector3DType(testVoxelInstance->Offset(0), testVoxelInstance->Offset(1), testVoxelInstance->Offset(2)))

testVoxelChunk->Octree = testVoxelInstance->Octree
testVoxelCHunk->FeaturedVoxelCOunt = testVoxelInstance->FeaturedVoxelCount
testVoxelChunk->DataStage = VoxelChunkType.HAS_QEF_DATA

testVoxelChunk->ExtractMesh_GenerateContour()

'delete(testVoxelInstance)

'The test voxel grid.
'(#_of_voxels_per_grid(cubed), scale_of_each_voxel, level_of_detail_count, level_of_detail_distance_constant)
dim testVoxelGrid as VoxelGridType ptr

testVoxelGrid = new VoxelGridType(32,GLOBAL_GRID_ALIGNMENT_SCALE,13,12)

testVoxelGrid->shaderProgram = new OpenGL_Shader_TYPE("ShaderPack/Shaders/TerrainVert.txt", _
                                                      "ShaderPack/Shaders/TerrainFrag.txt")

dim DeferredBuffer as DeferredShaderType ptr

DeferredBuffer = new DeferredShaderType(WINDOW_WIDTH, WINDOW_HEIGHT)

DeferredBuffer->GeometryProgram = new OpenGL_Shader_TYPE("ShaderPack/Shaders/DeferredGeometryVert.txt", _
                                                         "ShaderPack/Shaders/DeferredGeometryFrag.txt")

DeferredBuffer->ResultProgram = new OpenGL_Shader_TYPE("ShaderPack/Shaders/DeferredResultVert.txt", _
                                                       "ShaderPack/Shaders/DeferredResultFrag.txt")

dim Skybox as SkyboxType ptr = new SkyBoxType("Media/sky_posz.bmp", _
                                              "Media/sky_negz.bmp", _
                                              "Media/sky_negy.bmp", _
                                              "Media/sky_posy.bmp", _
                                              "Media/sky_negx.bmp", _
                                              "Media/sky_posx.bmp", _
                                              "ShaderPack/Shaders/SkyboxVert.txt", _
                                              "ShaderPack/Shaders/SkyboxFrag.txt")

'''''''''''''''''''''''''''''''''''OpenGL INIT'''''''''''''''''''''''''''''''''

'Turn this on if you want...
glEnable(GL_CULL_FACE)
'glDisable(GL_CULL_FACE)
glCullFace(GL_BACK)
'glCullFace(GL_FRONT)

glEnable(GL_DEPTH_TEST)

glClearColor(0, 0, 1, 1)

'''''''''''''''''''''''''''''''''''''TEMPS'''''''''''''''''''''''''''''''''''''
dim as integer fillerA, fillerB, Buttons

dim i as integer
OctreeShader = new OpenGL_Shader_TYPE("ShaderPack/Shaders/OctreeVert.txt", "ShaderPack/Shaders/OctreeFrag.txt")

/'dprint("Size of VoxelNodeType: ";sizeof(VoxelNodeType))
dprint("Size of OctreeType: ";sizeof(OctreeType))
dprint("Size of LinkedListType: ";sizeof(LinkedListType))
dprint("Size of NodeType: ";sizeof(NodeType))
'/

dim shared TempPos as Vector3DType
dim toggle as byte = 1

dim UpdateTimer as double
dim count as integer
dim tempTime as double = 0
dim UpdateTime as double

tempPos = Cam.Position

testVoxelGrid->CameraReference = @tempPos

sleep(100)

dim height as single = 0

'''''''''''''''''''''''''''''''''''MAIN LOOP'''''''''''''''''''''''''''''''''''
while not MultiKey(FB.SC_ESCAPE) AND (inkey() <> (chr(255) + "k"))
        
    UpdateTimer = timer
        
    Cam.UpdateFlyAround(0.0233, 60)

    ''''''''''''''''''RENDER CODE''''''''''''''''''

    glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
    
    'DeferredBuffer->BindForWriting()
    
    Skybox->Render(Cam.viewMatrix(), Cam.projMatrix())
    
    'SetUniformMatrix4(DeferredBuffer->GeometryProgram->ShaderHandle, "viewMatrix", Cam.viewMatrix())
    'SetUniformMatrix4(DeferredBuffer->GeometryProgram->ShaderHandle, "projMatrix", Cam.projMatrix())
    
    'SetUniformInt(DeferredBuffer->GeometryProgram->ShaderHandle, "material1", 0)
    'SetUniformInt(DeferredBuffer->GeometryProgram->ShaderHandle, "material2", 1)
    'SetUniformInt(DeferredBuffer->GeometryProgram->ShaderHandle, "material3", 2)
    
    testVoxelGrid->RenderEnterProtocol(testVoxelGrid->ChunkOctree)
    
        testVoxelGrid->Render(Cam.viewMatrix(), Cam.projMatrix(), testVoxelGrid->ChunkOctree)
        testVoxelGrid->CleanList()
        
        testVoxelCHunk->Render()
        
    testVoxelGrid->RenderExitProtocol(testVoxelGrid->ChunkOctree)
    
    'DeferredBuffer->VisualizeBuffer(DeferredShaderType.TEXTURE_TYPE_POSITION)
    
    'DeferredBuffer->RenderFullScreen()
    
    if toggle then
        tempPos = Cam.Position
    end if
    
    UpdateTimer = timer
    
    /'testVoxelGrid->LockData()
        
        'CheckIntegrity(testVoxelGrid->TestOctree)
        
        if NOT MultiKey(FB.SC_P) then
        'testVoxelGrid->ChunkOctree->Render3D(OctreeShader->ShaderHandle, _
        '                                    Cam.viewMatrix(), _
        '                                    Cam.projMatrix(), 0)
        
        
        else
            testVoxelGrid->VoxelOctree->Render3D(OctreeShader->ShaderHandle, _
                                                Cam.viewMatrix(), _
                                                Cam.projMatrix(), 1)
    
        end if
    
    testVoxelGrid->UnlockData()
    '/
    
    
    ''''''''''''''''END RENDER CODE''''''''''''''''''
    
    'testVoxelInstance->Octree->Render3D(OctreeShader->ShaderHandle, Cam.viewMatrix(), Cam.projMatrix(),1)
    
    flip
    
    'dprint("Frame time: ";timer - UpdateTimer)

    'WaitTimer(1000.0/60.0)

    WindowTitle(VoxelChunkTypeCount &" "& VoxelNodeTypeCount &" "& OctreeTypeCount &" "& DynamicBufferTypeCount &" "& _
        CachedBufferTypeCount &" "& OpenglObjectTypeCount &" "& NodeTypeCount &" "& " : " & cam.Position & " " & toggle)
    
    GetMouse(FillerA, FillerB,,Buttons)
    
    if MultiKey(FB.SC_P) then
        toggle = 1
    end if
    
    if MultiKey(FB.SC_O) then
        toggle = 0
    end if
    
    if (Buttons AND 1) then
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    end if
    
wend

''''''''''''''''''''''''''''''''''''CLEANUP''''''''''''''''''''''''''''''''''''

delete(testVoxelGrid)
delete(Skybox)

delete(OctreeShader)

delete(DeferredBuffer)

#endif