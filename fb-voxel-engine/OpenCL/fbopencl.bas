#ifndef fbopencl_bas
#define fbopencl_bas

#ifdef __FB_64BIT__
    #inclib "OpenCL"
#else
    #inclib "OpenCL32"
#endif

#include once "fbgfx.bi"

#include once "cl.bi"
#include once "cl_gl.bi"

#include once "fbopencl.bi"

#ifndef NULL
#define NULL cptr(any ptr,0)
#endif

#define CL_DEBUG 1

sub CLCheckError(status as cl_int, Desc as String)
    
    if status <> CL_SUCCESS then
        #ifdef USE_DPRINT
        dprint(Desc;" Error: "; status)
        #else
        print Desc;" Error: "; status
        #endif
        sleep
    end if
    
end sub

' OpenCL related initialization
' Create Context, Device list, Command Queue
' Create OpenCL memory buffer objects
' Load CL file, compile, link CL source
' Build program and kernel objects

Constructor OpenCL_TYPE()

end Constructor

sub OpenCL_TYPE.initOpenCL(OpenCLFileName as string, CompilerFlags as zstring)
    
    dim as cl_int status = 0
    dim as size_t deviceListSize
    
    dim USE_CPU as integer = 0
    
    ' Have a look at the available platforms
    ' and pick either the AMD or NVIDIA.
    dim as cl_uint numPlatforms
    dim as  cl_platform_id_t platform = NULL
    status = clGetPlatformIDs(0, NULL, @numPlatforms)
    if (status<>CL_SUCCESS) then
        print "Error: Getting Platforms. (clGetPlatformsIDs)"
        return
    end if


    if (numPlatforms > 0) then
        
        dim as cl_platform_id_t ptr platforms = callocate(numPlatforms*sizeof(cl_platform_id_t))
        status = clGetPlatformIDs(numPlatforms, platforms, NULL)
        
        if (status<>CL_SUCCESS) then
            print "Error: Getting Platform Ids. (clGetPlatformsIDs)"
            return
        end if
        dim as zstring ptr pbuff = callocate(255)
        for i as uinteger = 0  to numPlatforms-1
            status = clGetPlatformInfo(platforms[i], _
                                       CL_PLATFORM_VENDOR, _
                                       255, _
                                       pbuff, _
                                       NULL)
            if (status<>CL_SUCCESS) then
                print "Error: Getting Platform Info. (clGetPlatformInfo)"
                return
            end if
            dprint("platform[" & i & "] = " & *pbuff)
        
            platform = platforms[i]
            
            if (i = 0) AND (inStr(*pbuff, "Intel") <> 0) then
                USE_CPU = 1
                dprint("Using CPU implementation")
            else
                dprint("Using GPU implementation")
            end if
            
        next
        
        deallocate(pbuff)
        deallocate(platforms)        
    end if

    ' If we could find our platform, use it.
    ' Otherwise pass a NULL and get whatever
    ' the implementation thinks we should be using.
    
    dim as cl_context_properties_t cps(6)
    cps(0) = CL_CONTEXT_PLATFORM
    cps(1) = cast(cl_context_properties_t,platform)
    cps(2) = CL_GL_CONTEXT_KHR
#ifdef __FB_WIN32__
    cps(3) = cast(cl_context_properties_t, wglGetCurrentContext())
    cps(4) = CL_WGL_HDC_KHR
    cps(5) = cast(cl_context_properties_t, wglGetCurrentDC())
#else
    'Linux version (This is yet untested)
    cps(3) = cast(cl_context_properties_t, glxGetCurrentContext())
    cps(4) = CL_WGL_HDC_KHR
    cps(5) = cast(cl_context_properties_t, glxGetCurrentDC())
#endif
    cps(6) = 0
    
    
    dim as cl_context_properties_t ptr cprops
    if (NULL=platform) then
        cprops = NULL
    else
        cprops = @cps(0)
    end if
    
    'if inStr(
    
    if USE_CPU then
        'Create an OpenCL context on the CPU
        this.CONTEXT = clCreateContextFromType(cprops, _
                                               CL_DEVICE_TYPE_CPU, _
                                               NULL, _
                                               NULL, _
                                               @status)
    else
    
        ' Create an OpenCL context on the GPU
        this.CONTEXT = clCreateContextFromType(cprops, _
                                               CL_DEVICE_TYPE_GPU, _
                                               NULL, _
                                               NULL, _
                                               @status)
    end if

    if (status<>CL_SUCCESS) then
        print "Error: Creating Context. (clCreateContextFromType)"
        return
    end if

    ' First, get the size of device list data
    status = clGetContextInfo(this.CONTEXT, _
                              CL_CONTEXT_DEVICES, _
                              0, _
                              NULL, _
                              @deviceListSize)
    
    if(status <> CL_SUCCESS) then
        print "Error: Getting Context Info (device list size, clGetContextInfo)"
        return
    end if
    
    ' Detect OpenCL devices
    this.DEVICES = callocate(deviceListSize)
    if (this.DEVICES=0) then
        print "Error: No devices found."
        return
    end if

    ' Now, get the device list data
    ' NOTE: this only _assumes_ that OpenGL interop is supported on this device
    status = clGetContextInfo(this.CONTEXT, _
                              CL_CONTEXT_DEVICES, _
                              deviceListSize, _
                              this.DEVICES, _
                              NULL)
       
    if (status<>CL_SUCCESS) then
        print "Error: Getting Context Info (device list, clGetContextInfo)"
        return
    end if
    
    ' Create an OpenCL command queue
    this.COMMAND_QUEUE = clCreateCommandQueue(this.CONTEXT, _
                                              this.DEVICES[0], _
                                              0, _
                                              @status)
    if (status<>CL_SUCCESS) then
        print "Creating Command Queue. (clCreateCommandQueue)"
        return
    end if

    ' Load CL file
    ' build CL program object
    ' create CL kernel object
    dim filename as string = OpenCLFileName
    chdir exepath

    dim as integer hFile = FreeFile
    if open(filename for binary access read as #hfile) then
        print "Error: load cl file !"
        beep:sleep:return
    end if

    dim as integer FileSize=lof(hFile)
    if FileSize<1 then
        close #hFile
        print "error: empty cl file !"
        beep:sleep:return
    end if

    dim as string strSource = space(filesize)
    get #hFile,,strSource
    close #hFile
    dim as zstring ptr pSource=@strSource[0]

    this.PROGRAM = clCreateProgramWithSource(this.CONTEXT, _
                                             1, _
                                             @pSource, _
                                             @filesize, _
                                             @status)
    if (status<>CL_SUCCESS) then
        print "Error: Loading source code (clCreateProgramWithSource)"
        if CL_DEBUG then
            beep:sleep:end
        else
            beep:sleep:return
        end if
    end if

    ' create a cl program executable
    ' for all the devices specified
    status = clBuildProgram(this.PROGRAM, 1, this.DEVICES, @CompilerFlags, NULL, NULL)
    if (status<>CL_SUCCESS) then
        print "Error: Building Program (clBuildProgram)"
        if CL_DEBUG then
            beep:sleep:end
        else
            beep:sleep:return
        end if
    end if
    
end sub

function OpenCL_TYPE.initOpenCLKernel(kernelName as string) as cl_kernel_t
    
    dim status as cl_int = 0
    
    dim returnKernelHandle as cl_kernel_t = 0
    
    returnKernelHandle = clCreateKernel(this.PROGRAM, kernelName, @status)
    if (status<>CL_SUCCESS) then
        print "Error: Creating Kernel from program. (clCreateKernel)"; kernelName
        if CL_DEBUG then
            beep:sleep:end
        else
            beep:sleep:return 0
        end if
    end if
    
    return returnKernelHandle
    
end function

' Release OpenCL resources (Context, Memory etc.)
Destructor OpenCL_TYPE()
    
    dprint("~OpenCL_TYPE")
    
    dim status as cl_int = 0
    
    status = clReleaseProgram(this.PROGRAM)
    if status <> CL_SUCCESS then
        print "Error: In clReleaseProgram"
    end if
    
    status = clReleaseCommandQueue(this.COMMAND_QUEUE)
    if status <> CL_SUCCESS then
        print "Error: In clReleaseCommandQueue"
    end if
    
    status = clReleaseContext(this.CONTEXT)
    if status <> CL_SUCCESS then
        print "Error: In clReleaseContext"
    end if

    
    if this.DEVICES <> NULL then
        deallocate this.DEVICES
        assert(this.DEVICES = NULL)
    end if
    
end Destructor

Constructor OpenCL_Kernel_Type()
    
    this.KernelHandle = 0
    
end Constructor

Destructor OpenCL_Kernel_TYPE()
    
    dprint("~OpenCL_Kernel_TYPE")
    
    dim status as cl_int
    if this.KernelHandle <> 0 then
        status = clReleaseKernel(this.KernelHandle)
        if (status<>CL_SUCCESS) then
            print "Error: In clReleaseContext"
        end if
    end if

end Destructor

' Print no more than 256 elements of the given array.
' Print Array name followed by elements.
sub print1DArrayInteger(arrayName as string, _
                        arrayData as integer ptr, _
                        length    as uinteger)
  dim as cl_uint i
  dim as cl_uint numElementsToPrint = iif(length>256,256,length)
  dprint(arrayName)
  for i = 0 to numElementsToPrint-1
    dprint(arrayData[i] & " ";)
  next
  dprint()
end sub

sub print1DArraySingle(arrayName as string, _
                        arrayData as single ptr, _
                        length    as uinteger)
  dim as cl_uint i
  dim as cl_uint numElementsToPrint = iif(length>256,256,length)
  dprint(arrayName)
  for i = 0 to numElementsToPrint-1
    dprint(arrayData[i] & " ";)
  next
  dprint()
end sub

#endif