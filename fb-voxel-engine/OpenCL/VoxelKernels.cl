/*
 * Sample kernel which multiplies every element of the input array with
 * a constant and stores it at the corresponding output array
 */
 
#include "MarchingCubesCL.cl"
#include "DualContouringCL.cl"
#include "Compression.cl"
 
#define SIMPLEX_F2 0.3660254037844
#define SIMPLEX_G2 0.2113248654051

#define SIMPLEX_F3 0.3333333333333
#define SIMPLEX_G3 0.1666666666667
 
/*__constant unsigned char SimplexPerm[512] = {151,160,137,91,90,15,

  131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,

  190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,

  88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,

  77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,

  102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,

  135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,

  5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,

  223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,

  129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,

  251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,

  49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,

  138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,

  151,160,137,91,90,15,

  131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,

  190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,

  88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,

  77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,

  102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,

  135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,

  5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,

  223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,

  129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,

  251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,

  49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,

  138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180,  

};
*/

#define FastFloor(value) ((int)(value - (1 * (value <= 0))))

__constant char GRAD3[12][3] = {
{1,1,0},{-1,1,0},{1,-1,0},{-1,-1,0},{1,0,1},
{-1,0,1},{1,0,-1},{-1,0,-1},{0,1,1},{0,-1,1},
{0,1,-1},{0,-1,-1} };

//Many "if" statements were replaced with functional equivelants
//that should eliminate instruction branching and give full
//core/thread saturation
	
float noise2D(const float seed, const float x, const float y){
	
	//Returns a value between 0 and 1 with resolution of 2^14
	//Repeat period = ?
    int n = (x * 32767) * seed + y * seed;
    return (((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) & 32767) / 32768.0;
	
}

unsigned char SimplexPerm(const int inVal, const float seed){

	return (unsigned char)(noise2D(seed, inVal & 255, -(inVal & 255)) * 255);

}

float SimplexNoise2D(const float x, const float y, const float seed){
	
	float n0,n1,n2;
	
	// Skew the input space to determine which simplex cell we're in
	float s = (x + y) * SIMPLEX_F2;
	int   i = FastFloor(x + s);
	int   j = FastFloor(y + s);
	
	float t = (i + j) * SIMPLEX_G2;
    
    //Unskew the cell origin back to (x,y) space
    float A0 = i-t;
    float B0 = j-t;
    
    //The x,y distances from the cell origin
    float x0 = x-A0;
    float y0 = y-B0;

    // For the 2D case, the simplex shape is an equilateral triangle.
    // Determine which simplex we are in.
    int i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
	
	//Optimization:
	i1 = 1 * (x0 > y0);
	j1 = 1 * (x0 <= y0);
	
    /*if (x0 > y0){
        // lower triangle, XY order: (0,0)->(1,0)->(1,1)
        i1=1;
        j1=0;
    }else{
        // upper triangle, YX order: (0,0)->(0,1)->(1,1)
        i1=0;
        j1=1;
    }*/

    // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
    // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
    // c = (3-sqrt(3))/6
    float x1 = x0 - i1 + SIMPLEX_G2; // Offsets for middle corner in (x,y) unskewed coords
    float y1 = y0 - j1 + SIMPLEX_G2;
    float x2 = x0 - 1.0 + 2.0 * SIMPLEX_G2; // Offsets for last corner in (x,y) unskewed coords
    float y2 = y0 - 1.0 + 2.0 * SIMPLEX_G2;

    // Work out the hashed gradient indices of the three simplex corners
    int ii = i & 255;
	int jj = j & 255;
    int gi0 = SimplexPerm(ii+SimplexPerm(jj, seed), seed) % 12;
    int gi1 = SimplexPerm(ii+i1+SimplexPerm(jj+j1, seed), seed) % 12;
    int gi2 = SimplexPerm(ii+1+SimplexPerm(jj+1, seed), seed) % 12;

    // Calculate the contribution from the three corners
    float t0 = 0.5 - x0*x0-y0*y0;
    //Optimization:
	t0 = t0 * t0 * (t0 >= 0);
	n0 = t0 * t0 * (GRAD3[gi0][0] * x0 + GRAD3[gi0][1] * y0) * (t0 >= 0);
	
	/*if (t0 < 0){
        n0 = 0.0;
    }else{
        t0 = t0 * t0;
        n0 = t0 * t0 * (GRAD3[gi0][0] * x0 + GRAD3[gi0][1] * y0); // (x,y) of GRAD3 used for 2D gradient
    }*/

    float t1 = 0.5 - x1*x1-y1*y1;
	//Optimization:
	t1 = t1 * t1 * (t1 >= 0);
	n1 = t1 * t1 * (GRAD3[gi1][0] * x1 + GRAD3[gi1][1] * y1) * (t1 >= 0);
	
    /*if (t1 < 0){
        n1 = 0.0;
    }else{
        t1 *= t1;
        n1 = t1 * t1 * (GRAD3[gi1][0] * x1 + GRAD3[gi1][1] * y1);
    }*/

    float t2 = 0.5 - x2*x2-y2*y2;
	//Optimization:
	t2 = t2 * t2 * (t2 >= 0);
	n2 = t2 * t2 * (GRAD3[gi2][0] * x2 + GRAD3[gi2][1] * y2) * (t2 >= 0);
	
    /*if (t2 < 0){
        n2 = 0.0;
    }else{
        t2 *= t2;
        n2 = t2 * t2 * (GRAD3[gi2][0] * x2 + GRAD3[gi2][1] * y2);
    }*/

    // Add contributions from each corner to get the final noise value.
    // The result is scaled to return values in the interval [-1,1].
	
    return 70.0 * (n0 + n1 + n2);
	
}

float SimplexNoise3D(const float x, const float y, const float z, const float seed){

    float4 n;

    // Skew the input space to determine which simplex cell we//re in
    float s = (x+y+z)*SIMPLEX_F3;
	
	int4 skew;
	skew.x = FastFloor( x + s );
	skew.y = FastFloor( y + s );
	skew.z = FastFloor( z + s );
	
	/*int i = FastFloor( x + s );
    int j = FastFloor( y + s );
    int k = FastFloor( z + s );
	*/
	
    float t = (skew.x+skew.y+skew.z)*SIMPLEX_G3;
    
    //Unskew the cell origin back to (x,y,z) space
    float4 unskew;
	//unskew = skew - (float4)(t,t,t,0.0);
	unskew.x = skew.x-t;
	unskew.y = skew.y-t;
	unskew.z = skew.z-t;
	
	/*float A0 = skew.x-t;
    float B0 = skew.y-t;
    float C0 = skew.z-t;
    */
    //The x,y,z distances from the cell origin
	
	float4 d;
	d = ((float4)(x,y,z,1.0)) - unskew;
	/*d.x = x-unskew.x;
	d.y = y-unskew.y;
	d.z = z-unskew.z;*/
	
	/*float x0 = x-unskew.x;
    float y0 = y-unskew.y;
    float d.z = z-unskew.z;*/

    // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
    // Determine which simplex we are in.
    int i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
    int i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
	
	//Optimization:
	i1 = (((d.y >= d.z) || (d.x >= d.z)) && (d.x >= d.y));
	j1 = ((d.y >= d.z) && (d.x < d.y));
	k1 = ((d.y < d.z) && (d.x < d.z) && (d.x >= d.y)) + ((d.y < d.z) && (d.x < d.y));
	i2 = ((d.x < d.y) && (d.y >= d.z) && (d.x >= d.z)) + (d.x >= d.y);
	j2 = ((d.y >= d.z) && (d.x >= d.y)) + ((d.x < d.y));
	k2 = ((d.y < d.z) && (d.x >= d.y)) + (((d.y < d.z) || (d.x < d.z)) && (d.x < d.y));
	
    /*if(d.x >= d.y){
        
		//Optimization:
		i1 = 1 * ((d.y >= d.z) || (d.x >= d.z));
		j1 = 0;
		k1 = 1 * ((d.y < d.z) && (d.x < d.z));
		i2 = 1;
		j2 = 1 * (d.y >= d.z);
		k2 = 1 * (d.y < d.z);
		
        if(d.y >= d.z){
            i1=1; j1=0; k1=0; i2=1; j2=1; k2=0; // X Y Z order
        }else if(d.x >= d.z){
            i1=1; j1=0; k1=0; i2=1; j2=0; k2=1; // X Z Y order
        }else{
            i1=0; j1=0; k1=1; i2=1; j2=0; k2=1; // Z X Y order
        }
        
    }else{ // d.x<d.y
        
		//Optimization
		i1 = 0;
		j1 = 1 * (d.y >= d.z);
		k1 = 1 * (d.y < d.z);
		i2 = 1 * ((d.y >= d.z) && (d.x >= d.z));
		j2 = 1;
		k2 = 1 * ((d.y < d.z) || (d.x < d.z));
		
        if(d.y < d.z){
            i1=0; j1=0; k1=1; i2=0; j2=1; k2=1; // Z Y X order
        }else if(d.x < d.z){
            i1=0; j1=1; k1=0; i2=0; j2=1; k2=1; // Y Z X order
        }else{ 
            i1=0; j1=1; k1=0; i2=1; j2=1; k2=0; // Y X Z order
        }
        
    }*/

    // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
    // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
    // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
    // c = 1/6.
	float4 o1; //Offsets for second corner in (x,y,z) coord
	o1 = d - (float4)(i1,j1,k1,0.0) + (float4)(SIMPLEX_G3);
	
	/*o1.x = d.x - i1 + SIMPLEX_G3;
	o1.y = d.y - j1 + SIMPLEX_G3;
	o1.z = d.z - k1 + SIMPLEX_G3;*/
    /*float x1 = d.x - i1 + SIMPLEX_G3;
    float y1 = d.y - j1 + SIMPLEX_G3;
    float z1 = d.z - k1 + SIMPLEX_G3;
	*/
	float4 o2; //Offsets for third corner in (x,y,z) coords
	o2 = d - (float4)(i2,j2,k2,0.0) + (float4)(2.0*SIMPLEX_G3);
    /*float x2 = d.x - i2 + 2.0*SIMPLEX_G3;
    float y2 = d.y - j2 + 2.0*SIMPLEX_G3;
    float z2 = d.z - k2 + 2.0*SIMPLEX_G3;
	*/
	float4 o3; //Offsets for last corner in (x,y,z) coords
    o3 = d - (float4)(1.0,1.0,1.0,0.0) + (float4)(3.0*SIMPLEX_G3);

	/*float x3 = d.x - 1.0 + 3.0*SIMPLEX_G3;
    float y3 = d.y - 1.0 + 3.0*SIMPLEX_G3;
    float z3 = d.z - 1.0 + 3.0*SIMPLEX_G3;
	*/
	
    // Work out the hashed gradient indices of the four simplex corners
	int4 grad = skew & 0xff;
    /*int ii = skew.x & 0xff;
    int jj = skew.y & 0xff;
    int kk = skew.z & 0xff;*/
    int gi0 = SimplexPerm(grad.x+SimplexPerm(grad.y+SimplexPerm(grad.z, seed), seed), seed) % 12;
    int gi1 = SimplexPerm(grad.x+i1+SimplexPerm(grad.y+j1+SimplexPerm(grad.z+k1, seed), seed), seed) % 12;
    int gi2 = SimplexPerm(grad.x+i2+SimplexPerm(grad.y+j2+SimplexPerm(grad.z+k2, seed), seed), seed) % 12;
    int gi3 = SimplexPerm(grad.x+1+SimplexPerm(grad.y+1+SimplexPerm(grad.z+1, seed), seed), seed) % 12;

    // Calculate the contribution from the four corners
    float t0 = 0.6 - d.x*d.x - d.y*d.y - d.z*d.z;
	//Optimization:
	t0 = t0 * t0 * (t0 >= 0);
	n.x = t0 * t0 * (GRAD3[gi0][0]* d.x + GRAD3[gi0][1]* d.y + GRAD3[gi0][2]* d.z) * (t0 >= 0);
	
    /*if(t0 < 0){
        n.x = 0.0;
    }else{
        t0 *= t0;
        n.x = t0 * t0 * (GRAD3[gi0][0]* d.x + GRAD3[gi0][1]* d.y + GRAD3[gi0][2]* d.z);
    }*/

    float t1 = 0.6 - o1.x*o1.x - o1.y*o1.y - o1.z*o1.z;
	//Optimization
	t1 = t1 * t1 * (t1 >= 0);
    n.y = t1 * t1 * (GRAD3[gi1][0]* o1.x + GRAD3[gi1][1]* o1.y + GRAD3[gi1][2]* o1.z) * (t1 >= 0);
	
    /*if(t1 < 0){
        n.y = 0.0;
    }else{
        t1 *= t1;
        n.y = t1 * t1 * (GRAD3[gi1][0]* x1 + GRAD3[gi1][1]* y1 + GRAD3[gi1][2]* z1);
        //n.y = t1 * t1 * dot(GRAD3[gi1], x1, y1, z1)
    }*/

    float t2 = 0.6 - o2.x*o2.x - o2.y*o2.y - o2.z*o2.z;
	//Optimization
    t2 = t2 * t2 * (t2 >= 0);
    n.z = t2 * t2 * (GRAD3[gi2][0]* o2.x + GRAD3[gi2][1]* o2.y + GRAD3[gi2][2]* o2.z) * (t2 >= 0);
	
	/*if(t2 < 0){
        n.z = 0.0;
    }else{
        t2 *= t2;
        n.z = t2 * t2 * (GRAD3[gi2][0]* x2 + GRAD3[gi2][1]* y2 + GRAD3[gi2][2]* z2);
        //n.z = t2 * t2 * dot(GRAD3[gi2], x2, y2, z2)
    }*/

    float t3 = 0.6 - o3.x*o3.x - o3.y*o3.y - o3.z*o3.z;
	//Optimization
	t3 = t3 * t3 * (t3 >= 0);
	n.w = t3 * t3 * (GRAD3[gi3][0]* o3.x + GRAD3[gi3][1]* o3.y + GRAD3[gi3][2]* o3.z) * (t3 >= 0);
	
    /*if(t3 < 0){
        n.w = 0.0;
    }else{
        t3 *= t3;
        n.w = t3 * t3 * (GRAD3[gi3][0]* x3 + GRAD3[gi3][1]* y3 + GRAD3[gi3][2]* z3);
        //n.w = t3 * t3 * dot(GRAD3[gi3], x3, y3, z3)
    }*/

    // Add contributions from each corner to get the final noise value.
    // The result is scaled to stay just inside [-1,1]
    return 32.0*(n.x + n.y + n.z + n.w);

}

float WorleyNoise2D(const float seed, const float x, const float y){

	const int pointsPerSquare = 1;
	
	const char NEIGHBOR_MAP[9][2] = {{0,0}, { 1,0},{ 0, 1},{-1, 0}, {0,-1},
									 {1,1}, {-1,1},{-1,-1},{ 1,-1}};

	int2 main = (int2)(FastFloor(x), FastFloor(y));
	//int mainX = FastFloor(x);
	//int mainY = FastFloor(y);
	
	int2 grid;
	//int gridX = 0;
	//int gridY = 0;
	
	float2 cellPoint = (float2)(0.0, 0.0);
	
	float dist = 0.0;
	float lastDist = 0.0;
	
	float2 distVal;
	//float distValX = 0.0;
	//float distValY = 0.0;

	//Sub Optimization:
	//This might cause problems?...
	lastDist = 9999999999;
	
	for(int i = 0; i < 9; ++i){
		
		grid = main + (int2)(NEIGHBOR_MAP[i][0], NEIGHBOR_MAP[i][1]);
		
		//grid.x = main.x + NEIGHBOR_MAP[i][0];
		//grid.y = main.y + NEIGHBOR_MAP[i][1];
		
		for(int j = 0; j < pointsPerSquare; ++j){
		
			cellPoint.x = noise2D(seed, -grid.x + j * 2, grid.y + j);
			cellPoint.y = noise2D(seed, grid.y + j * 3, grid.x + j * 5);
			
			//distVal = (float2)(x,y) - grid + cellpoint;
			distVal = (float2)(x,y) - (float2)((float)grid.x, (float)grid.y) - cellPoint;
			
			//distVal.x = x - grid.x - cellPoint.x;
			//distVal.y = y - grid.y - cellPoint.y;
			
			dist = dot(distVal, distVal);
			//dist = distValX * distValX + distValY * distValY;
			
			//Optimization
			lastDist = (lastDist * (dist >= lastDist)) + (dist * (dist < lastDist));
			
			/*if(dist < lastDist){
				lastDist = dist;
			}*/
		}
		
	}
	
	//The theoretical max it can return without scale is 0.75
	lastDist  = (sqrt(lastDist));
	lastDist *= 1.333333;
	
	return lastDist;

}

float WorleyNoise3D(const float seed, const float x, const float y, const float z){

	const int pointsPerSquare = 1;
	
	const char NEIGHBOR_MAP[27][3] = {{0,0,-1}, { 1,0,-1},{ 0, 1,-1},{-1, 0,-1}, {0,-1,-1}, {1,1,-1}, {-1,1,-1},{-1,-1,-1},{ 1,-1,-1},
									  {0,0,0}, { 1,0,0},{ 0, 1,0},{-1, 0,0}, {0,-1,0}, {1,1,0}, {-1,1,0},{-1,-1,0},{ 1,-1,0},
									  {0,0, 1}, { 1,0, 1},{ 0, 1, 1},{-1, 0, 1}, {0,-1, 1}, {1,1, 1}, {-1,1, 1},{-1,-1, 1},{ 1,-1, 1}};

	int4 main = (int4)(FastFloor(x), FastFloor(y), FastFloor(z), 0);
	//int4 main = (int4)(floor(x), floor(y), floor(z), 0);
	//int mainX = FastFloor(x);
	//int mainY = FastFloor(y);

	int4 grid;
	//int gridX = 0;
	//int gridY = 0;
	
	float4 cellPoint = (float4)(0.0, 0.0, 0.0, 0.0);
	
	float dist = 0.0;
	float2 lastDist;
	
	float4 distVal;
	//float distValX = 0.0;
	//float distValY = 0.0;

	//Sub Optimization:
	//This might cause problems?...
	lastDist = (float2)(9999999999, 9999999999);
	
	for(int i = 0; i < 27; ++i){
		
		grid = main + (int4)(NEIGHBOR_MAP[i][0], NEIGHBOR_MAP[i][1], NEIGHBOR_MAP[i][2], 0);
		
		//grid.x = main.x + NEIGHBOR_MAP[i][0];
		//grid.y = main.y + NEIGHBOR_MAP[i][1];
		
		cellPoint.x = noise2D(seed, -grid.x * grid.y + 5.7, grid.y * -grid.z + -6.27);
		cellPoint.y = noise2D(seed, grid.y * -grid.z + 17, -grid.x * grid.z + -13.01);
		cellPoint.z = noise2D(seed, grid.y * grid.z + -11, grid.x * -grid.y + 3);
		
		//distVal = (float2)(x,y) - grid + cellpoint;
		distVal = (float4)(x,y,z,0.0) - (float4)((float)grid.x, (float)grid.y, (float)grid.z, 0.0) - cellPoint;
		
		//distVal = cellPoint;
		
		//distVal.x = x - grid.x - cellPoint.x;
		//distVal.y = y - grid.y - cellPoint.y;
		
		//distVal.w = 1.0;
		
		dist = dot(distVal, distVal);
		//dist = distValX * distValX + distValY * distValY;
		
		//Optimization
		//lastDist = (lastDist * (dist >= lastDist)) + (dist * (dist < lastDist));
		
		if(dist < lastDist.x){
			lastDist.y = lastDist.x;
			lastDist.x = dist;
		}else if(dist <= lastDist.y){
			lastDist.y = dist;
		}
		
		/*if(dist < lastDist){
			lastDist = dist;
		}*/
		
	}
	
	//The theoretical max it can return without scale is 0.75
	lastDist.x  = (sqrt(lastDist.x));
	lastDist.x *= 1.166666;
	
	lastDist.y = (sqrt(lastDist.y));
	lastDist.y *= 1.166666;
	
	return (lastDist.y - lastDist.x) * 2 - 1;//(secondLastDist - lastDist);

}

float EvaluatePoint2D(const float x, const float y, const float z){
	
	const float seed = 15.366;

	float noiseHeightMap = 0.0;
	float heightMask  = 0.0;
	
	//float decimator = 92;
	float decimator = 1.0 / 4096;
	float amplitude = 1.0;
	float ampTotal = 0.0;
	
	//for(int i = 0; i < 0; ++i){
	for(int i = 0; i < 10; ++i){
		
		noiseHeightMap += (WorleyNoise2D(seed, x * decimator, z * decimator)) * amplitude;

		decimator *= 1.7;
		
		ampTotal += amplitude;
		amplitude *= .55;
		
	}

	noiseHeightMap /= ampTotal;
	heightMask = noiseHeightMap * noiseHeightMap;
	
	amplitude = 1.0;
	//decimator = 92;
	decimator = 1.0 / 2048;
	ampTotal  = 0.0;
	noiseHeightMap = 0;
	
	for(int i = 0; i < 12; ++i){
		
		noiseHeightMap += ((SimplexNoise2D(x * decimator, z * decimator, seed)) + 1) * amplitude;
		
		//Flip the sign every loop to keep things interesting
		decimator *= -2.0;
		
		ampTotal += amplitude;
		amplitude *= .47;
		
	}
	
	noiseHeightMap /= ampTotal;
	noiseHeightMap *= heightMask;
	
	//Scale the 2D noise for a 3D scalar field
	//noiseHeightMap = (noiseHeightMap * 40) - y;
	noiseHeightMap = (noiseHeightMap * 800) - y;
	
	if(y <= 1.0){
		return 1.0;
	}
	
	return noiseHeightMap;
	/*if(noiseResult3D < noiseHeightMap){
		return noiseResult3D;
	}else{
		return noiseHeightMap;
	}*/
	
}

float EvaluatePoint3D(const float x, const float y, const float z){
	
	const float seed = 0.112467;
	
	float worleyResult3D = 0.0;
	float simplexResult3D = 0.0;
	float result = 0.0;
	
	float decimator = 1.0 / 4096;
	float amplitude = 1.0;
	float ampTotal = 0.0;
	
	for(int i = 0; i < 9; ++i){
		
		//worleyResult3D += WorleyNoise3D(seed, x * decimator, y * decimator, z * decimator) * amplitude;
		worleyResult3D += WorleyNoise3D(seed, x * decimator * .5, y * decimator * 2.0, z * decimator * .5) * amplitude;
		
		//Flip the sign every loop to keep things interesting
		decimator *= -2.35;
		
		ampTotal += amplitude;
		amplitude *= .55;
		
	}

	worleyResult3D /= ampTotal;
	
	//Scale the noise to cap at X meters
	worleyResult3D -= ((y - 500) / 500) * (y > 500);

	//worleyResult3D = result - worleyResult3D;
	
	decimator = 1.0 / 4096;
	amplitude = 1.0;
	ampTotal = 0.0;
	
	for(int i = 0; i < 3; ++i){
	
		//simplexResult3D += SimplexNoise3D(x * decimator * 2.0, y * decimator * .5, z * decimator * 2.0, seed);
		//simplexResult3D += SimplexNoise3D(x * decimator * 2.0, y * decimator * .5, z * decimator * 2.0, seed);
		simplexResult3D += SimplexNoise3D(x * decimator * 0.7, y * decimator, z * decimator * 0.7, seed);
		
		decimator *= 2;
		
		ampTotal += amplitude;
		amplitude *= .6;
	
	}
	
	simplexResult3D /= ampTotal;
	
	//simplexResult3D -= (y - 600) / 150;
	
	/*float noiseResult3D;
	
	float4 length = (float4)(80,80,80,1);
	length /= 2.0;
	
	float xt = x;
    float yt = y - 500;
    float zt = z;
	
	/*float4 length2 = length * length;
	float xd = ((xt * xt)-length2.x);// + (y - length2.y)) / 2.0;
	float yd = ((yt * yt)-length2.y);// + (z - length2.z)) / 2.0;
	float zd = ((zt * zt)-length2.z);// + (x - length2.x)) / 2.0;

	float d = -max(zd, max(xd,yd));

	noiseResult3D = d;
	*/
	
	if(y <= 1.0){
		return 1.0;
	}
	
	return worleyResult3D - simplexResult3D;
	
}

__kernel void terrainNoise(__global write_only float* output,
						   const int   size,
						   const float4 offset,
						   const float  scale)
{

	float weight = 0.0;
	
    int xID = get_global_id(0);
    int yID = get_global_id(1);
    int zID = get_global_id(2);

	float xPos = xID * scale + offset.x;
	float yPos = yID * scale + offset.y;
	float zPos = zID * scale + offset.z;
	
	int tid = get3DIndex(xID,yID,zID,size);//xID * size * size + yID * size + zID;
	
	float Weight2D = EvaluatePoint2D(xPos, yPos, zPos);
	float Weight3D = EvaluatePoint3D(xPos, yPos, zPos);
	
	/*weight = ((Weight3D * (Weight3D < Weight2D)) + 
			 (Weight2D * (Weight3D >= Weight2D)));
	*/
	
	if(0 > Weight2D && Weight3D >= -scale){
		weight = max(Weight3D, Weight2D);
	}else{
		weight = Weight2D;
	}
	
	output[tid] = (weight / scale);
	
}

float4 getGradient(float4 vertex, float scale){

	float4 gradient;
	float density2D;
	float density3D;
	
	density2D = EvaluatePoint2D(vertex.x, vertex.y, vertex.z);
	density3D = EvaluatePoint3D(vertex.x, vertex.y, vertex.z);
	
	if(-scale > density2D && density3D >= -scale && max(density3D, density2D) == density3D){
	//if(density3D - density2D < .001){
	//if(1){
		if((vertex.y + scale <= 1.0) || (vertex.y - scale <= 1.0)){
			gradient = (float4)(0,1,0,1);
		}else{
			//Get the 3D's portion's normal
			gradient.x = EvaluatePoint3D(vertex.x - scale/2.0, vertex.y, vertex.z) - 
						 EvaluatePoint3D(vertex.x + scale/2.0, vertex.y, vertex.z);
								  
			gradient.y = EvaluatePoint3D(vertex.x, vertex.y - scale/2.0, vertex.z) - 
						 EvaluatePoint3D(vertex.x, vertex.y + scale/2.0, vertex.z);
			
			gradient.z = EvaluatePoint3D(vertex.x, vertex.y, vertex.z - scale/2.0) - 
						 EvaluatePoint3D(vertex.x, vertex.y, vertex.z + scale/2.0);
		}
	}else{
	
		//Get the 2D's portion's normal
		gradient.x = EvaluatePoint2D(vertex.x - scale, vertex.y, vertex.z) - 
					 EvaluatePoint2D(vertex.x + scale, vertex.y, vertex.z);

		gradient.y = 2.0 * scale;
		
		gradient.z = EvaluatePoint2D(vertex.x, vertex.y, vertex.z - scale) - 
					 EvaluatePoint2D(vertex.x, vertex.y, vertex.z + scale);
	}

	gradient.w = 1.0;
	
	//Normalize the gradient
	normalize(gradient);
	
	return gradient;
	
}

/*__kernel void getGradients3D(__global read_only float* verticies,
							 __global write_only float* output,
							 const float scale)
{
	
	int tid = get_global_id(0);

	//This uses a naive approach.  There exists duplicate verticies
	//that could easily be collapsed by only calculating verticies
	//on each edge as opposed to each voxel.
	
	//Get our working vertex
	float4 workingVertex;
	
	workingVertex.x = verticies[tid * 3 + 0];
	workingVertex.y = verticies[tid * 3 + 1];
	workingVertex.z = verticies[tid * 3 + 2];
	workingVertex.w = 1.0;
	
	float4 gradient;
	
	//Calculate the gradient along each axes.
	gradient = getGradient(workingVertex, scale);
	
	//Copy it over to our output array (the normal array passed in)
	output[tid * 3 + 0] = gradient.x;
	output[tid * 3 + 1] = gradient.y;
	output[tid * 3 + 2] = gradient.z;
	
}*/

//DUAL CONTOURING HERMITE DATA

__kernel void DCGetHermiteData(__global read_only float* voxelWeights,
							   __global read_only uchar4* edgeIndex,
							   __global write_only float* hermiteData,
							   const int   size,
							   const float4 offset,
							   const float scale)
{
	
	//Holds the X Y Z index and the axis value
	uchar4 tid = edgeIndex[get_global_id(0)];
	
	/*uchar xID = edgeIndex[get_global_id(0) * 4 + 0];
    uchar yID = edgeIndex[get_global_id(0) * 4 + 1];
    uchar zID = edgeIndex[get_global_id(0) * 4 + 2];
	
	//x = 0, y = 1, z = 2
    uchar axis = edgeIndex[get_global_id(0) * 4 + 3];*/

	float2 weight = (float2)(0.0,0.0);
	
	float4 edgePosition;
	float4 edgeNormal;
	
	float4 positionA;
	float4 positionB;
	
	int index = get3DIndex(tid.x, tid.y, tid.z, size);
	
	//Get the base weight
	weight.x = voxelWeights[index] * scale;
	
	//Get the base position
	positionA.x = (float)(tid.x * scale + offset.x);
	positionA.y = (float)(tid.y * scale + offset.y);
	positionA.z = (float)(tid.z * scale + offset.z);
	positionA.w = 1.0;
	
	positionB = positionA;
	
	//X axis vertex
	if(tid.w == 0){
		weight.y = voxelWeights[get3DIndex(tid.x + 1, tid.y, tid.z, size)] * scale;
		
		positionB.x = (float)((tid.x + 1) * scale + offset.x);

	//Y axis vertex
	}else if(tid.w == 1){
		weight.y = voxelWeights[get3DIndex(tid.x, tid.y + 1, tid.z, size)] * scale;
		
		positionB.y = (float)((tid.y + 1) * scale + offset.y);

	//Z axis vertex
	}else if(tid.w == 2){
		weight.y = voxelWeights[get3DIndex(tid.x, tid.y, tid.z + 1, size)] * scale;
		
		positionB.z = (float)((tid.z + 1) * scale + offset.z);

	}
	
	edgePosition = VertexInterp(positionB, positionA, weight.y, weight.x);
	
	hermiteData[index * 18 + tid.w*6 + 0] = edgePosition.x;
	hermiteData[index * 18 + tid.w*6 + 1] = edgePosition.y;
	hermiteData[index * 18 + tid.w*6 + 2] = edgePosition.z;
	
	edgeNormal = getGradient(edgePosition, scale);

	hermiteData[index * 18 + tid.w*6 + 3] = edgeNormal.x;
	hermiteData[index * 18 + tid.w*6 + 4] = edgeNormal.y;
	hermiteData[index * 18 + tid.w*6 + 5] = edgeNormal.z;
	
}