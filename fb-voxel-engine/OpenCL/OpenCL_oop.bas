#inclib "OpenCL"
#include "cl.bi"


#define DEBUG

#ifdef DEBUG
# define dprint(msg) :open err for output as #99: Print #99,"log: " & msg: Close #99:
#else
# define dprint(msg) :
#endif

Type CLProgram
  As Double CreateTime
End Type

type CLContext
  As Double CreateTime
End Type

Type CLDeviceProperties
  Declare Constructor(DeviceID As Any Ptr)
  Declare Destructor
  As Ulongint   DEVICE_TYPE
  As Uinteger   DEVICE_VENDOR
  As Uinteger   MAX_COMPUTE_UNITS
  As Uinteger   MAX_WORK_ITEM_DIMENSIONS
  As size_t Ptr MAX_WORK_ITEM_SIZES
  As size_t     MAX_WORK_GROUP_SIZE
  As Uinteger   PREFERRED_VECTOR_WIDTH_CHAR
  As Uinteger   PREFERRED_VECTOR_WIDTH_SHORT
  As Uinteger   PREFERRED_VECTOR_WIDTH_INT
  As Uinteger   PREFERRED_VECTOR_WIDTH_LONG
  As Uinteger   PREFERRED_VECTOR_WIDTH_FLOAT
  As Uinteger   PREFERRED_VECTOR_WIDTH_DOUBLE
  As Uinteger   MAX_CLOCK_FREQUENCY
  As Uinteger   ADDRESS_BITS
  As Ulongint   MAX_MEM_ALLOC_SIZE
  As cl_bool    IMAGE_SUPPORT
  As Uinteger   MAX_READ_IMAGE_ARGS
  As Uinteger   MAX_WRITE_IMAGE_ARGS
  As size_t     IMAGE2D_MAX_WIDTH
  As size_t     IMAGE2D_MAX_HEIGHT
  As size_t     IMAGE3D_MAX_WIDTH
  As size_t     IMAGE3D_MAX_HEIGHT
  As size_t     IMAGE3D_MAX_DEPTH
  As Uinteger   MAX_SAMPLERS
  As size_t     MAX_PARAMETER_SIZE
  As Uinteger   MEM_BASE_ADDR_ALIGN
  As Uinteger   MIN_DATA_TYPE_ALIGN_SIZE
  As cl_device_fp_config_t SINGLE_FP_CONFIG
  As cl_device_mem_cache_type_t GLOBAL_MEM_CACHE_TYPE
  As Uinteger   GLOBAL_MEM_CACHELINE_SIZE
  As Ulongint   GLOBAL_MEM_CACHE_SIZE
  As Ulongint   GLOBAL_MEM_SIZE
  As Ulongint   MAX_CONSTANT_BUFFER_SIZE
  As Uinteger   MAX_CONSTANT_ARGS
  As cl_device_local_mem_type_t LOCAL_MEM_TYPE
  As Ulongint   LOCAL_MEM_SIZE
  As cl_bool    ERROR_CORRECTION_SUPPORT
  As size_t     PROFILING_TIMER_RESOLUTION
  As cl_bool    ENDIAN_LITTLE
  As cl_bool    AVAILABLE
  As cl_bool    COMPILER_AVAILABLE
  As cl_device_exec_capabilities_t EXECUTION_CAPABILITIES
  As cl_command_queue_properties_T QUEUE_PROPERTIES
  As cl_platform_id_t PLATFORM
  As Zstring Ptr Name
  As Zstring Ptr VENDOR
  As Zstring Ptr DRIVER_VERSION
  As Zstring Ptr PROFILE
  As Zstring Ptr VERSION
  As Zstring Ptr EXTENSIONS
End Type

Type CLDevice
  Declare Constructor(DeviceID As Any Ptr)
  Declare Destructor
  As Any Ptr                   ID
  As CLDeviceProperties Ptr    Props
  As cl_uint                   CLStatus
  As Double                    CreateTime
  As Double                    RunTime
End Type

Type CLPlatform
  Declare Constructor(PlatformID As Any Ptr)
  Declare Destructor
  Declare Function GetInfo(info As cl_bool) As Zstring Ptr
  As Any Ptr                   ID
  As Zstring Ptr               Name
  As Zstring Ptr               Vendor
  As Zstring Ptr               Profile
  As Zstring Ptr               Version
  As Zstring Ptr               Extensions
  As cl_int                    nDevices
  As CLDevice Ptr ptr          Devices
  As cl_uint                   CLStatus
  As Double                    CreateTime
  As Double                    RunTime
End Type

Type CLPlatforms
  Declare Constructor
  Declare Destructor
  As cl_uint                   CLStatus
  As Double                    CreateTime
  As Double                    RunTime
  As cl_int                    nPlatforms
  As CLPlatform Ptr ptr        Platforms
End Type

Type OpenCL
  Declare Constructor
  Declare Destructor
  As cl_uint                   CLStatus
  As Double                    CreateTime
  As Double                    RunTime
  As CLPlatforms Ptr           Platforms
End Type

Constructor CLDeviceProperties(DeviceID As Any Ptr )
  dprint("CLDeviceProperties()")
  If DeviceID=NULL Then
    dprint("error: DeviceID=NULL !")
    Return
  End If
  Dim As cl_uint CLStatus
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_TYPE,                          SizeOf(cl_ulong),@DEVICE_TYPE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_VENDOR,                        SizeOf(cl_uint),@DEVICE_VENDOR,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_COMPUTE_UNITS,             SizeOf(cl_uint),@MAX_COMPUTE_UNITS,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,      SizeOf(cl_uint),@MAX_WORK_ITEM_DIMENSIONS,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_WORK_GROUP_SIZE,           SizeOf(size_t),@MAX_WORK_GROUP_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,   SizeOf(cl_uint),@PREFERRED_VECTOR_WIDTH_CHAR,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,  SizeOf(cl_uint),@PREFERRED_VECTOR_WIDTH_SHORT,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,    SizeOf(cl_uint),@PREFERRED_VECTOR_WIDTH_INT,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,   SizeOf(cl_uint),@PREFERRED_VECTOR_WIDTH_LONG,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,  SizeOf(cl_uint),@PREFERRED_VECTOR_WIDTH_FLOAT,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, SizeOf(cl_uint),@PREFERRED_VECTOR_WIDTH_DOUBLE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_CLOCK_FREQUENCY,        SizeOf(cl_uint),@MAX_CLOCK_FREQUENCY,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_ADDRESS_BITS,               SizeOf(cl_uint),@ADDRESS_BITS,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_MEM_ALLOC_SIZE,         SizeOf(cl_ulong),@MAX_MEM_ALLOC_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_IMAGE_SUPPORT,              SizeOf(cl_bool),@IMAGE_SUPPORT,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_READ_IMAGE_ARGS,        SizeOf(cl_uint),@MAX_READ_IMAGE_ARGS,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_WRITE_IMAGE_ARGS,       SizeOf(cl_uint),@MAX_WRITE_IMAGE_ARGS,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_IMAGE2D_MAX_WIDTH,          SizeOf(size_t),@IMAGE2D_MAX_WIDTH,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_IMAGE2D_MAX_HEIGHT,         SizeOf(size_t),@IMAGE2D_MAX_HEIGHT,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_IMAGE3D_MAX_WIDTH,          SizeOf(size_t),@IMAGE3D_MAX_WIDTH,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_IMAGE3D_MAX_HEIGHT,         SizeOf(size_t),@IMAGE3D_MAX_HEIGHT,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_IMAGE3D_MAX_DEPTH,          SizeOf(size_t),@IMAGE3D_MAX_DEPTH,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_SAMPLERS,               SizeOf(cl_uint),@MAX_SAMPLERS,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_PARAMETER_SIZE,         SizeOf(size_t),@MAX_PARAMETER_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MEM_BASE_ADDR_ALIGN,        SizeOf(cl_uint),@MEM_BASE_ADDR_ALIGN,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE,   SizeOf(cl_uint),@MIN_DATA_TYPE_ALIGN_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_SINGLE_FP_CONFIG,           SizeOf(cl_device_fp_config_t),@SINGLE_FP_CONFIG,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_GLOBAL_MEM_CACHE_TYPE ,     SizeOf(cl_device_mem_cache_type_t),@GLOBAL_MEM_CACHE_TYPE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,  SizeOf(cl_uint),@GLOBAL_MEM_CACHELINE_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,      SizeOf(cl_ulong),@GLOBAL_MEM_CACHE_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_GLOBAL_MEM_SIZE,            SizeOf(cl_ulong),@GLOBAL_MEM_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,   SizeOf(cl_ulong),@MAX_CONSTANT_BUFFER_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_CONSTANT_ARGS,          SizeOf(cl_uint),@MAX_CONSTANT_ARGS,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_LOCAL_MEM_TYPE,             SizeOf(cl_device_local_mem_type_t),@LOCAL_MEM_TYPE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_LOCAL_MEM_SIZE,             SizeOf(cl_ulong),@LOCAL_MEM_SIZE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_ERROR_CORRECTION_SUPPORT,   SizeOf(cl_bool),@ERROR_CORRECTION_SUPPORT,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PROFILING_TIMER_RESOLUTION, SizeOf(size_t),@PROFILING_TIMER_RESOLUTION,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_ENDIAN_LITTLE,              SizeOf(cl_bool),@ENDIAN_LITTLE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_AVAILABLE,                  SizeOf(cl_bool),@AVAILABLE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_COMPILER_AVAILABLE,         SizeOf(cl_bool),@COMPILER_AVAILABLE,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_EXECUTION_CAPABILITIES,     SizeOf(cl_device_exec_capabilities_t),@EXECUTION_CAPABILITIES,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_QUEUE_PROPERTIES,           SizeOf(cl_command_queue_properties_t),@QUEUE_PROPERTIES,NULL)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PLATFORM,                   SizeOf(cl_platform_id_t),@PLATFORM,NULL)

  Dim As cl_uint nBytes
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_WORK_ITEM_SIZES,0,NULL,cast(any ptr,@nBytes))
  MAX_WORK_ITEM_SIZES=callocate(nBytes)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_MAX_WORK_ITEM_SIZES,nbytes,MAX_WORK_ITEM_SIZES,NULL)

  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_NAME, 0,NULL,cast(any ptr,@nBytes))
  Name    = callocate(nbytes)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_NAME, nBytes,Name,NULL)
 
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_VENDOR, 0,NULL,cast(any ptr,@nBytes))
  VENDOR  = callocate(nbytes)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_VENDOR, nBytes,VENDOR,NULL)

  CLstatus=clGetDeviceInfo(DeviceID,CL_DRIVER_VERSION,0,NULL,cast(any ptr,@nBytes))
  DRIVER_VERSION= callocate(nBytes)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DRIVER_VERSION, nBytes,DRIVER_VERSION,NULL)

  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PROFILE,0,NULL,cast(any ptr,@nBytes))
  PROFILE = callocate(nbytes)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_PROFILE, nBytes,PROFILE,NULL)

  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_VERSION, 0,NULL,cast(any ptr,@nBytes))
  VERSION = callocate(nbytes)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_VERSION, nBytes,VERSION,NULL)

  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_EXTENSIONS,0,NULL,cast(any ptr,@nBytes))
  EXTENSIONS= callocate(nbytes)
  CLstatus=clGetDeviceInfo(DeviceID,CL_DEVICE_EXTENSIONS, nBytes,EXTENSIONS,NULL)

  #ifdef DEBUG
  
  dprint("  device info properties")
  dprint("---------------------------")
  Dim As String tmp
  If AVAILABLE=CL_TRUE Then
    dprint("device is aviable")
  Else
    dprint("device is busy")
  End If
  If (COMPILER_AVAILABLE=CL_TRUE) Then 
     dprint("OpenCL compiler aviable")
  end if  
  If (DEVICE_TYPE And CL_DEVICE_TYPE_GPU) Then
    tmp="GPU device"
  Else
    tmp="CPU device"
  End If
  tmp &= " with " & MAX_COMPUTE_UNITS & " cores"
  tmp &= " each with " & MAX_CLOCK_FREQUENCY & " MHz."
  tmp &= " timer resolution " & PROFILING_TIMER_RESOLUTION & " ns."
  dprint(tmp)

  tmp=*Name          :' dprint("name           : " & tmp)
  tmp=*VENDOR        :' dprint("vendor         : " & tmp)
  tmp=*VERSION       :' dprint("version        : " & tmp)
  tmp=*PROFILE       :' dprint("profile        : " & tmp)
  tmp=*DRIVER_VERSION:' dprint("driver version : " & tmp)
  tmp=*EXTENSIONS    :' dprint("extensions     : " & tmp)

  dprint("work dimensions : " & MAX_WORK_ITEM_DIMENSIONS)
  tmp=   "work items      : "
  For i As Integer =0 To MAX_WORK_ITEM_DIMENSIONS-1
    tmp &= MAX_WORK_ITEM_SIZES[i] & " "
  Next
  dprint(tmp)
  dprint("work goup size  : " & MAX_WORK_GROUP_SIZE)

  dprint("size of vector char   :" & PREFERRED_VECTOR_WIDTH_CHAR)
  dprint("size of vector short  :" & PREFERRED_VECTOR_WIDTH_SHORT)
  dprint("size of vector int    :" & PREFERRED_VECTOR_WIDTH_INT)
  dprint("size of vector long   :" & PREFERRED_VECTOR_WIDTH_LONG)
  dprint("size of vector float  :" & PREFERRED_VECTOR_WIDTH_FLOAT)
  dprint("size of vector double :" & PREFERRED_VECTOR_WIDTH_DOUBLE)
  dprint("address bits : " & ADDRESS_BITS)
 
  dprint("  floating point modes")
  dprint("------------------------")
  If SINGLE_FP_CONFIG And CL_FP_DENORM Then 
    dprint("  denorms are supported")
  end if  
  If SINGLE_FP_CONFIG And CL_FP_INF_NAN Then 
    dprint("  INF and quiet NaNs are supported")
  end if  
  If SINGLE_FP_CONFIG And CL_FP_ROUND_TO_NEAREST Then 
    dprint("  round to nearest are supported")
  end if  
  If SINGLE_FP_CONFIG And CL_FP_ROUND_TO_ZERO Then 
    dprint(" round to zero are supported")
  end if  
  If SINGLE_FP_CONFIG And CL_FP_ROUND_TO_INF Then 
    dprint("round to +ve and –ve infinity are supported")
  end if  
  If SINGLE_FP_CONFIG And CL_FP_FMA Then 
    dprint("IEEE754 fused multiply add is supported")
  end if  
  
  dprint("")

  If IMAGE_SUPPORT=CL_TRUE Then
    dprint("  image support")
    dprint("-----------------------")
    dprint("  max image reading args " & MAX_READ_IMAGE_ARGS)
    dprint("  max image write   args " & MAX_WRITE_IMAGE_ARGS)
    dprint("  max image samplers     " & MAX_SAMPLERS)
    dprint("  max 2D image width     " & IMAGE2D_MAX_WIDTH)
    dprint("  max 2D image height    " & IMAGE2D_MAX_HEIGHT)
    dprint("  max 3D image width     " & IMAGE3D_MAX_WIDTH)
    dprint("  max 3D image height    " & IMAGE3D_MAX_HEIGHT)
    dprint("  max 3D image depth     " & IMAGE3D_MAX_DEPTH)
    dprint("")
  End If
  dprint("  memory info")
  dprint("---------------")
  dprint("max parameter size   " & MAX_PARAMETER_SIZE)
  dprint("memory address align " & MEM_BASE_ADDR_ALIGN)
  dprint("min data type align  " & MIN_DATA_TYPE_ALIGN_SIZE)
  dprint("max constant args    " & MAX_CONSTANT_ARGS)
 
  If (GLOBAL_MEM_CACHE_TYPE<>CL_NONE) Then
    If (GLOBAL_MEM_CACHE_TYPE And CL_READ_ONLY_CACHE) Then
      dprint("global cache memory is read only")
    Else
      dprint("global cache is read write memory")
    End If
    dprint("global cacheline size " & GLOBAL_MEM_CACHELINE_SIZE & " bytes")
    dprint("cache    memory size  " & GLOBAL_MEM_CACHE_SIZE  & " bytes")
  End If
  dprint("global   memory size  " & GLOBAL_MEM_SIZE\1024\1024 & " MB")
  dprint("object   memory size  " & MAX_MEM_ALLOC_SIZE\1024\1024 & " MB")
  dprint("constant memory size  " & MAX_CONSTANT_BUFFER_SIZE\1024 & " KB")
  dprint("local    memory size  " & LOCAL_MEM_SIZE\1024 & " KB")
  dprint("")
  #endif
  
End Constructor

Destructor CLDeviceProperties
  deallocate MAX_WORK_ITEM_SIZES
  deallocate Name
  deallocate VENDOR
  deallocate DRIVER_VERSION
  deallocate PROFILE
  deallocate VERSION
  deallocate EXTENSIONS
 
  dprint("CLDeviceProperties~")
End Destructor

 
Constructor CLDevice(DeviceID As Any Ptr)
  dprint("CLDevice()")
  ID=DeviceID
  CreateTime=Timer()
  Props = New CLDeviceProperties(ID)
End Constructor

Destructor CLDevice
  If Props<>NULL Then Delete Props
  dprint("CLDevice~")
End Destructor

Function CLPlatform.GetInfo(info As cl_uint) As Zstring Ptr
  Dim As cl_bool nBytes
  CLStatus = clGetPlatformInfo(ID,info,0,NULL,cast(any ptr,@nBytes))

  Dim As Zstring Ptr ptmp=callocate(nBytes)
  CLStatus = clGetPlatformInfo(ID,info,nBytes,ptmp,NULL)
  If (CLStatus<>CL_SUCCESS) Then
    dprint("Error: getting platform info vendor.")
    Return NULL
  End If
  Return ptmp
End Function

Constructor CLPlatform(PlatformID As Any Ptr)
  dprint("CLPlatform()")
  CreateTime=Timer()
  ID=PlatformID
  Name      =GetInfo(CL_PLATFORM_NAME)
  Vendor    =GetInfo(CL_PLATFORM_VENDOR)
  Profile   =GetInfo(CL_PLATFORM_PROFILE)
  Version   =GetInfo(CL_PLATFORM_VERSION)
  Extensions=GetInfo(CL_PLATFORM_EXTENSIONS)

  #ifdef DEBUG
  Dim tmp As String
  tmp=*Name      :dprint("Name       : " & tmp)
  tmp=*Vendor    :dprint("Vendor     : " & tmp)
  tmp=*Profile   :dprint("Profile    : " & tmp)
  tmp=*Version   :dprint("Version    : " & tmp)
  tmp=*Extensions:dprint("Extensions : " & tmp)
  dprint("")
  #endif
  'get number of devices supported by plattform ID
  Dim As Any Ptr ptr IDs = New Any Ptr[100]
  CLStatus=clGetDeviceIDs(ID,&HFFFFFFFFLL,100,IDs,@nDevices)
  If (CLStatus<>CL_SUCCESS) Then
    Print "Error: getting DeviceIds."
    Return
  End If
  Devices = New CLDevice Ptr [nDevices]
  For i As Integer = 0 To nDevices-1
    Devices[i] = New CLDevice(Ids[i])
  Next
  Delete[] IDs
End Constructor

Destructor CLPlatform
  If Name       Then deallocate Name
  If Vendor     Then deallocate Vendor
  If Profile    Then deallocate Profile
  If Version    Then deallocate Version
  If Extensions Then deallocate Extensions
  'dprint("CLPlatform~")
End Destructor

Constructor CLPlatforms
  dprint("CLPlatforms()")
  CreateTime=Timer()
  CLStatus = clGetPlatformIDs(0, NULL, @nPlatforms)
  If (CLStatus<>CL_SUCCESS) Then
    dprint("Error: getting nPlatforms.")
    Return
  End If
  Dim As Any Ptr ptr IDs = New Any Ptr [nPlatforms]
  CLStatus = clGetPlatformIDs(nPlatforms, IDs, NULL)
  If (CLStatus<>CL_SUCCESS) Then
    dPrint("Error: getting PlatformIds.")
    Return
  End If
  Platforms = New CLPlatform Ptr [nPlatforms]
  For i As Integer = 0 To nPlatforms-1
    Platforms[i] = New CLPlatform(Ids[i])
  Next
  Delete[] IDs
End Constructor

Destructor CLPlatforms
  If nPlatforms>0 Then
    For i As Integer=nPlatforms-1 To 0 Step -1
      Dim As CLPlatform Ptr Platform = Platforms[i]
      If Platform<>NULL Then
        Delete Platform
        Platforms[i]=NULL
      End If
    Next
  End If
  dprint("CLPlatforms~")
End Destructor

Constructor OpenCL
  dprint("OpenCL()")
  CreateTime=Timer()
  Platforms=New CLPlatforms
End Constructor

Destructor OpenCL
  If Platforms<>NULL Then
    Delete Platforms
  End If
  dprint("OpenCL~")
  #ifdef DEBUG
  Beep
  sleep
  #endif
End Destructor

' main
Dim As OpenCL cl
 
