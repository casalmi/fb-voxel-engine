/'*********************************************************************************
 * Copyright (c) 2008-2009 The Khronos Group Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and/or associated documentation files (the
 * "Materials"), to deal in the Materials without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Materials, and to
 * permit persons to whom the Materials are furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Materials.
 *
 * THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
 *********************************************************************************'/

/' $Revision: 8748 $ on $Date: 2009-08-27 04:24:36 -0700 (Thu, 27 Aug 2009) $ '/

#ifndef __OPENCL_CL_GL_BI__
#define __OPENCL_CL_GL_BI__

#include "cl_platform.bi"
#include "gl/gl.bi"

extern "C"

' NOTE:  Make sure that appropriate GL header file is included separately

type cl_gl_object_type as cl_uint
type cl_gl_texture_info as cl_uint
type cl_gl_platform_info as cl_uint

' cl_gl_object_type
#define CL_GL_OBJECT_BUFFER             &H2000
#define CL_GL_OBJECT_TEXTURE2D          &H2001
#define CL_GL_OBJECT_TEXTURE3D          &H2002
#define CL_GL_OBJECT_RENDERBUFFER       &H2003

' cl_gl_texture_info
#define CL_GL_TEXTURE_TARGET            &H2004
#define CL_GL_MIPMAP_LEVEL              &H2005

declare function _
clCreateFromGLBuffer(context     as cl_context_t, _
                     flags       as cl_mem_flags_t, _
                     bufobj      as GLuint, _
                     errcode_ret as cl_int ptr) as cl_mem_t

declare function _
clCreateFromGLTexture2D(context     as cl_context_t, _
                        flags       as cl_mem_flags_t, _
                        target      as GLenum, _
                        miplevel    as GLint, _
                        texture     as GLuint, _
                        errcode_ret as cl_int ptr) as cl_mem_t

declare function _
clCreateFromGLTexture3D(context     as cl_context_t, _
                        flags       as cl_mem_flags_t, _
                        target      as GLenum, _
                        miplevel    as GLint, _
                        texture     as GLuint, _
                        errcode_ret as cl_int ptr)  as cl_mem_t

declare function _
clCreateFromGLRenderbuffer(context      as cl_context_t, _
                           flags        as cl_mem_flags_t, _
                           renderbuffer as GLuint, _
                           errcode_ret  as cl_int ptr)  as cl_mem_t

declare function _
clGetGLObjectInfo(memobj         as cl_mem_t, _
                  param_gl_object_type as cl_gl_object_type ptr, _
                  gl_object_name as GLuint ptr) as cl_int
                  
declare function _
clGetGLTextureInfo(memobj               as cl_mem_t, _
                   param_name           as cl_gl_texture_info, _
                   param_value_size     as size_t, _
                   param_value          as any ptr, _
                   param_value_size_ret as size_t ptr) as cl_int

declare function _
clEnqueueAcquireGLObjects(command_queue   as cl_command_queue_t, _
                          num_objects     as cl_uint, _
                          mem_objects     as cl_mem_t ptr, _
                          nevents_in_list as cl_uint, _
                          event_list      as cl_event_t ptr, _
                          event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueReleaseGLObjects(command_queue   as cl_command_queue_t, _
                          num_objects     as cl_uint, _
                          mem_objects     as cl_mem_t ptr, _
                          nevents_in_list as cl_uint, _
                          event_list      as cl_event_t ptr, _
                          event           as cl_event_t ptr) as cl_int

/' cl_khr_gl_sharing_extension '/

#define cl_khr_gl_sharing 1

type cl_gl_context_info as cl_uint

/' Additional Error Codes '/
#define CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR -100

/' cl_gl_context_info  '/
#define CL_CURRENT_DEVICE_FOR_GL_CONTEXT_KHR    &h2006
#define CL_DEVICES_FOR_GL_CONTEXT_KHR           &h2007
    
/' Additional cl_context_properties  '/
#define CL_GL_CONTEXT_KHR                       &h2008
#define CL_EGL_DISPLAY_KHR                      &h2009
#define CL_GLX_DISPLAY_KHR                      &h200A
#define CL_WGL_HDC_KHR                          &h200B
#define CL_CGL_SHAREGROUP_KHR                   &h200C

/'declare function _
'clGetGLContextInfoKHR(context              as cl_context_t, _
'                      param_value_name     as cl_gl_context_info, _
'                      param_value_size     as size_t, _
'                      param_value          as any ptr, _
'                      param_value_size_ret as size_t ptr) as cl_int
'/

end extern

'Grab some missing functions from the OpenGL library


    #If Defined(__FB_WIN32__) Or Defined(__FB_CYGWIN__)
    Extern "Windows"
    #ElseIf Defined(__FB_UNIX__)
    Extern "C"
    #Else
    Extern "C"
    #EndIf
    
    #If Defined(__FB_WIN32__)
    #Inclib "opengl32"
    #ElseIf Defined(__FB_LINUX__)
    #Inclib "GL"
    #ElseIf Defined(__FB_DOS__)
    #Inclib "gl"
    #EndIf
    
    #if Defined(__FB_WIN32__)
        Declare Function wglGetCurrentContext() as long
        Declare Function wglGetCurrentDC() as long
        
        Type PFNWGLGETCURRENTCONTEXTPROC As Function() as long
        Type PFNWGLGETCURRENTDCPROC As Function() as long
    #elseif Defined(__FB_UNIX__)
        Declare Function glxGetCurrentContext() as long
        Declare Function glxGetCurrentDC() as long
    #endif
    
    End Extern
    

#endif  ' __OPENCL_CL_GL_BI__
