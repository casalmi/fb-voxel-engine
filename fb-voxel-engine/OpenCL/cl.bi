/'******************************************************************************
 * Copyright (c) 2008-2009 The Khronos Group Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and/or associated documentation files (the
 * "Materials"), to deal in the Materials without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Materials, and to
 * permit persons to whom the Materials are furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Materials.
 *
 * THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
 *****************************************************************************'/

/' $Revision: 9283 $ on $Date: 2009-10-14 10:18:57 -0700 (Wed, 14 Oct 2009) $ '/

#ifndef __OPENCL_CL_BI__
#define __OPENCL_CL_BI__

#include "cl_platform.bi"
 
#ifdef __FB_WIN32__
extern "Windows-MS"
#else
extern "C"
#endif

type cl_platform_id_t     as any ptr
type cl_device_id_t       as any ptr
type cl_context_t         as any ptr
type cl_command_queue_t   as any ptr
type cl_mem_t             as any ptr
type cl_program_t         as any ptr
type cl_kernel_t          as any ptr
type cl_event_t           as any ptr
type cl_sampler_t         as any ptr
                    
/' WARNING!  
  Unlike cl_ types in cl_platform.h, 
  cl_bool is not guaranteed to be the same size as the bool in kernels. 
'/
type cl_bool                       as cl_uint
type cl_bitfield_t                 as cl_ulong
type cl_device_type_t              as cl_bitfield_t
type cl_platform_info_t            as cl_uint
type cl_device_info_t              as cl_uint
type cl_device_address_info_t      as cl_bitfield_t
type cl_device_fp_config_t         as cl_bitfield_t
type cl_device_mem_cache_type_t    as cl_uint
type cl_device_local_mem_type_t    as cl_uint
type cl_device_exec_capabilities_t as cl_bitfield_t
type cl_command_queue_properties_t as cl_bitfield_t

type cl_context_properties_t     as intptr_t
type cl_context_info_t           as cl_uint
type cl_command_queue_info_t     as cl_uint
type cl_channel_order_t          as cl_uint
type cl_channel_type_t           as cl_uint
type cl_mem_flags_t              as cl_bitfield_t
type cl_mem_object_type_t        as cl_uint
type cl_mem_info_t               as cl_uint
type cl_image_info_t             as cl_uint
type cl_addressing_mode_t        as cl_uint
type cl_filter_mode_t            as cl_uint
type cl_sampler_info_t           as cl_uint
type cl_map_flags_t              as cl_bitfield_t
type cl_program_info_t           as cl_uint
type cl_program_build_info_t     as cl_uint
type cl_build_status_t           as cl_int
type cl_kernel_info_t            as cl_uint
type cl_kernel_work_group_info_t as cl_uint
type cl_event_info_t             as cl_uint
type cl_command_type_t           as cl_uint
type cl_profiling_info_t         as cl_uint

type cl_image_format_t
  as cl_channel_order_t image_channel_order
  as cl_channel_type_t  image_channel_data_type
end type


/'****************************************************************************'/

' Error Codes
#define CL_SUCCESS                                  0
#define CL_DEVICE_NOT_FOUND                         -1
#define CL_DEVICE_NOT_AVAILABLE                     -2
#define CL_COMPILER_NOT_AVAILABLE                   -3
#define CL_MEM_OBJECT_ALLOCATION_FAILURE            -4
#define CL_OUT_OF_RESOURCES                         -5
#define CL_OUT_OF_HOST_MEMORY                       -6
#define CL_PROFILING_INFO_NOT_AVAILABLE             -7
#define CL_MEM_COPY_OVERLAP                         -8
#define CL_IMAGE_FORMAT_MISMATCH                    -9
#define CL_IMAGE_FORMAT_NOT_SUPPORTED               -10
#define CL_BUILD_PROGRAM_FAILURE                    -11
#define CL_MAP_FAILURE                              -12

#define CL_INVALID_VALUE                            -30
#define CL_INVALID_DEVICE_TYPE                      -31
#define CL_INVALID_PLATFORM                         -32
#define CL_INVALID_DEVICE                           -33
#define CL_INVALID_CONTEXT                          -34
#define CL_INVALID_QUEUE_PROPERTIES                 -35
#define CL_INVALID_COMMAND_QUEUE                    -36
#define CL_INVALID_HOST_PTR                         -37
#define CL_INVALID_MEM_OBJECT                       -38
#define CL_INVALID_IMAGE_FORMAT_DESCRIPTOR          -39
#define CL_INVALID_IMAGE_SIZE                       -40
#define CL_INVALID_SAMPLER                          -41
#define CL_INVALID_BINARY                           -42
#define CL_INVALID_BUILD_OPTIONS                    -43
#define CL_INVALID_PROGRAM                          -44
#define CL_INVALID_PROGRAM_EXECUTABLE               -45
#define CL_INVALID_KERNEL_NAME                      -46
#define CL_INVALID_KERNEL_DEFINITION                -47
#define CL_INVALID_KERNEL                           -48
#define CL_INVALID_ARG_INDEX                        -49
#define CL_INVALID_ARG_VALUE                        -50
#define CL_INVALID_ARG_SIZE                         -51
#define CL_INVALID_KERNEL_ARGS                      -52
#define CL_INVALID_WORK_DIMENSION                   -53
#define CL_INVALID_WORK_GROUP_SIZE                  -54
#define CL_INVALID_WORK_ITEM_SIZE                   -55
#define CL_INVALID_GLOBAL_OFFSET                    -56
#define CL_INVALID_EVENT_WAIT_LIST                  -57
#define CL_INVALID_EVENT                            -58
#define CL_INVALID_OPERATION                        -59
#define CL_INVALID_GL_OBJECT                        -60
#define CL_INVALID_BUFFER_SIZE                      -61
#define CL_INVALID_MIP_LEVEL                        -62
#define CL_INVALID_GLOBAL_WORK_SIZE                 -63

' OpenCL Version
#define CL_VERSION_1_0                              1

' cl_bool
#define CL_FALSE                                    0
#define CL_TRUE                                     1

' cl_platform_info
#define CL_PLATFORM_PROFILE                         &H0900
#define CL_PLATFORM_VERSION                         &H0901
#define CL_PLATFORM_NAME                            &H0902
#define CL_PLATFORM_VENDOR                          &H0903
#define CL_PLATFORM_EXTENSIONS                      &H0904

' cl_device_type - bitfield
#define CL_DEVICE_TYPE_DEFAULT                      (1 SHL 0)
#define CL_DEVICE_TYPE_CPU                          (1 SHL 1)
#define CL_DEVICE_TYPE_GPU                          (1 SHL 2)
#define CL_DEVICE_TYPE_ACCELERATOR                  (1 SHL 3)
#define CL_DEVICE_TYPE_ALL                          &HFFFFFFFF

' cl_device_info
#define CL_DEVICE_TYPE                              &H1000
#define CL_DEVICE_VENDOR_ID                         &H1001
#define CL_DEVICE_MAX_COMPUTE_UNITS                 &H1002
#define CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS          &H1003
#define CL_DEVICE_MAX_WORK_GROUP_SIZE               &H1004
#define CL_DEVICE_MAX_WORK_ITEM_SIZES               &H1005
#define CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR       &H1006
#define CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT      &H1007
#define CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT        &H1008
#define CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG       &H1009
#define CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT      &H100A
#define CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE     &H100B
#define CL_DEVICE_MAX_CLOCK_FREQUENCY               &H100C
#define CL_DEVICE_ADDRESS_BITS                      &H100D
#define CL_DEVICE_MAX_READ_IMAGE_ARGS               &H100E
#define CL_DEVICE_MAX_WRITE_IMAGE_ARGS              &H100F
#define CL_DEVICE_MAX_MEM_ALLOC_SIZE                &H1010
#define CL_DEVICE_IMAGE2D_MAX_WIDTH                 &H1011
#define CL_DEVICE_IMAGE2D_MAX_HEIGHT                &H1012
#define CL_DEVICE_IMAGE3D_MAX_WIDTH                 &H1013
#define CL_DEVICE_IMAGE3D_MAX_HEIGHT                &H1014
#define CL_DEVICE_IMAGE3D_MAX_DEPTH                 &H1015
#define CL_DEVICE_IMAGE_SUPPORT                     &H1016
#define CL_DEVICE_MAX_PARAMETER_SIZE                &H1017
#define CL_DEVICE_MAX_SAMPLERS                      &H1018
#define CL_DEVICE_MEM_BASE_ADDR_ALIGN               &H1019
#define CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE          &H101A
#define CL_DEVICE_SINGLE_FP_CONFIG                  &H101B
#define CL_DEVICE_GLOBAL_MEM_CACHE_TYPE             &H101C
#define CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE         &H101D
#define CL_DEVICE_GLOBAL_MEM_CACHE_SIZE             &H101E
#define CL_DEVICE_GLOBAL_MEM_SIZE                   &H101F
#define CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE          &H1020
#define CL_DEVICE_MAX_CONSTANT_ARGS                 &H1021
#define CL_DEVICE_LOCAL_MEM_TYPE                    &H1022
#define CL_DEVICE_LOCAL_MEM_SIZE                    &H1023
#define CL_DEVICE_ERROR_CORRECTION_SUPPORT          &H1024
#define CL_DEVICE_PROFILING_TIMER_RESOLUTION        &H1025
#define CL_DEVICE_ENDIAN_LITTLE                     &H1026
#define CL_DEVICE_AVAILABLE                         &H1027
#define CL_DEVICE_COMPILER_AVAILABLE                &H1028
#define CL_DEVICE_EXECUTION_CAPABILITIES            &H1029
#define CL_DEVICE_QUEUE_PROPERTIES                  &H102A
#define CL_DEVICE_NAME                              &H102B
#define CL_DEVICE_VENDOR                            &H102C
#define CL_DRIVER_VERSION                           &H102D
#define CL_DEVICE_PROFILE                           &H102E
#define CL_DEVICE_VERSION                           &H102F
#define CL_DEVICE_EXTENSIONS                        &H1030
#define CL_DEVICE_PLATFORM                          &H1031

' cl_device_fp_config - bitfield
#define CL_FP_DENORM                                (1 SHL 0)
#define CL_FP_INF_NAN                               (1 SHL 1)
#define CL_FP_ROUND_TO_NEAREST                      (1 SHL 2)
#define CL_FP_ROUND_TO_ZERO                         (1 SHL 3)
#define CL_FP_ROUND_TO_INF                          (1 SHL 4)
#define CL_FP_FMA                                   (1 SHL 5)

' cl_device_mem_cache_type
#define CL_NONE                                     &H0
#define CL_READ_ONLY_CACHE                          &H1
#define CL_READ_WRITE_CACHE                         &H2

' cl_device_local_mem_type
#define CL_LOCAL                                    &H1
#define CL_GLOBAL                                   &H2

' cl_device_exec_capabilities - bitfield
#define CL_EXEC_KERNEL                              (1 SHL 0)
#define CL_EXEC_NATIVE_KERNEL                       (1 SHL 1)

' cl_command_queue_properties - bitfield
#define CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE      (1 SHL 0)
#define CL_QUEUE_PROFILING_ENABLE                   (1 SHL 1)

' cl_context_info
#define CL_CONTEXT_REFERENCE_COUNT                  &H1080
#define CL_CONTEXT_DEVICES                          &H1081
#define CL_CONTEXT_PROPERTIES                       &H1082

' cl_context_properties
#define CL_CONTEXT_PLATFORM                         &H1084

' cl_command_queue_info
#define CL_QUEUE_CONTEXT                            &H1090
#define CL_QUEUE_DEVICE                             &H1091
#define CL_QUEUE_REFERENCE_COUNT                    &H1092
#define CL_QUEUE_PROPERTIES                         &H1093

' cl_mem_flags - bitfield
#define CL_MEM_READ_WRITE                           (1 SHL 0)
#define CL_MEM_WRITE_ONLY                           (1 SHL 1)
#define CL_MEM_READ_ONLY                            (1 SHL 2)
#define CL_MEM_USE_HOST_PTR                         (1 SHL 3)
#define CL_MEM_ALLOC_HOST_PTR                       (1 SHL 4)
#define CL_MEM_COPY_HOST_PTR                        (1 SHL 5)

' cl_channel_order
#define CL_R                                        &H10B0
#define CL_A                                        &H10B1
#define CL_RG                                       &H10B2
#define CL_RA                                       &H10B3
#define CL_RGB                                      &H10B4
#define CL_RGBA                                     &H10B5
#define CL_BGRA                                     &H10B6
#define CL_ARGB                                     &H10B7
#define CL_INTENSITY                                &H10B8
#define CL_LUMINANCE                                &H10B9

' cl_channel_type
#define CL_SNORM_INT8                               &H10D0
#define CL_SNORM_INT16                              &H10D1
#define CL_UNORM_INT8                               &H10D2
#define CL_UNORM_INT16                              &H10D3
#define CL_UNORM_SHORT_565                          &H10D4
#define CL_UNORM_SHORT_555                          &H10D5
#define CL_UNORM_INT_101010                         &H10D6
#define CL_SIGNED_INT8                              &H10D7
#define CL_SIGNED_INT16                             &H10D8
#define CL_SIGNED_INT32                             &H10D9
#define CL_UNSIGNED_INT8                            &H10DA
#define CL_UNSIGNED_INT16                           &H10DB
#define CL_UNSIGNED_INT32                           &H10DC
#define CL_HALF_FLOAT                               &H10DD
#define CL_FLT                                      &H10DE

' cl_mem_object_type
#define CL_MEM_OBJECT_BUFFER                        &H10F0
#define CL_MEM_OBJECT_IMAGE2D                       &H10F1
#define CL_MEM_OBJECT_IMAGE3D                       &H10F2

' cl_mem_info
#define CL_MEM_TYPE                                 &H1100
#define CL_MEM_FLAGS                                &H1101
#define CL_MEM_SIZE                                 &H1102
#define CL_MEM_HOST_PTR                             &H1103
#define CL_MEM_MAP_COUNT                            &H1104
#define CL_MEM_REFERENCE_COUNT                      &H1105
#define CL_MEM_CONTEXT                              &H1106

' cl_image_info
#define CL_IMAGE_FORMAT                             &H1110
#define CL_IMAGE_ELEMENT_SIZE                       &H1111
#define CL_IMAGE_ROW_PITCH                          &H1112
#define CL_IMAGE_SLICE_PITCH                        &H1113
#define CL_IMAGE_WIDTH                              &H1114
#define CL_IMAGE_HEIGHT                             &H1115
#define CL_IMAGE_DEPTH                              &H1116

' cl_addressing_mode
#define CL_ADDRESS_NONE                             &H1130
#define CL_ADDRESS_CLAMP_TO_EDGE                    &H1131
#define CL_ADDRESS_CLAMP                            &H1132
#define CL_ADDRESS_REPEAT                           &H1133

' cl_filter_mode
#define CL_FILTER_NEAREST                           &H1140
#define CL_FILTER_LINEAR                            &H1141

' cl_sampler_info
#define CL_SAMPLER_REFERENCE_COUNT                  &H1150
#define CL_SAMPLER_CONTEXT                          &H1151
#define CL_SAMPLER_NORMALIZED_COORDS                &H1152
#define CL_SAMPLER_ADDRESSING_MODE                  &H1153
#define CL_SAMPLER_FILTER_MODE                      &H1154

' cl_map_flags - bitfield
#define CL_MAP_READ                                 (1 SHL 0)
#define CL_MAP_WRITE                                (1 SHL 1)

' cl_program_info
#define CL_PROGRAM_REFERENCE_COUNT                  &H1160
#define CL_PROGRAM_CONTEXT                          &H1161
#define CL_PROGRAM_NUM_DEVICES                      &H1162
#define CL_PROGRAM_DEVICES                          &H1163
#define CL_PROGRAM_SOURCE                           &H1164
#define CL_PROGRAM_BINARY_SIZES                     &H1165
#define CL_PROGRAM_BINARIES                         &H1166

' cl_program_build_info
#define CL_PROGRAM_BUILD_STATUS                     &H1181
#define CL_PROGRAM_BUILD_OPTIONS                    &H1182
#define CL_PROGRAM_BUILD_LOG                        &H1183

' cl_build_status
#define CL_BUILD_SUCCESS                            0
#define CL_BUILD_NONE                               -1
#define CL_BUILD_ERROR                              -2
#define CL_BUILD_IN_PROGRESS                        -3

' cl_kernel_info
#define CL_KERNEL_FUNCTION_NAME                     &H1190
#define CL_KERNEL_NUM_ARGS                          &H1191
#define CL_KERNEL_REFERENCE_COUNT                   &H1192
#define CL_KERNEL_CONTEXT                           &H1193
#define CL_KERNEL_PROGRAM                           &H1194

' cl_kernel_work_group_info
#define CL_KERNEL_WORK_GROUP_SIZE                   &H11B0
#define CL_KERNEL_COMPILE_WORK_GROUP_SIZE           &H11B1
#define CL_KERNEL_LOCAL_MEM_SIZE                    &H11B2

' cl_event_info
#define CL_EVENT_COMMAND_QUEUE                      &H11D0
#define CL_EVENT_COMMAND_TYPE                       &H11D1
#define CL_EVENT_REFERENCE_COUNT                    &H11D2
#define CL_EVENT_COMMAND_EXECUTION_STATUS           &H11D3

' cl_command_type
#define CL_COMMAND_NDRANGE_KERNEL                   &H11F0
#define CL_COMMAND_TASK                             &H11F1
#define CL_COMMAND_NATIVE_KERNEL                    &H11F2
#define CL_COMMAND_READ_BUFFER                      &H11F3
#define CL_COMMAND_WRITE_BUFFER                     &H11F4
#define CL_COMMAND_COPY_BUFFER                      &H11F5
#define CL_COMMAND_READ_IMAGE                       &H11F6
#define CL_COMMAND_WRITE_IMAGE                      &H11F7
#define CL_COMMAND_COPY_IMAGE                       &H11F8
#define CL_COMMAND_COPY_IMAGE_TO_BUFFER             &H11F9
#define CL_COMMAND_COPY_BUFFER_TO_IMAGE             &H11FA
#define CL_COMMAND_MAP_BUFFER                       &H11FB
#define CL_COMMAND_MAP_IMAGE                        &H11FC
#define CL_COMMAND_UNMAP_MEM_OBJECT                 &H11FD
#define CL_COMMAND_MARKER                           &H11FE
#define CL_COMMAND_ACQUIRE_GL_OBJECTS               &H11FF
#define CL_COMMAND_RELEASE_GL_OBJECTS               &H1200

' command execution status
#define CL_COMPLETE                                 &H0
#define CL_RUNNING                                  &H1
#define CL_SUBMITTED                                &H2
#define CL_QUEUED                                   &H3
  
' cl_profiling_info
#define CL_PROFILING_COMMAND_QUEUED                 &H1280
#define CL_PROFILING_COMMAND_SUBMIT                 &H1281
#define CL_PROFILING_COMMAND_START                  &H1282
#define CL_PROFILING_COMMAND_END                    &H1283

' Platform API
declare function _
clGetPlatformIDs (num_entries   as cl_uint, _
                  platforms     as cl_platform_id_t ptr, _
                  num_platforms as cl_uint ptr) as cl_int

declare function _ 
clGetPlatformInfo(platform        as cl_platform_id_t, _
                  pname           as cl_platform_info_t, _
                  pvalue_size     as size_t, _ 
                  pvalue          as any ptr, _
                  pvalue_size_ret as size_t ptr) as cl_int

' Device APIs
declare function _
clGetDeviceIDs(platform    as cl_platform_id_t, _
               device_type as cl_device_type_t, _
               num_entries as cl_uint, _
               devices     as cl_device_id_t ptr, _ 
               num_devices as cl_uint ptr ) as cl_int

declare function _
clGetDeviceInfo(device               as cl_device_id_t, _
                param_name           as cl_device_info_t, _
                param_value_size     as size_t, _
                param_value          as any ptr, _
                param_value_size_ret as size_t ptr) as cl_int

' Context APIs  
declare function _
clCreateContext(properties  as cl_context_properties_t ptr, _
                num_devices as cl_uint, _
                devices     as cl_device_id_t ptr, _
                pfn_notify  as sub (pstr as zstring ptr,pany as any ptr,size as size_t,pany2 as any ptr), _
                user_data   as any ptr, _
                errcode_ret as cl_int ptr) as cl_context_t

declare function _
clCreateContextFromType(properties  as cl_context_properties_t ptr, _
                        device_type as cl_device_type_t, _
                        pfn_notify  as sub (pstr as zstring ptr,pany as any ptr,size as size_t,pany2 as any ptr), _
                        user_data   as any ptr, _
                        errcode_ret as cl_int ptr) as cl_context_t

declare function _
clRetainContext(context as cl_context_t) as cl_int

declare function _
clReleaseContext(context as cl_context_t) as cl_int

declare function _
clGetContextInfo(context          as cl_context_t, _
                 param_name       as cl_context_info_T, _
                 param_value_size as size_t, _ 
                 param_value      as any ptr, _ 
                 param_value_size_ret as size_t ptr) as cl_int

' Command Queue APIs
declare function _
clCreateCommandQueue(context     as cl_context_t, _
                     device      as cl_device_id_t, _
                     properties  as cl_command_queue_properties_t, _
                     errcode_ret as cl_int ptr) as cl_command_queue_t

declare function _
clRetainCommandQueue(command_queue as cl_command_queue_t) as cl_int

declare function _
clReleaseCommandQueue(command_queue as cl_command_queue_t) as cl_int

declare function _
clGetCommandQueueInfo(command_queue    as cl_command_queue_t, _
                      param_name       as cl_command_queue_info_t, _
                      param_value_size as size_t, _
                      param_value      as any ptr, _
                      param_value_size_ret as size_t ptr) as cl_int

declare function _
clSetCommandQueueProperty(command_queue  as cl_command_queue_t, _
                          properties     as cl_command_queue_properties_t, _
                          enable         as cl_bool, _
                          old_properties as cl_command_queue_properties_t ptr) as cl_int

' Memory Object APIs
declare function _
clCreateBuffer(context     as cl_context_t, _
               flags       as cl_mem_flags_t, _
               size        as size_t, _
               host_ptr    as any ptr, _
               errcode_ret as cl_int ptr) as cl_mem_t

declare function _
clCreateImage2D(context         as cl_context_t, _
                flags           as cl_mem_flags_t, _
                image_format    as cl_image_format_t ptr, _
                image_width     as size_t, _
                image_height    as size_t, _
                image_row_pitch as size_t, _
                host_ptr        as any ptr, _
                errcode_ret     as cl_int ptr) as cl_mem_t
                        
declare function _
clCreateImage3D(context     as cl_context_t, _
                flags       as cl_mem_flags_t, _
                format      as cl_image_format_t ptr, _
                w           as size_t, _
                h           as size_t, _
                depth       as size_t, _
                row_pitch   as size_t, _
                slice_pitch as size_t, _
                host_ptr    as any ptr, _
                errcode_ret as cl_int ptr) as cl_mem_t
                        
declare function _
clRetainMemObject(memobj as cl_mem_t) as cl_int

declare function _
clReleaseMemObject(memobj as cl_mem_t) as cl_int

declare function _
clGetSupportedImageFormats(context    as cl_context_t, _
                           flags      as cl_mem_flags_t, _
                           image_type as cl_mem_object_type_t, _
                           entries    as cl_uint, _
                           formats    as cl_image_format_t ptr, _
                           nformats   as cl_uint ptr) as cl_int
                                    
declare function _
clGetMemObjectInfo(memobj         as cl_mem_t, _
                   param_name     as cl_mem_info_t, _
                   value_size     as size_t, _
                   value          as any ptr, _
                   value_size_ret as size_t ptr) as cl_int

declare function _
clGetImageInfo(image          as cl_mem_t, _
               param_name     as cl_image_info_t, _
               value_size     as size_t, _
               value          as any ptr, _
               value_size_ret as size_t ptr) as cl_int

' Sampler APIs
declare function _
clCreateSampler(context           as cl_context_t, _
                normalized_coords as cl_bool, _
                addressing_mode   as cl_addressing_mode_t, _
                filter_mode       as cl_filter_mode_t, _
                errcode_ret       as cl_int ptr) as cl_sampler_t

declare function _
clRetainSampler(sampler as cl_sampler_t) as cl_int

declare function _
clReleaseSampler(sampler as cl_sampler_t) as cl_int

declare function _
clGetSamplerInfo(sampler     as cl_sampler_t, _
                 pname       as cl_sampler_info_t, _
                 pvalue_size as size_t, _
                 pvalue      as any ptr, _
                 psize_ret   as size_t ptr) as cl_int
                            
' Program Object APIs
declare function _
clCreateProgramWithSource(context     as cl_context_t, _
                          count       as cl_uint, _
                          strings     as zstring ptr ptr, _
                          lengths     as size_t ptr, _
                          errcode_ret as cl_int ptr) as cl_program_t

declare function _
clCreateProgramWithBinary(context       as cl_context_t, _
                          num_devices   as cl_uint, _
                          device_list   as cl_device_id_t ptr, _
                          lengths       as size_t ptr, _
                          binaries      as zstring ptr ptr, _ 
                          binary_status as cl_int ptr, _
                          errcode_ret   as cl_int ptr) as cl_program_t

declare function _
clRetainProgram(program as cl_program_t) as cl_int

declare function _
clReleaseProgram(program as cl_program_t) as cl_int

declare function _
clBuildProgram(program     as cl_program_t, _
               num_devices as cl_uint, _
               device_list as cl_device_id_t ptr, _
               options     as zstring ptr, _
               pfn_notify  as sub (program as cl_program_t,user_data as any ptr), _
               user_data   as any ptr) as cl_int

declare function _
clUnloadCompiler() as cl_int

declare function _
clGetProgramInfo(program         as cl_program_t, _
                 pname           as cl_program_info_t, _
                 pvalue_size     as size_t, _
                 pvalue          as any ptr, _
                 pvalue_size_ret as size_t ptr) as cl_int

declare function _
clGetProgramBuildInfo(program          as cl_program_t, _
                      device           as cl_device_id_t, _
                      param_name       as cl_program_build_info_t, _
                      param_value_size as size_t, _
                      param_value      as any ptr, _
                      param_value_size_ret as size_t ptr) as cl_int
                            
' Kernel Object APIs
declare function _
clCreateKernel(program     as cl_program_t, _
               kernel_name as zstring ptr, _
               errcode_ret as cl_int ptr) as cl_kernel_t

declare function _
clCreateKernelsInProgram(program         as cl_program_t, _
                         num_kernels     as cl_uint, _
                         kernels         as cl_kernel_t ptr, _
                         num_kernels_ret as cl_uint ptr) as cl_int

declare function _
clRetainKernel(kernel as cl_kernel_t) as cl_int

declare function _
clReleaseKernel(kernel as cl_kernel_t) as cl_int

declare function _
clSetKernelArg(kernel    as cl_kernel_t, _
               arg_index as cl_uint, _
               arg_size  as size_t, _
               arg_value as any ptr) as cl_int

declare function _
clGetKernelInfo(kernel          as cl_kernel_t, _
                pname           as cl_kernel_info_t, _
                pvalue_size     as size_t, _
                pvalue          as any ptr, _
                pvalue_size_ret as size_t ptr) as cl_int

declare function _
clGetKernelWorkGroupInfo(kernel          as cl_kernel_t, _
                         device          as cl_device_id_t, _
                         pname           as cl_kernel_work_group_info_t, _
                         pvalue_size     as size_t, _
                         pvalue          as any ptr, _
                         pvalue_size_ret as size_t ptr) as cl_int

' Event Object APIs
declare function _
clWaitForEvents(num_events as cl_uint, _
                event_list as cl_event_t ptr) as cl_int

declare function _
clGetEventInfo(event           as cl_event_t, _
               pname           as cl_event_info_t, _
               pvalue_size     as size_t, _
               pvalue          as any ptr, _
               pvalue_size_ret as size_t ptr) as cl_int
                            
declare function _
clRetainEvent(event as cl_event_t ) as cl_int

declare function _
clReleaseEvent(event as cl_event_t) as cl_int

' Profiling APIs
declare function _
clGetEventProfilingInfo(event           as cl_event_t, _
                        pname           as cl_profiling_info_t, _
                        pvalue_size     as size_t, _
                        pvalue          as any ptr, _
                        pvalue_size_ret as size_t ptr) as cl_int
                                
' Flush and Finish APIs
declare function _
clFlush(command_queue as cl_command_queue_t) as cl_int

declare function _
clFinish(command_queue as cl_command_queue_t) as cl_int

' Enqueued Commands APIs
declare function _
clEnqueueReadBuffer(command_queue   as cl_command_queue_t, _
                    buffer          as cl_mem_t, _
                    blocking_read   as cl_bool, _
                    offset          as size_t, _
                    cb              as size_t, _
                    p               as any ptr, _
                    nevents_in_list as cl_uint, _
                    event_wait_list as cl_event_t ptr, _
                    event           as cl_event_t ptr) as cl_int
                            
declare function _
clEnqueueWriteBuffer(command_queue   as cl_command_queue_t, _
                     buffer          as cl_mem_t, _
                     blocking_write  as cl_bool, _
                     offset          as size_t, _
                     cb              as size_t, _
                     p               as any ptr, _
                     nevents_in_list as cl_uint, _
                     event_wait_list as cl_event_t ptr, _
                     event           as cl_event_t ptr) as cl_int
                            
declare function _
clEnqueueCopyBuffer(command_queue   as cl_command_queue_t, _
                    src_buffer      as cl_mem_t, _
                    dst_buffer      as cl_mem_t, _
                    src_offset      as size_t, _
                    dst_offset      as size_t, _
                    cb              as size_t, _
                    nevents_in_list as cl_uint, _
                    event_list      as cl_event_t ptr, _
                    event           as cl_event_t ptr) as cl_int
                            
declare function _
clEnqueueReadImage(command_queue   as cl_command_queue_t, _
                   image           as cl_mem_t, _
                   blocking_read   as cl_bool, _
                   origin          as size_t ptr, _
                   region          as size_t ptr, _
                   row_pitch       as size_t, _
                   slice_pitch     as size_t, _
                   p               as  any ptr, _
                   nevents_in_list as cl_uint, _
                   event_list      as cl_event_t ptr, _
                   event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueWriteImage(command_queue     as cl_command_queue_t, _
                    image             as cl_mem_t, _
                    blocking_write    as cl_bool, _
                    origin            as size_t ptr, _
                    region            as size_t ptr, _
                    input_row_pitch   as size_t, _
                    input_slice_pitch as size_t, _
                    p                 as any ptr, _
                    nevents_in_list   as cl_uint, _
                    event_wait_list   as cl_event_t ptr, _
                    event             as cl_event_t ptr) as cl_int

declare function _
clEnqueueCopyImage(command_queue   as cl_command_queue_t, _
                   src_image       as cl_mem_t, _
                   dst_image       as cl_mem_t, _
                   src_origin      as size_t ptr, _
                   dst_origin      as const size_t ptr, _
                   region          as size_t ptr, _
                   nevents_in_list as cl_uint, _
                   event_wait_list as cl_event_t ptr, _
                   event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueCopyImageToBuffer(command_queue   as cl_command_queue_t, _
                           src_image       as cl_mem_t, _
                           dst_buffer      as cl_mem_t, _
                           src_origin      as size_t ptr, _
                           region          as size_t ptr, _
                           dst_offset      as size_t, _
                           nevents_in_list as cl_uint, _
                           event_wait_list as cl_event_t ptr, _
                           event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueCopyBufferToImage(command_queue   as cl_command_queue_t, _
                           src_buffer      as cl_mem_t, _
                           dst_image       as cl_mem_t, _
                           src_offset      as size_t, _
                           dst_origin      as size_t ptr, _
                           region          as size_t ptr, _
                           nevents_in_list as cl_uint, _
                           event_wait_list as cl_event_t ptr, _
                           event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueMapBuffer(command_queue   as cl_command_queue_t, _
                   buffer          as cl_mem_t, _
                   blocking_map    as cl_bool, _
                   map_flags       as cl_map_flags_t, _
                   offset          as size_t, _
                   cb              as size_t, _
                   nevents_in_list as cl_uint, _
                   event_wait_list as cl_event_t ptr, _
                   event           as cl_event_t ptr, _
                   errcode_ret     as cl_int ptr) as any ptr

declare function _
clEnqueueMapImage(command_queue   as cl_command_queue_t, _
                  image           as cl_mem_t, _
                  blocking_map    as cl_bool, _
                  map_flags       as cl_map_flags_t, _
                  origin          as size_t ptr, _
                  region          as size_t ptr, _
                  image_row_pitch as size_t ptr, _
                  image_slice_pitch as size_t ptr, _
                  num_events_list as cl_uint, _
                  event_wait_list as cl_event_t ptr, _
                  event           as cl_event_t ptr, _
                  errcode_ret     as cl_int ptr) as any ptr

declare function _
clEnqueueUnmapMemObject(command_queue   as cl_command_queue_t, _
                        memobj          as cl_mem_t, _
                        mapped_ptr      as any ptr, _
                        nevents_in_list as cl_uint, _
                        event_list      as cl_event_t ptr, _
                        event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueNDRangeKernel(command_queue      as cl_command_queue_t, _
                       kernel             as cl_kernel_t, _
                       work_dim           as cl_uint, _
                       global_work_offset as size_t ptr, _
                       global_work_size   as size_t ptr, _
                       local_work_size    as size_t ptr, _
                       nevents_in_list    as cl_uint, _
                       event_wait_list    as cl_event_t ptr, _
                       event              as cl_event_t ptr) as cl_int

declare function _
clEnqueueTask(command_queue   as cl_command_queue_t, _
              kernel          as cl_kernel_t, _
              nevents_in_list as cl_uint, _
              event_wait_list as cl_event_t ptr, _
              event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueNativeKernel(command_queue   as cl_command_queue_t, _
                      user_func       as sub (p as any ptr), _ 
                      args            as any ptr, _
                      cb_args         as size_t, _
                      num_mem_objects as cl_uint, _
                      mem_list        as cl_mem_t ptr, _
                      args_mem_loc    as any ptr ptr, _
                      nevents_in_list as cl_uint, _
                      event_wait_list as cl_event_t ptr, _
                      event           as cl_event_t ptr) as cl_int

declare function _
clEnqueueMarker(command_queue as cl_command_queue_t, _
                event as cl_event_t ptr) as cl_int

declare function _
clEnqueueWaitForEvents(command_queue as cl_command_queue_t, _
                       num_events as cl_uint, _
                       event_list as const cl_event_t ptr) as cl_int

declare function _
clEnqueueBarrier(command_queue as cl_command_queue_t) as cl_int

' Extension function access
'
' Returns the extension function address for the given function name,
' or NULL if a valid function can not be found.  The client must
' check to make sure the address is not NULL, before using or 
' calling the returned function address.
'
declare function clGetExtensionFunctionAddress( func_name as zstring ptr) as any ptr

end extern


#endif  ' __OPENCL_CL_BI__

