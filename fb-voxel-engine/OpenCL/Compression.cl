#ifndef Compression_cl
#define Compression_cl

//Returns a byte containing in fixed point decimal notation
//Max values -1 to 1, 6 bits decimal precision
//(1) (1).(111111)
//(sign 1)(integer 1).(decimal 6)
uchar compressFloatToByte(const float inFloat){

	float inVal = inFloat;
	uchar retVal = 0x00;
	
	if(inVal < 0){
		retVal = 0x80;
	}
	
	inVal = (inVal < 0) ? inVal * -1 : inVal;

	if(inVal > 1){
		return (retVal | 0x40);
	}

	inVal = inVal - (int)(inVal);
	
	for(int i = 5; i > 0; --i){
		inVal *= 2;
		if(inVal > 1){
			retVal = retVal | (0x01 << i);
			inVal -= 1;
		}
	}
	
	return retVal;

}

//Returns a value between -1 and 1
//with 6 bits of floating precision
float decompressByteToFloat(const uchar inChar){
	
	uchar inVal = inChar;
	
	float retVal = 0.0;
	
	if(inVal & 0x20){ retVal += 0.500000;}
	if(inVal & 0x10){ retVal += 0.250000;}
	if(inVal & 0x08){ retVal += 0.125000;}
	if(inVal & 0x04){ retVal += 0.062500;}
	if(inVal & 0x02){ retVal += 0.031250;}
	if(inVal & 0x01){ retVal += 0.015625;}
	
	if(inVal & 0x40){ retVal += 1;}
			
	if(inVal & 0x80){ retVal *= -1;}
	
	return retVal;

}

//Returns a byte containing in fixed point decimal notation
//Max values -1 to 1, 6 bits decimal precision
//+-(1) (1).(111111111)(11111)

//(sign 1)(integer 1)(decimal 9)(undecided 5)
ushort compressFloatToShort(const float inFloat){

	float inVal = inFloat;
	ushort retVal = 0x0000;
	
	if(inVal < 0){
		retVal = 0x8000;
	}
	
	inVal = (inVal < 0) ? inVal * -1 : inVal;

	if(inVal > 1){
		return (retVal | 0x4000);
	}

	inVal = inVal - (int)(inVal);
	
	for(int i = 13; i > 4; --i){
		inVal *= 2;
		if(inVal > 1){
			retVal = retVal | (0x0001 << i);
			inVal -= 1;
		}
	}
	
	return retVal;

}

//Returns a value between -1 and 1
//with 9 bits of floating precision
float decompressShortToFloat(const ushort inShort){
	
	ushort inVal = inShort;
	
	float retVal = 0.0;
	
	if(inVal & 0x2000){ retVal += 0.500000;}
	if(inVal & 0x1000){ retVal += 0.250000;}
	if(inVal & 0x0800){ retVal += 0.125000;}
	if(inVal & 0x0400){ retVal += 0.062500;}
	if(inVal & 0x0200){ retVal += 0.031250;}
	if(inVal & 0x0100){ retVal += 0.015625;}
	if(inVal & 0x0080){ retVal += 0.0078125;}
	if(inVal & 0x0040){ retVal += 0.00390625;}
	if(inVal & 0x0020){ retVal += 0.001953125;}
	
	//retVal += 1 * (inVal & 0x4000);
	
	if(inVal & 0x4000){ retVal += 1;}
	
	//retVal *= (-1 * (inVal & 0x8000)) + (inVal * !(inVal & 0x8000));
	
	if(inVal & 0x8000){ retVal *= -1;}
	
	return retVal;

}

#endif