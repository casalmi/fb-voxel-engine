' templateC.bas

' comment it out and it will work on ATI/AMD devices too :-)
'#define USE_ATI 

#inclib "OpenCL"
#include once "fbgfx.bi"

#include once "TemplateC.bi"
#include once "OpenCLTypes.bi"

#ifndef NULL
#define NULL cptr(any ptr,0)
#endif

#define CL_DEBUG 1

sub CLCheckError(status as cl_int, Desc as String)
    
    if status <> CL_SUCCESS then
        print Desc
        sleep
    end if
    
end sub

' OpenCL related initialization
' Create Context, Device list, Command Queue
' Create OpenCL memory buffer objects
' Load CL file, compile, link CL source
' Build program and kernel objects
sub initOpenCL(OpenCLFileName as string)
    dim as cl_int status = 0
    dim as size_t deviceListSize
    
    ' Have a look at the available platforms
    ' and pick either the AMD or NVIDIA.
    dim as cl_uint numPlatforms
    dim as  cl_platform_id_t platform = NULL
    status = clGetPlatformIDs(0, NULL, @numPlatforms)
    if (status<>CL_SUCCESS) then
        print "Error: Getting Platforms. (clGetPlatformsIDs)"
        return
    end if


    if (numPlatforms > 0) then
        
        dim as cl_platform_id_t ptr platforms = callocate(numPlatforms*sizeof(cl_platform_id_t))
        status = clGetPlatformIDs(numPlatforms, platforms, NULL)
        
        if (status<>CL_SUCCESS) then
            print "Error: Getting Platform Ids. (clGetPlatformsIDs)"
            return
        end if
        dim as zstring ptr pbuff = callocate(255)
        for i as uinteger = 0  to numPlatforms-1
            status = clGetPlatformInfo(platforms[i], _
                                       CL_PLATFORM_VENDOR, _
                                       255, _
                                       pbuff, _
                                       NULL)
            if (status<>CL_SUCCESS) then
                print "Error: Getting Platform Info. (clGetPlatformInfo)"
                return
            end if
            print "platform[" & i & "] = " & *pbuff
        
            platform = platforms[i]
        
        next
        
        deallocate(pbuff)
        deallocate(platforms)        
    end if

    ' If we could find our platform, use it.
    ' Otherwise pass a NULL and get whatever
    ' the implementation thinks we should be using.
    dim as cl_context_properties_t cps(2)
    cps(0) = CL_CONTEXT_PLATFORM
    cps(1) = cast(cl_context_properties_t,platform)
    cps(2) = 0
    
    dim as cl_context_properties_t ptr cprops
    if (NULL=platform) then
        cprops = NULL
    else
        cprops = @cps(0)
    end if
    /'#ifdef USE_CPU
    ' Create an OpenCL context
    'g_context = clCreateContextFromType(cprops, _
    '                                    CL_DEVICE_TYPE_CPU, _
    '                                    NULL, _
    '                                    NULL, _
    '                                    @status)
    '/
    
    ' Create an OpenCL context on the GPU
    CL_GLOBAL_CONTEXT = clCreateContextFromType(cprops, _
                                                CL_DEVICE_TYPE_GPU, _
                                                NULL, _
                                                NULL, _
                                                @status)

    if (status<>CL_SUCCESS) then
        print "Error: Creating Context. (clCreateContextFromType)"
        return
    end if
    
    ' First, get the size of device list data
    status = clGetContextInfo(CL_GLOBAL_CONTEXT, _
                              CL_CONTEXT_DEVICES, _
                              0, _
                              NULL, _
                             @deviceListSize)
    if(status <> CL_SUCCESS) then
        print "Error: Getting Context Info (device list size, clGetContextInfo)"
        return
    end if
    
    ' Detect OpenCL devices
    CL_GLOBAL_DEVICES = callocate(deviceListSize)
    if (CL_GLOBAL_DEVICES=0) then
        print "Error: No devices found."
        return
    end if

    ' Now, get the device list data
    status = clGetContextInfo(CL_GLOBAL_CONTEXT, _
                              CL_CONTEXT_DEVICES, _
                              deviceListSize, _
                              CL_GLOBAL_DEVICES, _
                              NULL)
    if (status<>CL_SUCCESS) then
        print "Error: Getting Context Info (device list, clGetContextInfo)"
        return
    end if
    
    ' Create an OpenCL command queue
    CL_GLOBAL_COMMAND_QUEUE = clCreateCommandQueue(CL_GLOBAL_CONTEXT, _
                                                   CL_GLOBAL_DEVICES[0], _
                                                   0, _
                                                   @status)
    if (status<>CL_SUCCESS) then
        print "Creating Command Queue. (clCreateCommandQueue)"
        return
    end if

    ' Load CL file
    ' build CL program object
    ' create CL kernel object
    dim filename as string = OpenCLFileName
    chdir exepath

    dim as integer hFile = FreeFile
    if open(filename for binary access read as #hfile) then
        print "Error: load cl file !"
        beep:sleep:return
    end if

    dim as integer FileSize=lof(hFile)
    if FileSize<1 then
        close #hFile
        print "error: empty cl file !"
        beep:sleep:return
    end if

    dim as string strSource = space(filesize)
    get #hFile,,strSource
    close #hFile
    dim as zstring ptr pSource=@strSource[0]

    CL_GLOBAL_PROGRAM = clCreateProgramWithSource(CL_GLOBAL_CONTEXT, _
                                                  1, _
                                                  @pSource, _
                                                  @filesize, _
                                                  @status)
    if (status<>CL_SUCCESS) then
        print "Error: Loading source code (clCreateProgramWithSource)"
        if CL_DEBUG then
            beep:sleep:end
        else
            beep:sleep:return
        end if
    end if

    ' create a cl program executable
    ' for all the devices specified
    status = clBuildProgram(CL_GLOBAL_PROGRAM, 1, CL_GLOBAL_DEVICES, NULL, NULL, NULL)
    if (status<>CL_SUCCESS) then
        print "Error: Building Program (clBuildProgram)"
        print "Error status: "; status
        if CL_DEBUG then
            beep:sleep:end
        else
            beep:sleep:return
        end if
    end if

end sub

function getMaxLocalThreads() as size_t
    
    dim MAX_WORK_GROUP_SIZE as size_t
    dim As cl_uint CLStatus

    CLStatus = clGetDeviceInfo(CL_GLOBAL_DEVICES[0], CL_DEVICE_MAX_WORK_GROUP_SIZE, SizeOf(size_t),@MAX_WORK_GROUP_SIZE,NULL)
    
    return MAX_WORK_GROUP_SIZE
    
end function

function initOpenCLKernel(kernelName as string) as cl_kernel_t
    
    dim status as cl_int = 0
    
    dim returnKernelHandle as cl_kernel_t = 0
    
    returnKernelHandle = clCreateKernel(CL_GLOBAL_PROGRAM, kernelName, @status)
    if (status<>CL_SUCCESS) then
        print "Error: Creating Kernel from program. (clCreateKernel)"
        if CL_DEBUG then
            beep:sleep:end
        else
            beep:sleep:return 0
        end if
    end if
    
    return returnKernelHandle
    
end function

' Release OpenCL resources (Context, Memory etc.)
sub cleanupCLResources()
    dim as cl_int status
    
    clReleaseKernel(NoiseKernel)
    clReleaseKernel(NormalsKernel)
    clReleaseKernel(ThermalErosionKernel)
    clReleaseKernel(ExtractErosionBufferKernel)
    clReleaseKernel(RemovePointWithDensityKernel)
    clReleaseKernel(BlurKernel)
    clReleaseKernel(ApplyMaskKernel)
    
    status = clReleaseProgram(CL_GLOBAL_PROGRAM)
    if (status<>CL_SUCCESS) then
        print "Error: In clReleaseProgram"
        return
    end if
    
    status = clReleaseCommandQueue(CL_GLOBAL_COMMAND_QUEUE)
    if (status<>CL_SUCCESS) then
        print "Error: In clReleaseCommandQueue"
        return
    end if
    
    status = clReleaseContext(CL_GLOBAL_CONTEXT)
    if (status<>CL_SUCCESS) then
        print "Error: In clReleaseContext"
        return
    end if
end sub

' Releases program's resources 
sub cleanupCLHost()
  
    dim status as cl_int = 0
    
    if (CL_GLOBAL_DEVICES<>NULL) then
        deallocate CL_GLOBAL_DEVICES
        CL_GLOBAL_DEVICES = NULL
    end if
end sub

' Print no more than 256 elements of the given array.
' Print Array name followed by elements.
sub print1DArray(arrayName as string, _
                 arrayData as single ptr, _
                 length    as uinteger)
  dim as cl_uint i
  dim as cl_uint numElementsToPrint = iif(length>256,256,length)
  dprint(arrayName)
  for i = 0 to numElementsToPrint-1
    dprint(arrayData[i] & " ";)
  next
  print
end sub