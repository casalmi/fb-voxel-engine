#ifndef MarchingCubesCL_cl
#define MarchingCubesCL_cl

#define __FAST_RELAXED_MATH__

#include "Compression.cl"
#include "VoxelTools.cl"

//__kernel void MCSortVoxels(__global read_only float* voxelWeights,
__kernel void MCSortVoxels(__global read_only float* voxelWeights,
						   __global write_only uchar* voxelScanArray,
						   const int   size,
						   const float4 offset)
{

	int xID = get_global_id(0);
    int yID = get_global_id(1);
    int zID = get_global_id(2);

	int tid = get3DIndex(xID,yID,zID,size);//xID * size * size + yID * size + zID;
	
	const char LocalOffset[8][3] = {{0,0,0}, {1,0,0}, {1,0,1}, {0,0,1},
                                    {0,1,0}, {1,1,0}, {1,1,1}, {0,1,1}};

	float weights[8]   = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

	//ushort compressedWeight;
	uchar cubeIndex = 0;
	
	//Extract all 8 corners of our working voxel
	for(int i = 0; i < 8; ++i){

		weights[i] = voxelWeights[get3DIndex(xID + LocalOffset[i][0],
										     yID + LocalOffset[i][1],
										     zID + LocalOffset[i][2],
										     size)];
		
	}

	cubeIndex += (weights[0] <= 0.0) * 1;
	cubeIndex += (weights[1] <= 0.0) * 2;
	cubeIndex += (weights[2] <= 0.0) * 4;
	cubeIndex += (weights[3] <= 0.0) * 8;
	cubeIndex += (weights[4] <= 0.0) * 16;
	cubeIndex += (weights[5] <= 0.0) * 32;
	cubeIndex += (weights[6] <= 0.0) * 64;
	cubeIndex += (weights[7] <= 0.0) * 128;
	
	voxelScanArray[tid] = cubeIndex;
	
}

/*__kernel void MCExtractMesh(__global read_only ushort* voxelWeights,  //Our thing of voxel weights
						    __global read_only uchar* voxelScanArray, //Our cubeIndex holder
							__global read_only uchar* indexArray,    //Our voxel index array (!!!32 BIT!!!)
							__global read_only uint* vertIndexOffset, //Our index offset for the vertexBuffer (for OpenGL)
						    __global write_only float* vertexBuffer,  //verticies for opengl
							const int    size,
						    const float4 offset,
						    const float  scale)
{

	//We should try to use float4 as much as possible.
	//Especially since we're reading/writing to global memory quite a bit,
	//there could be significant savings in writing one float4 as opposed
	//to 3 individual floating point values.
	//(Registers are 128 bit?... right?)

	int tid = get_global_id(0);

	int xID = indexArray[tid * 3 + 0];
    int yID = indexArray[tid * 3 + 1];
    int zID = indexArray[tid * 3 + 2];
	
	const char LocalOffset[8][3] = {{0,0,0}, {1,0,0}, {1,0,1}, {0,0,1},
                                    {0,1,0}, {1,1,0}, {1,1,1}, {0,1,1}};
	
	float4 positions[8];
	
	float weights[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
	ushort compressedWeight;
	
	uchar cubeIndex = voxelScanArray[tid];
	
	int vertIndex = (int)(vertIndexOffset[tid]);
	
	float4 vertList[12];

	//Extract all 8 corners of our working voxel
	for(int i = 0; i < 8; ++i){
		
		compressedWeight = voxelWeights[get3DIndex(xID + LocalOffset[i][0],
												   yID + LocalOffset[i][1],
												   zID + LocalOffset[i][2],
												   size)];
		
		weights[i] = decompressShortToFloat(compressedWeight) * scale;
			
		//Get the actual 3D positions of the voxel's 8 corners
		positions[i].x = (float)((xID + LocalOffset[i][0]) * scale + offset.x);
		positions[i].y = (float)((yID + LocalOffset[i][1]) * scale + offset.y);
		positions[i].z = (float)((zID + LocalOffset[i][2]) * scale + offset.z);
		positions[i].w = 1.0;

	}

	//Find the positions where the surface intersects the cube edges

	if(MarchingCubesEdgeTable[cubeIndex] & 1){
		vertList[0] = VertexInterp(positions[0],positions[1],weights[0],weights[1]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 2){
		vertList[1] = VertexInterp(positions[1],positions[2],weights[1],weights[2]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 4){
		vertList[2] = VertexInterp(positions[2],positions[3],weights[2],weights[3]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 8){
		vertList[3] = VertexInterp(positions[3],positions[0],weights[3],weights[0]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 16){
		vertList[4] = VertexInterp(positions[4],positions[5],weights[4],weights[5]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 32){
		vertList[5] = VertexInterp(positions[5],positions[6],weights[5],weights[6]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 64){
		vertList[6] = VertexInterp(positions[6],positions[7],weights[6],weights[7]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 128){
		vertList[7] = VertexInterp(positions[7],positions[4],weights[7],weights[4]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 256){
		vertList[8] = VertexInterp(positions[0],positions[4],weights[0],weights[4]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 512){
		vertList[9] = VertexInterp(positions[1],positions[5],weights[1],weights[5]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 1024){
		vertList[10] = VertexInterp(positions[2],positions[6],weights[2],weights[6]);
	}
	
	if(MarchingCubesEdgeTable[cubeIndex] & 2048){
		vertList[11] = VertexInterp(positions[3],positions[7],weights[3],weights[7]);
	}
	
	//Generate each unique vertex from this voxel
	for(int i = 0; i < MarchingCubesNumOfUniqueVerticies[cubeIndex]; ++i){
		vertexBuffer[vertIndex*3 + i*3 + 0] = (float)vertList[MarchingCubesIndexArray[cubeIndex][i]].x;
		vertexBuffer[vertIndex*3 + i*3 + 1] = (float)vertList[MarchingCubesIndexArray[cubeIndex][i]].y;
		vertexBuffer[vertIndex*3 + i*3 + 2] = (float)vertList[MarchingCubesIndexArray[cubeIndex][i]].z;
	}
	
}*/
	
#endif