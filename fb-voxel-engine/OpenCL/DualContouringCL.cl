#ifndef DualContouring_cl
#define DualContouring_cl

#include "VoxelTools.cl"

__kernel void DCSortEdges(__global read_only float* voxelWeights,
						  __global write_only uchar* edgeScanArray,
						  const int   size,
						  const float scale)
{

	int xID = get_global_id(0);
    int yID = get_global_id(1);
    int zID = get_global_id(2);
	
	int tid = get3DIndex(xID, yID, zID, size);

	float weightA = 0.0;
	float weightB = 0.0;

	weightA = voxelWeights[tid] * scale;

	//Check X edge
	if(xID < size-1){
		weightB = voxelWeights[get3DIndex(xID + 1, yID, zID, size)] * scale;

		edgeScanArray[tid] |= (0x01 * ((weightA <= 0) != (weightB <= 0)));
		
		//If their signs differ
		/*if((weightA <= 0) != (weightB <= 0)){
			edgeScanArray[tid] |= 0x01;
		}*/
	}
	
	//Check Y edge
	if(yID < size-1){
		weightB = voxelWeights[get3DIndex(xID, yID + 1, zID, size)] * scale;
		
		edgeScanArray[tid] |= (0x02 * ((weightA <= 0) != (weightB <= 0)));
		
		//If their signs differ
		/*if((weightA <= 0) != (weightB <= 0)){
			edgeScanArray[tid] |= 0x02;
		}*/
	}
	
	//Check Z edge
	if(zID < size-1){
		weightB = voxelWeights[get3DIndex(xID, yID, zID + 1, size)] * scale;
		
		edgeScanArray[tid] |= (0x04 * ((weightA <= 0) != (weightB <= 0)));
		
		//If their signs differ
		/*if((weightA <= 0) != (weightB <= 0)){
			edgeScanArray[tid] |= 0x04;
		}*/
	}
	
}

#endif