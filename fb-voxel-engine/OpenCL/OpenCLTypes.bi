#ifndef OpenCLTypes_bi
#define OpenCLTypes_bi

#inclib "OpenCL"

#include once "fbopencl.bas"

#include once "GL/gl.bi"
#include once "GL/glext.bi"

'"Server" is the GPU sided stuff
'"Client" is the CPU (RAM) sided stuff
type CLMemoryBufferTypeFloat
    
    'Forgive the naming schematic,
    'but "CPU" looked too similar to "GPU"
    dim GPUBuffer as cl_mem_t     'Buffer stored on the gpu
    dim RAMBuffer as cl_float ptr 'Buffer stored on system RAM
    
    dim BufferWidth       as cl_uint
    dim BufferSizeInBytes as cl_uint
    
end type

sub InitCLMemBufferFloat(ByRef CL as OpenCL_TYPE, _
                         ByRef MemBuffer as CLMemoryBufferTypeFloat, _
                         aBufferSize as cl_uint)

    dim status as cl_int = 0

    MemBuffer.BufferWidth = aBufferSize
    MemBuffer.BufferSizeInBytes = (aBufferSize * sizeof(cl_float))

    MemBuffer.RAMBuffer = New cl_float[MemBuffer.BufferWidth]
    if MemBuffer.RAMBuffer = NULL then
        print "Error: Failed to allocate Client Buffer Float"
        beep:sleep:end
    end if

    MemBuffer.GPUBuffer = clCreateBuffer(CL.CONTEXT, _
                                         CL_MEM_READ_WRITE OR CL_MEM_USE_HOST_PTR, _
                                         MemBuffer.BufferSizeInBytes, _
                                         MemBuffer.RAMBuffer, _
                                         @status)
                                    
    if status <> CL_SUCCESS then
        print "Error: clCreateBuffer (Float)"
        
        select case status
            case CL_INVALID_CONTEXT
                print "Invalid Context"
            case CL_INVALID_VALUE
                print "Invalid Value (flags not valid)"
            case CL_INVALID_BUFFER_SIZE
                print "Invalid Buffer Size"
            case CL_INVALID_HOST_PTR
                print "Invalid Host Ptr"
            case CL_MEM_OBJECT_ALLOCATION_FAILURE
                print "Failed to allocate memory object"
            case CL_OUT_OF_HOST_MEMORY
                print "Sink some more coin into your GPU..."
        end select
        print "other error: "; CL.CONTEXT
        sleep
    end if
    
end sub

sub DeleteCLMemBufferFloat(ByRef MemBuffer as CLMemoryBufferTypeFloat)

    dim status as cl_int = 0

    status = clReleaseMemObject(MemBuffer.GPUBuffer)
    if (status<>CL_SUCCESS) then
        print "Error: In clReleaseMemObject (Float)"
        return
    end if
    
    if MemBuffer.RAMBuffer <> NULL then
        delete [] MemBuffer.RAMBuffer
        assert(MemBuffer.RAMBuffer = 0)
    end if
    
end sub

#endif