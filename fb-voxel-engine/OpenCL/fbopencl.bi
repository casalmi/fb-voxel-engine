#ifndef FB_OPENCL_BI
#define FB_OPENCL_BI

type OpenCL_Kernel_TYPE
    
    dim kernelHandle as cl_kernel_t
    
    declare Constructor()
    declare Destructor()
    
end type

type OpenCL_TYPE

    dim CONTEXT as cl_context_t
    dim DEVICES as cl_device_id_t ptr
    dim COMMAND_QUEUE as cl_command_queue_t
    dim PROGRAM as cl_program_t
    
    declare function initOpenCLKernel(kernelName as string) as cl_kernel_t
    
    declare sub initOpenCL(OpenCLFileName as string, CompilerFlags as zstring)

    declare Constructor()
    declare Destructor()

end type

'''Global stuff'''

dim shared OpenCL as OpenCL_TYPE = OpenCL_TYPE()

dim shared NoiseKernel as OpenCL_Kernel_TYPE = OpenCL_Kernel_TYPE()
dim shared GetGradientKernel as OpenCL_Kernel_TYPE = OpenCL_Kernel_TYPE()
dim shared MCSortVoxelsKernel as OpenCL_Kernel_TYPE = OpenCL_Kernel_TYPE()
dim shared MCExtractMeshKernel as OpenCL_Kernel_TYPE = OpenCL_Kernel_TYPE()
dim shared DCSortEdgesKernel as OpenCL_Kernel_TYPE = OpenCL_Kernel_TYPE()
dim shared DCGetHermitData as OpenCL_Kernel_TYPE = OpenCL_Kernel_TYPE()

#endif