#ifndef VoxelGridOpenCLFunctions_bi
#define VoxelGridOpenCLFunctions_bi

#include once "../VoxelEngine.bi"

sub finishCLEvent(ByRef CL as OpenCL_TYPE, ByRef event as cl_event_t)
    
    'This must be called after ANYTHING that creates a cl_event,
    'and before whatever creates the next cl_event.
    'Otherwise, we get a system memory leak.
    '(This does not apply to blocking calls)
    
    'Furthermore, I THINK that all data associated with that event
    '(for instance, cl buffers that are used as arguments to a
    'clEnqueueNDRangeKernel kernel run call), must be deallocated
    'before finishing the event, or you get a memory leak.
    
    dim status as cl_int
    
    'Force the program to execute this call now
    'status = clFlush(CL.COMMAND_QUEUE)
    'clCheckError(status, "Error: Flushing the CL pipeline. (clFlush)")
    
    'Waits on the status of the call to be finished
    'status = clFinish(CL.COMMAND_QUEUE)
    'clCheckError(status, "Error: clFinish. (events)")
    
    ' wait for the kernel call to finish execution
    status = clWaitForEvents(1, @event)
    clCheckError(status, "Error: Waiting for kernel run to finish. (clWaitForEvents)")
    
    status = clReleaseEvent(event)
    clCheckError(status, "Error: clReleaseEvent. (events)")
    
end sub

function initCLBuffer(ByRef CL as OpenCL_TYPE, _
                      CreateBufferFlags as cl_mem_flags_t, _
                      BufferSize as size_t, Array as any ptr) as cl_mem_t
    
    dim retCLBuffer as cl_mem_t
    
    dim status as cl_int
    dim event as cl_event_t

    dim copyBuffer as any ptr

    retCLBuffer = clCreateBuffer(CL.CONTEXT, _
                                 CreateBufferFlags, _
                                 BufferSize, _
                                 Array, _
                                 @status)
    
    'If CL_MEM_COPY_HOST_PTR was used, then this copy is redundant
    'since the array was already copied over to the buffer
    if Array <> 0 and (CreateBufferFlags AND CL_MEM_COPY_HOST_PTR)=0 then

        clEnqueueWriteBuffer(CL.COMMAND_QUEUE, _ 'CL command queue
                             retCLBuffer, _ 'CL buffer
                             CL_TRUE, _ 'Blocking write
                             0, _ 'Offset
                             BufferSize, _ 'Size of bytes
                             Array, _ 'Pointer to the data
                             0, _ 'Events in wait list
                             0, _ 'Event_wait_list_ptr
                             @event) 'CL event ptr
        
        finishCLEvent(CL, event)
        
        /'
        copyBuffer = clEnqueueMapBuffer(CL.COMMAND_QUEUE, _
                                        retCLBuffer, _
                                        CL_TRUE, _
                                        CL_MAP_WRITE, _
                                        0, _
                                        BufferSize, _
                                        0, _
                                        0, _
                                        @event, _
                                        @status)
        
        memcpy(copyBuffer, Array, BufferSize)
        
        clEnqueueUnmapMemObject(CL.COMMAND_QUEUE, _
                                retCLBuffer, _
                                copyBuffer, _
                                0, _
                                0, _
                                @event)
        
        finishCLEvent(CL, event)
        '/
        
    end if
    
    return retCLBuffer
    
end function

sub copyCLBuffer(ByRef CL as OpenCL_TYPE, _
                 ByVal clBuffer as cl_mem_t, _
                 BufferSize as size_t, Array as any ptr)

    dim status as cl_int
    dim event as cl_event_t

    dim copyBuffer as any ptr
    
    
    /'clEnqueueReadBuffer(CL.COMMAND_QUEUE, _ 'CL command queue
                        clBuffer, _ 'CL buffer
                        CL_TRUE, _ 'Blocking read
                        0, _ 'Offset
                        BufferSize, _ 'Size of bytes
                        Array, _ 'Pointer to the data
                        0, _ 'Events in wait list
                        0, _ 'Event_wait_list_ptr
                        @event) 'CL event ptr
    finishCLEvent(CL, event)
    '/
    
    copyBuffer = clEnqueueMapBuffer(CL.COMMAND_QUEUE, _
                                    clBuffer, _
                                    CL_TRUE, _ 'Blocking map
                                    CL_MAP_READ, _
                                    0, _
                                    BufferSize, _
                                    0, _
                                    0, _
                                    @event, _
                                    @status)
    
    finishCLEvent(CL, event)
    
    memcpy(Array, copyBuffer, BufferSize)
    
    clEnqueueUnmapMemObject(CL.COMMAND_QUEUE, _
                            clBuffer, _
                            copyBuffer, _
                            0, _
                            0, _
                            @event)
    
    finishCLEvent(CL, event)
    
end sub

sub runCLNoiseKernel(ByRef CL as OpenCL_TYPE, ByRef Kernel as OpenCL_Kernel_TYPE, _
                     VoxelWeightBuffer as cl_mem_t, aDimentionSize as integer, _
                     aOffset() as single, aScale as cl_float)

    dim as cl_int status
    dim as cl_event_t ptr events
    dim as size_t globalThreads(2)
    dim as size_t localThreads(2)
    
    globalThreads(0) = aDimentionSize
    globalThreads(1) = aDimentionSize
    globalThreads(2) = aDimentionSize
    
    localThreads(0)  = aDimentionSize
    localThreads(1)  = 1
    localThreads(2)  = 1

    clSetKernelArg(Kernel.KernelHandle, 0, sizeof(cl_mem_t),     cptr(any ptr,@VoxelWeightBuffer))
    clSetKernelArg(Kernel.KernelHandle, 1, sizeof(cl_int),       cptr(any ptr,@aDimentionSize))
    clSetKernelArg(Kernel.KernelHandle, 2, sizeof(cl_float) * 4, cptr(any ptr,@aOffset(0)))
    clSetKernelArg(Kernel.KernelHandle, 3, sizeof(cl_float),     cptr(any ptr,@aScale))

    ' Enqueue a kernel run call.
    status = clEnqueueNDRangeKernel(CL.COMMAND_QUEUE, _
                                    Kernel.KernelHandle, _
                                    3, _
                                    NULL, _
                                    @globalThreads(0), _
                                    @localThreads(0), _
                                    0, _
                                    NULL, _
                                    @events)
    
    clCheckError(status, "Error runCLNoiseKernel: Enqueueing kernel onto command queue. (clEnqueueNDRangeKernel)")

    finishCLEvent(CL, events)
    
end sub

'*** Marching Cubes kernels

sub runCLMCSortVoxels(ByRef CL as OpenCL_TYPE, ByRef Kernel as OpenCL_Kernel_TYPE, _
                      VoxelWeightBuffer as cl_mem_t, ByRef voxelScanArray as DynamicBufferuByte, _
                      aDimentionSize as cl_int, aOffset() as single)

    dim as cl_int status
    dim as cl_event_t events
    dim as size_t globalThreads(2)
    dim as size_t localThreads(2)

    dim voxelScanArrayBuffer as cl_mem_t
    
    'We have (size)^3 points, so we have (size-1)^3 voxel cells
    globalThreads(0) = aDimentionSize - 1
    globalThreads(1) = aDimentionSize - 1
    globalThreads(2) = aDimentionSize - 1
    
    localThreads(0)  = 256'aDimentionSize - 1
    localThreads(1)  = 1
    localThreads(2)  = 1
    
    /'voxelScanArrayBuffer = clCreateBuffer(CL.CONTEXT, _
                                          CL_MEM_READ_ONLY OR CL_MEM_USE_HOST_PTR, _
                                          voxelScanArray.GetArraySizeInBytes(), _
                                          voxelScanArray.BufferData, _
                                          @status)
    '/
    
    voxelScanArrayBuffer = initCLBuffer(CL, _
                                        CL_MEM_WRITE_ONLY OR CL_MEM_USE_HOST_PTR, _
                                        voxelScanArray.GetArraySizeInBytes(), _
                                        voxelScanArray.BufferData)
                             
    clSetKernelArg(Kernel.KernelHandle, 0, sizeof(cl_mem_t)    , cptr(any ptr,@VoxelWeightBuffer))
    clSetKernelArg(Kernel.KernelHandle, 1, sizeof(cl_mem_t)    , cptr(any ptr,@voxelScanArrayBuffer))
    clSetKernelArg(Kernel.KernelHandle, 2, sizeof(cl_int)      , cptr(any ptr,@aDimentionSize))
    clSetKernelArg(Kernel.KernelHandle, 3, sizeof(cl_float) * 4, cptr(any ptr,@aOffset(0)))

    ' Enqueue a kernel run call.
    status = clEnqueueNDRangeKernel(CL.COMMAND_QUEUE, _
                                    Kernel.KernelHandle, _
                                    3, _
                                    NULL, _
                                    @globalThreads(0), _
                                    NULL, _ '@localThreads(0), _
                                    0, _
                                    NULL, _
                                    @events)
    
    clCheckError(status, "Error runCLMCSortVoxels: Enqueueing kernel onto command queue. (clEnqueueNDRangeKernel)")
    
    finishCLEvent(CL, events)
    
    copyCLBuffer(CL, _
                 voxelScanArrayBuffer, _
                 voxelScanArray.GetArraySizeInBytes(), _
                 voxelScanArray.BufferData)
    
    'Release the memory associated with this before releasing the event itself
    clReleaseMemObject(voxelScanArrayBuffer)
    
end sub

sub MCSortVoxels(VoxelWeightBuffer as CachedBuffer3DSingle ptr, ByRef voxelScanArray as DynamicBufferuByte, _
                 aDimentionSize as cl_int)
    
    dim as integer x,y,z,i
    dim as integer index
    
    dim LocalOffset(7,2) as ubyte = {{0,0,0}, {1,0,0}, {1,0,1}, {0,0,1}, _
                                     {0,1,0}, {1,1,0}, {1,1,1}, {0,1,1}}
    
    dim weights(7) as single = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
    
    dim cubeIndex as ubyte = 0
    
    for x = 0 to aDimentionSize - 2
        for y = 0 to aDimentionSize - 2
            for z = 0 to aDimentionSize - 2
                
                index = VoxelWeightBuffer->GetIndex(x,y,z)
                
                cubeIndex = 0
                
                for i = 0 to 7
                    
                    weights(i) = VoxelWeightBuffer->GetData(x + LocalOffset(i,0), _
                                                            y + LocalOffset(i,1), _
                                                            z + LocalOffset(i,2))
                    
                next
                
                
                
                cubeIndex = (cubeIndex OR (1   * ((weights(0) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (2   * ((weights(1) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (4   * ((weights(2) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (8   * ((weights(3) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (16  * ((weights(4) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (32  * ((weights(5) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (64  * ((weights(6) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (128 * ((weights(7) <= 0.0) AND 1)))
                
                /'if weights(0) <= 0.0 then cubeIndex = (cubeIndex OR 1  ) : end if
                if weights(1) <= 0.0 then cubeIndex = (cubeIndex OR 2  ) : end if
                if weights(2) <= 0.0 then cubeIndex = (cubeIndex OR 4  ) : end if
                if weights(3) <= 0.0 then cubeIndex = (cubeIndex OR 8  ) : end if
                if weights(4) <= 0.0 then cubeIndex = (cubeIndex OR 16 ) : end if
                if weights(5) <= 0.0 then cubeIndex = (cubeIndex OR 32 ) : end if
                if weights(6) <= 0.0 then cubeIndex = (cubeIndex OR 64 ) : end if
                if weights(7) <= 0.0 then cubeIndex = (cubeIndex OR 128) : end if
                '/
                voxelScanArray.BufferData[index] = cubeIndex
                
            next
        next
    next

end sub

'***Dual contouring kernels

sub runCLDCSortEdges(ByRef CL as OpenCL_TYPE, ByRef Kernel as OpenCL_Kernel_TYPE, _
                     VoxelWeightBuffer as cl_mem_t, ByRef edgeScanArray as DynamicBufferuByte, _
                     aDimentionSize as cl_int, aScale as single)

    dim as cl_int status
    dim as cl_event_t events
    dim as size_t globalThreads(2)
    dim as size_t localThreads(2)

    dim edgeScanArrayBuffer as cl_mem_t
    
    'We have (size)^3 points, so we have (size-1)^3 voxel cells
    globalThreads(0) = aDimentionSize
    globalThreads(1) = aDimentionSize
    globalThreads(2) = aDimentionSize
    
    localThreads(0)  = 256'aDimentionSize - 1
    localThreads(1)  = 1
    localThreads(2)  = 1
    
    /'edgeScanArrayBuffer = clCreateBuffer(CL.CONTEXT, _
                                         CL_MEM_WRITE_ONLY OR CL_MEM_USE_HOST_PTR, _
                                         edgeScanArray.GetArraySizeInBytes(), _
                                         edgeScanArray.BufferData, _
                                         @status)'/
    
    edgeScanArrayBuffer = initCLBuffer(CL, _
                                       CL_MEM_WRITE_ONLY OR CL_MEM_COPY_HOST_PTR, _
                                       edgeScanArray.GetArraySizeInBytes(), _
                                       edgeScanArray.BufferData)
    
    clSetKernelArg(Kernel.KernelHandle, 0, sizeof(cl_mem_t)    , cptr(any ptr,@VoxelWeightBuffer))
    clSetKernelArg(Kernel.KernelHandle, 1, sizeof(cl_mem_t)    , cptr(any ptr,@edgeScanArrayBuffer))
    clSetKernelArg(Kernel.KernelHandle, 2, sizeof(cl_int)      , cptr(any ptr,@aDimentionSize))
    clSetKernelArg(Kernel.KernelHandle, 3, sizeof(cl_float)    , cptr(any ptr,@aScale))

    ' Enqueue a kernel run call.
    status = clEnqueueNDRangeKernel(CL.COMMAND_QUEUE, _
                                    Kernel.KernelHandle, _
                                    3, _
                                    NULL, _
                                    @globalThreads(0), _
                                    NULL, _ '@localThreads(0), _
                                    0, _
                                    NULL, _
                                    @events)
    
    clCheckError(status, "Error runCLDCSortEdges: Enqueueing kernel onto command queue. (clEnqueueNDRangeKernel)")
    
    finishCLEvent(CL, events)
    
    copyCLBuffer(CL, _
                 edgeScanArrayBuffer, _
                 edgeScanArray.GetArraySizeInBytes(), _
                 edgeScanArray.BufferData)
    
    'Release the memory associated with this before releasing the event itself
    clReleaseMemObject(edgeScanArrayBuffer)
    
end sub

sub DCSortEdges(VoxelWeightBuffer as CachedBuffer3DSingle ptr, ByRef edgeScanArray as DynamicBufferuByte, _
                aDimentionSize as integer)
       
    dim as integer x,y,z,i
    dim as integer index
    
    dim weightA as single
    dim weightB as single
    
    for x = 0 to aDimentionSize - 1
        for y = 0 to aDimentionSize - 1
            for z = 0 to aDimentionSize - 1
                
                index = VoxelWeightBuffer->GetIndex(x,y,z)
                
                weightA = VoxelWeightBuffer->BufferData[index]
                
                if x < aDimentionSize - 1 then
                    weightB = VoxelWeightBuffer->GetData(x+1, y, z)
                    
                    edgeScanArray.BufferData[index] OR= &h01 * (((weightA <= 0) <> (weightB <= 0)) AND &h01)

                end if
                
                if y < aDimentionSize - 1 then
                    weightB = VoxelWeightBuffer->GetData(x, y+1, z)
                    
                    edgeScanArray.BufferData[index] OR= &h02 * (((weightA <= 0) <> (weightB <= 0)) AND &h01)

                end if
                
                if z < aDimentionSize - 1 then
                    weightB = VoxelWeightBuffer->GetData(x, y, z+1)
                    
                    edgeScanArray.BufferData[index] OR= &h04 * (((weightA <= 0) <> (weightB <= 0)) AND &h01)

                end if
                
            next
        next
    next
      
end sub


sub runCLDCGetHermiteData(ByRef CL as OpenCL_TYPE, ByRef Kernel as OpenCL_Kernel_TYPE, _
                          VoxelWeightBuffer as cl_mem_t, _
                          ByRef edgeIndex as DynamicBufferuByte, _
                          ByRef hermiteData as DynamicBufferSingle, _
                          edgeCount as integer, _
                          aDimentionSize as cl_int, aOffset() as single, aScale as single)

    dim as cl_int status
    dim as cl_event_t events
    dim as size_t globalThreads(0)
    dim as size_t localThreads(0)

    dim edgeIndexBuffer as cl_mem_t
    dim hermiteDataBuffer as cl_mem_t
    
    globalThreads(0) = edgeCount
    
    localThreads(0)  = 256'aDimentionSize - 1

    edgeIndexBuffer = initCLBuffer(CL, _
                                   CL_MEM_READ_ONLY OR CL_MEM_COPY_HOST_PTR, _
                                   edgeIndex.GetArraySizeInBytes(), _
                                   edgeIndex.BufferData)
    
    hermiteDataBuffer = initCLBuffer(CL, _
                                     CL_MEM_WRITE_ONLY OR CL_MEM_USE_HOST_PTR, _
                                     hermiteData.GetArraySizeInBytes(), _
                                     hermiteData.BufferData)
    
    clSetKernelArg(Kernel.KernelHandle, 0, sizeof(cl_mem_t)    , cptr(any ptr,@VoxelWeightBuffer))
    clSetKernelArg(Kernel.KernelHandle, 1, sizeof(cl_mem_t)    , cptr(any ptr,@edgeIndexBuffer))
    clSetKernelArg(Kernel.KernelHandle, 2, sizeof(cl_mem_t)    , cptr(any ptr,@hermiteDataBuffer))
    clSetKernelArg(Kernel.KernelHandle, 3, sizeof(cl_int)      , cptr(any ptr,@aDimentionSize))
    clSetKernelArg(Kernel.KernelHandle, 4, sizeof(cl_float) * 4, cptr(any ptr,@aOffset(0)))
    clSetKernelArg(Kernel.KernelHandle, 5, sizeof(cl_float)    , cptr(any ptr,@aScale))

    ' Enqueue a kernel run call.
    status = clEnqueueNDRangeKernel(CL.COMMAND_QUEUE, _
                                    Kernel.KernelHandle, _
                                    1, _
                                    NULL, _
                                    @globalThreads(0), _
                                    NULL, _'@localThreads(0), _
                                    0, _
                                    NULL, _
                                    @events)
    
    clCheckError(status, "Error runCLDCGetEdgeVerts: Enqueueing kernel onto command queue. (clEnqueueNDRangeKernel)")
    
    finishCLEvent(CL, events)
    
    copyCLBuffer(CL, _
                 hermiteDataBuffer, _
                 hermiteData.GetArraySizeInBytes(), _
                 hermiteData.BufferData)
    
    'Release the memory associated with this before releasing the event itself
    clReleaseMemObject(edgeIndexBuffer)
    clReleaseMemObject(hermiteDataBuffer)
    
end sub

sub runCLGetGradients(ByRef CL as OpenCL_TYPE, ByRef Kernel as OpenCL_Kernel_TYPE, _
                      Verticies as cl_mem_t, outGradient as cl_mem_t, _
                      VertexCount as integer, aScale as cl_float)
    
    dim as cl_int status
    dim as cl_event_t events
    dim as size_t globalThreads(2)
    
    'Since we cannot guarantee VertexCount is a number
    'divisible by anything but 3 or 1, we can't guarantee
    'a properly aligned local thread count. So we let the
    'device choose one for us.
    
    'dim as size_t localThreads(2)
    
    globalThreads(0) = VertexCount
    
    clSetKernelArg(Kernel.KernelHandle, 0, sizeof(cl_mem_t), cptr(any ptr,@Verticies))
    clSetKernelArg(Kernel.KernelHandle, 1, sizeof(cl_mem_t), cptr(any ptr,@outGradient))
    clSetKernelArg(Kernel.KernelHandle, 2, sizeof(cl_float), cptr(any ptr,@aScale))
    
    status = clEnqueueNDRangeKernel(CL.COMMAND_QUEUE, _
                                    Kernel.KernelHandle, _
                                    1, _
                                    NULL, _
                                    @globalThreads(0), _
                                    NULL, _
                                    0, _
                                    NULL, _
                                    @events)
    
    clCheckError(status, "Error runCLGetGradients: Enqueueing kernel onto command queue. (clEnqueueNDRangeKernel)")
    
    finishCLEvent(CL, events)
    
end sub

#endif