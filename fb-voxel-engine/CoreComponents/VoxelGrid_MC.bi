'Contains the Marching Cubes implementation for VoxelGrid.bi


sub VoxelChunkType.ExtractMeshMC()

    'Extract the mesh from the locally stored voxel buffer
    'using the Marching Cubes algorithm
    
    dim as integer x,y,z,i

    dim status as cl_int
    dim events as cl_event_t        

    if this.DensityType < 0 then
        'Negative values imply no weight data 
        '(it's already been created and deleted)
        return
    end if
    
    /'
     ' Step 1: Sort the voxels.
     ' 
     ' Run the first portion of the MC algorithm to extract a cube index
     ' for each voxel cube.  We iterate over the array of extracted cube
     ' indicies and all those that exibit terrain features are added to
     ' the list of voxels to update in step 2
     '
     '/
    
    '***Init our OpenCL buffer (is aware of LOD)***'
    this.InitOpenCLBuffer()
    
    'For each point, we have one cube index
    '*Also, these buffer types were only really useful for the CPU implementation.
    '*Using OpenCL, we could save some space and use manually allocated array & sizes.
    dim voxelScanArray as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount)
    
    'Sort the voxels and return their cube indicies in voxelScanArray.BufferData
    runCLMCSortVoxels(OpenCL, MCSortVoxelsKernel, _
                      this.OpenCLVoxelBuffer, voxelScanArray, _
                      this.size, this.offset(), this.Scale)
    
    'This will hold our cube indicies for later passing to OpenCL
    dim compactVoxelArray as DynamicBufferuByte = DynamicBufferuByte(this.VoxelCount)
    
    'Create a buffer to store the index of the voxel
    dim voxelIndex as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount * 3)
    
    dim voxelsToCheck as integer = 0
    dim TriCount as integer = 0

    dim cubeIndex as uByte = 0
    dim as integer indexCubeIndex 

    'Create our index array (which is very straight forward)
    dim indexBuffer as DynamicBufferuShort = DynamicBufferuShort(this.DataPointCount * 3 * 5)

    dim vertIndexOffset as DynamicBufferuLong = DynamicBufferuLong(this.DataPointCount * 3)
    
    /'
     ' Step 2: Scan the sorted array
     ' 
     ' Check each cube index to see if it has a terrain feature.
     '
     '/
    
    'Scan the array to see how many voxels we're checking
    for x = 0 to this.Size - 2
        for y = 0 to this.Size - 2
            for z = 0 to this.Size - 2

                'Our indicies for our new arrays
                indexCubeIndex = GetIndex3D(x,y,z,this.Size)
                
                'Get the cube index we retrieved from the sort function earlier
                cubeIndex = voxelScanArray.bufferData[indexCubeIndex]
                
                'If the cube index would have 
                'triangles (NOT 0 or 255), we check this voxel.
                if cubeIndex > 0 and cubeIndex < 255 then
                    
                    'Store the number of voxels we're checking
                    voxelsToCheck += 1

                    'Store the indexing for the vertex offset from each voxel
                    'e.g.: If the first voxel cube has 4 unique verticies,
                    'then it will get a 0, and the next voxel cube will get
                    'a 4.  This allows us to determine where OpenCL will
                    'store it's data in the actual buffer without wasting memory.
                    '[#,#,#, #,#,#, #,#,#, #,#,#, &,&,& ...]
                    'Where # is voxel 0's vertex data and & is voxel 1's
                    vertIndexOffset.push(cast(uLong, this.VertexCount))
                    
                    for i = 0 to MarchingCubesTriCountArray(cubeIndex) - 1
                        'The actual indexing is tricky...
                        'Note the MarchingCubesIndexArrayOFFSET is only an OFFSET,
                        'not an absolute reference like MarchingCubesTriTable
                        indexBuffer.push(this.VertexCount + MarchingCubesIndexArrayOffset(cubeIndex, i * 3 + 0))
                        indexBuffer.push(this.VertexCount + MarchingCubesIndexArrayOffset(cubeIndex, i * 3 + 1))
                        indexBuffer.push(this.VertexCount + MarchingCubesIndexArrayOffset(cubeIndex, i * 3 + 2))
                    next
                    
                    'Add the number of triangles from that voxel
                    this.triangleCount += MarchingCubesTriCountArray(cubeIndex)
                    'Add the number of unique verticies from that voxel
                    this.VertexCount   += MarchingCubesNumOfUniqueVerticies(CubeIndex)
                
                    'Add the cube index to the total
                    compactVoxelArray.push(cubeIndex)                                 
                    
                    'This voxel's location (bottom corner at x,y,z)
                    'is used to determine exactly which voxels to update
                    voxelIndex.push(cast(ushort, x))
                    voxelIndex.push(cast(ushort, y))
                    voxelIndex.push(cast(ushort, z))
                    
                end if
                
            next
        next
    next
    
    if voxelsToCheck = 0 then
            
        'Nothing left to do here.
            
        'Check what kind of density this chunk
        'has (air, solid, unknown?)
        if this.DensityType = 0 then
            
            if voxelScanArray.bufferData[0] = 0 then
                'Everything is air
                this.DensityType = -1
            elseif voxelScanArray.bufferData[0] = 255 then
                'Everything is solid
                this.DensityType = -2
            else
                'Not sure what this is
                this.DensityType = -3
                dprint("Unknown density type")
            end if
            
            'We don't need this buffer anymore since
            'there's nothing worthwhile in it anyways
            delete(this.VoxelBuffer)
            this.VoxelBuffer = 0
        
        end if
        
        this.DeleteOpenCLBuffer()
        
        this.Stage = DELETED
        
        'No point in going past here
        return
        
    end if
    
    'Has geometry
    this.DensityType = 1
    
    /'
     ' Step 3: Extract the mesh
     ' 
     ' For each voxel, we know its cube index, the offset to where
     ' it will store its generated verticies, and the voxel location.
     ' It's pretty straight forward from there.
     '
     '/
    
    'Create our OpenGL buffers and OpenCL buffers to go with it.

    glGenVertexArrays(1, @this.VAO)
    glBindVertexArray(this.VAO)
    
    glGenBuffers(1, @this.IndexIBO)
    glGenBuffers(1, @this.VertexVBO)
    glGenBuffers(1, @this.NormalVBO)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.IndexIBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)*this.triangleCount*3, @indexBuffer.bufferData[0], GL_STATIC_DRAW)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    
    'The "right" way to get attribute locations
    'is to use "glGetAttributeLocation()", but
    'we don't have the shader handle in this
    'structure/type so...
    
    'NOTE TO SELF: Fix that. Shader handles should
    'be accessable from anything that uses them.
    '(Make shader handles global?)

    dim attributeLocation as GLuint = 1'glGetAttribLocation(this.shaderHandle, "in_position")
    glBindBuffer(GL_ARRAY_BUFFER, this.VertexVBO)
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*this.VertexCount*3, NULL, GL_STATIC_DRAW)
    glEnableVertexAttribArray(attributeLocation)
    glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, 0)
    
    attributeLocation = 0'glGetAttribLocation(this.shaderHandle, "in_normal")
    glBindBuffer(GL_ARRAY_BUFFER, this.NormalVBO)
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*this.VertexCount*3, NULL, GL_STATIC_DRAW)
    glEnableVertexAttribArray(attributeLocation)
    glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, 0)
    
    'Call glFinish to ensure that OpenGL is done messing with its memory
    glFinish()
    
    'Create our OpenCL buffers from the OpenGL vertex buffers.
    dim CLVertexBuffer as cl_mem_t
    CLVertexBuffer = clCreateFromGLBuffer(OpenCL.CONTEXT, _
                                          CL_MEM_READ_WRITE, _
                                          this.VertexVBO, _
                                          @status)
    
    dim CLNormalBuffer as cl_mem_t
    CLNormalBuffer = clCreateFromGLBuffer(OpenCL.CONTEXT, _
                                          CL_MEM_WRITE_ONLY, _
                                          this.NormalVBO, _
                                          @status)
    
    'Run our OpenCL kernels on the data
    
    'As the name says, extract our polygon mesh from the voxels
    runCLMCExtractMesh(OpenCL, MCExtractMeshKernel, _
                       this.OpenCLVoxelBuffer, compactVoxelArray, _
                       voxelIndex, vertIndexOffset, _
                       CLVertexBuffer, _
                       voxelsToCheck, _
                       this.size, this.offset(), this.scale)
    
    /'
     ' Step 4: Obtain the normals
     ' 
     ' We're just calculating the values of the noise function
     ' 6 times, twice along each axis, and this gives us the
     ' gradient at any given point.
     ' ...
     ' This is gonna have to change.  It's slow, and the
     ' results suck, especially at thin features.
     '
     '/
    
    'At this point, vertexBuffer is filled with all the verticies
    'that we can send to OpenCL for normal calculation.
    'We can guarantee that the indexing isn't going to conflict
    'with the normals by guaranteeing individual vertex indexing.
    this.GetGradients(CLVertexBuffer, CLNormalBuffer, this.VertexCount)
    '''''''''''''''''''''''''''''''''''''''''''''''''''
    
    'Now that we're done using these buffers, we release them.
    clReleaseMemObject(CLVertexBuffer)
    clReleaseMemObject(CLNormalBuffer)
    
    this.DeleteOpenCLBuffer()
    
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
    
end sub


sub runCLMCExtractMesh(ByRef CL as OpenCL_TYPE, ByRef Kernel as OpenCL_Kernel_TYPE, _
                       ByVal VoxelWeightBuffer as cl_mem_t, ByRef voxelScanArray as DynamicBufferuByte, _
                       ByRef voxelIndex as DynamicBufferuByte, ByRef vertIndexOffset as DynamicBufferuLong, _
                       inVertexBuffer as cl_mem_t, _
                       voxelCount as integer, _
                       aDimentionSize as integer, aOffset() as single, aScale as cl_float)

    dim as cl_int status
    dim as cl_event_t events
    dim as size_t globalThreads(0)
    dim as size_t localThreads(0)

    dim voxelScanBuffer  as cl_mem_t
    dim voxelIndexBuffer as cl_mem_t
    dim vertIndexOffsetBuffer as cl_mem_t
    
    globalThreads(0) = voxelCount
    
    localThreads(0)  = 256'aDimentionSize - 1

    voxelScanBuffer = clCreateBuffer(CL.CONTEXT, _
                                     CL_MEM_READ_ONLY OR CL_MEM_COPY_HOST_PTR, _
                                     voxelScanArray.GetDataSizeInBytes(), _
                                     voxelScanArray.BufferData, _
                                     @status)

    voxelIndexBuffer = clCreateBuffer(CL.CONTEXT, _
                                      CL_MEM_READ_ONLY OR CL_MEM_COPY_HOST_PTR, _
                                      voxelIndex.GetDataSizeInBytes(), _
                                      voxelIndex.BufferData, _
                                      @status)

    vertIndexOffsetBuffer = clCreateBuffer(CL.CONTEXT, _
                                           CL_MEM_READ_ONLY OR CL_MEM_COPY_HOST_PTR, _
                                           vertIndexOffset.GetDataSizeInBytes(), _
                                           vertIndexOffset.BufferData, _
                                           @status)

    clEnqueueAcquireGLObjects(CL.COMMAND_QUEUE, _
                              1, _
                              @inVertexBuffer,_
                              0, _
                              NULL, _
                              @events)
    
    'Aquiring an OpenGL object is an event, we must finish it immediatly.
    finishCLEvent(CL, events)
    
    clSetKernelArg(Kernel.KernelHandle, 0, sizeof(cl_mem_t)    , cptr(any ptr,@VoxelWeightBuffer))
    clSetKernelArg(Kernel.KernelHandle, 1, sizeof(cl_mem_t)    , cptr(any ptr,@voxelScanBuffer))
    clSetKernelArg(Kernel.KernelHandle, 2, sizeof(cl_mem_t)    , cptr(any ptr,@voxelIndexBuffer))
    clSetKernelArg(Kernel.KernelHandle, 3, sizeof(cl_mem_t)    , cptr(any ptr,@vertIndexOffsetBuffer))
    clSetKernelArg(Kernel.KernelHandle, 4, sizeof(cl_mem_t)    , cptr(any ptr,@inVertexBuffer))
    clSetKernelArg(Kernel.KernelHandle, 5, sizeof(cl_int)      , cptr(any ptr,@aDimentionSize))
    clSetKernelArg(Kernel.KernelHandle, 6, sizeof(cl_float) * 4, cptr(any ptr,@aOffset(0)))
    clSetKernelArg(Kernel.KernelHandle, 7, sizeof(cl_float)    , cptr(any ptr,@aScale))

    ' Enqueue a kernel run call.
    status = clEnqueueNDRangeKernel(CL.COMMAND_QUEUE, _
                                    Kernel.KernelHandle, _
                                    1, _
                                    NULL, _
                                    @globalThreads(0), _
                                    NULL, _'@localThreads(0), _
                                    0, _
                                    NULL, _
                                    @events)
    
    clCheckError(status, "Error runCLMCExtractMesh: Enqueueing kernel onto command queue. (clEnqueueNDRangeKernel)")

    'Release the memory associated with this before releasing the event itself
    clReleaseMemObject(voxelScanBuffer)
    clReleaseMemObject(voxelIndexBuffer)
    clReleaseMemObject(vertIndexOffsetBuffer)
    
    finishCLEvent(CL, events)
    
    clEnqueueReleaseGLObjects(CL.COMMAND_QUEUE, _
                              1, _
                              @inVertexBuffer, _
                              0, _
                              NULL, _
                              @events)

    finishCLEvent(CL, events)

    /'status = clEnqueueReadBuffer(CL.COMMAND_QUEUE, _
                                 inVertexBuffer, _
                                 CL_TRUE, _
                                 0, _
                                 outputBuffer.GetArraySizeInBytes(), _
                                 outputBuffer.BufferData, _
                                 0, _
                                 NULL, _
                                 @events)
    
    finishCLEvent(CL, events)'/
    
end sub