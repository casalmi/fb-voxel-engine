#ifndef VoxelInstance3D_bas
#define VoxelInstance3D_bas

#include once "../VoxelEngine.bi"

'A class that generates a voxel octree centered at the origin used for instancing

type VoxelInstance3DType
    
    enum GenerationItemEnum
        
        UNIT_CUBE = 0    'This kind of thing
        ROCK_CLUSTER = 1
        
    end enum
    
    dim Size  as integer 'Each chunk is assumed to be a cube
    dim Scale as single
    dim Offset(2) as single
    
    dim WorkSizeBlock as integer
    
    dim VoxelCount     as integer
    dim DataPointCount as integer
    
    dim Octree as OctreeType ptr

    dim FeaturedVoxelCount as integer

    Declare Sub PopulateVoxels()
    
    Declare Sub SortEdges(VoxelWeightBuffer as CachedBuffer3DSingle ptr, _
                          edgeScanArray as DynamicBufferuByte ptr, _
                          Size as integer)
                          
    Declare Function ClassifyEdges(EdgeScanArray as DynamicBufferuByte ptr, _
                                   OutEdgeIndex as DynamicBufferuInteger ptr, _
                                   Size as integer) as integer
                                   
    Declare Sub GetHermiteData()
    
    Declare Sub SortVoxels(VoxelWeightBuffer as CachedBuffer3DSingle ptr, _
                           OutVoxelScanArray as DynamicBufferuByte ptr, _
                           Size as integer)
                           
    Declare Function CompressVoxelArray(VoxelScanArray as DynamicBufferuByte ptr, _
                                        OutCompactVoxelArray as DynamicBufferuByte ptr, _
                                        OutVoxelIndexArray as DynamicBufferuinteger ptr, _
                                        Size as integer) as integer
                           
    Declare Sub BuildOctreeBase(BottomUpOctree as OctreeType ptr ptr, _
                                VoxelIndexArray as DynamicBufferuinteger ptr, _
                                CompactVoxelArray as DynamicBufferuByte ptr, _
                                HermiteData as DynamicBufferSingle ptr, _
                                VoxelsToCheck as integer, _
                                Size as integer, _
                                Scale as single, _
                                Offset() as single)
    
    Declare Sub LoadInstance(VoxelInstance3DFile as String)
    
    Declare function GenerateInstance(ItemType as VoxelInstance3DType.GenerationItemEnum) as integer
    
    Declare Constructor(sizeCubed as integer, inWorkSize as integer)
    
    Declare Destructor()
    
end type

Constructor VoxelInstance3DType(sizeCubed as integer, inWorkSizeBlock as integer)
    
    const MAX_SIZE as integer = 128
    
    if this.Size > MAX_SIZE then
        dprint("Instance Size Too Large, max is ";MAX_SIZE)
    end if
    
    this.Size = sizeCubed + 1
    this.Scale = GLOBAL_GRID_ALIGNMENT_SCALE
    
    this.Offset(0) = -(this.Size * this.Scale * .5)
    this.Offset(1) = -(this.Size * this.Scale * .5)
    this.Offset(2) = -(this.Size * this.Scale * .5)
    
    this.VoxelCount     = sizeCubed * sizeCubed * sizeCubed
    this.DataPointCount = this.Size * this.Size * this.Size
    
    this.WorkSizeBlock = inWorkSizeBlock
    
end Constructor

sub testNoise(VoxelBuffer as CachedBuffer3DSingle ptr, Size as integer, Scale as single, Offset() as single)
    
    dim as integer x,y,z
    
    dim as single xPos, yPos, zPos
    
    dim noiseVal as single
    
    for x = 0 to Size - 1
        for y = 0 to Size - 1
            for z = 0 to Size - 1
                
                xPos = x * Scale + Offset(0)
                yPos = y * Scale + Offset(1)
                zPos = z * Scale + Offset(2)
                
                'noiseVal = CubeFunction(xPos / 16.0, yPos / 16.0, zPos / 16.0)
                'noiseVal = CubeFunction(xPos, yPos, zPos)
                noiseVal = SphereFunction(xPos / 4.0, yPos / 4.0, zPos / 4.0)
                'noiseVal = SphereFunction(xPos, yPos, zPos)
                'noiseVal = SimplexNoise3D(x / 16.0, y / 16.0, z / 16.0)
                
                VoxelBuffer->SetData(x,y,z, noiseVal)
                
            next
        next
    next
    
end sub

sub TestGetHermiteData(VoxelBuffer as CachedBuffer3DSingle ptr, _
                       EdgeIndex as DynamicBufferuInteger ptr, _
                       OutHermiteData as DynamicBufferSingle ptr, _
                       EdgeCount as integer, _
                       Size as integer, _
                       Scale as single, _
                       Offset() as single)

    dim as integer i,x,y,z
    
    dim as single xPos,yPos,zPos
    
    dim PositionA as Vector3DType
    dim PositionB as Vector3DType
    
    dim EdgePosition as Vector3DType
    dim EdgeNormal as Vector3DType
    
    dim EdgeID as uInteger
    
    dim as single WeightA, WeightB
    
    dim index as integer
    
    for i = 0 to EdgeCount - 1
        
        x = EdgeIndex->BufferData[i * 4 + 0]
        y = EdgeIndex->BufferData[i * 4 + 1]
        z = EdgeIndex->BufferData[i * 4 + 2]

        EdgeID = EdgeIndex->BufferData[i * 4 + 3]
        
        xPos = cast(single, x)
        yPos = cast(single, y)
        zPos = cast(single, z)
        
        PositionA[0] = xPos * Scale + Offset(0)
        PositionA[1] = yPos * Scale + Offset(1)
        PositionA[2] = zPos * Scale + Offset(2)
        
        PositionB = PositionA
        
        index = GetIndex3D(x,y,z, Size)
        
        WeightA = VoxelBuffer->BufferData[index] * Scale
        
        if EdgeID = 0 then
            WeightB = VoxelBuffer->BufferData[GetIndex3D(x+1, y, z, Size)] * Scale
            PositionB[0] = (xPos + 1.0) * Scale + Offset(0)
            
        elseif EdgeID = 1 then
            WeightB = VoxelBuffer->BufferData[GetIndex3D(x, y+1, z, Size)] * Scale
            PositionB[1] = (yPos + 1.0) * Scale + Offset(1)
            
        elseif EdgeID = 2 then
            WeightB = VoxelBuffer->BufferData[GetIndex3D(x, y, z+1, Size)] * Scale
            PositionB[2] = (zPos + 1.0) * Scale + Offset(2)
    
        else
            dprint("Severe error GetHermiteData")
        end if
        
        EdgePosition = VertexInterp(PositionA, PositionB, WeightA, WeightB)
        
        OutHermiteData->BufferData[index * 18 + EdgeID*6 + 0] = EdgePosition[0]
        OutHermiteData->BufferData[index * 18 + EdgeID*6 + 1] = EdgePosition[1]
        OutHermiteData->BufferData[index * 18 + EdgeID*6 + 2] = EdgePosition[2]
        
        'Just for now...
        EdgeNormal = EdgePosition
        EdgeNormal.Normalize()
        
        OutHermiteData->BufferData[index * 18 + EdgeID*6 + 3] = EdgeNormal[0]
        OutHermiteData->BufferData[index * 18 + EdgeID*6 + 4] = EdgeNormal[1]
        OutHermiteData->BufferData[index * 18 + EdgeID*6 + 5] = EdgeNormal[2]

    next
    
end sub

function VoxelInstance3DType.GenerateInstance(ItemType as VoxelInstance3DType.GenerationItemEnum) as integer
    
    if this.Size > this.WorkSizeBlock then
        'Handle this case
    end if
    
    dprint("Running test noise")
    
    dim VoxelBuffer as CachedBuffer3DSingle = CachedBuffer3DSingle(this.Size, this.Size, this.Size)
    
    testNoise(@VoxelBuffer, _
              this.Size, _
              this.Scale, _
              this.Offset())
    
    dprint("Running edge scan")
    
    dim EdgeScanArray as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount)
    
    this.SortEdges(@VoxelBuffer, _
                   @EdgeScanArray, _
                   this.Size)
    
    dim EdgeIndex as DynamicBufferuInteger = DynamicBufferuInteger(this.DataPointCount * 4)
    
    dim FeaturedEdgeCount as integer
    
    dprint("Running classify edge")
    
    FeaturedEdgeCount = this.ClassifyEdges(@EdgeScanArray, _
                                           @EdgeIndex, _
                                           this.Size)
    
    if FeaturedEdgeCount = 0 then
        'Nothing to generate (return error)
        dprint("Error FeaturedEdgeCount = 0")
        return 0
    end if
    
    dim HermiteData as DynamicBufferSingle = DynamicBufferSingle(this.DataPointCount * 3 * 6)
    
    dprint("Running test hermite noise")
    
    TestGetHermiteData(@VoxelBuffer, _
                       @EdgeIndex, _
                       @HermiteData, _
                       FeaturedEdgeCount, _
                       this.Size, _
                       this.Scale, _
                       this.Offset())
    
    dprint("Allocating stuff")
    
    dim VoxelScanArray as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount)
    
    dprint("Running Sort voxels")
    
    this.SortVoxels(@VoxelBuffer, _
                    @VoxelScanArray, _
                    this.Size)
    
    dim CompactVoxelArray as DynamicBufferuByte = DynamicBufferuByte(this.VoxelCount)

    dim VoxelIndexArray as DynamicBufferuInteger = DynamicBufferuInteger(this.DataPointCount * 3)
    
    dim CheckVoxelCount as integer = 0
    
    dprint("Running compress Voxel Array")
    
    CheckVoxelCount = this.CompressVoxelArray(@VoxelScanArray, _
                                              @CompactVoxelArray, _
                                              @VoxelIndexArray, _
                                              this.Size)

    if CheckVoxelCount = 0 then
        'Nothing to generate (return error)
        dprint("Error CheckVoxelCount = 0")
        return 0
    end if
    
    dim BottomUpOctree as OctreeType ptr ptr
    BottomUpOctree =  new OctreeType ptr[this.DataPointCount]
    
    dprint("Running build octree base")
    
    this.BuildOctreeBase(BottomUpOctree, _
                         @VoxelIndexArray, _
                         @CompactVoxelArray, _
                         @HermiteData, _
                         CheckVoxelCount, _
                         this.Size, _
                         this.Scale, _
                         this.Offset())
    
    dprint("Running Build bottom up")
    
    this.Octree = OctreeType.BuildBottomUp(BottomUpOctree, _
                                           this.Size - 1, _
                                           this.Scale, _
                                           this.Offset(), _
                                           0, _ 'Current depth
                                           1, _ 'Max elements
                                           0, _ 'Compare function pointer
                                           0)   'Get index function pointer
    
    delete [] BottomUpOctree
    
    this.FeaturedVoxelCount = CheckVoxelCount
    
    dprint("Returning...")
    
    return 1
    
end function

Sub VoxelInstance3DType.SortEdges(VoxelWeightBuffer as CachedBuffer3DSingle ptr, _
                                  edgeScanArray as DynamicBufferuByte ptr, _
                                  Size as integer)
    
    dim as integer x,y,z,i
    dim as integer index
    
    dim weightA as single
    dim weightB as single
    
    for x = 0 to Size - 1
        for y = 0 to Size - 1
            for z = 0 to Size - 1
                
                index = VoxelWeightBuffer->GetIndex(x,y,z)
                
                weightA = VoxelWeightBuffer->BufferData[index]'GetData(x,y,z)
                
                if x < Size - 1 then
                    weightB = VoxelWeightBuffer->GetData(x+1, y, z)
                    
                    edgeScanArray->BufferData[index] OR= &h01 * (((weightA <= 0.0) <> (weightB <= 0.0)) AND &h01)

                end if
                
                if y < Size - 1 then
                    weightB = VoxelWeightBuffer->GetData(x, y+1, z)
                    
                    edgeScanArray->BufferData[index] OR= &h02 * (((weightA <= 0.0) <> (weightB <= 0.0)) AND &h01)

                end if
                
                if z < Size - 1 then
                    weightB = VoxelWeightBuffer->GetData(x, y, z+1)
                    
                    edgeScanArray->BufferData[index] OR= &h04 * (((weightA <= 0.0) <> (weightB <= 0.0)) AND &h01)
                
                end if
                
            next
        next
    next
    
end sub

Function VoxelInstance3DType.ClassifyEdges(EdgeScanArray as DynamicBufferuByte ptr, _
                                           OutEdgeIndex as DynamicBufferuInteger ptr, _
                                           Size as integer) as integer
    
    dim featuredEdgeCount as integer = 0
    
    dim as integer x,y,z,i
    
    for x = 0 to Size - 1
     for y = 0 to Size - 1
      for z = 0 to Size - 1
    
        i = GetIndex3D(x,y,z,this.size)
        
        'If the edge features a sign change, add it to the array
        
        'X edge
        if EdgeScanArray->BufferData[i] AND &h01 then

            'And push that instead
            OutEdgeIndex->FastPush(x)
            OutEdgeIndex->FastPush(y)
            OutEdgeIndex->FastPush(z)
            OutEdgeIndex->FastPush(0)
            featuredEdgeCount += 1
        end if
        
        'Y edge
        if EdgeScanArray->BufferData[i] AND &h02 then
            OutEdgeIndex->FastPush(x)
            OutEdgeIndex->FastPush(y)
            OutEdgeIndex->FastPush(z)
            OutEdgeIndex->FastPush(1)
            
            featuredEdgeCount += 1
        end if
        
        'Z edge
        if EdgeScanArray->BufferData[i] AND &h04 then
            OutEdgeIndex->FastPush(x)
            OutEdgeIndex->FastPush(y)
            OutEdgeIndex->FastPush(z)
            OutEdgeIndex->FastPush(2)
            
            featuredEdgeCount += 1
        end if

      next
     next
    next
    
    return featuredEdgeCount
    
end function

sub VoxelInstance3DType.GetHermiteData()
    
end sub

sub VoxelInstance3DType.SortVoxels(VoxelWeightBuffer as CachedBuffer3DSingle ptr, _
                                   OutVoxelScanArray as DynamicBufferuByte ptr, _
                                   Size as integer)
    
    dim as integer x,y,z,i
    dim as integer index
    
    dim LocalOffset(7,2) as ubyte = {{0,0,0}, {1,0,0}, {1,0,1}, {0,0,1}, _
                                     {0,1,0}, {1,1,0}, {1,1,1}, {0,1,1}}
    
    dim weights(7) as single = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}
    
    dim cubeIndex as ubyte = 0
    
    for x = 0 to Size - 2
        for y = 0 to Size - 2
            for z = 0 to Size - 2
                
                index = VoxelWeightBuffer->GetIndex(x,y,z)
                
                cubeIndex = 0
                
                for i = 0 to 7
                    
                    weights(i) = VoxelWeightBuffer->GetData(x + LocalOffset(i,0), _
                                                            y + LocalOffset(i,1), _
                                                            z + LocalOffset(i,2))
                    
                next
               
                cubeIndex = (cubeIndex OR (1   * ((weights(0) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (2   * ((weights(1) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (4   * ((weights(2) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (8   * ((weights(3) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (16  * ((weights(4) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (32  * ((weights(5) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (64  * ((weights(6) <= 0.0) AND 1)))
                cubeIndex = (cubeIndex OR (128 * ((weights(7) <= 0.0) AND 1)))

                OutVoxelScanArray->BufferData[index] = cubeIndex
                
            next
        next
    next
    
end sub

Function VoxelInstance3DType.CompressVoxelArray(VoxelScanArray as DynamicBufferuByte ptr, _
                                                OutCompactVoxelArray as DynamicBufferuByte ptr, _
                                                OutVoxelIndexArray as DynamicBufferuinteger ptr, _
                                                Size as integer) as integer
    
    dim as uinteger x,y,z
    dim i as integer = 0
    
    dim voxelsToCheck as integer = 0

    dim cubeIndex as uByte = 0
    
    'Scan the array to see how many voxels we're checking
    for x = 0 to Size - 2
     for y = 0 to Size - 2
      for z = 0 to Size - 2
        
        'Get the cube index we retrieved from the sort function earlier
        cubeIndex = VoxelScanArray->BufferData[i]
        
        i += 1
        
        'If the cube index would have 
        'triangles (NOT 0 or 255), we check this voxel.
        if (cubeIndex > 0) and (cubeIndex < 255) then                                        
            
            'Store the number of voxels we're checking
            voxelsToCheck += 1
        
            'Add the cube index to the total
            OutCompactVoxelArray->FastPush(cubeIndex)
            
            'This voxel's location (bottom corner at x,y,z)
            'is used to determine exactly which voxels to update
            OutVoxelIndexArray->FastPush(x)
            OutVoxelIndexArray->FastPush(y)
            OutVoxelIndexArray->FastPush(z)
            
        end if
    
      next
      i += 1
     next
     i += Size
    next
    
    return VoxelsToCheck
    
end Function

Sub VoxelInstance3DType.BuildOctreeBase(BottomUpOctree as OctreeType ptr ptr, _
                                        VoxelIndexArray as DynamicBufferuinteger ptr, _
                                        CompactVoxelArray as DynamicBufferuByte ptr, _
                                        HermiteData as DynamicBufferSingle ptr, _
                                        VoxelsToCheck as integer, _
                                        Size as integer, _
                                        Scale as single, _
                                        Offset() as single)
    
    dim as integer x,y,z,i,j
    
    'Octree stuff
    dim newNode as VoxelNodeType ptr

    dim OctreeNode as OctreeType ptr

    dim AABBMin as Vector3DType
    dim AABBMax as Vector3DType

    'Vertex stuff
    dim vertex as Vector3DType
    dim normal as Vector3DType
    
    dim vertPtr as single ptr
    dim normPtr as single ptr
    
    dim count as integer = 0
    
    'dim QEM as DualContourQuadricErrorMetric
    'dim planes(11) as Plane3
    
    dim HermiteOffset(11)  as byte = {0, 12, 0, 12, 0, 12, 0, 12, 6, 6, 6, 6}
    dim HermiteIndex(11,2) as byte = {{0,0,0}, {1,0,0}, {0,0,1}, {0,0,0}, {0,1,0}, {1,1,0}, _
                                      {0,1,1}, {0,1,0}, {0,0,0}, {1,0,0}, {1,0,1}, {0,0,1}}

    'Calculate the mass point for each voxel cube
    for i = 0 to voxelsToCheck - 1
        
        'Create the data node
        newNode = new VoxelNodeType
        
        VoxelNodeTypeCount += 1
        
        newNode->Vertex = 0.0
        newNode->Normal = 0.0

        count = 0
        
        x = VoxelIndexArray->BufferData[i * 3 + 0]
        y = VoxelIndexArray->BufferData[i * 3 + 1]
        z = VoxelIndexArray->BufferData[i * 3 + 2]
        
        'Go through each edge and check for a feature
        for j = 0 to 11
            
            if MarchingCubesEdgeTable(CompactVoxelArray->BufferData[i]) AND (1 SHL j) then

                vertPtr = @HermiteData->BufferData[GetIndex3D(x+HermiteIndex(j, 0), _
                                                              y+HermiteIndex(j, 1), _,
                                                              z+HermiteIndex(j, 2), _,
                                                              Size) * 18 + HermiteOffset(j)]
                
                vertex = vertPtr
                
                newNode->Vertex += vertex
                
                'The normals follow immediatly after
                normPtr = (vertPtr + 3)
                
                normal = normPtr
                
                newNode->Normal += normal
                
                count += 1
                
            end if
            
        next

        'The "mass point" is just the average weights of the edge verticies
        '(Also, the count is guaranteed to be > 0)
        
        if count = 0 then
            dprint("!!!Divide by 0 in BuildOctreeBase()!!!")
            assert(0)
        end if
        
        newNode->Vertex /= cast(single, count)
        newNode->Normal.Normalize()
        
        '''Octree generation
        
        newNode->cubeIndex = compactVoxelArray->BufferData[i]

        newNode->vertIndex = cast(ushort, -1)
        newNode->normIndex = cast(ushort, -1)
        
        'The bounding box is the size of one voxel cube
        AABBMin[0] = Offset(0) + (x * Scale)
        AABBMin[1] = Offset(1) + (y * Scale)
        AABBMin[2] = Offset(2) + (z * Scale)
        
        AABBMax = AABBMin + Scale
        
        'Create the octree leaf node
        OctreeNode = new OctreeType(AABBMin, AABBMax, 0, 1, 0)

        'Add the data to the octree grid for bottom up building
        OctreeNode->SetData(newNode)
        
        BottomUpOctree[GetIndex3D(x,y,z,Size)] = OctreeNode
        
    next
    
end sub

Destructor VoxelInstance3DType()

    'Delete the data in the octree
    dim i as integer
    
    dim inList as LinkedListType
    dim node as NodeType ptr
    
    dim dataToRemove as VoxelNodeType ptr
    
    'Iteratively delete the octree items
    if this.Octree then
        
        'Remove all leaf nodes
        this.Octree->GetAllData(@inList)
        
        node = inList.GetHeadNode()
        
        if node then
            for i = 0 to inList.GetItemCount() - 1

                dataToRemove = cast(VoxelNodeType ptr, node->GetData())
                
                delete(dataToRemove)
                
                VoxelNodeTypeCount -= 1
                
                node = node->GetNext()
                
            next
        end if
        
        'Delete the octree recursively
        delete(this.Octree)
        
        inList.Empty()

    end if

end Destructor

#endif