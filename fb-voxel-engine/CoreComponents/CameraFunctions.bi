#ifndef CameraFunctions_bi
#define CameraFunctions_bi

#include once "../VoxelEngine.bi"

type CameraType
    
    public:
    
        projMatrix(15) as single
        viewMatrix(15) as single
        
        Position as Vector3DType
        ViewVec  as Vector3DType
        LookAt   as Vector3DType
        Rot      as Vector2DType
    
        declare sub InitMouse(inWinWidth as integer, inWinHeight as integer)
       
        declare sub SetPosition overload (inPosition() as single)
        declare sub SetPosition          (inPosition as Vector3DType)
        declare sub SetPosition          (X as single, Y as single, Z as single)
        
        declare sub SetSensitivity(inSensitivity as single)
        
        declare function CheckMouseInWindowFocus() as integer                

        declare sub UpdateKeyPress(MoveSpeed as single)
        declare sub UpdateFlyAround(MoveSpeed as double, refreshRateHzF as integer)
        
        declare function Mouse_XD() as single
        declare function Mouse_YD() as single
        
        declare Constructor(aFOV as double, _
                            aRatio as double, _
                            aNearPlane as double, _
                            aFarPlane as double)
        
    private:
        
        WindowWidth  as single
        WindowHeight as single
        
        FOV         as double
        Ratio       as double
        NearPlane   as double
        FarPlane    as double
        
        Sensitivity as single
        
        'Bounding Sphere Radius
        BSR         as single
        'Bounding Sphere Position
        BSPos       as Vector3DType
        
        declare sub InitCamBoundingSphere(FOV as double, _
                                          NearPlane as double, _
                                          FarPlane as double)
        
        declare sub GetMouseToCameraRotation()
        declare sub GetLookAtPos()
        declare sub UpdateFrustumSphere()
        
        declare sub SetProjectionMatrix()
        declare sub SetViewMatrix()
        
        declare sub Update()
        
end type

Constructor CameraType(aFOV as double, aRatio as double, aNearPlane as double, aFarPlane as double)
    
    this.FOV       = aFOV
    this.Ratio     = aRatio
    this.NearPlane = aNearPlane
    this.FarPlane  = aFarPlane
    
    this.InitCamBoundingSphere(aFOV, aNearPlane, aFarPlane)
    
end Constructor

sub CameraType.InitMouse(inWinWidth as integer, inWinHeight as integer)
    
    this.WindowWidth = inWinWidth
    this.WindowHeight = inWinHeight
    SetMouse(this.WindowWidth / 2.0, this.WindowHeight / 2.0, 0, 1)
    
end sub

sub CameraType.InitCamBoundingSphere(FOV as double, NearPlane as double, FarPlane as double)
    
    dim ViewLength as double = FarPlane - NearPlane
    
    dim ViewHeight as double = ViewLength * tan((FOV * 0.0174532925) * .5)
    
    dim ViewWidth as double = ViewHeight * (this.WindowHeight / this.WindowWidth)
    
    dim TempVec(2) as double = {-ViewWidth, -ViewHeight,_
                                (NearPlane + ViewLength * .5) - ViewLength}
    
    this.BSR = FarPlane
    
end sub

'dim shared PrevMouseX as integer
'dim shared PrevMouseY as integer

function CameraType.Mouse_XD() as single
    
    dim as integer RetX, RetY
    
    static PrevMouseX as integer
    static PrevMouseY as integer
    
    dim Difference as integer
    
    GetMouse(RetX, RetY, , )

    Difference = RetX - PrevMouseX
    
    if Abs(RetX - (this.WindowWidth/2.0)) > (this.WindowWidth/2.0) - (this.WindowWidth * .1) then

        RetX = this.WindowWidth / 2.0
        
        'X,Y,   Visible,Clip to Window
        SetMouse(RetX, RetY, 0, 1)

    endif
    
    PrevMouseX = RetX
    
    return Difference
    
end function

function CameraType.Mouse_YD() as single
    
    dim as integer RetX, RetY
    
    static PrevMouseX as integer
    static PrevMouseY as integer
    
    dim Difference as integer
    
    GetMouse(RetX, RetY, , )
    
    Difference = RetY - PrevMouseY
    
    if Abs(RetY - (this.WindowHeight/2.0)) > (this.WindowHeight/2.0) - (this.WindowHeight * .1) then

        RetY = this.WindowHeight / 2.0
        
        'X,Y,Visible,Clip to Window
        SetMouse(RetX, RetY, 0, 1)
        
    endif
    
    PrevMouseY = RetY
    
    return Difference
    
end function

sub CameraType.SetPosition(inPosition() as single)
    
    this.Position = Vector3DType(inPosition(0), inPosition(1), inPosition(2))

end sub

sub CameraType.SetPosition(inPosition as Vector3DType)
    
    this.Position = inPosition
    
end sub

sub CameraType.SetPosition(X as single, Y as single, Z as single)
    
    this.Position = Vector3DType(X,Y,Z)

end sub

sub CameraType.SetSensitivity(inSensitivity as single)

    this.Sensitivity = inSensitivity
    
end sub

sub CameraType.GetMouseToCameraRotation()
    
    if this.Sensitivity = 0 then this.Sensitivity = .1:endif

    this.rot[0] = this.rot[0] + mouse_xd() * this.Sensitivity
    if this.rot[0] > 360 then this.rot[0] = 0:endif
    if this.rot[0] < 0 then this.rot[0] = 360:endif
    this.rot[1] = this.rot[1] - mouse_yd() * this.Sensitivity
    if this.rot[1] > 80 then this.rot[1] = 80:endif
    if this.rot[1] < -80 then this.rot[1] = -80:endif
    
end sub

sub CameraType.GetLookAtPos()

    this.lookat[0] = this.position[0] - (sind(this.rot[0])  * cosd(this.rot[1])) * 5
    this.lookat[1] = this.position[1] + (sind(this.rot[1])) * 5
    this.lookat[2] = this.position[2] + (cosd(this.rot[0])  * cosd(this.rot[1])) * 5
    
end sub

sub CameraType.UpdateFrustumSphere()
    
    this.ViewVec = this.LookAt - this.Position
    this.ViewVec.Normalize()

    this.BSPos = (this.Position + this.ViewVec) * this.BSR

end sub

function CameraType.CheckMouseInWindowFocus() as integer
    
    dim RetX as integer
    dim RetY as integer
    GetMouse(RetX, RetY,,)
    
    'If the mouse is outside the window focus or the mouse
    'doesn't exist (wasn't instantiated), then RetX and RetY
    'will have the value -1
    if RetX = -1 and RetY = -1 then
        return 0
    end if
    
    return 1
    
end function

sub CameraType.Update()
    
    if this.CheckMouseInWindowFocus() = 0 then
        'If the mouse is not in focus, then don't update the camera
        return
    end if
    
    this.GetMouseToCameraRotation()
    this.GetLookatPos()
    this.UpdateFrustumSphere()        
    
end sub

sub CameraType.SetProjectionMatrix()
    
    with this
        buildProjectionMatrix(.projMatrix(), .FOV, .Ratio, .NearPlane, .FarPlane)
    end with

end sub

sub CameraType.SetViewMatrix()
    
    SetCamera(this.viewMatrix(), this.Position, this.Lookat)
            
end sub

sub CameraType.UpdateFlyAround(MoveSpeed as double, refreshRateHz as integer)
    
    static ElapsedTime as double
    
    dim UpdateTime as double
    
    dim newtime as double
    
    UpdateTime = 1.0 / cast(double, refreshRateHz)
    
    this.SetViewMatrix()

    this.SetProjectionMatrix()
    
    newTime = timer - ElapsedTime
    
    'if newTime >= UpdateTime then

        ElapsedTime = Timer
        
        this.UpdateKeyPress(MoveSpeed * (newTime / UpdateTime))
        
    'end if
    
    'The actual mouse rotation update portion is best left uninhibited,
    'otherwise the mouse jumps around from time to time.
    this.Update()
    
end sub

sub CameraType.UpdateKeyPress(MoveSpeed as single) 'WASD based

    dim tempVec as Vector3DType
    
    if MultiKey(FB.SC_LSHIFT)  then MoveSpeed = MoveSpeed * 2.41:endif
    if MultiKey(FB.SC_CONTROL) then MoveSpeed = MoveSpeed * 0.45:endif
    if MultiKey(FB.SC_SPACE)   then MoveSpeed = MoveSpeed * 80:endif

    if MultiKey(FB.SC_W) then
        tempVec = this.LookAt - this.Position
        tempVec.Normalize()
        this.Position += tempVec * MoveSpeed
   
    elseif MultiKey(FB.SC_S) then
        tempVec = this.Position - this.LookAt
        tempVec.Normalize()
        this.Position += tempVec * MoveSpeed
        
        'this.Position = this.Position - Normalize(this.LookAt-this.Pos) * Sensitivity
    endif
    if MultiKey(FB.SC_A) then
        
        tempVec = Vector3DType(-sind(this.Rot[0]-90), 0, cosd((this.Rot[0]-90)))
        tempVec.Normalize()
        this.Position += tempVec * MoveSpeed

        'this.Position = this.Position + Normalize(vec3(-sind(this.Rot(0)-90),0,cosd(this.Rot(0)-90))) * Sensitivity
    elseif MultiKey(FB.SC_D) then
        
        tempVec = Vector3DType(-sind(this.Rot[0]-90), 0, cosd((this.Rot[0]-90)))
        tempVec.Normalize()
        this.Position -= tempVec * MoveSpeed

        'this.Position = this.Position - Normalize(vec3(-sind(this.Rot(0)-90),0,cosd(this.Rot(0)-90))) * Sensitivity
    endif
    
end sub

#endif