#ifndef VoxelGrid_bas
#define VoxelGrid_bas

#include once "../VoxelEngine.bi"

type VoxelChunkType
    
    enum ChunkStageEnum
        
        TEMP_DATA           = -4
        'Was deleted due to lack of features
        DELETED             = -3
        'Was found to have no features
        IGNORE              = -2
        'Has no data (set in constructor)
        NO_DATA             = -1
        'Has weights
        HAS_WEIGHTS         = 0
        'QEF data was put into the octree
        HAS_QEF_DATA        = 1
        'Vertex data was taken from the octree
        HAS_VERTEX_DATA     = 2
        'Tell the main process to upload
        'the buffers to OpenGL
        UPLOAD_TO_GPU_READY = 3
        'Available for rendering
        RENDER_READY        = 4
        
    end enum
    
    enum RenderEnum
        
        NO_RENDER_DATA        = -1
        
        USE_OLD_DATA          = 0
        USE_TEMP_RENDER_DATA  = 1
        USE_NEW_DATA          = 2
    
    end enum

    dim DataStage as ChunkStageEnum
    dim RenderStage as RenderEnum
    
    dim Offset(3) as single '4 elements pads it for use with OpenCL's float4
    dim Size      as integer 'Each chunk is assumed to be a cube
    dim Scale     as single
    
    dim CurrentLOD as integer
    
    dim DensityType as byte
    
    dim VoxelCount     as integer
    dim DataPointCount as integer
    dim TriangleCount  as integer
    
    dim RenderTriCount as uinteger
    
    dim FeaturedVoxelCount as integer
    
    dim OpenCLVoxelBuffer as cl_mem_t
    
    dim Octree as OctreeType ptr
    
    ''''''''''''''Persistant buffers''''''''''''''
    
    dim VoxelBuffer as CachedBuffer3DSingle ptr

    dim QEFData as DynamicBufferSingle ptr
    dim QEFNorms as DynamicBufferSingle ptr
    
    'This one gets deleted after the
    'indicies are uploaded to the GPU
    dim IndexBuffer as DynamicBufferuShort ptr

    ''''''''''''''OpenGL Buffers''''''''''''''
    
    dim GLObject as OpenGLObjectType ptr
    
    'Data stored as: (GLObject, TriCount), (GLObject, TriCount)...
    dim OldObjectList as DynamicBufferuInteger ptr
    
    ''''''''''''''''''''''''''''''''''''''''''
    
    Declare Sub InitOpenCLBuffer()
    Declare Sub PopulateVoxels()
    
    Declare Sub SortEdges()
    Declare Sub GetHermiteData()
    Declare Sub SortVoxels()
    Declare Sub BuildOctree()
    
    Declare Sub ExtractMesh_BuildOctree()
    
    Declare Function ExtractMesh_SimplifyOctree(inNode as OctreeType ptr, _
                                                  distanceThreshold as single, _
                                                  normalThreshold as single, _
                                                  manifoldCollapseLevel as ubyte) as integer
    Declare Sub ExtractMesh_GenerateContour()
    
    Declare Sub ExtractMesh_GetVerticiesRecursive(inNode as OctreeType ptr)
    Declare Sub ExtractMesh_GetNeighborVerticies(inNode as OctreeType ptr, Mask as ubyte)
    Declare Sub ExtractMesh_GetVerticies(inNeighbors() as OctreeType ptr)
    
    Declare Sub ExtractMesh_ProcessCell(inNode as OctreeType ptr)

    Declare Sub ExtractMesh_ProcessEdge(Nodes() as OctreeType ptr, edge as integer)
    
    
    Declare Sub GetGradients(Verticies  as cl_mem_t, _
                             outNormals as cl_mem_t, _
                             VertexCount as integer)
                             
    Declare Sub DeleteOpenCLBuffer()
    Declare Sub Render()
    
    Declare Sub UploadGLBufferData()
    Declare Sub DeleteGLBufferData()
    
    Declare Sub ClearOctreeData()

    Declare Constructor()
    Declare Constructor(inScale as single, sizeCubed as integer, inOffset as Vector3DType)
    
    Declare Destructor()

end type

type VoxelGridType
    
    dim ShaderProgram as OpenGL_Shader_TYPE ptr

    dim ChunkOctree as OctreeType ptr

    dim VoxelOctree as OctreeType ptr
    
    dim ChunkOctreeDepth as integer
    dim VoxelOctreeDepth as integer
    
    dim VoxelSizeCubed as integer
    
    dim ChunkDeleteList as DynamicBufferuInteger
    
    dim CameraReference as Vector3DType ptr

    dim LODCount as integer
    dim LODDistConst as single
    
    dim UpdateThread as any ptr
    dim SeamThread as any ptr
    dim ThreadEndCondition as integer
    
    dim DataMutex as any ptr
    dim WorkThreadMutex as any ptr
    
    dim RenderFlag as ubyte
    dim ExtractMeshFlag as ubyte
    
    dim MaterialTex1 as GLuint
    dim MaterialTex2 as GLuint
    dim MaterialTex3 as GLuint
    
    Declare sub SpawnThread()
    Declare sub KillThread()
    
    Declare sub LockData()
    Declare sub UnlockData()
    
    Declare sub RenderEnterProtocol(ChunkNode as OctreeType ptr)
    Declare sub RenderExitProtocol(ChunkNode as OctreeType ptr)
    
    Declare sub ExtractMeshEnterProtocol(ChunkNode as OctreeType ptr)
    Declare sub ExtractMeshExitProtocol(ChunkNode as OctreeType ptr)
    
    Declare sub NodeDeletionEnterProtocol(ChunkNode as OctreeType ptr)
    Declare sub NodeDeletionExitProtocol(ChunkNode as OctreeType ptr)
    
    Declare sub RenderRecurse(inNode as OctreeType ptr)
    
    Declare sub RenderDebug(ViewMat() as single, ProjMat() as single, inNode as OctreeType ptr)
    Declare sub Render(ViewMat() as single, ProjMat() as single, inNode as OctreeType ptr)
    
    Declare sub SendRepatchSignal(ChunkNode as OctreeType ptr)
    
    Declare sub UpdateOctreeToCamera(ChunkNode as OctreeType ptr, VoxelNode as OctreeType ptr)
    
    Declare function OctreePostUpdate(ChunkNode as OctreeType ptr) as integer
    
    Declare sub UpdateOctreeChunks(inNode as OctreeType ptr)
    Declare sub UpdateOctreePatcher(inNode as OctreeType ptr)
    
    Declare sub VoxelOctreeRecurse(inNode as OctreeType ptr)
    
    Declare sub DeleteSubNodes(inNode as OctreeType ptr, GLList as DynamicBufferuInteger ptr)
    
    Declare sub CleanList()
    
    Declare static sub UpdateVoxelChunks(VoxelGrid as VoxelGridType ptr)
    Declare static sub PatchChunkSeams(VoxelGrid as VoxelGridType ptr)
                        
    Declare Constructor(SizeCubed as integer, Scale as single, inLODCount as integer, LODDistanceConst as single)
    
    Declare Destructor()
    
end type

dim shared AmbiguousCubeIndex(255) as ubyte

'Calculates all cube indicies that are ambiguous (result in non manifold triangulation)
sub CalculateAmbiguousCubeIndex()
    
    dim Faces(7, 3) as ubyte = {{0,1,2,3}, {4,5,6,7}, {0,3,7,4}, {1,2,6,5}, _
                                {0,1,5,4}, {2,3,7,6}, {0,2,6,4}, {1,3,7,5}}

    dim as integer i,j
    
    for i = 0 to 255
        AmbiguousCubeIndex(i) = 0
        'Check ambiguity on each face
        for j = 0 to 7
            
            if (sgn(i AND (1 SHL Faces(j,0))) = sgn(i AND (1 SHL Faces(j,2)))) AND _
               (sgn(i AND (1 SHL Faces(j,1))) = sgn(i AND (1 SHL Faces(j,3)))) then
                
                if (sgn(i AND (1 SHL Faces(j,0))) <> sgn(i AND (1 SHL Faces(j,1)))) then
                    
                    AmbiguousCubeIndex(i) = 1
                    exit for
                
                end if
                
            end if
        next
        
    next

end sub

CalculateAmbiguousCubeIndex()

Constructor VoxelChunkType(inScale as single, sizeCubed as integer, inOffset as Vector3DType)
    
    this.Scale = inScale
    
    this.Size = sizeCubed + 1
    
    this.Offset(0) = inOffset[0]
    this.Offset(1) = inOffset[1]
    this.Offset(2) = inOffset[2]
    this.Offset(3) = 1.0
    
    this.VoxelCount     = sizeCubed * sizeCubed * sizeCubed
    this.DataPointCount = this.Size * this.Size * this.Size
    
    this.GLObject = 0
    this.OldObjectList = 0
    
    this.DataStage = this.NO_DATA
    this.RenderStage = this.NO_RENDER_DATA

end Constructor

Constructor VoxelChunkType()

    this.DataStage = NO_DATA
    this.RenderStage = NO_RENDER_DATA
    
    this.OldObjectList = 0
    
end Constructor

function ComparePointers(inA as any ptr, inB as any ptr) as integer
    
    dim as integer A,B
    
    A = cast(integer, inA)
    B = cast(integer, inB)
    
    return sgn(A - B)
    
end function

Constructor VoxelGridType(SizeCubed as integer, Scale as single, inLODCount as integer, LODDistanceConst as single)
    
    'Naw son, use something smaller
    ' > 32 screws with my backend code
    if SizeCubed > 32 then
        dprint(__FILE__;" (";__LINE__;"): Grid type too large (size <= 64) ")
        assert(SizeCubed <= 32)
        SizeCubed = 32
    end if
    
    if SizeCubed MOD 2 then SizeCubed += 1 : end if

    dim as integer x,y,z,i
    
    this.VoxelSizeCubed = SizeCubed
    
    this.LODCount = inLODCount
    
    this.LODDistConst = LODDistanceConst
    
    'Allocate space for the LOD rings

    dim OctreeDepth as integer = 0
    
    dim AABBMin as Vector3DType
    dim AABBMax as Vector3DType
    
    AABBMin = 0.0
    'AABBMin(0) = 0 : AABBMin(1) = 0 : AABBMin(2) = 0
    
    'GridScale *= pow(2, LODCount-1)
    
    'while GridScale > pow(2, OctreeDepth)
    while pow(2, LODCount-1) > pow(2, OctreeDepth)
        OctreeDepth += 1
    wend
    
    this.ChunkOctreeDepth = OctreeDepth
    
    AABBMax = pow(2, OctreeDepth) * SizeCubed * Scale
    AABBMin = (AABBMax * -1) / 2
    AABBMax = AABBMin + AABBMax

    this.ChunkOctree = new OctreeType(AABBMin, _
                                      AABBMax, _
                                      OctreeDepth, _
                                      1, _
                                      @CompareVoxelToAABB)
    
    OctreeTypeCount += 1
    
    this.ChunkOctree->SetGetIndexFunction(@GetAABBIndex)

    OctreeDepth = 0

    while SizeCubed > pow(2, OctreeDepth)
        OctreeDepth += 1
    wend
    
    this.VoxelOctreeDepth = OctreeDepth
    
    this.VoxelOctree = new OctreeType(AABBMin, _
                                     AABBMax, _
                                     this.ChunkOctreeDepth + this.VoxelOctreeDepth, _
                                     1, _
                                     @CompareVoxelToAABB)
    
    OctreeTypeCount += 1
    
    this.VoxelOctree->SetGetIndexFunction(@GetAABBIndex)

    this.ChunkOctree->PrepareChildren()
    this.VoxelOctree->PrepareChildren()

    this.ThreadEndCondition = 0
    
    this.SpawnThread()
    
    MaterialTex1 = load_texture("Media/grass.bmp", 512, 512, GL_REPEAT)
    MaterialTex2 = load_texture("Media/dirt.bmp", 512, 512, GL_REPEAT)
    MaterialTex3 = load_texture("Media/stone.bmp", 512, 512, GL_REPEAT)
    
    dprint("Final textures: ";MaterialTex1;", ";MaterialTex2;", ";MaterialTex3)
    
end Constructor

sub VoxelGridType.RenderRecurse(inNode as OctreeType ptr)
    
    dim i as integer

    dim node as NodeType ptr
    dim currChunk as VoxelChunkType ptr
    
    dim child as OctreeType ptr
    
    currChunk = cast(VoxelChunkType ptr, inNode->GetDataItem())
    
    if currChunk then
        'SetUniformInt(this.ShaderProgram->ShaderHandle, "isWireFrame", 0)
        'glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        currChunk->Render()
        
        if currChunk->DataStage <> VoxelChunkType.TEMP_DATA then
            return
        end if
        
    end if
    
    if inNode->HasChildren() then
        
        for i = 0 to 7
            
            child = inNode->GetChild(i)
            
            if child then
                this.RenderRecurse(child)
            end if
            
        next
        
        return
        
    end if

end sub

sub VoxelGridType.RenderDebug(ViewMat() as single, ProjMat() as single, inNode as OctreeType ptr)
    
end sub

sub VoxelGridType.Render(ViewMat() as single, ProjMat() as single, inNode as OctreeType ptr)
    
    dim i as integer

    dim node as NodeType ptr
    dim currChunk as VoxelChunkType ptr
    
    dim child as OctreeType ptr
    
    glUseProgram(this.ShaderProgram->ShaderHandle)

    SetUniformMatrix4(this.ShaderProgram->ShaderHandle, "viewMatrix", ViewMat())
    SetUniformMatrix4(this.ShaderProgram->ShaderHandle, "projMatrix", ProjMat())
    
    SetUniformInt(this.ShaderProgram->ShaderHandle, "material1", 0)
    SetUniformInt(this.ShaderProgram->ShaderHandle, "material2", 1)
    SetUniformInt(this.ShaderProgram->ShaderHandle, "material3", 2)

    glEnable(GL_TEXTURE_2D)
    
    glActiveTexture(GL_TEXTURE0)
    glBindTexture(GL_TEXTURE_2D, this.MaterialTex1)
    
    glActiveTexture(GL_TEXTURE1)
    glBindTexture(GL_TEXTURE_2D, this.MaterialTex2)
    
    glActiveTexture(GL_TEXTURE2)
    glBindTexture(GL_TEXTURE_2D, this.MaterialTex3)
    
    this.RenderRecurse(inNode)
    
    'glUseProgram(0)    
    
end sub

sub VoxelGridType.CleanList()
    
    dim i as integer
    
    dim node as NodeType ptr

    dim currObject as OpenGLObjectType ptr
    
    if this.ChunkDeleteList.Count <> 0 then

        for i = 0 to this.ChunkDeleteList.Count - 1
                
            currObject = cast(OpenGLObjectType ptr, this.ChunkDeleteList.Pop())

            delete(currObject)
            
            OpenGLObjectTypeCount -= 1
            
        next

    end if
    
end sub

sub VoxelGridType.DeleteSubNodes(inNode as OctreeType ptr, GLList as DynamicBufferuInteger ptr)
    
    'NOT A THREAD SAFE FUNCTION
    
    dim i as integer
    
    dim currChunk as VoxelChunkType ptr
    
    dim currObject as OpenGLObjectType ptr
    
    dim child as OctreeType ptr

    if inNode->HasChildren() then
        
        for i = 0 to 7
            
            child = inNode->GetChild(i)
            
            if child then
                DeleteSubNodes(child, GLList)
            end if

        next
        
    end if
    
    currChunk = cast(VoxelChunkType ptr, inNode->GetDataItem())
    
    if currChunk then
        
        'Delete the node associated with this
        
        if currChunk->GLObject <> 0 then
            'dprint("Pushing for deletion ";currChunk->GLObject)
            'Technically, a ptr is the size of an integer.
            '(This is why you use a language with proper pointers : )

            if GLList then
                
                'If we're saving the data, push it into the  list
                
                GLList->Push(currChunk->RenderTriCount)
                GLList->Push(cast(uinteger, currChunk->GLObject))
            
            else
                
                this.ChunkDeleteList.Push(cast(uinteger, currChunk->GLObject))
                
            end if
            
            currChunk->GLObject = 0

        end if
        
        if currChunk->OldObjectList then
            
            if currChunk->OldObjectList->Count < 2 then
                dprint("OLD OBJECT LIST DOESN'T HAVE ENOUGH ITEMS DeleteSubNodes()")
                sleep
            end if
            
            for i = 0 to (currChunk->OldObjectList->Count SHR 1) - 1
                
                currObject = cast(OpenGLObjectType ptr, currChunk->OldObjectList->Pop())
                
                if GLList then
                    
                    'Push the data into the temporary list
                    GLList->Push(currChunk->OldObjectList->Pop())
                
                    GLList->Push(cast(uinteger, currObject))
                
                else

                    'Push the data for deletion
                    currChunk->OldObjectList->Pop()
                    this.ChunkDeleteList.Push(cast(uinteger, currChunk->GLObject))
                    
                end if
                
            next
            
            delete(currChunk->OldObjectList)
            currChunk->OldObjectList = 0

        end if
        
        delete(currChunk)
        
        VoxelChunkTypeCount -= 1

        inNode->SetDataItem(0)

    end if

end sub

sub VoxelGridType.SendRepatchSignal(ChunkNode as OctreeType ptr)
    
    'Sends a signal to the neighboring chunks to update their
    'mesh after a chunk was deleted/modified
    
    dim i as integer
    
    dim ContourNeighbor(5, 2) as ubyte = _
        {{0,0,-1}, {0,-1,0}, {-1,0,0}, {0,-1,-1}, {-1,0,-1}, {-1,-1,0}}
    
    dim ContourChunkIndexMasks(5) as ubyte = _
        {&haa, &hcc, &hf0, &h88, &ha0, &hc0}
    
    dim neighbor as OctreeType ptr
    dim neighborChunk as VoxelChunkType ptr
    
    dim ChildList as LinkedListType
    dim node as NodeType ptr
    
    dim ChildChunk as VoxelChunkType ptr
    
    for i = 0 to 5

        neighbor = ChunkNode->GetNeighbor(ContourNeighbor(i, 0), _
                                          ContourNeighbor(i, 1), _
                                          ContourNeighbor(i, 2))

        if neighbor then
                
            if neighbor->HasChildren() then
                
                'Case where the neighbor is a higher resolution
                'chunk than the current chunk
                
                neighbor->GetIndexedChildren(@ChildList, _
                                             ContourChunkIndexMasks(i))
                node = ChildList.GetHeadNode()
                
                while node
                    ChildChunk = cast(OctreeType ptr, node->GetData())->GetDataItem()

                    if ChildChunk then
                        if ChildChunk->DataStage = VoxelChunkType.RENDER_READY then
                            
                            ChildChunk->DataStage = VoxelChunkType.HAS_QEF_DATA
    
                            'Use the old render data for the time being
                            ChildChunk->RenderStage = VoxelChunkType.USE_OLD_DATA
                            
                        end if
                    end if
                    
                    node = node->GetNext()
                wend
                
                ChildList.Empty()
                
            else
                
                'Case where the neighbor is equal or lower resolution
                'than the current chunk
                
                neighborChunk = neighbor->GetDataItem()
                
                if neighbor then

                    if neighborChunk->DataStage = VoxelChunkType.RENDER_READY then
                            
                        neighborChunk->DataStage = VoxelChunkType.HAS_QEF_DATA
    
                        'Use the old render data for the time being
                        neighborChunk->RenderStage = VoxelChunkType.USE_OLD_DATA
                        
                    end if
                    
                end if
                
            end if
            
        end if
        
    next
    
end sub

sub VoxelGridType.UpdateOctreeToCamera(ChunkNode as OctreeType ptr, _
                                       VoxelNode as OctreeType ptr)

    dim distanceActual as single
    dim distanceThreshold as single
    
    dim as integer i,j
    
    dim currChunk as VoxelChunkType ptr
    
    dim SaveList as DynamicBufferuInteger ptr = 0
    
    dim ChunkChild as OctreeType ptr
    dim VoxelChild as OctreeType ptr
    
    dim chunkScale as single
    
    dim Position as Vector3DType
    
    if this.CameraReference then
        Position = *this.CameraReference
    else
        Position = Vector3DType(0,0,0)
    end if
    
    
    'dprint("Center = ";Center;" Depth = ";inNode->GetDepth())
    
    distanceActual = (Position - ChunkNode->GetMidpoint()).Dot()
    'distanceActual = (Vector3dType(0,0,0) - ChunkNode->GetMidpoint()).Dot()
    
    distanceThreshold = this.LODDistConst * (1 SHL ChunkNode->GetDepth())'pow(2, ChunkNode->GetDepth())
    distanceThreshold *= distanceThreshold
    
    'dprint("testing distance "; distance; " max tolerance ";distConst * pow(2, inNode->GetDepth());" depth ";inNode->GetDepth())

    dim currObject as OpenGLObjectType ptr

    'currChunk = ChunkNode->GetDataItem()
    
    'Now check the actual thing
    
    if distanceActual < distanceThreshold then
        
        'Don't go past the final depth of the octree
        if ChunkNode->GetDepth() = 0 then

            if ChunkNode->GetDataItem() = 0 then

                'If we're at a leaf node and don't have a chunk, make one.

                chunkScale = ChunkNode->GetAABBMax()[0] - ChunkNode->GetAABBMin()[0]
                chunkScale /= this.VoxelSizeCubed
                
                currChunk = new VoxelChunkType(chunkScale, this.VoxelSizeCubed, ChunkNode->GetAABBMin())
                
                VoxelChunkTypeCount += 1
                
                currChunk->Octree = VoxelNode
                
                ChunkNode->SetDataItem(currChunk)
                VoxelNode->SetDataItem(0)
                
            end if
            
            return
            
        end if
        
        'If we have children, recurse over them
        if ChunkNode->HasChildren() then

            for i = 0 to 7
                
                ChunkChild = ChunkNode->GetChild(i)
                VoxelChild = VoxelNode->GetChild(i)
                
                if (ChunkChild <> 0) AND (VoxelChild <> 0) then

                    UpdateOctreeToCamera(ChunkChild, VoxelChild)
                
                end if
                
            next
            
            return
            
        end if
        
        'At this point, we are at a node with no children (but should have)
        
        if ChunkNode->GetDataItem() then
            
            'This will rarely have more than 1 temporary render object in it
            SaveList = new DynamicBufferuInteger(2)
            
            'this.LockData()

            this.NodeDeletionEnterProtocol(ChunkNode)
                
                this.DeleteSubNodes(ChunkNode, SaveList)
            
            this.NodeDeletionExitProtocol(ChunkNode)
            
            'this.UnlockData()
            
            if SaveList->Count = 0 then
                delete(SaveList)
                SaveList = 0
            else

                'Dummy node that holds temporary render data
                
                chunkScale = ChunkNode->GetAABBMax()[0] - ChunkNode->GetAABBMin()[0]
                chunkScale /= this.VoxelSizeCubed
                
                currChunk = new VoxelChunkType(chunkScale, this.VoxelSizeCubed, ChunkNode->GetAABBMin())
                
                VoxelChunkTypeCount += 1
                
                currChunk->OldObjectList = SaveList
                currChunk->DataStage   = VoxelChunkType.TEMP_DATA
                currChunk->RenderStage = VoxelChunkType.USE_TEMP_RENDER_DATA
                
                ChunkNode->SetDataItem(currChunk)
                
                'Repatch after the other data has been updated
                
            end if

        end if

        
        if ChunkNode->PrepareChildren() + VoxelNode->PrepareChildren() = 2 then
            
            'If both were successful
            
            for i = 0 to 7
                
                ChunkChild = ChunkNode->GetChild(i)
                VoxelChild = VoxelNode->GetChild(i)
                
                if (ChunkChild <> 0) AND (VoxelChild <> 0) then

                    UpdateOctreeToCamera(ChunkChild, VoxelChild)

                end if
            next
            
            return
            
        else
            
            dprint("Failed to prepare children ";i)
            sleep
            return
            
        end if

    else
        
        if ChunkNode->HasChildren() then
            
            if ChunkNode->GetDataItem() = 0 then
                
                SaveList = new DynamicBufferuInteger(16)
                
                'Mark all children for deletion
                
                this.NodeDeletionEnterProtocol(ChunkNode)

                    for i = 0 to 7
                        
                        ChunkChild = ChunkNode->GetChild(i)
                        
                        if ChunkChild then
                            
                            this.DeleteSubNodes(ChunkChild, SaveList)

                        end if
                        
                    next
                    
                    'Collapse the chunks below it
                    
                    ChunkNode->CollapseChildren()

                this.NodeDeletionExitProtocol(ChunkNode)
                
                if SaveList->Count = 0 then
                    delete(SaveList)
                    SaveList = 0
                end if
                
                'dprint("Collapsing children")
                VoxelNode->CollapseChildren()

            end if

        end if

        if ChunkNode->GetDataItem() = 0 then
            
            'If this node doesn't have a chunk, make one
            
            chunkScale = ChunkNode->GetAABBMax()[0] - ChunkNode->GetAABBMin()[0]
            chunkScale /= this.VoxelSizeCubed
            
            currChunk = new VoxelChunkType(chunkScale, this.VoxelSizeCubed, ChunkNode->GetAABBMin())
            
            VoxelChunkTypeCount += 1
            
            currChunk->Octree = VoxelNode
            
            'If we have render data from children nodes, put them in this chunk
            'They'll be deleted "eventually"
            if SaveList then
                currChunk->OldObjectList = SaveList
                currChunk->RenderStage = VoxelChunkType.USE_TEMP_RENDER_DATA
            end if
            
            ChunkNode->SetDataItem(currChunk)
            
        else
            
            if SaveList then
                
                dprint("FOUND THE PROBLEM: Save list with null data item")
                sleep
                
            end if
            
        end if
        
        
    end if
    
end sub

function VoxelGridType.OctreePostUpdate(ChunkNode as OctreeType ptr) as integer
    
    dim retVal as integer = 1
    
    dim i as integer
    
    dim child as OctreeType ptr
    dim currChunk as VoxelChunkType ptr
    
    dim currObject as OpenGLObjectType ptr
    
    currChunk = ChunkNode->GetDataItem()
    
    'Check for old render data in new nodes
    if currChunk then
        
        'Check to see if we have an old object list
        
        if currChunk->OldObjectList then

            if currChunk->DataStage = VoxelChunkType.DELETED OR _
               currChunk->DataStage = VoxelChunkType.RENDER_READY then
                
                if currChunk->RenderStage = VoxelChunkType.USE_TEMP_RENDER_DATA then
                    dprint("FAILED TO CHANGE RENDER STAGE OctreePostUpdate()")
                    sleep
                end if
                
                'this.LockData()

                    for i = 0 to currChunk->OldObjectList->Count - 1 step 2
                        
                        currObject = cast(OpenGLObjectType ptr, currChunk->OldObjectList->Pop())
                        
                        currChunk->OldObjectList->Pop()
                        
                        this.ChunkDeleteList.Push(cast(uinteger, currObject))

                    next

                this.NodeDeletionEnterProtocol(ChunkNode)
                    
                    delete(currChunk->OldObjectList)
                    currChunk->OldObjectList = 0
                    
                    this.SendRepatchSignal(ChunkNode)
                 
                this.NodeDeletionExitProtocol(ChunkNode)

                'this.UnlockData()
                
                
                
            end if
            
        end if
        
    end if
    
    'Check for old render data in old (dummy) nodes
    if currChunk then
        
        if currChunk->DataStage = VoxelChunkType.TEMP_DATA then
            
            'We're at a chunk that has a dummy node with temporary render data
            
            if ChunkNode->HasChildren() = 0 then
                dprint("MAJOR PROBLEM OctreePostUpdate()")
                sleep
            end if
            
            'Check to see if all the children are ready for rendering
            
            for i = 0 to 7
                
                child = ChunkNode->GetChild(i)
                
                if child then
                    
                    retVal *= OctreePostUpdate(child)
                    
                end if
                
            next
            
            'If they were, delete this node
            if retVal = 1 then
                
                if currChunk->OldObjectList->Count < 2 then
                    dprint("ERROR, OLD OBJECT LIST DOESN'T CONTAIN ENOUGH ITEMS OctreePostUpdate()")
                    'sleep
                end if
                
                'this.LockData()
                
                
                for i = 0 to currChunk->OldObjectList->Count - 1 step 2
                        
                    currObject = cast(OpenGLObjectType ptr, currChunk->OldObjectList->Pop())
                    
                    currChunk->OldObjectList->Pop()
                    
                    this.ChunkDeleteList.Push(cast(uinteger, currObject))
    
                next

                this.NodeDeletionEnterProtocol(ChunkNode)
                
                    delete(currChunk->OldObjectList)
                    currChunk->OldObjectList = 0
                        
                    delete(currChunk)
                    
                    this.SendRepatchSignal(ChunkNode)
                 
                this.NodeDeletionExitProtocol(ChunkNode)

                'this.UnlockData()
                
                ChunkNode->SetDataItem(0)
                
                VoxelChunkTypeCount -= 1

            end if
            
            return 1
            
        elseif currChunk->DataStage = VoxelChunkType.DELETED OR _
               currChunk->DataStage = VoxelChunkType.RENDER_READY then
            
            'If we're here, we're guaranteed to be a decendent of a temp chunk
            
            'Return 1 if the chunk is finished
            return 1
        
        else
            
            'Same here, decendent of a temp chunk
            
            'Return 0 if it isn't
            return 0
            
        end if
        
    end if
    
    'If we're here, we're at a node with no chunk
    'Simply traverse through these nodes
    
    for i = 0 to 7
            
        child = ChunkNode->GetChild(i)
        
        if child then
            
            retVal *= OctreePostUpdate(child)
            
        end if
        
    next
      
    return retVal

end function

sub VoxelGridType.UpdateOctreeChunks(inNode as OctreeType ptr)
    
    dim as integer i,j
    
    dim currChunk as VoxelChunkType ptr
    
    dim child as OctreeType ptr
    
    if this.ThreadEndCondition then
        return
    end if
    
    if inNode->HasChildren() then
        
        for i = 0 to 7
            
            child = inNode->GetChild(i)
            
            if child then
                UpdateOctreeChunks(child)
            end if
            
        next
        
        'return
        
    end if

    currChunk = cast(VoxelChunkType ptr, inNode->GetDataItem())

    if currChunk then

        if currChunk->DataStage = VoxelChunkType.NO_DATA then
            
            'dprint("POPULATING OCTREE ";currChunk;" ... ";)
            
            currChunk->PopulateVoxels()
            
            'dprint("Done populating octree";currChunk)
            
        end if
        
        if currChunk->DataStage = VoxelChunkType.HAS_WEIGHTS then
            
            'dprint("Building octree ";currChunk->Octree->HasChildren();" ";inNode->HasChildren())
            currChunk->ExtractMesh_BuildOctree() 
            'dprint("Done building octree")
            
        end if

    end if
    
end sub

static sub VoxelGridType.UpdateVoxelChunks(VoxelGrid as VoxelGridType ptr)
    
    dprint("VoxelGridType.UpdateVoxelChunks started")
    
    dim i as integer
    
    dim ChunkChild as OctreeType ptr
    dim VoxelChild as OctreeType ptr
    
    while VoxelGrid->ThreadEndCondition = 0

        VoxelGrid->UpdateOctreeToCamera(VoxelGrid->ChunkOctree, VoxelGrid->VoxelOctree)
        
        VoxelGrid->UpdateOctreeChunks(VoxelGrid->ChunkOctree)
        
        VoxelGrid->OctreePostUpdate(VoxelGrid->ChunkOctree)

        sleep(1000.0 / 15.0)
        
    wend
    
    dprint("VoxelGridType.UpdateVoxelChunks: kill signal recieved")
    
end sub

sub VoxelGridType.UpdateOctreePatcher(inNode as OctreeType ptr)
    
    dim as integer i,j
    
    dim currChunk as VoxelChunkType ptr
    
    dim child as OctreeType ptr
    
    if this.ThreadEndCondition then
        return
    end if
    
    if inNode->HasChildren() then
        
        for i = 0 to 7
            
            child = inNode->GetChild(i)
            
            if child then
                UpdateOctreePatcher(child)
            end if
            
        next
        
        'return
        
    end if
    
    dim ContourNeighbor(5, 2) as ubyte = _
        {{0,0,1}, {0,1,0}, {0,1,1}, {1,0,0}, {1,0,1}, {1,1,0}}
    
    dim ContourChunkIndexMasks(5) as ubyte = _
        {&h55, &h33, &h11, &h0f, &h05, &h03}
    
    dim ChildList as LinkedListType
    dim ChildChunk as VoxelChunkType ptr
    dim node as NodeType ptr
    
    dim contourNeighbors as OctreeType ptr
    dim neighborChunk as VoxelChunkType ptr
    
    dim GenerateSeamFlag as byte
    
    dim performanceTimer as double
    
    currChunk = cast(VoxelChunkType ptr, inNode->GetDataItem())
    
    if currChunk then
        
        if currChunk->DataStage = VoxelChunkType.HAS_QEF_DATA then
            
            GenerateSeamFlag = 1

            for i = 0 to 5

                contourNeighbors = inNode->GetNeighbor(ContourNeighbor(i, 0), _
                                                       ContourNeighbor(i, 1), _
                                                       ContourNeighbor(i, 2))

                if contourNeighbors then
                        
                    if contourNeighbors->HasChildren() then
                        
                        'Case where the neighbor is a higher resolution
                        'chunk than the current chunk
                        
                        contourNeighbors->GetIndexedChildren(@ChildList, _
                                                             ContourChunkIndexMasks(i))
                        node = ChildList.GetHeadNode()
                        
                        'dprint("Has children: ";i)
                        
                        while node
                            ChildChunk = cast(OctreeType ptr, node->GetData())->GetDataItem()
                            
                            'dprint("Chunk ";ChildChunk;": ";)
                            
                            if ChildChunk = 0 then
                                'dprint("Jumping out due to null chunk")
                                ChildList.Empty()
                                GenerateSeamFlag = 0
                                exit for
                            end if
                            
                            if ChildChunk->DataStage >= VoxelChunkType.HAS_QEF_DATA OR _
                               ChildChunk->DataStage = VoxelChunkType.DELETED then
                                
                                'dprint("Seems fine")
                                
                            else
                                
                                'dprint("Jumping out due to undeveloped chunk")
                                
                                ChildList.Empty()
                                GenerateSeamFlag = 0
                                exit for
                                
                            end if
                            
                            node = node->GetNext()
                        wend
                        
                        ChildList.Empty()
                        
                    else
                        
                        'Case where the neighbor is equal or lower resolution
                        'than the current chunk
                        
                        neighborChunk = contourNeighbors->GetDataItem()
                        
                        if neighborChunk then

                            if neighborChunk->DataStage >= VoxelChunkType.HAS_QEF_DATA OR _
                               neighborChunk->DataStage = VoxelChunkType.DELETED then

                            else
                                
                                'dprint("  Main found undeveloped chunk")
                                
                                GenerateSeamFlag = 0
                                exit for
                                
                            end if
                            
                        else
                            
                            'dprint("   Main found neighbor seems missing")
                            
                            GenerateSeamFlag = 0
                            exit for
                            
                        end if
                        
                    end if
                    
                end if
                
            next
            
            if GenerateSeamFlag then
                'dprint("extracting chunk")
                currChunk->ExtractMesh_GenerateContour()
                'dprint("Done extracting chunk")
            end if
            
        end if
        
    end if
    
end sub

static sub VoxelGridType.PatchChunkSeams(VoxelGrid as VoxelGridType ptr)
    
    dprint("VoxelGridType.PatchChunkSeams started")

    dim i as integer
    
    dim ChunkChild as OctreeType ptr
    dim VoxelChild as OctreeType ptr
    
    while VoxelGrid->ThreadEndCondition = 0

        
        'MutexLock(VoxelGrid->WorkThreadMutex)
        VoxelGrid->ExtractMeshEnterProtocol(VoxelGrid->ChunkOctree)
            VoxelGrid->UpdateOctreePatcher(VoxelGrid->ChunkOctree)
        VoxelGrid->ExtractMeshExitProtocol(VoxelGrid->ChunkOctree)
        'MutexUnlock(VoxelGrid->WorkThreadMutex)

        sleep(1000.0 / 15.0)
        
    wend
    
    dprint("VoxelGridType.UpdateVoxelChunks: kill signal recieved")
    
end sub

sub VoxelGridType.SpawnThread()
    
    this.DataMutex = MutexCreate()
    this.WorkThreadMutex = MutexCreate()
    
    this.ThreadEndCondition = 0
    
    this.RenderFlag = 0
    this.ExtractMeshFlag = 0
    
    dprint("Creating VoxelGridType.UpdateVoxelChunks thread")
    this.UpdateThread = ThreadCreate(cast(any ptr, @UpdateVoxelChunks), @this)
    dprint("Creating VoxelGridType.PatchChunkSeams thread")
    this.SeamThread   = ThreadCreate(cast(any ptr, @PatchChunkSeams), @this)
    
end sub

sub VoxelGridType.KillThread()
    
    this.ThreadEndCondition = 1
    
end sub

sub VoxelGridType.LockData()
    
    'MutexLock(this.WorkThreadMutex)
    MutexLock(this.DataMutex)
    
end sub

sub VoxelGridType.UnlockData()
    
    MutexUnlock(this.DataMutex)
    'MutexUnlock(this.WorkThreadMutex)
    
end sub

sub VoxelGridType.RenderEnterProtocol(ChunkNode as OctreeType ptr)
    this.RenderFlag = 1
    MutexLock(this.DataMutex)
end sub

sub VoxelGridType.RenderExitProtocol(ChunkNode as OctreeType ptr)
    this.RenderFlag = 0
    MutexUnlock(this.DataMutex)
end sub

sub VoxelGridType.ExtractMeshEnterProtocol(ChunkNode as OctreeType ptr)
    this.ExtractMeshFlag = 1
    MutexLock(this.WorkThreadMutex)
end sub

sub VoxelGridType.ExtractMeshExitProtocol(ChunkNode as OctreeType ptr)
    MutexUnlock(this.WorkThreadMutex)
    this.ExtractMeshFlag = 0
end sub

sub VoxelGridType.NodeDeletionEnterProtocol(ChunkNode as OctreeType ptr)
    
    MutexLock(this.WorkThreadMutex)
    MutexLock(this.DataMutex)
    
    /'if ChunkNode->GetParent() then
        ChunkNode->GetParent()->SetChild(0, ChunkNode->GetIndex())
    end if
    
    while((this.RenderFlag = 1) OR (this.ExtractMeshFlag = 1))

    wend
    '/
    
end sub
sub VoxelGridType.NodeDeletionExitProtocol(ChunkNode as OctreeType ptr)
    
    MutexUnlock(this.DataMutex)
    MutexUnlock(this.WorkThreadMutex)
    
    /'if ChunkNode->GetParent() then
        ChunkNode->GetParent()->SetChild(ChunkNode, ChunkNode->GetIndex())
    end if
    '/
    
end sub

Destructor VoxelGridType()
    
    if this.UpdateThread <> 0 then
        
        dprint("Sending VoxelGridType.UpdateVoxelChunks kill signal")
        
        this.KillThread()
        
        if this.UpdateThread then
            ThreadWait(this.UpdateThread)
        end if
        if this.SeamThread then
            ThreadWait(this.SeamThread)
        end if
        
        if this.DataMutex then
            MutexDestroy(this.DataMutex)
        end if
        if this.WorkThreadMutex then
            MutexDestroy(this.WorkThreadMutex)
        end if
        
    end if
    
    dim as integer i,j
    
    dim inList as LinkedListType
    dim node as NodeType ptr
    
    dim currChunk as VoxelChunkType ptr
    
    if this.ShaderProgram then
        Delete(this.ShaderProgram)
    end if
    
    glDeleteTextures(1, @this.MaterialTex1)
    glDeleteTextures(1, @this.MaterialTex2)
    glDeleteTextures(1, @this.MaterialTex3)
    
    if this.ChunkOctree then
        
        this.ChunkOctree->GetAllData(@inList)
        
        node = inList.GetHeadNode()
        
        if node then
            for i = 0 to inList.GetItemCount() - 1
                
                currChunk = cast(VoxelChunkType ptr, node->GetData())
                
                delete(currChunk)

                node = node->GetNext()
                
            next
            
        end if
        
        inList.Empty()
        
        dprint("~VoxelGridType: DELETING THE CHUNK OCTREE")
        
        Delete(this.ChunkOctree)
        
        OctreeTypeCount -= 1
        
    end if
    
    'Deleting the chunks also deletes all data holding voxel nodes
    
    if this.VoxelOctree then
        dprint("~VoxelGridType: DELETING THE VOXEL OCTREE")
        Delete(this.VoxelOctree)
        
        OctreeTypeCount -= 1
        
    end if
    
    dprint("~VoxelGridType: DONE")

end Destructor

Sub VoxelChunkType.InitOpenCLBuffer()
    
    dim status as cl_int
    dim events as cl_event_t
    
    if this.VoxelBuffer = 0 then
        this.VoxelBuffer = new CachedBuffer3DSingle(this.Size, this.Size, this.Size)
        
        CachedBufferTypeCount += 1
        
        this.OpenCLVoxelBuffer = clCreateBuffer(OpenCL.CONTEXT, _
                                                CL_MEM_READ_WRITE OR CL_MEM_USE_HOST_PTR, _
                                                this.VoxelBuffer->sizeInBytes, _
                                                this.VoxelBuffer->BufferData, _
                                                @status)
    end if
    
end Sub

Sub VoxelChunkType.DeleteOpenCLBuffer()
    
    if this.OpenCLVoxelBuffer <> 0 then
        
        clReleaseMemObject(this.OpenCLVoxelBuffer)
        this.OpenCLVoxelBuffer = 0
        
        'clFlush(OpenCL.COMMAND_QUEUE)
        'clFinish(OpenCL.COMMAND_QUEUE)
        
    end if
    
    if this.VoxelBuffer then
        delete(this.VoxelBuffer)
        
        CachedBufferTypeCount -= 1
        
        this.VoxelBuffer = 0
    end if

end sub

sub VoxelChunkType.PopulateVoxels()
    
    dim events as cl_event_t
    
    if this.DataStage >= HAS_WEIGHTS then
        return
    end if
    
    if this.OpenCLVoxelBuffer = 0 then
        this.InitOpenCLBuffer()
    end if
    
    'Assumes length = width = height in voxel size
    runCLNoiseKernel(OpenCL, NoiseKernel, _
                     this.OpenCLVoxelBuffer, this.size, _
                     this.offset(), this.scale)
    
    clEnqueueReadBuffer(OpenCL.COMMAND_QUEUE, _ 'CL command queue
                        this.OpenCLVoxelBuffer, _ 'CL buffer
                        1, _ 'Blocking write
                        0, _ 'Offset
                        this.VoxelBuffer->sizeInBytes, _ 'Size of bytes
                        this.VoxelBuffer->BufferData, _ 'Pointer to the data
                        0, _ 'Events in wait list
                        0, _ 'Event_wait_list_ptr
                        @events) 'CL event ptr

    finishCLEvent(OpenCL, events)

    this.DataStage = HAS_WEIGHTS
    
end sub

sub VoxelChunkType.GetGradients(Verticies  as cl_mem_t, _
                                outNormals as cl_mem_t, _
                                VertexCount as integer)
    
    runCLGetGradients(OpenCL, GetGradientKernel, _
                      Verticies, outNormals, _
                      VertexCount, this.scale)
    
end sub

function VoxelChunkType.ExtractMesh_SimplifyOctree(inNode as OctreeType ptr, _
                                                     distanceThreshold as single, _
                                                     normalThreshold as single, _
                                                     manifoldCollapseLevel as ubyte) as integer
    
    'Course cube: The 3x3x3 cube of weights that are made up by
    '             the cube indicies of the 8 child cubes
    
    dim voxelNode as VoxelNodeType ptr
    
    /'if inNode = 0 then
        return 0
    end if
    '/
    'Check to see if this leaf node is ambiguous
    if inNode->HasChildren() = 0 then
        voxelNode = inNode->GetDataItem()
        return 1 - AmbiguousCubeIndex(voxelNode->CubeIndex)
    end if
    
    dim as integer i,j,x,y,z
    dim as integer xID, yID, zID
    dim child as OctreeType ptr

    dim Threshhold as Vector3DType = 0.0

    dim count as integer = 0
    
    dim CollapseCount as ubyte = 0
    
    dim indicies(7) as ubyte = {&h01,&h08,&h10,&h80,&h02,&h04,&h20,&h40}
    
    dim courseCubeIndex(2,2,2) as ubyte
    
    dim axis(2) as ubyte
    
    dim newIndex as ubyte = 0
    dim collapseIndex as ubyte = 0
    
    for i = 0 to 7
        
        axis(0) = (i AND 4) SHR 2
        axis(1) = (i AND 2) SHR 1
        axis(2) = (i AND 1) SHR 0
        
        child = inNode->GetChild(i)
        if child <> 0 then
            
            'Recursively collapse child nodes first
            CollapseCount += ExtractMesh_SimplifyOctree(child, _
                                                          distanceThreshold, _
                                                          normalThreshold, _
                                                          manifoldCollapseLevel)
            
            voxelNode = child->GetDataItem()

            if voxelNode <> 0 then
                
                'Construct the "index" which represents the organization of the
                'child nodes we're attempting to collapse
                collapseIndex = collapseIndex OR Indicies(i)
                
                courseCubeIndex(axis(0)  ,axis(1)  ,axis(2)  ) OR= voxelNode->CubeIndex AND 1
                courseCubeIndex(axis(0)+1,axis(1)  ,axis(2)  ) OR= voxelNode->CubeIndex AND 2
                courseCubeIndex(axis(0)+1,axis(1)  ,axis(2)+1) OR= voxelNode->CubeIndex AND 4
                courseCubeIndex(axis(0)  ,axis(1)  ,axis(2)+1) OR= voxelNode->CubeIndex AND 8
                courseCubeIndex(axis(0)  ,axis(1)+1,axis(2)  ) OR= voxelNode->CubeIndex AND 16
                courseCubeIndex(axis(0)+1,axis(1)+1,axis(2)  ) OR= voxelNode->CubeIndex AND 32
                courseCubeIndex(axis(0)+1,axis(1)+1,axis(2)+1) OR= voxelNode->CubeIndex AND 64
                courseCubeIndex(axis(0)  ,axis(1)+1,axis(2)+1) OR= voxelNode->CubeIndex AND 128
                
                'Calculate the new cube index of the course cube
                if newIndex = 0 then
                    'The first index we come across is always representative
                    'of the child nodes that don't exist
                    newIndex = voxelNode->CubeIndex

                    for x = 0 to 2
                     xID = (x + axis(0)) MOD 3
                     
                     for y = 0 to 2
                      yID = (y + axis(1)) MOD 3
                      
                      for z = 0 to 2
                        
                        if (x = 2) OR (y = 2) OR (z = 2) then
                            zID = (z + axis(2)) MOD 3
                            
                            if courseCubeIndex(xID, yID, zID) XOR &hff then
                            
                                courseCubeIndex(xID, yID, zID) = _
                                    courseCubeIndex(axis(0) - (axis(0)=0), _
                                                    axis(1) - (axis(1)=0), _
                                                    axis(2) - (axis(2)=0))
                            
                            end if
                            
                        end if
                        
                      next
                     next
                    next

                else

                    'Otherwise, each child claims it's index corner
                    newIndex = newIndex AND (NOT indicies(i))
                    newIndex = newIndex OR (voxelNode->CubeIndex AND Indicies(i))

                end if

            end if
            
        else
            
            'This is the corner associated with this child on the course cube index
            'The single time that a boolean operation evaluting to -1 instead of 1 is usefull
            courseCubeIndex(axis(0)-(axis(0)=1),axis(1)-(axis(1)=1),axis(2)-(axis(2)=1)) = &hff
            
            'Even if there are missing children, we can still collapse this one
            CollapseCount += 1
        end if
    next
    
    if CollapseCount <> 8 then
        'All children MUST be eligible for collapse
        return 0
    end if
    
    'Check to see if we should preserve the topology at this level
    if inNode->GetDepth() > manifoldCollapseLevel then
    
        'Since the course cube doesn't check diagonals, we check them with this
        if AmbiguousCubeIndex(newIndex) OR AmbiguousCubeIndex(collapseIndex) then
            return 0
        end if
        
        'Check to see if the collapse will remove a feature (modify the topology)
        if (newIndex = 0) OR (newIndex = 255) then
            return 0
        end if
        
        count = 0
        
        'Check to see if the resulting collapse will result in an ambiguous mesh
        for i = 0 to 2
         for j = 0 to 2
            
            'Check each course edge, if the midpoint doesn't agree with
            'one of the end points, we do not collapse this cell
            
            'Test the Z axis edges
            if (courseCubeIndex(i,j,0) <> &hff) AND (courseCubeIndex(i,j,2) <> &hff) then
             if (courseCubeIndex(i,j,0) > 0) = (courseCubeIndex(i,j,2) > 0) then
              if (courseCubeIndex(i,j,0) > 0) <> (courseCubeIndex(i,j,1) > 0) then
                return 0
              end if
             end if
            end if
            
            'Y axis
            if (courseCubeIndex(i,0,j) <> &hff) AND (courseCubeIndex(i,2,j) <> &hff) then
             if (courseCubeIndex(i,0,j) > 0) = (courseCubeIndex(i,2,j) > 0) then
              if (courseCubeIndex(i,0,j) > 0) <> (courseCubeIndex(i,1,j) > 0) then
                return 0
              end if
             end if
            end if
            
            'X axis
            if (courseCubeIndex(0,i,j) <> &hff) AND (courseCubeIndex(2,i,j) <> &hff) then
             if (courseCubeIndex(0,i,j) > 0) = (courseCubeIndex(2,i,j) > 0) then
              if (courseCubeIndex(0,i,j) > 0) <> (courseCubeIndex(1,i,j) > 0) then
                return 0
              end if
             end if
            end if
                
         next
        next
    
    end if

    dim myData as VoxelNodeType ptr

    dim verts(7) as single ptr
    dim norms(7) as single ptr
    dim normVec as Vector3DType

    dim maxDistance as single = (inNode->GetAABBMax()[0] - inNode->GetAABBMin()[0])
    maxDistance *= distanceThreshold + ((distanceThreshold * .01) * inNode->GetDepth())
    maxDistance *= maxDistance
    
    dim maxNormal as single = normalThreshold - ((normalThreshold * .005) * inNode->GetDepth())

    'At this point all children are eligable for collapse
    
    'If we made it here, we can safely collapse the children.
    'Also, each child is guaranteed to have a valid VoxelNode
    
    myData = new VoxelNodeType
    
    VoxelNodeTypeCount += 1
    
    myData->CubeIndex = 0
    
    'vert(0)=vert(1)=vert(2)=0
    'norm(0)=norm(1)=norm(2)=0

    for i = 0 to 7
        child = inNode->GetChild(i)
        if child then
            
            voxelNode = cast(voxelNodeType ptr, child->GetDataItem())

            myData->Vertex += voxelNode->Vertex
            
            verts(count) = @voxelNode->Vertex[0]
            
            myData->Normal += voxelNode->Normal
            
            norms(count) = @voxelNode->Normal[0]
            
            count += 1

        end if

    next

    myData->CubeIndex = newIndex
    
    'myData->Normal /= count
    'myData->Normal.Normalize()

    myData->Vertex /= count
    
    'Check to see if any vertex exceeds the max merge distance we allow
    'or if any normal differs from the average normal too much
    for i = 0 to count - 1
        
        if maxDistance > 0.0 then
            if (pow(myData->Vertex[0] - verts(i)[0], 2) + _
                pow(myData->Vertex[1] - verts(i)[1], 2) + _
                pow(myData->Vertex[2] - verts(i)[2], 2)) > maxDistance then
                
                delete(myData)
                
                VoxelNodeTypeCount -= 1
                
                return 0
                
            end if
        end if
        
        if maxNormal < 1.0 then
            normVec = norms(i)
            
            if myData->Normal.Dot(normVec) < maxNormal then
                
                delete(myData)
                
                VoxelNodeTypeCount -= 1
                
                return 0
                
            end if
        end if
        
    next
    
    'Delete the data attached to the children we're going to collapse
    for i = 0 to 7
        child = inNode->GetChild(i)
        if child then
            voxelNode = cast(voxelNodeType ptr, child->GetDataItem())
            delete(voxelNode)
            
            VoxelNodeTypeCount -= 1
            
        end if
    next
    
    myData->Normal.Normalize()
    
    myData->VertIndex = cast(ushort, -1)
    myData->NormIndex = cast(ushort, -1)

    inNode->CollapseChildren()
    inNode->SetData(myData)
    
    return 1
    
end function

Sub VoxelChunkType.ExtractMesh_BuildOctree()
    
    'Extract the mesh from the locally stored voxel buffer
    'using the Dual Contouring algorithm
    '(Lies, this is only surface nets... for now)

    if this.DataStage = DELETED then
        'It's already been created and deleted
        return
    end if
    
    dim as integer x,y,z,i,j
    
    /'
     ' Step 1: Sort the edges.
     '/
    
    'The buffer of each edge
    'Edges are stored as entire blocks:
    'All X edges (this.Size * this.Size * (this.Size-1))
    'All Y edges (same)
    'All Z edges (same)
    'Access goes from the bottom, left, near most vertex
    dim edgeScanArray as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount)
    
    dim UsedTime as double = 0.0
    dim profileTime as double = Timer
    
    /'runCLDCSortEdges(OpenCL, DCSortEdgesKernel, _
                     this.OpenCLVoxelBuffer, edgeScanArray, _
                     this.size, this.Scale)
    '/
    DCSortEdges(this.VoxelBuffer, edgeScanArray, this.Size)
    
    dim edgeIndex as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount * 4)
    'dim edgeIndex as DynamicBufferuLong = DynamicBufferuLong(this.DataPointCount)

    dim featuredEdgeCount as integer = 0
    
    'dim as long pushVal = 0
    
    'i = 0
    
    for x = 0 to this.Size - 1
     for y = 0 to this.Size - 1
      for z = 0 to this.Size - 1
    
        i = GetIndex3D(x,y,z,this.size)
        
        'If the edge features a sign change, add it to the array
        
        'pushVal = (0 SHL 24) OR (z SHL 16) OR (y SHL 8) OR (x)
        
        'X edge
        if edgeScanArray.BufferData[i] AND &h01 then
            
            'Ya know, these could technically be
            'set in one "Push()" call.  They're
            'only the size of a 32 bit int.
            'So, we could make one value and set it as:
            '(x SHL 24) OR (y SHL 16) OR (z SHL 8) OR (0/1/2)
            'And push that instead
            edgeIndex.FastPush(x)
            edgeIndex.FastPush(y)
            edgeIndex.FastPush(z)
            edgeIndex.FastPush(0)
            'pushVal = (x SHL 24) OR (y SHL 16) OR (z SHL 8) OR (0)

            'edgeIndex.FastPush(pushVal)
            
            featuredEdgeCount += 1
        end if
        
        'Y edge
        if edgeScanArray.BufferData[i] AND &h02 then
            edgeIndex.FastPush(x)
            edgeIndex.FastPush(y)
            edgeIndex.FastPush(z)
            edgeIndex.FastPush(1)
            
            'pushVal OR= (1 SHL 24)
            
            'edgeIndex.FastPush(pushVal)
            
            featuredEdgeCount += 1
        end if
        
        'Z edge
        if edgeScanArray.BufferData[i] AND &h04 then
            edgeIndex.FastPush(x)
            edgeIndex.FastPush(y)
            edgeIndex.FastPush(z)
            edgeIndex.FastPush(2)
            
            'pushVal AND= &h00ffffff
            'pushVal OR= (2 SHL 24)
            
            'edgeIndex.FastPush(pushVal)
        
            featuredEdgeCount += 1
        end if
        
        'i += 1
        
      next
     next
    next
    
    UsedTime = Timer - profileTime
    
    'dprint("")
    'dprint("Sort edges took:   ";Usedtime)
    
    if featuredEdgeCount = 0 then

        'No triangles to generate
        'Sample the voxel weights
        if this.DensityType = 0 then

            if this.VoxelBuffer->BufferData[0] > 0 then
                'Everything is air
                this.DensityType = -1
            else
                'Everything is solid
                this.DensityType = -2
            end if
            
            'We don't need this buffer anymore since
            'there's nothing worthwhile in it anyways

            delete(this.VoxelBuffer)
            
            CachedBufferTypeCount -= 1
            
            this.VoxelBuffer = 0

        else
            
            'If this isn't the initial generation pass,
            'then don't delete the weights buffer
            this.DataStage = IGNORE
        
        end if
        
        this.Octree->CollapseChildren()
        this.Octree->SetData(0)
        
        this.DataStage = DELETED
        this.RenderStage = NO_RENDER_DATA
        
        this.DeleteOpenCLBuffer()
        
        this.TriangleCount = 0
        this.FeaturedVoxelCount = 0
        
        'No point in going past here
        return
        
    end if
    
    
    'dprint("FEATURED ";@this)
    
    'This chunk has some kind of density
    this.DensityType = 1
    
    /'
     ' Step 2: get the hermite data
     '
     ' Calculate the hermite data only at the edges with features
     '
     '/
    
    dim clHermiteData as DynamicBufferSingle = DynamicBufferSingle(this.DataPointCount * 3 * 6)
    
    profileTime = timer
    
    runCLDCGetHermiteData(OpenCL, DCGetHermitData, _
                          this.OpenCLVoxelBuffer, _
                          edgeIndex, _
                          clHermiteData, _
                          featuredEdgeCount, _
                          this.size, this.offset(), this.scale)
    
    Usedtime = Timer - profileTime
    
    'dprint("Hermite data took: ";Usedtime)
    
    /'
     ' Step 3: sort the voxels
     '
     ' This is the same as the marching cubes implementation
     '
     '/
    
    'dprint("Allocating space")
    
    dim voxelScanArray as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount)

    'dprint("Done allocating space")

    'Sort the voxels and return their cube indicies in voxelScanArray.BufferData
    
    'dprint("Sorting voxels")
    
    profileTime = timer
    
    runCLMCSortVoxels(OpenCL, MCSortVoxelsKernel, _
                      this.OpenCLVoxelBuffer, voxelScanArray, _
                      this.size, this.offset())
    
    'MCSortVoxels(this.VoxelBuffer, voxelScanArray, this.size)

    'We're done with the OpenCL buffer and weight's
    'this.DeleteOpenCLBuffer()
    
    'This will hold our cube indicies for later passing to OpenCL
    dim compactVoxelArray as DynamicBufferuByte = DynamicBufferuByte(this.VoxelCount)
    
    'Create a buffer to store the index of the voxel
    dim voxelIndex as DynamicBufferuByte = DynamicBufferuByte(this.DataPointCount * 3)
    
    dim voxelsToCheck as integer = 0

    dim cubeIndex as uByte = 0
    dim indexOfCubeIndex as uinteger

    /'
     ' Step 4: Scan the sorted array
     ' 
     ' Check each cube index to see if it has a terrain feature.
     '
     '/
    
    i = 0
    
    'Scan the array to see how many voxels we're checking
    for x = 0 to this.Size - 2
     for y = 0 to this.Size - 2
      for z = 0 to this.Size - 2
    
        'Our indicies for our new arrays
        'indexOfCubeIndex = i'GetIndex3D(x,y,z,this.Size)

        'Get the cube index we retrieved from the sort function earlier
        cubeIndex = voxelScanArray.bufferData[i]
        
        i += 1
        
        'If the cube index would have 
        'triangles (NOT 0 or 255), we check this voxel.
        if cubeIndex > 0 and cubeIndex < 255 then                                        
            
            'Store the number of voxels we're checking
            voxelsToCheck += 1
        
            'Add the cube index to the total
            compactVoxelArray.FastPush(cubeIndex)
            
            'This voxel's location (bottom corner at x,y,z)
            'is used to determine exactly which voxels to update
            voxelIndex.FastPush(cast(ushort, x))
            voxelIndex.FastPush(cast(ushort, y))
            voxelIndex.FastPush(cast(ushort, z))

        end if
    
      next
      i += 1
     next
     i += this.Size
    next
    
    Usedtime = Timer - profileTime

    'dprint("Sort voxels took:  ";Usedtime)
    
    /'
     ' Step 5: compute the QEF
     '/
    
    'Octree stuff
    dim newNode as VoxelNodeType ptr
    
    dim BottomUpOctree as OctreeType ptr ptr
    
    dim OctreeNode as OctreeType ptr

    dim AABBMin as Vector3DType
    dim AABBMax as Vector3DType
    
    BottomUpOctree = new OctreeType ptr[this.DataPointCount]
    
    'Vertex stuff
    dim vertex as Vector3DType
    dim normal as Vector3DType
    
    dim vertPtr as single ptr
    dim normPtr as single ptr
    
    dim count as integer = 0
    
    'dim QEM as DualContourQuadricErrorMetric
    'dim planes(11) as Plane3
    
    dim HermiteOffset(11)  as byte = {0, 12, 0, 12, 0, 12, 0, 12, 6, 6, 6, 6}
    dim HermiteIndex(11,2) as byte = {{0,0,0}, {1,0,0}, {0,0,1}, {0,0,0}, {0,1,0}, {1,1,0}, _
                                      {0,1,1}, {0,1,0}, {0,0,0}, {1,0,0}, {1,0,1}, {0,0,1}}
    
    profileTime = timer

    'Calculate the mass point for each voxel cube
    for i = 0 to voxelsToCheck - 1
        
        'Create the data node
        newNode = new VoxelNodeType
        
        VoxelNodeTypeCount += 1
        
        newNode->Vertex = 0.0
        newNode->Normal = 0.0

        count = 0
        
        x = cast(integer, voxelIndex.BufferData[i * 3 + 0])
        y = cast(integer, voxelIndex.BufferData[i * 3 + 1])
        z = cast(integer, voxelIndex.BufferData[i * 3 + 2])
        
        'Go through each edge and check for a feature
        for j = 0 to 11
            
            if MarchingCubesEdgeTable(compactVoxelArray.BufferData[i]) AND (1 SHL j) then

                vertPtr = @clHermiteData.BufferData[GetIndex3D(x+HermiteIndex(j, 0), _
                                                               y+HermiteIndex(j, 1), _,
                                                               z+HermiteIndex(j, 2), _,
                                                               this.size) * 18 + HermiteOffset(j)]
                
                vertex = vertPtr
                
                newNode->Vertex += vertex
                
                'The normals follow immediatly after
                normPtr = (vertPtr + 3)
                
                normal = normPtr
                
                newNode->Normal += normal
                
                count += 1
                
            end if
            
        next

        'The "mass point" is just the average weights of the edge verticies
        '(Also, the count is guaranteed to be > 0)
        
        if count = 0 then
            dprint("!!!Divide by 0 in ExtractMesh_BuildOctree()!!!")
            assert(0)
        end if
        
        newNode->Vertex /= cast(single, count)
        newNode->Normal.Normalize()

        'dprint("Corner vert: ";CornerVert;" Vert: ";newNode->Vertex)
        
        '''Octree generation
        
        newNode->cubeIndex = compactVoxelArray.BufferData[i]

        newNode->vertIndex = cast(ushort, -1)
        newNode->normIndex = cast(ushort, -1)
        
        'The bounding box is the size of one voxel cube
        AABBMin[0] = this.offset(0) + (x * this.scale)
        AABBMin[1] = this.offset(1) + (y * this.scale)
        AABBMin[2] = this.offset(2) + (z * this.scale)
    
        AABBMax = AABBMin + this.Scale

        'Create the octree leaf node
        OctreeNode = new OctreeType(AABBMin, AABBMax, this.Octree->GetDepth() - 5, 1, @CompareVoxelToAABB)
        
        OctreeTypeCount += 1
        
        'Add the data to the octree grid for bottom up building
        OctreeNode->SetData(newNode)
        OctreeNode->SetGetIndexFunction(@GetAABBIndex)
        
        BottomUpOctree[GetIndex3D(x,y,z,this.Size)] = OctreeNode
        
    next
    
    usedTime = timer - ProfileTime
    
    'dprint("calculating took:  ";Usedtime)
    
    profileTime = timer
    
    'Build the octree from the bottom up
    OctreeNode = this.Octree->BuildBottomUp(BottomUpOctree, this.Size-1)
    
    usedTime = timer - ProfileTime
    
    'dprint("building took:     ";Usedtime)
    
    delete [] BottomUpOctree
    
    'The bottom up thing returns a new node,
    'so delete this one and replace it

    'delete(this.Octree)
    'this.Octree = OctreeNode
    
    'dprint("Simplifying ";this.Octree;" -> ";)
    
    '(in_node, Distance_threshold, normal_threshold, mainfold_collapse_level)
    
    profileTime = Timer
    
    'Distance_threshold:      Higher = More culling (0.0 to 1.0)
    'Normal_threshold:        Higher = Less culling (0.0 to 1.0)
    'Manifold_collapse_level: Higher = More culling (0 to Depth of Octree)
    
    'this.ExtractMesh_SimplifyOctree(OctreeNode, .525, .92, 1)
    'this.ExtractMesh_SimplifyOctree(OctreeNode, .725, .97, 0) '<-- Best
    this.ExtractMesh_SimplifyOctree(OctreeNode, .50 + ((this.Octree->GetDepth() - 5) * 0.005), _
                                                .96 - ((this.Octree->GetDepth() - 5) * 0.0025), _
                                                0)
    
    usedTime = Timer - profileTime
    
    'dprint("simplify took:     ";Usedtime)
    
    profileTime = Timer
    
    'this.ExtractMesh_SimplifyOctree(OctreeNode, 1.0, 0.0, -1)
    
    this.Octree->CollapseChildren()
    this.Octree->Copy(OctreeNode)
    
    OctreeNode->Children = 0
    
    delete(OctreeNode)
    
    OctreeTypeCount -= 1
    
    'dprint(" NEW ";this.Octree;" ";)

    this.DeleteOpenCLBuffer()
    
    this.FeaturedVoxelCount = voxelsToCheck

    this.DataStage = HAS_QEF_DATA
    
    usedTime = Timer - profileTime
    
    'dprint("cleanup took:     ";Usedtime)
    
end sub

Sub VoxelChunkType.ExtractMesh_ProcessEdge(Nodes() as OctreeType ptr, _
                                             edge as integer)

    'NodeA = this node
    'NodeB = X Neighbor
    'NodeC = Y Neighbor
    'NodeD = XY Neighbor
    
    if (Nodes(0) = 0) then
        return
    end if
    
    dim i as integer
    
    dim Children(3) as OctreeType ptr
    
    dim Neighbors(3) as OctreeType ptr

    dim ReturnFlag as ubyte = 0
    
    dim ChildNodes(2, 9) as ubyte = {{0,1,2,3,0,1,4,5,0,1}, _ 'X axis
                                     {0,1,2,3,0,2,4,6,0,2}, _ 'Y axis
                                     {0,2,4,6,0,1,4,5,0,4}}   'Z axis
    
    dim Offset(2, 5, 2) as byte = {{{-1,1,0}, {0,1,0}, {1,-1,0}, {1,0,0}, {0,-1,0}, {-1,0,0}}, _
                                   {{-1,0,1}, {0,0,1}, {1,0,-1}, {1,0,0}, {0,0,-1}, {-1,0,0}}, _
                                   {{0,1,-1}, {0,1,0}, {0,-1,1}, {0,0,1}, {0,-1,0}, {0,0,-1}}}
    
    if Nodes(1) <> 0 then
        
        if Nodes(1)->HasChildren() then
            
            'Loop over each face that pairs with node1
            for i = 0 to 3
                
                'Only get the relevant child nodes
                Children(i) = Nodes(1)->GetChild(ChildNodes(edge, i))
                
                if Children(i) then
                    
                    Neighbors(0) = Nodes(0)
                    Neighbors(1) = Children(i)
                    Neighbors(2) = Children(i)->GetNeighbor(Offset(edge, 0, 0), Offset(edge, 0, 1), Offset(edge, 0, 2))
                    Neighbors(3) = Children(i)->GetNeighbor(Offset(edge, 1, 0), Offset(edge, 1, 1), Offset(edge, 1, 2))
                    
                    ExtractMesh_ProcessEdge(Neighbors(), edge)
                    
                end if
            next
            
            ReturnFlag = 1

        end if
    
    end if
    
    if Nodes(2) <> 0 then
        if Nodes(2)->HasChildren() then

            for i = 0 to 3
                
                Children(i) = Nodes(2)->GetChild(ChildNodes(edge, i+4))
                
                if Children(i) then
                    
                    Neighbors(0) = Nodes(0)
                    Neighbors(1) = Children(i)->GetNeighbor(Offset(edge, 2, 0), Offset(edge, 2, 1), Offset(edge, 2, 2))
                    Neighbors(2) = Children(i)
                    Neighbors(3) = Children(i)->GetNeighbor(Offset(edge, 3, 0), Offset(edge, 3, 1), Offset(edge, 3, 2))
                    
                    ExtractMesh_ProcessEdge(Neighbors(), edge)
                    
                end if
            next
            
            ReturnFlag = 1

        end if
    
    end if
    
    if Nodes(3) <> 0 then
        if Nodes(3)->HasChildren() then

            for i = 0 to 1
                
                Children(i) = Nodes(3)->GetChild(ChildNodes(edge, i+8))
                
                if Children(i) then
                    
                    Neighbors(0) = Nodes(0)
                    Neighbors(1) = Children(i)->GetNeighbor(Offset(edge, 4, 0), Offset(edge, 4, 1), Offset(edge, 4, 2))
                    Neighbors(2) = Children(i)->GetNeighbor(Offset(edge, 5, 0), Offset(edge, 5, 1), Offset(edge, 5, 2))
                    Neighbors(3) = Children(i)
                    
                    ExtractMesh_ProcessEdge(Neighbors(), edge)
                    
                end if
            next

            ReturnFlag = 1

        end if
    end if

    if ReturnFlag then
        return
    end if

    'At this point, Nodes(0) through Nodes(3) are leaf nodes

    if Nodes(0) = 0 then
        dprint("FOUND THE PROBLEM 1")
        sleep
    end if
    
    dim NeighborNode as VoxelNodeType ptr
    
    dim MinimalOctreeNode as OctreeType ptr
    
    dim BackFacing as ubyte = 0

    dim CubeIndex(2, 3) as ubyte = {{32, 16, 2, 1} , _
                                    {64, 128, 32, 16}, _
                                    {64, 32, 4, 2}}

    NeighborNode = 0
    
    'Get the leaf node with the most resolution
    '(Greatest depth in the octree)
    
    'Start with the current node
    MinimalOctreeNode = Nodes(0)
    NeighborNode = cast(VoxelNodeType ptr, Nodes(0)->GetDataItem())
    
    if NeighborNode->CubeIndex AND CubeIndex(edge, 0) then
        BackFacing = 1
    end if

    for i = 0 to 3
        if Nodes(i) then
            if Nodes(i)->GetDataItem() = 0 then
                'dprint("    !!!JUMPING OUT FROM NULL DATA ITEM!!! ";i)
                'dprint("       Depth = ";Nodes(i)->GetDepth();" curr depth = ";Nodes(0)->GEtDepth())
                return
            end if
        end if
    next
    
    for i = 1 to 3
        
        if Nodes(i) then
            if MinimalOctreeNode->GetDepth() > Nodes(i)->GetDepth() then
                MinimalOctreeNode = Nodes(i)
                NeighborNode = cast(VoxelNodeType ptr, Nodes(i)->GetDataItem())
                
                if NeighborNode = 0 then
                    'return
                    dprint("FOUND PROBLEM 5 ";Nodes(i))
                    dprint("Has children: ";Nodes(i)->HasChildren())
                    dprint("Depth = ";Nodes(i)->GetDepth();", trace: ")
                    while MinimalOctreeNode
                        dprint(MinimalOctreeNode)
                        MinimalOctreeNode = MinimalOctreeNode->GetParent()
                    wend
                end if
                
                if (NeighborNode->CubeIndex AND CubeIndex(edge, i)) then
                    BackFacing = 1
                else
                    BackFacing = 0
                end if
            end if
        end if
        
    next
    
    if NeighborNode = 0 then
        dprint("FOUND PROBLEM 3")
    end if
    
    if MinimalOctreeNode = 0 then
        dprint("FOUND PROBLEM 4")
    end if
    
    dim EdgeFeature(2, 3) as ushort = {{32, 128, 2, 8}, _
                                       {1024, 2048, 512, 256}, _
                                       {64, 16, 4, 1}}
    
    'Check to see if the minimum node contains a feature
    for i = 0 to 3
        if MinimalOctreeNode = Nodes(i) then
            if (MarchingCubesEdgeTable(NeighborNode->cubeIndex) AND EdgeFeature(edge, i)) = 0 then
                'dprint("    !!!JUMPING OUT FROM LACK OF FEATURE!!! ";i)
                return
            end if
        end if
    next
    
    'Insert the vertex data into the arrays
    for i = 0 to 3
        if Nodes(i) then
            NeighborNode = cast(VoxelNodeType ptr, Nodes(i)->GetDataItem())
        else
            continue for
        end if
        
        if NeighborNode then
            'dprint("Made it to neighbor stuff")
            if NeighborNode->vertIndex = cast(ushort, -1) then

                NeighborNode->vertIndex = cast(ushort, this.QEFData->Push(NeighborNode->Vertex[0]) / 3) 'Returns the index to this vertex
                                                       this.QEFData->Push(NeighborNode->Vertex[1])
                                                       this.QEFData->Push(NeighborNode->Vertex[2])

                NeighborNode->normIndex = cast(ushort, this.QEFNorms->Push(NeighborNode->Normal[0]) / 3) 'Returns the index to this vertex
                                                       this.QEFNorms->Push(NeighborNode->Normal[1])
                                                       this.QEFNorms->Push(NeighborNode->Normal[2])
            
            end if
        else
            dprint("FOUND PROBLEM 2")
            sleep
        end if
    next
    
    dim vertPtr(3) as single ptr
    
    dim len1 as single = 0
    dim len2 as single = 0
    
    if (Nodes(1) <> 0) AND (Nodes(2) <> 0) AND (Nodes(3) <> 0) then
        
        'Triangulate along the shortest edge
        
        NeighborNode = Nodes(0)->GetDataItem() : vertPtr(0) = @QEFData->BufferData[NeighborNode->VertIndex*3]
        NeighborNode = Nodes(3)->GetDataItem() : vertPtr(3) = @QEFData->BufferData[NeighborNode->VertIndex*3]

        len1 = pow(*(vertPtr(0)+0) - *(vertPtr(3)+0), 2) + _
               pow(*(vertPtr(0)+1) - *(vertPtr(3)+1), 2) + _
               pow(*(vertPtr(0)+2) - *(vertPtr(3)+2), 2)
        
        NeighborNode = Nodes(1)->GetDataItem() : vertPtr(1) = @QEFData->BufferData[NeighborNode->VertIndex*3]
        NeighborNode = Nodes(2)->GetDataItem() : vertPtr(2) = @QEFData->BufferData[NeighborNode->VertIndex*3]

        len2 = pow(*(vertPtr(1)+0) - *(vertPtr(2)+0), 2) + _
               pow(*(vertPtr(1)+1) - *(vertPtr(2)+1), 2) + _
               pow(*(vertPtr(1)+2) - *(vertPtr(2)+2), 2)

    else
        
        if (Nodes(1) <> 0) AND (Nodes(3) <> 0) then
            len1 = 0 : len2 = 1
        else
            len1 = 1 : len2 = 0
        end if
    
    end if
    
    'Significant nodes (The order of triangulation)
    dim SigNodes(1,1,2) as ubyte = {{{0,1,2}, {1,3,2}}, _ 'Case where 0-3 < 1-2
                                    {{0,1,3}, {0,3,2}}}   'Case where 3-0 >= 1-2
    
    dim Triangulate as ubyte = 1

    if len1 >= len2 then
        Triangulate = 0
    end if
    
    for i = 0 to 1
        
        if (Nodes(SigNodes(Triangulate,i,0)) <> 0) AND _
           (Nodes(SigNodes(Triangulate,i,1)) <> 0) AND _
           (Nodes(SigNodes(Triangulate,i,2)) <> 0) then
            
            if (Nodes(SigNodes(Triangulate,i,1)) <> Nodes(SigNodes(Triangulate,i,2))) then
                
                if BackFacing = 0 then
                    NeighborNode = Nodes(SigNodes(Triangulate,i,0))->GetDataItem() : indexBuffer->Push(NeighborNode->vertIndex)
                    NeighborNode = Nodes(SigNodes(Triangulate,i,1))->GetDataItem() : indexBuffer->Push(NeighborNode->vertIndex)
                    NeighborNode = Nodes(SigNodes(Triangulate,i,2))->GetDataItem() : indexBuffer->Push(NeighborNode->vertIndex)
                else
                    NeighborNode = Nodes(SigNodes(Triangulate,i,1))->GetDataItem() : indexBuffer->Push(NeighborNode->vertIndex)
                    NeighborNode = Nodes(SigNodes(Triangulate,i,0))->GetDataItem() : indexBuffer->Push(NeighborNode->vertIndex)
                    NeighborNode = Nodes(SigNodes(Triangulate,i,2))->GetDataItem() : indexBuffer->Push(NeighborNode->vertIndex)
                end if

                this.TriangleCount += 1
            end if

        end if
   
    next
    
    'Reset the vertex data
    for i = 0 to 3
        if Nodes(i) then
            NeighborNode = Nodes(i)->GetDataItem()
        else
            continue for
        end if
        
        if NeighborNode then
            NeighborNode->vertIndex = cast(ushort, -1)
            NeighborNode->normIndex = cast(ushort, -1)
        end if
    next
    
end sub

Sub VoxelChunkType.ExtractMesh_ProcessCell(inNode as OctreeType ptr)
    
    dim i as integer
    
    dim OctreeChild as OctreeType ptr
    
    'This iterates over the entire octree
    if inNode->HasChildren() then

        for i = 0 to 7
            OctreeChild = inNode->GetChild(i)
            if OctreeChild then
                this.ExtractMesh_ProcessCell(OctreeChild)
            end if
        next
        
        return
        
    end if

    'At this point, we're at a leaf node.
    
    'Generate the contour on each axis of the voxel
    'X axis
    dim Neighbors(3) as OctreeType ptr

    Neighbors(0) = inNode
    Neighbors(1) = inNode->GetNeighbor(1,0,0)
    Neighbors(2) = inNode->GetNeighbor(0,1,0)
    Neighbors(3) = inNode->GetNeighbor(1,1,0)
    
    ExtractMesh_ProcessEdge(Neighbors(), 0)
    
    'Y axis
    Neighbors(1) = inNode->GetNeighbor(1,0,0)
    Neighbors(2) = inNode->GetNeighbor(0,0,1)
    Neighbors(3) = inNode->GetNeighbor(1,0,1)
    
    ExtractMesh_ProcessEdge(Neighbors(), 1)

    'Z axis
    Neighbors(1) = inNode->GetNeighbor(0,0,1)
    Neighbors(2) = inNode->GetNeighbor(0,1,0)
    Neighbors(3) = inNode->GetNeighbor(0,1,1)

    ExtractMesh_ProcessEdge(Neighbors(), 2)

end sub

Sub VoxelChunkType.ExtractMesh_GenerateContour()

    if this.QEFData then
        delete(this.QEFData)
        
        DynamicBufferTypeCount -= 1
        
    end if
    if this.QEFNorms then
        delete(this.QEFNorms)
        
        DynamicBufferTypeCount -= 1
        
    end if
    
    if this.IndexBuffer then
        delete(this.IndexBuffer)
        
        DynamicBufferTypeCount -= 1
        
    end if
    
    this.TriangleCount = 0
    
    'Initialize the vertex data buffers
    this.QEFData  = new DynamicBufferSingle(this.FeaturedVoxelCount * 4)
    this.QEFNorms = new DynamicBufferSingle(this.FeaturedVoxelCount * 4)
    
    this.indexBuffer = new DynamicBufferuShort(this.FeaturedVoxelCount*4*6)
    
    DynamicBufferTypeCount += 3
    
    'Generate the contour
    'dprint("  Processing cell")
    this.ExtractMesh_ProcessCell(this.Octree)
    'dprint("  Done processing cell")
    
    'Notify the main process that we have
    'to upload our data to the GPU
    
    if this.RenderStage = NO_RENDER_DATA then
        'Set the render triangle count when we've transitioned
        'from having no OpenGL buffer data (either this is the
        'first time updating the chunk, or we've come from a
        'LOD that had no features)
        this.RenderTriCount = this.TriangleCount
    end if

    this.DataStage = UPLOAD_TO_GPU_READY
    
end sub

Sub VoxelChunkType.DeleteGLBufferData()
    
    dim i as integer
    
    dim currObject as OpenGLObjectType ptr
    
    if this.GLObject then
        delete(this.GLObject)
        
        OpenGLObjectTypeCount -= 1
        
        this.GLObject = 0
    end if
    
    if this.OldObjectList then
        
        for i = 0 to (this.OldObjectList->Count / 2) - 1
                            
            currObject = cast(OpenGLObjectType ptr, this.OldObjectList->Pop())
            
            this.OldObjectList->Pop()
            
            delete(currObject)
            
            OpenglObjectTypeCount -= 1
    
        next
        
        delete(this.OldObjectList)
        this.OldObjectList = 0
        
    end if
    
end sub

Sub VoxelChunkType.UploadGLBufferData()
    
    if this.RenderStage = USE_NEW_DATA then
        dprint("Ya done goofed, son.")
        assert(0)
    end if
    
    'Delete the old data
    if this.RenderStage = USE_OLD_DATA then

        this.DeleteGLBufferData()

    end if

    this.GLObject = new OpenGLObjectType
    
    OpenGLObjectTypeCount += 1
    
    glGenVertexArrays(1, @this.GLObject->VAO)
    glBindVertexArray(this.GLObject->VAO)

    glGenBuffers(1, @this.GLObject->IndexIBO)
    glGenBuffers(1, @this.GLObject->VertexVBO)
    glGenBuffers(1, @this.GLObject->NormalVBO)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.GLObject->IndexIBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)*this.TriangleCount*3, @IndexBuffer->BufferData[0], GL_STATIC_DRAW)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)

    dim attributeLocation as GLuint = 0'glGetAttribLocation(this.shaderHandle, "in_position")
    glBindBuffer(GL_ARRAY_BUFFER, this.GLObject->VertexVBO)
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*QEFData->Count, @QEFData->BufferData[0], GL_STATIC_DRAW)
    glEnableVertexAttribArray(attributeLocation)
    glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, 0)
    
    attributeLocation = 1'glGetAttribLocation(this.shaderHandle, "in_normal")
    glBindBuffer(GL_ARRAY_BUFFER, this.GLObject->NormalVBO)
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*QEFNorms->Count, @QEFNorms->BufferData[0], GL_STATIC_DRAW)
    glEnableVertexAttribArray(attributeLocation)
    glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, 0)

    'This is completely useless after here...
    '...for now.  It might be used later for physics stuff.
    /'delete(this.IndexBuffer)
    this.IndexBuffer = 0
    
    delete(this.QEFData)
    this.QEFData = 0
    
    delete(this.QEFNorms)
    this.QEFNorms = 0
    
    DynamicBufferTypeCount -= 3
    '/
    
    this.DataStage = RENDER_READY
    this.RenderStage = USE_NEW_DATA
    
    this.RenderTriCount = this.TriangleCount
    
end sub

Sub VoxelChunkType.Render()
    
    dim i as integer
    dim currObject as OpenGLObjectType ptr
    dim currTriCount as uInteger
    
    'The chunk was found to be featureless
    if this.DataStage = DELETED then
        return
    end if
    
    if this.DataStage = UPLOAD_TO_GPU_READY then

        this.UploadGLBufferData()
        
    end if
    
    if this.RenderStage = USE_TEMP_RENDER_DATA then
        
        if this.OLDObjectList->Count = 2 then
            'dprint("RENDERING THE THING")
        end if
        
        if this.OldObjectList = 0 then
            dprint("NO OBJECT LIST IN VoxelChunkType.Render()!")
            sleep
        end if
        
        for i = 0 to (this.OldObjectList->Count / 2) - 1
            
            currTriCount = this.OldObjectList->BufferData[i * 2 + 0]
            currObject   = cast(OpenGLObjectType ptr, this.OldObjectList->BufferData[i * 2 + 1])

            glBindVertexArray(currObject->VAO)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, currObject->IndexIBO)
            glDrawElements(GL_TRIANGLES, currTriCount*3, GL_UNSIGNED_SHORT, 0)
            
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
            glBindVertexArray(0)
        
        next
        
        return
        
    end if
    
    if (this.DataStage = RENDER_READY) OR (this.RenderStage = USE_OLD_DATA) then

        assert(this.RenderTriCount > 0)
        
        glBindVertexArray(this.GLObject->VAO)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.GLObject->IndexIBO)
        glDrawElements(GL_TRIANGLES, this.RenderTriCount*3, GL_UNSIGNED_SHORT, 0)
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
        glBindVertexArray(0)
        
    end if
    
end sub

Sub VoxelChunkType.ClearOctreeData()
    
    if this.OpenCLVoxelBuffer <> 0 then
        this.DeleteOpenCLBuffer()
    end if
    
    if this.QEFData <> 0 then
        delete(this.QEFData)
        
        DynamicBufferTypeCount -= 1
        
        this.QEFData = 0
    end if
    
    if this.QEFNorms <> 0 then
        delete(this.QEFNorms)
        
        DynamicBufferTypeCount -= 1
        
        this.QEFNorms = 0
    end if
    
    if this.IndexBuffer <> 0 then
        delete(this.IndexBuffer)
        
        DynamicBufferTypeCount -= 1
        
        this.IndexBuffer = 0
    end if
    
    'Free OpenGL buffers    
    'this.DeleteGLBufferData()
    
    if this.VoxelBuffer <> 0 then
        Delete(this.VoxelBuffer)
        
        CachedBufferTypeCount -= 1
        
        this.VoxelBuffer = 0
    end if
    
    'Delete the data in the octree
    dim i as integer
    
    dim inList as LinkedListType
    dim node as NodeType ptr
    
    dim dataToRemove as VoxelNodeType ptr
    
    'dprint("Deleting octree node ";this.Octree; " ... ";)

    'Delete the octree itself
    if this.Octree then
        'Remove all leaf nodes
        
        this.Octree->GetAllData(@inList)
        
        node = inList.GetHeadNode()
        
        'dprint("Size = ";inList.GetItemCount();" has children: ";this.Octree->HasChildren())
        
        if node then
            for i = 0 to inList.GetItemCount() - 1

                dataToRemove = cast(VoxelNodeType ptr, node->GetData())
                
                delete(dataToRemove)
                
                VoxelNodeTypeCount -= 1
                
                
                node = node->GetNext()
                
            next
        end if
        
        'Delete the octree children, but leave this node

        this.Octree->CollapseChildren()
        
        inList.Empty()

    end if
    
    if this.DataStage = RENDER_READY then
        this.RenderStage = USE_OLD_DATA
    else
        this.DataStage = DELETED
    end if
    
end sub

Destructor VoxelChunkType()
    
    'dprint("~VoxelChunkType")
    
    'Free OpenCL buffers
    if this.OpenCLVoxelBuffer <> 0 then
        this.DeleteOpenCLBuffer()
    end if
    
    if this.QEFData <> 0 then
        delete(this.QEFData)
        
        DynamicBufferTypeCount -= 1
        
        this.QEFData = 0
    end if
    
    if this.QEFNorms <> 0 then
        delete(this.QEFNorms)
        
        DynamicBufferTypeCount -= 1
        
        this.QEFNorms = 0
    end if
    
    if this.IndexBuffer <> 0 then
        delete(this.IndexBuffer)
        
        DynamicBufferTypeCount -= 1
        
        this.IndexBuffer = 0
    end if
    
    'Free OpenGL buffers
    this.DeleteGLBufferData()
    
    if this.VoxelBuffer <> 0 then
        Delete(this.VoxelBuffer)
        
        CachedBufferTypeCount -= 1
        
        this.VoxelBuffer = 0
    else
        'dprint("something may not have deallocated properly ~VoxelChunkType.VoxelBuffer")
    end if
    
    'Delete the data in the octree
    dim i as integer
    
    dim inList as LinkedListType
    dim node as NodeType ptr
    
    dim dataToRemove as VoxelNodeType ptr
    
    'dprint("Deleting octree node ";this.Octree; " ... ";)

    'Delete the octree itself
    if this.Octree then
        'Remove all leaf nodes
        
        this.Octree->GetAllData(@inList)
        
        node = inList.GetHeadNode()
        
        'dprint("Size = ";inList.GetItemCount();" has children: ";this.Octree->HasChildren())
        
        if node then
            for i = 0 to inList.GetItemCount() - 1

                dataToRemove = cast(VoxelNodeType ptr, node->GetData())
                
                delete(dataToRemove)
                
                VoxelNodeTypeCount -= 1
                
                node = node->GetNext()
                
            next
        end if
        'Delete the octree recursively
        'delete(this.Octree)
        'this.Octree = 0
        
        /'if this.Octree->GetDataItem() then
            this.Octree->SetDataItem(0)
        end if
        '/
        this.Octree->CollapseChildren()
        
        inList.Empty()

    end if
    
    'dprint("Done ";this.Octree)
    
    'dprint("Exiting the destructor ";@this)
    
end Destructor

#endif