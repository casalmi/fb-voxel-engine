#ifndef VoxelInstanceFunctions_bas
#define VoxelInstanceFunctions_bas

#include once "../VoxelEngine.bi"

function CubeFunction(xPos as single, yPos as single, zPos as single) as single
    
    dim length2 as single = 1.5 * 1.5
    
    dim xD as single = (xPos * xPos) - length2
    dim yD as single = (yPos * yPos) - length2
    dim zD as single = (zPos * zPos) - length2
    
    return -((((xD > yD) AND (xD > zD)) AND 1) * xD + _
             (((yD >= xD) AND (yD > zD)) AND 1) * yD + _
             (((zD >= xD) AND (zD >= yD)) AND 1) * zD)
    
    /'if (xD > yD) AND (xD > zD) then
        return -xD
    elseif (yD >= xD) AND (yD > zD) then
        return -yD
    else
        return -zD
    end if
    '/
end function

function SphereFunction(xPos as single, yPos as single, zPos as single) as single
    
    dim xD as single = xPos * xPos
    dim yD as single = yPos * yPos
    dim zD as single = zPos * zPos
    
    return -(sqr(xD + yD + zD) - 1.0)
    
end function
    
#endif