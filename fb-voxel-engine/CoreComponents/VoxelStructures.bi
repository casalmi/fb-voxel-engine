#ifndef VoxelTypes_bi
#define VoxelTypes_bi

#include once "../VoxelEngine.bi"

type OctreeNodeType
    
    dim Vertex as Vector3DType
    
end type

type VoxelNodeType extends OctreeNodeType

    dim cubeIndex as ubyte
    
    dim Normal as Vector3DType
    
    dim vertIndex as ushort
    dim normIndex as ushort
    
end type

function CompareVoxelToAABB(ByRef inData as any ptr, _
                            AABBMin as Vector3DType, _
                            AABBMax as Vector3DType) as integer
    
    dim inNode as OctreeNodeType ptr = cast(OctreeNodeType ptr, inData)
    
    if (inNode->Vertex[0] >= AABBMin[0]) AND (inNode->Vertex[0] < AABBMax[0]) then
     if (inNode->Vertex[1] >= AABBMin[1]) AND (inNode->Vertex[1] < AABBMax[1]) then
      if (inNode->Vertex[2] >= AABBMin[2]) AND (inNode->Vertex[2] < AABBMax[2]) then

        return 1
        
      end if
     end if
    end if

    return 0
    
end function

function GetAABBIndex(ByRef inData as any ptr, _
                      AABBMin as Vector3DType, _
                      AABBMax as Vector3DType) as uinteger

    dim inNode as OctreeNodeType ptr = cast(OctreeNodeType ptr, inData)
    
    dim retIndex as uinteger
    
    dim Diff as Vector3DType
    
    Diff = AABBMax - AABBMin
    Diff = (inNode->Vertex - AABBMin) / Diff
    
    if (Diff[0] < 0) OR (Diff[1] < 0) OR (Diff[2] < 0) then
        return -1
    end if
    
    if (Diff[0] > 1) OR (Diff[1] > 1) OR (Diff[2] > 1) then
        return -1
    end if
    
    retIndex = retIndex OR (cast(uinteger, Diff.X()) SHL 2)
    retIndex = retIndex OR (cast(uinteger, Diff.Y()) SHL 1)
    retIndex = retIndex OR (cast(uinteger, Diff.Z()) SHL 0)
    
    return retIndex
    
end function

#endif