
#include once "GL/gl.bi"
#include once "GL/glu.bi"
#include once "GL/glext.bi"

const DEFERRED_FRAMEBUFFER_TEXTURE_COUNT as integer = 3

type DeferredShaderType
    
    public:
        
        enum TextureTypeEnum
            
            TEXTURE_TYPE_DIFFUSE  = 0
            TEXTURE_TYPE_NORMAL   = 1
            TEXTURE_TYPE_POSITION = 2
            
        end enum
        
        dim GeometryProgram as OpenGL_Shader_TYPE ptr
        
        dim ResultProgram as OpenGL_Shader_TYPE ptr
        
        declare Constructor(aWindowWidth as uinteger, aWindowHeight as uinteger)
        
        declare Destructor()
        
        declare sub BindForReading()
        declare sub BindForWriting()
        
        declare sub SetReadBuffer(TextureType as TextureTypeEnum)
        
        declare sub VisualizeBuffer(TextureType as TextureTypeEnum)
        
        declare sub RenderFullScreen()
        
    private:
    
        dim FBO as GLuint
        dim ColorTextures as GLuint ptr
        
        dim DepthTexture as GLuint
        
        dim WindowWidth as uinteger
        dim WindowHeight as uinteger        
        
        dim GLObject as OpenGLObjectType
        
end type

Constructor DeferredShaderType(aWindowWidth as uinteger, aWindowHeight as uinteger)

    dim i as integer
    
    this.WindowWidth  = aWindowWidth
    this.WindowHeight = aWindowHeight
    
    'Generate the frame buffer object
    glGenFramebuffers(1, @this.FBO)
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this.FBO)
    
    this.ColorTextures = new GLuint[DEFERRED_FRAMEBUFFER_TEXTURE_COUNT]
    
    'Generate the textures for each texture component
    glGenTextures(DEFERRED_FRAMEBUFFER_TEXTURE_COUNT, This.ColorTextures)
    
    'Bind the blank textures to their respective framebuffer
    for i = 0 to DEFERRED_FRAMEBUFFER_TEXTURE_COUNT - 1
        glBindTexture(GL_TEXTURE_2D, this.ColorTextures[i])
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, WindowWidth, WindowHeight, 0, GL_RGB, GL_FLOAT, 0)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, this.ColorTextures[i], 0)
    next
    
    'Generate and bind the depth buffer
    glGenTextures(1, @this.DepthTexture)
    
    glBindTexture(GL_TEXTURE_2D, this.DepthTexture)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, WindowWidth, WindowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0)
    
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, this.DepthTexture, 0)
    
    'Set the draw buffers
    dim DrawBuffers(DEFERRED_FRAMEBUFFER_TEXTURE_COUNT) as GLenum
    
    for i = 0 to DEFERRED_FRAMEBUFFER_TEXTURE_COUNT - 1
        DrawBuffers(i) = GL_COLOR_ATTACHMENT0 + i
    next
    
    glDrawBuffers(DEFERRED_FRAMEBUFFER_TEXTURE_COUNT, @DrawBuffers(0))

    'Check the final result
    dim status as GLenum
    
    status = glCheckFramebufferStatus(GL_FRAMEBUFFER)
    
    if status <> GL_FRAMEBUFFER_COMPLETE then
        print "Error creating framebuffer: ";status
        beep:sleep:end
    end if
    
    'Unbind our framebuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0)
    
    'Initialize the full screen quad for final rendering

    dim Verts(12) as GLfloat = {-1.0, -1.0, 0.0, _
                                 1.0, -1.0, 0.0, _
                                -1.0,  1.0, 0.0, _
                                 1.0,  1.0, 0.0}
    
    dim Indicies(5) as GLubyte = {0,1,3, 0,3,2}
    
    glGenVertexArrays(1, @this.GLObject.VAO)
    glBindVertexArray(this.GLObject.VAO)

    glGenBuffers(1, @this.GLObject.IndexIBO)
    glGenBuffers(1, @this.GLObject.VertexVBO)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.GLObject.IndexIBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte)*6, @Indicies(0), GL_STATIC_DRAW)

    
    glBindBuffer(GL_ARRAY_BUFFER, this.GLObject.VertexVBO)
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*12, @Verts(0), GL_STATIC_DRAW)
    glEnableVertexAttribArray(0)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0)

    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    glBindVertexArray(0)

end Constructor

sub DeferredShaderType.BindForReading()
    
    dim i as integer
    
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0)
    
    glClear(GL_COLOR_BUFFER_BIT OR GL_DEPTH_BUFFER_BIT)
    
    glBindFramebuffer(GL_READ_FRAMEBUFFER, this.FBO)
    
    glEnable(GL_TEXTURE_2D)
    
    for i = 0 to DEFERRED_FRAMEBUFFER_TEXTURE_COUNT - 1
        glActiveTexture(GL_TEXTURE0 + i)
        glBindTexture(GL_TEXTURE_2D, this.ColorTextures[i])
    next
    
end sub

sub DeferredShaderType.BindForWriting()
    
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this.FBO)
    
    glClear(GL_COLOR_BUFFER_BIT OR GL_DEPTH_BUFFER_BIT)
    
    glUseProgram(this.GeometryProgram->ShaderHandle)
    
end sub

sub DeferredShaderType.SetReadBuffer(TextureType as TextureTypeEnum)
    
    glReadBuffer(GL_COLOR_ATTACHMENT0 + TextureType)
    
end sub

sub DeferredShaderType.VisualizeBuffer(TextureType as TextureTypeEnum)
    
    this.BindForReading()
    
    this.SetReadBuffer(TextureType)
    glBlitFrameBuffer(0,0, this.WindowWidth, this.WindowHeight, _
                      0,0, this.WindowWidth, this.WindowHeight, _
                      GL_COLOR_BUFFER_BIT, GL_NEAREST)
    
end sub

sub DeferredShaderType.RenderFullScreen()
    
    dim i as integer
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    
    this.BindForReading()
    
    glUseProgram(this.ResultProgram->ShaderHandle)
    
    SetUniformInt(this.ResultProgram->ShaderHandle, "DiffuseSampler", 0)
    SetUniformInt(this.ResultProgram->ShaderHandle, "NormalSampler", 1)
    SetUniformInt(this.ResultProgram->ShaderHandle, "PositionSampler", 2)

    glBindVertexArray(this.GLObject.VAO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.GLObject.IndexIBO)
    
    glDrawElements(GL_TRIANGLES, 2*3, GL_UNSIGNED_BYTE, 0)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
    
    glUseProgram(0)
    
end sub

Destructor DeferredShaderType()

    dim i as integer
    
    glDeleteTextures(DEFERRED_FRAMEBUFFER_TEXTURE_COUNT, this.ColorTextures)
    delete [] this.ColorTextures
    this.ColorTextures = 0
    
    glDeleteTextures(1, @this.DepthTexture)
    this.DepthTexture = 0
    
    glDeleteFramebuffers(1, @this.FBO)
    this.FBO = 0
    
    if this.ResultProgram then
        delete(this.ResultProgram)
    end if
    
    if this.GeometryProgram then
        delete(this.GeometryProgram)
    end if
    
end destructor