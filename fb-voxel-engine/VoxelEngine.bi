#ifndef Voxel_Engine_bi
#define Voxel_Engine_bi

dim shared VoxelChunkTypeCount as integer = 0
dim shared VoxelNodeTypeCount as integer = 0
dim shared OctreeTypeCount as integer = 0
dim shared DynamicBufferTypeCount as integer = 0
dim shared CachedBufferTypeCount as integer = 0
dim shared OpenglObjectTypeCount as integer = 0
dim shared NodeTypeCount as integer = 0

' enable dprint()
#define USE_DPRINT

'--------------------GLOBAL DEFINES--------------------'

#define GLOBAL_GRID_ALIGNMENT_SCALE .25

#macro GetIndex3D(x,y,z,size)
    ((x) * (size) * (size) + (y) * (size) + (z))
#endmacro

'--------------------MISC--------------------'

#include once "fbgfx.bi"
#include once "crt.bi"

'--------------------OPENGL--------------------'

'''OPENGL SETUP UTILITIES

'#define GL_GLEXT_PROTOTYPES

#include once "GL/gl.bi"
#include once "GL/glu.bi"

#include once "Utilities/GLScreen.bi"
#include once "Utilities/OpenGLExtensions.bi"
#include once "Utilities/OpenGLTypes.bi"

'--------------------SHADERS--------------------'

'''MAIN SHADER HEADER
#include once "ShaderPack/Shaders.bi"

'--------------------UTILITIES--------------------'

'''OPENGL RELATED UTILITIES
#include once "Utilities/CreateTex.bi"
#include once "Utilities/Skybox.bi"

'''DATA STRUCTURES
#include once "Utilities/CachedBuffer.bi"
#include once "Utilities/DynamicBuffer.bi"
#include once "Utilities/Node.bi"
#include once "Utilities/LinkedList.bi"
#include once "Utilities/Octree.bi"
#include once "Utilities/BitArrayType.bi"

'''MATH AND RELATED UTILITIES
#include once "Utilities/Vector3D.bi"
#include once "Utilities/Vector2D.bi"
#include once "Utilities/FreeBasicVectorMath.bi"
#include once "Utilities/MatrixOperations.bi"
#include once "Utilities/NoiseHeader.bi"
#include once "Utilities/SimplexNoise.bi"
#include once "Utilities/Compression.bi"
#include once "Utilities/QuadraticErrorMetric.bi"

'''CPU MESH EXTRACTION IMPLEMENTATIONS
#include once "Utilities/MarchingCubes.bi"

'''MISC
#include once "Utilities/WaitTimer.bi"

'--------------------OPENCL--------------------'

'''MAIN OPENCL HEADER
#include once "OpenCL/fbopencl.bas"

'--------------------CORE ENGINE COMPONENTS--------------------'

'''CAMERA COMPONENTS
#include once "CoreComponents/CameraFunctions.bi"

'''RENDER ENGINE COMPONENTS
#include once "CoreComponents/DeferredShader.bas"

'''VOXEL STRUCTURES
#include once "CoreComponents/VoxelStructures.bi"

'''VOXEL INSTANCE COMPONENTS
#include once "CoreComponents/VoxelInstanceFunctions.bas"
#include once "CoreComponents/VoxelInstance3D.bas"

'''VOXEL GRID COMPONENTS
#include once "CoreComponents/VoxelGridOpenCLFunctions.bi"
#include once "CoreComponents/VoxelGrid.bas"

#endif