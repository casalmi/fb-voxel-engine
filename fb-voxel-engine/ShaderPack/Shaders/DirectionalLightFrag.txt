#version 330 core

//precision mediump float;
 
in vec3 out_normal;
flat in int out_WireFrame;
out vec4 fragColor;
 
void main()
{

	float diffuse = max(-1.0, dot(normalize(out_normal), normalize(vec3(1,.7,.8))));
	diffuse = (diffuse + 1) / 2.0;
	diffuse *= diffuse;

	if(out_WireFrame == 0){
		fragColor = vec4(vec3(1,1,.8) * (.25+diffuse),1);
	}else{
		fragColor = vec4(0,0,0,1);
	}
}