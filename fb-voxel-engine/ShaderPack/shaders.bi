#ifndef Shaders_bi
#define Shaders_bi

#include once "..\VoxelEngine.bi"

#define NL Chr(&h0A)

#ifdef USE_DPRINT
    'This way I can throw this around in whatever file
    #undef dprint
    #define dprint(msg) if DEBUG_MODE = 1 then open err for output as #99 : print  #99,msg : close #99 : endif:
#else
    #undef dprint
    #define dprint(msg) :
#endif

type OpenGL_Shader_TYPE
    
    dim ShaderHandle as GLuint
    
    declare sub DeleteShader()
    
    declare Constructor(VertexPath as string, FragmentPath as string, GeometryPath as string)
    declare Constructor(VertexPath as string, FragmentPath as string)
    declare Constructor()
    declare Destructor()
    
end type

'declare function NewShader(VertexPath as string, FragmentPath as string) as GLuint

'include ShaderPack/BloomShader.inc
'include ShaderPack/ShadowMaps.inc

'Some shaders to come preloaded

'dim PhongShader as GLuint = NewShader("ShaderPack/Shaders/PhongVert.txt", "ShaderPack/Shaders/PhongFrag.txt")
'dim BumpMapShader = NewShader("ShaderPack/Shaders/Vertex Bump Map.txt", "ShaderPack/Shaders/Fragment Bump Map.txt")

function LoadShader(ByRef hShader as GLuint,Path as zstring) as ubyte
    
    dim ErrorHandle as uinteger
    
    dim fHandle as integer
    fHandle = FreeFile
    
    if glIsShader(hShader) = GL_FALSE then
        return GL_FALSE
    end if
    
    Open Path For Input As #fHandle
    
    if ErrorHandle<>0 then        
        dprint("error: LoadShader() file not found!")
        return GL_FALSE
    end if
    
    dim Code as string
    dim Text as string
    while not EoF(fHandle)
        
        Line Input #fHandle, Text
        Code = Code & Text & NL
        
    wend
    
    Close #fHandle
    glShaderSource(hShader, 1, Cast(byte ptr ptr, @Code), 0)
    return GL_TRUE
end function

Constructor OpenGL_Shader_TYPE(VertexPath as string, FragmentPath as string)

    this.Constructor(VertexPath, FragmentPath, "")

end constructor

Constructor OpenGL_Shader_TYPE(VertexPath as string, FragmentPath as string, GeometryPath as string)

    dim as GLuint hProgram,hVertex,hFragment,hGeometry
    dim as byte FS = 1,VS = 1, GS = 1
    dim as long status, status2, status3
    dim as zstring ptr pInfo
    dim as GLsizei errorLength
    
    if FragmentPath = "" then
        FS = 0
    end if
    if VertexPath = "" then
        VS = 0
    end if
    if GeometryPath = "" then
        GS = 0
    end if
    
    ' create vertex shader object
    if VS then
        hVertex = glCreateShader(GL_VERTEX_SHADER)
        if glIsShader(hVertex)=GL_FALSE then
          dprint("error: CreateVertexShader !")
          sleep:end
        end if
    end if
    ' create fragment shader
    if FS then
        hFragment = glCreateShader(GL_FRAGMENT_SHADER)
        if glIsShader(hFragment)=GL_FALSE then
          dprint("error: CreateFragmentShader !")
          sleep:end
        end if
    end if
    
    if GS then
        hGeometry = glCreateShader(GL_GEOMETRY_SHADER)
        if glIsShader(hGeometry)=GL_FALSE then
          dprint("error: CreateGeometryShader !")
          sleep:end
        end if
    end if
    
    if VS then
        LoadShader(hVertex, VertexPath)
    endif 
    
    if FS then
        LoadShader(hFragment,FragmentPath)
    endif
    
    if GS then
        LoadShader(hGeometry, GeometryPath)
    end if
    
    ' compile vertex shader
    if VS then
        
        glCompileShader(hVertex)
        
        glGetShaderiv(hVertex, GL_COMPILE_STATUS, @status)
        if status=GL_FALSE then
            glGetShaderiv(hVertex, GL_INFO_LOG_LENGTH, @status)
            pInfo = callocate(status + 1)
            glGetShaderInfoLog(hVertex, status, 0, pInfo)
            dprint("Shader " & VertexPath)
            dprint("compile error: vertex shader")
            dprint("Shader error: " & *pInfo)
            deallocate(pInfo)
            sleep:end
        end if

    end if
    
    'compile frag shader
    if FS then

        glCompileShader(hFragment)
        
        glGetShaderiv(hFragment, GL_COMPILE_STATUS, @status2)
        if status2=GL_FALSE then
            glGetShaderiv(hFragment, GL_INFO_LOG_LENGTH, @status2)
            pInfo = callocate(status2 + 1)
            glGetShaderInfoLog(hFragment, status2, 0, pInfo)
            dprint("Shader " & FragmentPath)
            dprint("compile error: fragment shader")
            dprint("Shader error: " & *pInfo)
            deallocate(pInfo)
            sleep:end
        end if

    end if
    
    'compile geometry shader
    if GS then

        glCompileShader(hGeometry)
        
        glGetShaderiv(hGeometry, GL_COMPILE_STATUS, @status3)
        if status3=GL_FALSE then
            glGetShaderiv(hGeometry, GL_INFO_LOG_LENGTH, @status3)
            pInfo = callocate(status3 + 1)
            glGetShaderInfoLog(hGeometry, status3, 0, pInfo)
            dprint("Shader " & GeometryPath)
            dprint("compile error: geometry shader")
            dprint("Shader error: " & *pInfo)
            deallocate(pInfo)
            sleep:end
        end if

    end if
    
    ' create program object
    hProgram = glCreateProgram()
    
    if glIsProgram(hProgram)=GL_FALSE then
        dprint("error: glCreateProgram !")
        end
    end if
    ' add vertex shader to program
    if VS then
        glAttachShader(hProgram, hVertex)
    end if
   
    ' add frag shader to program
    if FS then
        glAttachShader(hProgram, hFragment)
    end if
    
    if GS then
        glAttachShader(hProgram, hGeometry)
    end if
    ' link program
    glLinkProgram(hProgram)

    glGetProgramiv(hProgram,GL_LINK_STATUS,@status)
    if status=GL_FALSE then
        if GS then
            dprint("link error in shaders " & VertexPath & " " & FragmentPath & " " & GeometryPath & " :")
        else
            dprint("link error in shaders " & VertexPath & " " & FragmentPath & " :")
        end if
        
        glGetShaderInfoLog(hProgram, 1024, @errorLength, pInfo)
        dprint(pInfo)
        sleep:end
    end if
    
    ' free shaders
    if VS then
        glDeleteShader(hVertex)
    end if 

    if FS then
        glDeleteShader(hFragment) 
    end if
    
    if GS then
        glDeleteShader(hGeometry)
    end if
    
    'Attach an empty program
    glUseProgram(0)
    
    this.ShaderHandle = hProgram

end Constructor

Constructor OpenGL_Shader_TYPE()

    this.ShaderHandle = 0

end Constructor

sub OpenGL_Shader_TYPE.DeleteShader()
    
    glUseProgram(0)
    glDeleteProgram(this.ShaderHandle)
    
end sub

Destructor OpenGL_Shader_TYPE()

    glUseProgram(0)
    glDeleteProgram(this.ShaderHandle)

end Destructor

sub SetUniformInt(Shader as GLuint, ByVal VarName as string, Value as integer)
    
    dim Location as GLuint = glGetUniformLocation(Shader, @VarName[0])
    glUniform1i(Location, Value)

end sub

sub SetUniformVector3(Shader as GLuint, ByVal VarName as string, Value() as single)
    
    dim Location as GLuint = glGetUniformLocation(Shader, @VarName[0])
    glUniform3fv(Location, 1, @Value(0))
    
end sub

sub SetUniformVector4(Shader as GLuint, ByVal VarName as string, Value() as single)
    
    dim Location as GLuint = glGetUniformLocation(Shader, @VarName[0])
    glUniform4fv(Location, 1, @Value(0))
    
end sub

sub SetUniformMatrix4(Shader as GLuint, ByVal VarName as string, Value() as single)

    dim Location as GLuint = glGetUniformLocation(Shader, @VarName[0])
    glUniformMatrix4fv(Location,  1, 0, @Value(0))

end sub

sub SetUniformFloat(Shader as GLuint, ByVal VarName as string, Value as single)
    
    dim Location as GLuint = glGetUniformLocation(Shader, @VarName[0])
    glUniform1f(Location, Value)

end sub

#endif