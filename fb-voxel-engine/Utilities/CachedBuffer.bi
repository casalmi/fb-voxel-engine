#ifndef CachedBuffers_bi
#define CashedBuffers_bi

'Spellcheck (lol)
#define CashedBuffer2D CachedBuffer2D
#define CashedBuffer3D CachedBuffer3D

#define Index2D(x, y, ySize) ((x) * (ySize) + (y))
#define Index3D(x, y, z, ySize, zSize) ((x) * (ySize) * (zSize) + (y) * (zSize) + (z))

#macro CREATE_CACHED_BUFFER_DATA_TYPE(THIS_DATA_TYPE)

type CachedBuffer2D##THIS_DATA_TYPE##

    dim bufferData as ##THIS_DATA_TYPE## ptr

    dim sizeInBytes as integer

    dim xLength as integer        
    dim yLength as integer        
    
    dim maxIndexX as integer
    dim maxIndexY as integer
    
    declare function getData(x as integer, y as integer) as ##THIS_DATA_TYPE##
    declare sub      setData(x as integer, y as integer, item as ##THIS_DATA_TYPE##)
    
    declare function getIndex(x as integer, y as integer) as integer
    
    Declare Constructor(xSize as integer, ySize as integer)
    Declare Destructor()
    
end type

type CachedBuffer3D##THIS_DATA_TYPE##
    
    dim bufferData as ##THIS_DATA_TYPE## ptr

    dim sizeInBytes as integer

    dim xLength as integer
    dim yLength as integer
    dim zLength as integer
    
    dim maxIndexX as integer
    dim maxIndexY as integer
    dim maxIndexZ as integer
    
    declare function getData(x as integer, y as integer, z as integer) as ##THIS_DATA_TYPE##
    declare sub      setData(x as integer, y as integer, z as integer, item as ##THIS_DATA_TYPE##)
    
    declare function getIndex(x as integer, y as integer, z as integer) as integer
    
    Declare Constructor(xSize as integer, ySize as integer, zSize as integer)
    Declare Destructor()
    
end type

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''2D Buffer''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Constructor CachedBuffer2D##THIS_DATA_TYPE##(xSize as integer, ySize as integer)

    this.bufferData = New ##THIS_DATA_TYPE##[xSize * ySize]
    
    this.sizeInBytes = sizeof(##THIS_DATA_TYPE##) * xSize * ySize
    
    this.xLength = xSize
    this.yLength = ySize
    
    this.maxIndexX = xSize - 1
    this.maxIndexY = ySize - 1

end Constructor

sub CachedBuffer2D##THIS_DATA_TYPE##.setData(x as integer, y as integer, item as ##THIS_DATA_TYPE##)
    
    if x < 0 OR x > this.maxIndexX then return: end if
    if y < 0 OR y > this.maxIndexY then return: end if
    
    this.bufferData[x * this.yLength + y] = item
    
end sub

function CachedBuffer2D##THIS_DATA_TYPE##.getData(x as integer, y as integer) as ##THIS_DATA_TYPE##
    
    if x < 0 OR x > this.maxIndexX then return 0: end if
    if y < 0 OR y > this.maxIndexY then return 0: end if

    return this.bufferData[x * this.yLength + y]
    
end function

function CachedBuffer2D##THIS_DATA_TYPE##.getIndex(x as integer, y as integer) as integer

    return x * this.yLength + y
    
end function

Destructor CachedBuffer2D##THIS_DATA_TYPE##()

    Delete [] this.bufferData
    
end Destructor

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''3D Buffer''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Constructor CachedBuffer3D##THIS_DATA_TYPE##(xSize as integer, ySize as integer, zSize as integer)

    this.bufferData = New ##THIS_DATA_TYPE##[xSize * ySize * zSize]
    
    this.sizeInBytes = sizeof(##THIS_DATA_TYPE##) * xSize * ySize * zSize
    
    this.xLength = xSize
    this.yLength = ySize
    this.zLength = zSize
    
    this.maxIndexX = xSize - 1
    this.maxIndexY = ySize - 1
    this.maxIndexZ = zSize - 1

end Constructor

sub CachedBuffer3D##THIS_DATA_TYPE##.setData(x as integer, y as integer, z as integer, item as ##THIS_DATA_TYPE##)
    
    if x < 0 OR x > this.maxIndexX then return: end if
    if y < 0 OR y > this.maxIndexY then return: end if
    if z < 0 OR z > this.maxIndexZ then return: end if
    
    this.bufferData[x * this.yLength*this.zLength + y * this.zLength + z] = item
    
end sub

function CachedBuffer3D##THIS_DATA_TYPE##.getData(x as integer, y as integer, z as integer) as ##THIS_DATA_TYPE##
    
    if x < 0 OR x > this.maxIndexX then return 0: end if
    if y < 0 OR y > this.maxIndexY then return 0: end if
    if z < 0 OR z > this.maxIndexZ then return 0: end if
    
    return this.bufferData[x * this.yLength*this.zLength + y * this.zLength + z]
    
end function

function CachedBuffer3D##THIS_DATA_TYPE##.getIndex(x as integer, y as integer, z as integer) as integer

    return x * this.yLength*this.zLength + y * this.zLength + z
    
end function

Destructor CachedBuffer3D##THIS_DATA_TYPE##()
    
    Delete [] this.bufferData
    
end Destructor

#endmacro

CREATE_CACHED_BUFFER_DATA_TYPE(Byte)
CREATE_CACHED_BUFFER_DATA_TYPE(uByte)
CREATE_CACHED_BUFFER_DATA_TYPE(Short)
CREATE_CACHED_BUFFER_DATA_TYPE(uShort)
CREATE_CACHED_BUFFER_DATA_TYPE(Integer)
CREATE_CACHED_BUFFER_DATA_TYPE(Single)
CREATE_CACHED_BUFFER_DATA_TYPE(Double)

#endif