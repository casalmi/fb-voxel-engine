'Stripped mostly from DJLinux's "glScreen.bi"
#ifndef OpenGLExtensions_bi
#define OpenGLExtensions_bi

#include once "GL/gl.bi"
#include once "GL/glext.bi"

#ifndef glDef
    #define glDef(n) dim shared as PFN##n##PROC n
#endif

#ifndef glProc
    #define glProc(n) n = ScreenGLProc(#n) : if n = 0 then screen 0 : ? "error: " & #n : sleep: end 
#endif

#ifndef GL_DEFINE_EXTENSIONS
#define GL_DEFINE_EXTENSIONS

    ' VBO
    glDef(glBindBuffer)
    glDef(glDeleteBuffers)
    glDef(glGenBuffers)
    glDef(glIsBuffer)
    glDef(glBufferData)
    glDef(glBufferSubData)
    glDef(glGetBufferSubData)
    glDef(glMapBuffer)
    glDef(glUnmapBuffer)
    glDef(glGetBufferParameteriv)
    glDef(glGetBufferPointerv)
    
    ' VAO
    glDef(glGenVertexArrays)
    glDef(glDeleteVertexArrays)
    glDef(glBindVertexArray)
    
    'FBO
    glDef(glGenFramebuffers)
    glDef(glBindFramebuffer)
    glDef(glFramebufferTexture2D)
    glDef(glDeleteFramebuffers)
    glDef(glActiveTexture)
    glDef(glDrawBuffers)
    glDef(glCheckFramebufferStatus)
    glDef(glBlitFramebuffer)
    glDef(glFramebufferTexture)

    ' Shader
    glDef(glCreateProgram)
    glDef(glDeleteProgram)
    glDef(glCreateShader)
    glDef(glDeleteShader)
    glDef(glCompileShader)
    glDef(glIsProgram)
    glDef(glIsShader)
    glDef(glShaderSource)
    glDef(glGetShaderInfoLog)
    glDef(glGetShaderSource)
    glDef(glGetShaderiv)
    glDef(glAttachShader)
    glDef(glDetachShader)
    glDef(glLinkProgram)
    glDef(glUseProgram)
    glDef(glGetProgramiv)
    glDef(glGetUniformLocation)
    glDef(glGetAttribLocation)
    glDef(glEnableVertexAttribArray)
    glDef(glVertexAttribPointer)
    glDef(glBindSampler)
    
    glDef(glUniform1f)
    glDef(glUniform2f)
    glDef(glUniform3f)
    glDef(glUniform4f)
    glDef(glUniform1i)
    glDef(glUniform2i)
    glDef(glUniform3i)
    glDef(glUniform4i)
    glDef(glUniform1fv)
    glDef(glUniform2fv)
    glDef(glUniform3fv)
    glDef(glUniform4fv)
    glDef(glUniform1iv)
    glDef(glUniform2iv)
    glDef(glUniform3iv)
    glDef(glUniform4iv)
    glDef(glUniformMatrix2fv)
    glDef(glUniformMatrix3fv)
    glDef(glUniformMatrix4fv)

#endif


#macro InitOpenGLExt()
        
    'Include extra extensions here
    'glDef():glProc()
    ' VBO
    glProc(glBindBuffer)
    glProc(glDeleteBuffers)
    glProc(glGenBuffers)
    glProc(glIsBuffer)
    glProc(glBufferData)
    glProc(glBufferSubData)
    glProc(glGetBufferSubData)
    glProc(glMapBuffer)
    glProc(glUnmapBuffer)
    glProc(glGetBufferParameteriv)
    glProc(glGetBufferPointerv)
    
    ' VAO
    glProc(glGenVertexArrays)
    glProc(glDeleteVertexArrays)
    glProc(glBindVertexArray)
    
    'FBO
    glProc(glGenFramebuffers)
    glProc(glBindFramebuffer)
    glProc(glFramebufferTexture2D)
    glProc(glDeleteFramebuffers)
    glProc(glActiveTexture)
    glProc(glDrawBuffers)
    glProc(glCheckFramebufferStatus)
    glProc(glBlitFramebuffer)
    glProc(glFramebufferTexture)
    
    ' Shader
    glProc(glCreateProgram)
    glProc(glDeleteProgram)
    glProc(glCreateShader)
    glProc(glDeleteShader)
    glProc(glCompileShader)
    glProc(glIsProgram)
    glProc(glIsShader)
    glProc(glShaderSource)
    glProc(glGetShaderInfoLog)
    glProc(glGetShaderSource)
    glProc(glGetShaderiv)
    glProc(glAttachShader)
    glProc(glDetachShader)
    glProc(glLinkProgram)
    glProc(glUseProgram)
    glProc(glGetProgramiv)
    glProc(glGetUniformLocation)
    glProc(glGetAttribLocation)
    glProc(glEnableVertexAttribArray)
    glProc(glVertexAttribPointer)
    glProc(glBindSampler)
      
    glProc(glUniform1f)
    glProc(glUniform2f)
    glProc(glUniform3f)
    glProc(glUniform4f)
    glProc(glUniform1i)
    glProc(glUniform2i)
    glProc(glUniform3i)
    glProc(glUniform4i)
    glProc(glUniform1fv)
    glProc(glUniform2fv)
    glProc(glUniform3fv)
    glProc(glUniform4fv)
    glProc(glUniform1iv)
    glProc(glUniform2iv)
    glProc(glUniform3iv)
    glProc(glUniform4iv)
    glProc(glUniformMatrix2fv)
    glProc(glUniformMatrix3fv)
    glProc(glUniformMatrix4fv)
    
#endmacro

sub CheckGLError(CodeLine as integer)
    
    dim glError as GLenum = glGetError()
    
    if glError <> GL_NO_ERROR then
        dprint("GLError: ";glError;" at line:";CodeLine)
    end if
    
end sub

#endif