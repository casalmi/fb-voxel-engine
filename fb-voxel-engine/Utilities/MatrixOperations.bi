#ifndef Matrix_Operations_bi
#define Matrix_Operations_bi

#include once "FreeBasicVectorMath.bi"

/'
 ' Matrix operations came from:
 ' http://www.lighthouse3d.com/cg-topics/code-samples/opengl-3-3-glsl-1-5-sample/
 '
'/

'res = a cross b
sub crossProduct(res() as single, a() as single, b() as single)
 
    res(0) = a(1) * b(2)  -  b(1) * a(2)
    res(1) = a(2) * b(0)  -  b(2) * a(0)
    res(2) = a(0) * b(1)  -  b(0) * a(1)

end sub

' sets the square matrix mat to the identity matrix,
' size refers to the number of rows (or columns)
sub setIdentityMatrix(mat() as single, size as integer) 
 
    dim i as integer
    
    ' fill matrix with 0s
    for i = 0 to size * size - 1
        mat(i) = 0.0
    next

    for i = 0 to size - 1
        mat(i + i * size) = 1.0
    next

end sub

'a = a * b
sub multMatrix(a() as single, b() as single)
 
    dim res(15) as single
    
    dim as integer i,j,k
    
    for i = 0 to 3
        for j = 0 to 3
            
            res(j*4 + i) = 0.0
            
            for k = 0 to 3
                res(j*4 + i) += a(k*4 + i) * b(j*4 + k)
            next
        next
    next
    
    for i = 0 to 15
        a(i) = res(i)
    next
 
end sub

'Defines a transformation matrix mat with a translation
sub setTranslationMatrix(mat() as single, position as Vector3DType)
 
    setIdentityMatrix(mat(), 4)
    mat(12) = position[0]
    mat(13) = position[1]
    mat(14) = position[2]
    
end sub

'-------------------------PROJECTION MATRIX-------------------------

'Pass the projMatrix to be built
sub buildProjectionMatrix(projMatrix() as single,_
                          fov as single, ratio as single,_
                          nearP as single, farP as single)
 
    dim f as single = 1.0 / tan (fov * (3.14159265358979 / 360.0))
 
    setIdentityMatrix(projMatrix(), 4)
 
    projMatrix(0) = f / ratio
    projMatrix(1 * 4 + 1) = f
    projMatrix(2 * 4 + 2) = (farP + nearP) / (nearP - farP)
    projMatrix(3 * 4 + 2) = (2.0 * farP * nearP) / (nearP - farP)
    projMatrix(2 * 4 + 3) = -1.0
    projMatrix(3 * 4 + 3) =  0.0

end sub

'----------------------------VIEW MATRIX----------------------------

'functionally equivilant to gluLookat, assumes an un-tilted camara (up vec = (0,1,0))
'Pass the viewMatrix to be built
sub setCamera(viewMatrix() as single,_
              position as Vector3DType,_
              lookat as Vector3DType)

    dim tempViewMatrix(15) as single

    dim direction as Vector3DType
    dim rightVec as Vector3DType
    dim up as Vector3DType
 
    up = Vector3DType(0.0,1.0,0.0)
    
    direction = lookat - position
    direction.Normalize()
    
    rightVec = direction.CrossProduct(up)
    rightVec.Normalize()
    
    up = rightVec.CrossProduct(direction)
    up.Normalize()

    dim aux(16) as single
 
    viewMatrix(0)  = rightVec[0]
    viewMatrix(4)  = rightVec[1]
    viewMatrix(8)  = rightVec[2]
    viewMatrix(12) = 0.0
 
    viewMatrix(1)  = up[0]
    viewMatrix(5)  = up[1]
    viewMatrix(9)  = up[2]
    viewMatrix(13) = 0.0
 
    viewMatrix(2)  = -direction[0]
    viewMatrix(6)  = -direction[1]
    viewMatrix(10) = -direction[2]
    viewMatrix(14) =  0.0

    viewMatrix(3)  = 0.0
    viewMatrix(7)  = 0.0
    viewMatrix(11) = 0.0
    viewMatrix(15) = 1.0
 
    setTranslationMatrix(aux(), -position)

    multMatrix(viewMatrix(), aux())

end sub

#endif