#ifndef FreeBasicVectorMath_bi
#define FreeBasicVectorMath_bi

#ifndef boolean_
#define boolean_
    #define true -1
    #define false 0
#endif

sub Normalize(Vec() as single)

    dim normal as single = sqr(Vec(0) * Vec(0) +_
                               Vec(1) * Vec(1) +_
                               Vec(2) * Vec(2))
    
    
    if normal > 0.00001 then
        Vec(0) /= normal
        Vec(1) /= normal
        Vec(2) /= normal
    endif
    
end sub

sub AddVec3D(VecA() as single, VecB() as single)
        
    VecA(0) += VecB(0)
    VecA(1) += VecB(1)
    VecA(2) += VecB(2)
    
end sub

sub SubVec3D(VecA() as single, VecB() as single)
        
    VecA(0) -= VecB(0)
    VecA(1) -= VecB(1)
    VecA(2) -= VecB(2)
    
end sub

sub MultVec3D(VecA() as single, Scalar as single)
        
    VecA(0) *= Scalar
    VecA(1) *= Scalar
    VecA(2) *= Scalar
    
end sub

sub SetVec3D(Vec() as single, X as single, Y as single, Z as single)
    
    Vec(0) = X
    Vec(1) = Y
    Vec(2) = Z

end sub

sub CopyVec3D(VecA() as single, VecB() as single)
    
    VecA(0) = VecB(0)
    VecA(1) = VecB(1)
    VecA(2) = VecB(2)

end sub

function Length3D(VecA() as single) as single
    
    return sqr(VecA(0)*VecA(0) + VecA(1)*VecA(1) + VecA(2)*VecA(2))
    
end function

'------------------------------------------------------------------------------'
sub Normalize2D(Vec() as single)

    dim normal as single = sqr(Vec(0) * Vec(0) +_
                               Vec(1) * Vec(1))
    
    if normal <> 0 then
        Vec(0) /= normal
        Vec(1) /= normal
    endif
    
end sub

sub AddVec2D(VecA() as single, VecB() as single)
        
    VecA(0) += VecB(0)
    VecA(1) += VecB(1)
    
end sub

sub SubVec2D(VecA() as single, VecB() as single)
        
    VecA(0) -= VecB(0)
    VecA(1) -= VecB(1)
    
end sub

sub MultVec2D(VecA() as single, Scalar as single)
        
    VecA(0) *= Scalar
    VecA(1) *= Scalar
    
end sub

sub SetVec2D(Vec() as single, X as single, Y as single)
    
    Vec(0) = X
    Vec(1) = Y

end sub

sub CopyVec2D(VecA() as single, VecB() as single)
    
    VecA(0) = VecB(0)
    VecA(1) = VecB(1)

end sub

function Length2D(VecA() as single) as single
    
    return sqr(VecA(0)*VecA(0) + VecA(1)*VecA(1))
    
end function

function sind(angle as single) as single
    
    return sin(angle * (3.14159265358979 / 180.0))
    
end function

function cosd(angle as single) as single
    
    return cos(angle * (3.14159265358979 / 180.0))
    
end function
#endif