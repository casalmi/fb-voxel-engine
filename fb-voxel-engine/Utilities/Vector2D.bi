#ifndef Vector2DType_bi
#define Vector2DType_bi

type Vector2DType
    
    public:
        
        declare Constructor(initX as single, initY as single)
        declare Constructor(vector() as single)
        declare Constructor(value as single)
        declare Constructor()
        
        declare Destructor()
        
        declare sub Normalize()
        
        declare function Dot overload () as single
        declare function Dot          (otherVec as Vector2DType) as single
        
        declare function Length() as single
        
        declare function X() as single
        declare function Y() as single
        declare function Z() as single
        
        declare sub SetX(inX as single)
        declare sub SetY(inY as single)
        
        declare Operator += (ByRef rightSide as Vector2DType)
        declare Operator -= (ByRef rightSide as Vector2DType)
        
        declare Operator *= (ByRef rightSide as Vector2DType)
        declare Operator /= (ByRef rightSide as Vector2DType)
        
        declare Operator Let (ByRef rightSide as Vector2DType)
        declare Operator Let (value as single)
        declare Operator Let (value as single ptr)
        declare Operator Let (value as integer)
        declare Operator Let (value as uinteger)
        
        declare Operator [] (index as integer) ByRef as single
        
        declare Operator Cast() as String
        
    private:
    
        dim Vec(1) as single
    
end type

Constructor Vector2DType(initX as single, initY as single)

    Vec(0) = initX : Vec(1) = initY
    
end constructor

Constructor Vector2DType(vector() as single)
    
    this.Constructor(vector(0), vector(1))

end constructor

Constructor Vector2DType(value as single)
    
    this.Constructor(value, value)
    
end constructor

Constructor Vector2DType()
    this.Constructor(0.0, 0.0)
end constructor

sub Vector2DType.Normalize()
    
    dim normal as single = sqr(Vec(0) * Vec(0) + _
                               Vec(1) * Vec(1))
    
    
    if normal > 0.00001 then
        Vec(0) /= normal
        Vec(1) /= normal
    endif
    
end sub

function Vector2DType.Dot() as single
    
    return (Vec(0)*Vec(0)) + (Vec(1)*Vec(1))
    
end function

function Vector2DType.Dot(otherVec as Vector2DType) as single
    
    return (Vec(0)*otherVec.Vec(0)) + (Vec(1)*otherVec.Vec(1))
    
end function

function Vector2DType.Length() as single
    
    return sqr(this.Dot())
    
end function

function Vector2DType.X() as single
    return this.Vec(0)
end function

function Vector2DType.Y() as single
    return this.Vec(1)
end function

sub Vector2DType.SetX(inX as single)
    this.Vec(0) = inX
end sub

sub Vector2DType.SetY(inY as single)
    this.Vec(1) = inY
end sub

Operator + (ByRef leftSide as Vector2DType, ByRef rightSide as Vector2DType) as Vector2DType
    return Vector2DType(leftSide.X()+rightSide.X(), leftSide.Y()+rightSide.Y())
end operator

Operator + (ByRef leftSide as Vector2DType, Scalar as single) as Vector2DType
    return Vector2DType(leftSide.X()+Scalar, leftSide.Y()+Scalar)
end operator

Operator Vector2DType.+=(ByRef rightSide as Vector2DType)
    this.Vec(0) += rightSide.Vec(0) : this.Vec(1) += rightSide.Vec(1)
end operator

Operator - (ByRef leftSide as Vector2DType, ByRef rightSide as Vector2DType) as Vector2DType
    return Vector2DType(leftSide.X()-rightSide.X(), leftSide.Y()-rightSide.Y())
end operator

Operator - (ByRef leftSide as Vector2DType, Scalar as single) as Vector2DType
    return Vector2DType(leftSide.X()-Scalar, leftSide.Y()-Scalar)
end operator

'Negate
Operator - (ByRef leftSide as Vector2DType) as Vector2DType
    return Vector2DType(leftSide.X() * -1, leftSide.Y() * -1)
end operator

Operator Vector2DType.-=(ByRef rightSide as Vector2DType)
    this.Vec(0) -= rightSide.Vec(0) : this.Vec(1) -= rightSide.Vec(1)
end operator

Operator * (ByRef leftSide as Vector2DType, ByRef rightSide as Vector2DType) as Vector2DType
    return Vector2DType(leftSide.X()*rightSide.X(), leftSide.Y()*rightSide.Y())
end operator

Operator * (ByRef leftSide as Vector2DType, Scalar as single) as Vector2DType
    return Vector2DType(leftSide.X()*Scalar, leftSide.Y()*Scalar)
end operator

Operator Vector2DType.*=(ByRef rightSide as Vector2DType)
    this.Vec(0) *= rightSide.Vec(0) : this.Vec(1) *= rightSide.Vec(1)
end operator

Operator / (ByRef leftSide as Vector2DType, ByRef rightSide as Vector2DType) as Vector2DType
    
    dim divisor(2) as single = {1.0, 1.0}
    
    if (abs(rightSide.X()) > .000001) then
        divisor(0) = rightSide.X()
    end if
    if (abs(rightSide.Y()) > .000001) then
        divisor(1) = rightSide.Y()
    end if
    
    return Vector2DType(leftSide.X()/divisor(0), leftSide.Y()/divisor(1))

end operator

Operator / (ByRef leftSide as Vector2DType, Scalar as single) as Vector2DType

    if abs(Scalar) > 0.000001 then
        return Vector2DType(leftSide.X()/Scalar, leftSide.Y()/Scalar)
    else
        return leftSide
    end if

end operator

Operator Vector2DType./=(ByRef rightSide as Vector2DType)
    if (abs(rightSide.Vec(0)) > .000001) then
        this.Vec(0) /= rightSide.Vec(0)
    end if
    if (abs(rightSide.Vec(1)) > .000001) then
        this.Vec(1) /= rightSide.Vec(1)
    end if
end operator

Operator = (ByRef leftSide as Vector2DType, ByRef rightSide as Vector2DType) as integer
    return (leftSide.X() = rightSide.X()) AND (leftSide.Y() = rightSide.Y())
end operator

Operator Vector2DType.Let(ByRef rightSide as Vector2DType)
    this.Vec(0) = rightSide.X() : this.Vec(1) = rightSide.Y()
end operator

Operator Vector2DType.Let(value as single)
    this.Vec(0) = value : this.Vec(1) = value
end operator

Operator Vector2DType.Let(value as single ptr)
    this.Vec(0) = value[0] : this.Vec(1) = value[1]
end operator

Operator Vector2DType.Let(value as integer)
    this.Vec(0) = value : this.Vec(1) = value
end operator

Operator Vector2DType.Let(value as uinteger)
    this.Vec(0) = value : this.Vec(1) = value
end operator

Operator Vector2DType.[] (index as integer) ByRef as single

    return Vec(index)
    
end operator

Operator Vector2DType.cast() as String
    return "("&Vec(0)&", "&Vec(1)&")"
end operator

Destructor Vector2DType()
end destructor

#endif