#ifndef LinkedList_bi
#define LinkedList_bi

#include once "Node.bi"

#ifndef Node_bi
    #error Missing "Node.bi"
#endif

'Linked list
type LinkedListType
    
    public:
        
        declare Constructor()
        
        declare Destructor()
        
        declare sub InsertLast(inData as any ptr)

        declare function GetHeadNode() as NodeType ptr
        
        declare function GetItemCount() as integer
        
        'See note below
        declare sub SetCompareFunction(CompareFuncPtr as any ptr)
        
        declare sub Sort()
        
        'Implicitly calls Sort()
        declare sub RemoveDuplicates()
        
        declare sub Empty()
        
    private:
        
        declare sub SiftDown(inArray as any ptr ptr, first as integer, last as integer)
        declare sub Heapify(inArray as any ptr ptr, ArrayCount as integer)
        
        'Compares two items.  Returns:
        ' -1 if inA <  inB
        '  0 if inA = inB
        '  1 if inA > inB
        dim CompareFunc as function(inA as any ptr, inB as any ptr) as integer
        
        dim ItemCount as short
        
        dim RootNode as NodeType ptr
        dim LastNode as NodeType ptr

end type

Constructor LinkedListType()

    this.RootNode = 0
    this.LastNode = this.RootNode
    this.ItemCount = 0

end Constructor

sub LinkedListType.InsertLast(inData as any ptr)
    
    dim tempNode as NodeType ptr
    
    if this.RootNode <> 0 then
        
        'Init a temp node with the last node as it's "previous"
        tempNode = new NodeType(this.LastNode, 0, 0)
        
        NodeTypeCount += 1
        
        this.LastNode->SetNext(tempNode)
        this.LastNode = this.LastNode->GetNext()
        
    else
        'Initialize the head
        this.RootNode = new NodeType(this.LastNode)
        
        NodeTypeCount += 1
        
        this.LastNode = this.RootNode
        
    end if
    
    this.LastNode->SetData(inData)
    
    this.ItemCount += 1
    
end sub

function LinkedListType.GetHeadNode() as NodeType ptr
    return this.RootNode
end function

function LinkedListType.GetItemCount() as integer
    return this.ItemCount
end function

'Setting this is optional unless you're using Sort()/RemoveDuplicates()
sub LinkedListType.SetCompareFunction(CompareFuncPtr as any ptr)
    this.CompareFunc = CompareFuncPtr
end sub

sub LinkedListType.SiftDown(inArray as any ptr ptr, _
                            first as integer, _
                            last as integer)
    
    dim root as integer = first
    
    dim child as integer = 0
    
    dim temp as any ptr
    
    while (root * 2) + 1 <= last
        
        child = (root * 2) + 1

        if (child + 1 <= last) AND _
           (this.CompareFunc(inArray[child], inArray[child + 1]) = -1) then
           
           child += 1
           
        end if
        
        if this.CompareFunc(inArray[root], inArray[child]) = -1 then
            
            temp = inArray[root]
            inArray[root] = inArray[child]
            inArray[child] = temp
            
            root = child
            
        else
            return
        end if
        
    wend
    
end sub

sub LinkedListType.Heapify(inArray as any ptr ptr, _
                           ArrayCount as integer)
    
    dim start as integer = (ArrayCount - 2) / 2
    
    while start >= 0
        
        this.SiftDown(inArray, start, ArrayCount-1)
        
        start -= 1
        
    wend
    
end sub

sub LinkedListType.Sort()
    
    if this.ItemCount = 0 then
        return
    end if
    
    if this.CompareFunc = 0 then
        'You can't sort unless you have a
        'compare function to use
        return
    end if
    
    dim array as any ptr ptr
    dim node as NodeType ptr
    
    dim temp as any ptr
    
    dim last as integer
    dim i as integer
    
    array = new any ptr[this.ItemCount]
    
    node = this.RootNode

    'Copy the list over
    for i = 0 to this.ItemCount - 1

        array[i] = node->GetData()
        node = node->GetNext()
        
    next

    this.Heapify(array, this.ItemCount-1)
    
    last = this.ItemCount-1
    
    while last > 0
        
        temp = array[last]
        array[last] = array[0]
        array[0] = temp
        
        last -= 1
        
        this.SiftDown(array, 0, last)
        
    wend
    
    node = this.GetHeadNode()
    
    for i = 0 to this.ItemCount - 1
        
        node->SetData(array[i])
        node = node->GetNext()
    
    next
    
    delete [] array
    
end sub

sub LinkedListType.RemoveDuplicates()
    
    if this.ItemCount <= 1 then
        return
    end if
    
    if this.CompareFunc = 0 then
        return
    end if
    
    dim i as integer
    
    dim node as NodeType ptr
    dim nextNode as NodeType ptr
    
    dim removedCount as integer = 0
    
    this.Sort()

    node = this.GetHeadNode()
    nextNode = node->GetNext()
    
    for i = 0 to this.ItemCount - 2
        
        while this.CompareFunc(node->GetData(), nextNode->GetData()) = 0

            this.ItemCount -= 1

            nextNode->Remove()

            nextNode = node->GetNext()
            
            if nextNode = 0 then
                exit while
            end if
            
        wend

        node = nextNode
        
        if node <> 0 then
            
            nextNode = node->GetNext()
        
            if nextNode = 0 then
                exit for
            end if
            
        else
            exit for
        end if
    
    next
    
end sub

'Deletes all nodes in the list
sub LinkedListType.Empty()
    
    dim node as NodeType ptr
    dim tempNode as NodeType ptr
    
    node = RootNode

    while node
        
        tempNode = node
        node = node->GetNext()
        
        tempNode->Remove()
        
    wend
    
    this.RootNode = 0
    this.LastNode = this.RootNode
    
    this.ItemCount = 0
    
end sub

Destructor LinkedListType()
    
    this.Empty()
    
end Destructor

#endif