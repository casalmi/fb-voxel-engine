#ifndef Noise_Header_bi
#define Noise_Header_bi

#include once "crt.bi"

#define GLOBAL_SEED 90
#define RND_MAX     32767

const NOISE_MAX as integer = 10000

const WORLEY_NOISE_GRID_SIZE as integer = 2048
const WORLEY_CELL_VALUE_MAX as integer = 5

declare function noise(ByVal x as single, ByVal y as single) as integer

'Returns a random value between 0 and NOISE_MAX
function noise(ByVal x as single, ByVal y as single) as integer

    dim n as integer = (x SHL 13) * GLOBAL_SEED + y * GLOBAL_SEED
    return ((n * (n * n * 15731 + 789221) + 1376312589) AND &H7fffffff) MOD NOISE_MAX

end function

#endif