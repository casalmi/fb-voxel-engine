
#include once "../VoxelEngine.bi"

type VoxelOctreeType extends OctreeType
    
    public:
        
        declare Constructor(AABBMin() as single, _
                            AABBMax() as single, _
                            inMaxDepth as integer, _
                            inMaxElements as integer, _
                            ComparisonFunction as any ptr)
        
        declare Constructor(MinX as single, MinY as single, MinZ as single, _
                            MaxX as single, MaxY as single, MaxZ as single, _
                            inMaxDepth as integer, _
                            inMaxElements as integer, _
                            ComparisonFunction as any ptr)
        
        declare Destructor()
        dim LODValue as ubyte
    private:
    
        
    
end type

Constructor VoxelOctreeType(inAABBMin() as single, _
                            inAABBMax() as single, _
                            inMaxDepth as integer, _
                            inMaxElements as integer, _
                            ComparisonFunction as any ptr)

    Base(inAABBMin(), inAABBMax(), inMaxDepth, inMaxElements, ComparisonFunction)
    
end constructor

Constructor VoxelOctreeType(MinX as single, MinY as single, MinZ as single, _
                            MaxX as single, MaxY as single, MaxZ as single, _
                            inMaxDepth as integer, _
                            inMaxElements as integer, _
                            ComparisonFunction as any ptr)
    
    Base(MinX, MinY, MinZ, MaxX, MaxY, MaxZ, _
         inMaxDepth, inMaxElements, ComparisonFunction)
         
end constructor

Destructor VoxelOctreeType()
    Base.Destructor()
end Destructor