#ifndef Octree_bi
#define Octree_bi

#include once "crt.bi"
#include once "LinkedList.bi"
#include once "Vector3D.bi"

#ifndef LinkedList_bi
    #error Missing "LinkedList.bi"
#endif

#ifndef Vector3DType_bi
    #error Missing "Vector3DType.bi"
#endif

'Octree class
'Stores internal references (as opposed to internal copies of data)

#ifdef __gl_h_
dim shared OctreeShader as OpenGL_Shader_TYPE ptr
#endif

'Child structure looks like:

'     __________
'    / 7 / 3  / | ^
'   /___/____/|3| |
'  /_6_/__2_/ | | |
' |    |    |2|/| Y
' | 6  | 2  | / | 
' |____|____|/|1| /\
' |    |    |0|/ /
' | 4  | 0  | / /
' |____|____|/ Z 
'
'   <--X (The axis' are simply considered positive)
'        (Just assume so and treat it them though they're positive)

type OctreeType
    
    public:
        
        declare Constructor()
        
        declare Constructor(AABBMin as Vector3DType, _
                            AABBMax as Vector3DType, _
                            inMaxDepth as integer, _
                            inMaxElements as integer, _
                            ComparisonFunction as any ptr)
        
        declare Constructor(AABBMin() as single, _
                            AABBMax() as single, _
                            inMaxDepth as integer, _
                            inMaxElements as integer, _
                            ComparisonFunction as any ptr)
        
        declare Constructor(MinX as single, MinY as single, MinZ as single, _
                            MaxX as single, MaxY as single, MaxZ as single, _
                            inMaxDepth as integer, _
                            inMaxElements as integer, _
                            ComparisonFunction as any ptr)
        
        declare Destructor()
        
        declare function PrepareChildren() as integer
        
        declare sub Copy(copyNode as OctreeType ptr)
        
        declare function Insert(ByRef inData as any ptr) as OctreeType ptr
        
        declare function HasChildren() as integer
        
        declare function IsRoot() as integer
        
        declare function GetDepth() as integer
        
        declare function GetChild(index as integer) as OctreeType ptr
        declare function GetParent() as OctreeType ptr
        
        declare sub SaturateOctree(depth as integer)
        
        'Special function for octrees with exactly one element per node
        declare function GetDataItem() as any ptr
        declare sub SetDataItem(inData as any ptr)
        
        declare sub GetAllData(inList as LinkedListType ptr)
        declare sub GetAllChildren(inList as LinkedListType ptr)
        
        'declare sub GetAABB(inMin as single ptr, inMax as single ptr)
        declare function GetAABBMin() as Vector3DType
        declare function GetAABBMax() as Vector3DType
        
        declare function GetMidpoint() as Vector3DType
        
        declare sub GetIndexedChildren(inList as LinkedListType ptr, IndexMask as ubyte)
        
        declare sub CollapseChildren()
        
        declare sub SetComparisonFunction(comparisonFunction as any ptr)
        declare sub SetGetIndexFunction(getIndexFunction as any ptr)
        
        declare function GetItemCount() as integer
        
        declare function BuildBottomUp(BottomUpOctree as OctreeType ptr ptr, _
                                       BaseSize as integer) as OctreeType ptr
        
        declare static Function BuildBottomUp Overload (BottomUpOctree as OctreeType ptr ptr, _
                                                        BaseSize as integer, _
                                                        inScale as single, _
                                                        inOffset() as single, _
                                                        inCurrentDepth as ubyte, _
                                                        inMaxElements as ushort, _
                                                        inCompareFunction as any ptr, _
                                                        inGetIndexFunction as any ptr) as OctreeType ptr
        
        'Gets at most 1 neighbor distance in each direction
        '(i.e. don't pass anything but -1, 0, or 1 as arguments)
        declare function GetNeighbor(xDir as byte, _
                                     yDir as byte, _
                                     zDir as byte) as OctreeType ptr
        
        declare function GetIndex() as ubyte
        
        'For building the octree bottom up and manual modifications
        declare sub SetParent(inNode as OctreeType ptr)
        declare sub SetChild(inChild as OctreeType ptr, index as ubyte)
        declare sub SetChildNode(inChild as OctreeType ptr, inData as any ptr)
        declare sub SetData(inData as any ptr)
        declare sub SetDepth(inDepth as integer)
        
        declare sub PrintOctree()
        
        'Created, rendered, and destroyed on the fly
        'Large octrees may hurt performance tremendously
    #ifdef __gl_h_
        declare sub Render3D(ShaderHandle as GLuint, _
                             ViewMat() as single, _
                             ProjMat() as single, _
                             LeafNodesOnly as integer)
    #endif
        
        dim Children as OctreeType ptr ptr
        
    private:

    #ifdef __gl_h_
        declare sub Render3DRecursive(ShaderHandle as GLuint, LeafNodesOnly as integer)
    #endif
        'Must compare the input data to an Axis Aligned Bounding Box
        'and return an integer
        'Return values:
        '   0 for not inside the bounding box (completely outside)
        '   1 for inside the bounding box (completely inside)
        '   2 for partially inside/outside the bounding box
        'If the value is 2, we need to continue checking other
        'children to see if this input data needs to be
        'referenced by other children nodes (In the case that
        'the InData is a 3D volume and it resides in multiple
        'leaf nodes at once)
        'In that case, do NOT set the GetIndexFunction()
        '
        'Usually this function would compare a bounding box to a
        'point, or a bounding box to bounding box
        'Takes an Any Ptr, the bounding box (of the AABB) and returns an integer
        dim CompareFunction as function(ByRef inData as any ptr, _
                                        AABBMin as Vector3DType, _
                                        AABBMax as Vector3DType) as integer
        
        'This allows the user to specify which child this InData thing
        'is going to reside in.
        'Writing this properly speeds up top-down inserts greatly
        dim GetIndexFunction as function(ByRef inData as any ptr, _
                                         AABBMin as Vector3DType, _
                                         AABBMax as Vector3DType) as integer
        
        'Critical data:
        dim AABBMin as Vector3DType
        dim AABBMax as Vector3DType
        
        dim MaxElements as ushort
        
        dim CurrentDepth as ubyte
        
        'Internal data:
        dim Parent as OctreeType ptr

        'dim isLeafNode as byte
        
        Union
            'Stores references to the inserted data
            dim BufferData as LinkedListType ptr
            
            'This will be used as a memory saver
            'if the user defines only 1 data item
            'per leaf node.
            dim Data as any ptr
        end union
        'The index this child is addressed as (from the parent)
        dim Index as ubyte

end type

Constructor OctreeType()

end Constructor

Constructor OctreeType(inAABBMin as Vector3DType, _
                       inAABBMax as Vector3DType, _
                       inMaxDepth as integer, _
                       inMaxElements as integer, _
                       ComparisonFunction as any ptr)

    dim i as integer
    
    this.AABBMin = inAABBMin
    this.AABBMax = inAABBMax

    this.CurrentDepth = inMaxDepth
    
    this.MaxElements = inMaxElements

    this.CompareFunction = ComparisonFunction
    
    if inMaxElements = 1 then
        this.Data = 0
    else
        this.BufferData = 0'new LinkedListType()
    end if
    
    this.Parent = 0

    this.Children = 0
    
    this.Index = 0

end Constructor

Constructor OctreeType(inAABBMin() as single, _
                       inAABBMax() as single, _
                       inMaxDepth as integer, _
                       inMaxElements as integer, _
                       ComparisonFunction as any ptr)

    this.Constructor(inAABBMin(0), inAABBMin(1), inAABBMin(2), _
                     inAABBMax(0), inAABBMax(1), inAABBMax(2), _
                     inMaxDepth, inMaxElements, ComparisonFunction)

end Constructor

Constructor OctreeType(MinX as single, MinY as single, MinZ as single, _
                       MaxX as single, MaxY as single, MaxZ as single, _
                       inMaxDepth as integer, _
                       inMaxElements as integer, _
                       ComparisonFunction as any ptr)
                       
    dim AABBMin as Vector3DType = Vector3DType(MinX, MinY, MinZ)
    dim AABBMax as Vector3DType = Vector3DType(MaxX, MaxY, MaxZ)

    this.Constructor(AABBMin, AABBMax, inMaxDepth, inMaxElements, ComparisonFunction)

end Constructor

function OctreeType.PrepareChildren() as integer
    
    if this.CurrentDepth = 0 then
        'We have hit the maximum depth of the tree
        'print "OctreeType.bi (";__LINE__;"): Maximum depth reached"
        return 0
    end if
    
    dim as integer x,y,z
    
    dim min as Vector3DType
    dim max as Vector3DType
    
    dim halfDiff as Vector3DType
    
    dim index as integer
    
    'We're working with a node that's "half" our size on each axis
    halfDiff = (this.AABBMax - this.AABBMin) * 0.5

    this.Children = new OctreeType ptr[8]

    for x = 0 to 1
        for y = 0 to 1
            for z = 0 to 1
                
                index = x * 4 + y * 2 + z
                
                'Calculate the minimum bound of the new child node
                min = this.AABBMin + (halfDiff * Vector3DType(x,y,z))
                max = min + halfDiff
                
                this.Children[index] = new OctreeType(min, _
                                                      max, _
                                                      this.CurrentDepth - 1, _
                                                      this.MaxElements, _
                                                      this.CompareFunction)
                
                OctreeTypeCount += 1
                
                'Give the child a reference to it's parent
                this.Children[index]->Parent = @this
                this.Children[index]->Index = index
                
                this.Children[index]->GetIndexFunction = this.GetIndexFunction
                
                if this.MaxElements = 1 then
                    this.Children[index]->Data = 0
                end if
                
            next
        next
    next
    
    dim node as NodeType ptr
    
    if this.MaxElements <> 1 then
        
        node = this.BufferData->GetHeadNode()
    
        'Re-insert the data into the new leaf nodes
        for x = 0 to this.BufferData->GetItemCount() - 1
            
            for y = 0 to 7
                this.Children[y]->Insert(node->GetData())
            next
            
            node = node->GetNext()
            
        next
        
        delete(this.BufferData)
        this.BufferData = 0
        
    else
        
        if this.Data <> 0 then
            
            'I'm debating as to whether this should
            'try to insert a single object into it's
            'new children
            
            /'for x = 0 to 7
                if this.Children[x]->Insert(this.Data) then
                    this.Data = 0
                    exit for
                end if
            next
            '/
        end if

    end if

    return 1
    
end function

sub OctreeType.Copy(copyNode as OctreeType ptr)
    
    dim i as integer
    
    if copyNode = 0 then
        return
    end if
    
    'this.AABBMin = copyNode->AABBMin
    'this.AABBMax = copyNode->AABBMax
    
    this.MaxElements = copyNode->MaxElements
    
    'this.CurrentDepth = copyNode->CurrentDepth
    
    if this.Children then
        for i = 0 to 7
            if this.Children[i] then
                delete(this.Children[i])
                
                OctreeTypeCount -= 1
                
            end if
        next
        
        delete [] this.Children
    end if
    
    this.Children = copyNode->Children
    
    for i = 0 to 7
        if copyNode->GetChild(i) then
            copyNode->GetChild(i)->SetParent(@this)
        end if
    next
    
    if copyNode->MaxElements = 1 then
        this.Data = copyNode->Data
    else
        this.BufferData = copyNode->BufferData
    end if
    
end sub

'Returns the pointer to the octree node that the data was inserted into
function OctreeType.Insert(ByRef inData as any ptr) as OctreeType ptr

    if this.CompareFunction = 0 then
        return 0
    end if
    
    dim i as integer
    
    dim compareValue as integer
    dim retChild as OctreeType ptr

    if this.HasChildren() then
        
        'If we have children, insert the data into them
        
        if this.GetIndexFunction <> 0 then
            
            i = this.GetIndexFunction(inData, this.AABBMin, this.AABBMax)

            if i = -1 then
                return 0
            end if
            
            retChild = this.Children[i]->Insert(inData)
            
        else
        
            for i = 0 to 7
    
                'Insert into each child and check to see what the result was
                retChild = this.Children[i]->Insert(inData)
                if retChild then
                    'If the compare value found it was
                    'completely inside, we break here
                    '(This saves some computation)
                    exit for
                end if
            next
            
        end if

        return retChild
        
    else
        
        'Otherwise we're a leaf node
        
        if this.MaxElements <> 1 then

            'Check to see if we have enough room
            if this.BufferData->GetItemCount() >= this.MaxElements then
                
                'If not, create children and insert into them
                if this.PrepareChildren() then
                    retChild = this.Insert(inData)
                else
                    'Return a unique error code
                    return 0
                end if
                
                return retChild
                
            end if
        
        else
            
            'Single element insert
            if this.Data <> 0 then

                if this.PrepareChildren() then
                    retChild = this.Insert(inData)
                else
                    return 0
                end if
                
                return retChild
                
            end if
            
        end if

        compareValue = this.CompareFunction(inData, _
                                            this.AABBMin, _
                                            this.AABBMax)

        'If the compare function found the item to be fully in
        'or partially in, insert it into our data list
        if compareValue > 0 then
            
            if this.MaxElements <> 1 then
                this.BufferData->InsertLast(inData)
            else
                this.Data = inData
            end if
            
        end if
        
        if compareValue = 1 then
            return @this
        else
            return 0
        end if
        
    end if

end function

function OctreeType.HasChildren() as integer
    
    return this.Children <> 0
    
end function

function OctreeType.GetChild(index as integer) as OctreeType ptr
    
    if this.Children then
        return this.Children[index]
    else
        return 0
    end if
end function

function OctreeType.GetParent() as OctreeType ptr
    return this.Parent
end function

function OctreeType.IsRoot() as integer
    return (this.Parent = 0)
end function

function OctreeType.GetDepth() as integer
    return cast(integer, this.CurrentDepth)
end function

sub OctreeType.SetParent(inNode as OctreeType ptr)
    this.Parent = inNode
end sub

sub OctreeType.SetChild(inChild as OctreeType ptr, index as ubyte)
    
    if this.Children = 0 then
        this.Children = new OctreeType ptr[8]
    end if
    
    this.Children[index] = inChild
    
    if inChild <> 0 then
        this.Children[index]->Index = index
    end if
    
end sub

sub OctreeType.SetData(inData as any ptr)
        
    if this.MaxElements <> 1 then
        if this.BufferData = 0 then
            this.BufferData = new LinkedListType()
        end if
        
        if this.BufferData->GetItemCount() < this.MaxElements then
            this.BufferData->InsertLast(inData)
        end if
        
    else
        
        if this.Data <> 0 then
            return
        end if
        
        this.Data = inData
    
    end if
        
end sub

sub OctreeType.SetDataItem(inData as any ptr)
    
    if this.MaxElements = 1 then
        this.Data = inData
    end if
    
end sub
    
sub OctreeType.SetDepth(inDepth as integer)
    
    this.CurrentDepth = inDepth
    
end sub

'Generates all possible leaf nodes for the octree
'***ONLY USE ON NEWLY CREATED OCTREES***'
sub OctreeType.SaturateOctree(depth as integer)
    
    if this.Parent then
        return
    end if
    
    if depth = 0 then
        return
    end if
    
    dim i as integer
    
    dim node as OctreeType ptr
    node = @this
    
    this.PrepareChildren()
    
    for i = 0 to 7
        this.Children[i]->SaturateOctree(depth - 1)
    next
    
end sub

function OctreeType.GetDataItem() as any ptr
    
    if this.MaxElements = 1 then
        return this.Data
    else
        return 0
    end if
    
end function

'Insert an empty linked list into here
'Returns a continuous linked list of
'all data from all children
sub OctreeType.GetAllData(inList as LinkedListType ptr)
    
    dim i as integer
    
    dim node as NodeType ptr
    
    if this.HasChildren() then

        for i = 0 to 7
            if this.Children[i] then
                this.Children[i]->GetAllData(inList)
            end if
        next
        
        'return
        
    end if
    
    if this.MaxElements <> 1 then

        if this.BufferData <> 0 then
            
            'Start from our head node
            node = this.BufferData->GetHeadNode()
            
            'Insert all values into the list
            for i = 0 to this.BufferData->GetItemCount() - 1
                inList->InsertLast(node->GetData())
                node = node->GetNext()
            next
            
        end if
        
    else
        
        if this.Data <> 0 then
            inList->InsertLast(this.Data)
        end if
        
    end if

end sub

sub OctreeType.GetAllChildren(inList as LinkedListType ptr)
    
    dim i as integer

    if this.HasChildren() then

        for i = 0 to 7
            if this.Children[i] then
                this.Children[i]->GetAllChildren(inList)
            end if
        next
        
        return
        
    end if
    
    inList->InsertLast(@this)
    
end sub

function OctreeType.GetAABBMin() as Vector3DType
    
    return this.AABBMin
    
end function

function OctreeType.GetAABBMax() as Vector3DType
    
    return this.AABBMax

end function

function OctreeType.GetMidpoint() as Vector3DType 
    
    return this.AABBMin + ((this.AABBMax - this.AABBMin) * .5)'(this.AABBMax + this.AABBMin) * .5
    
end function

'Allows getting only children that fall within the mask
'Useful for processing only the edges or faces of an octree
'The mask is bitwise positional:
'0x01 is for index 0 nodes (00000001)
'0x80 is for index 7 nodes (10000000)
'etc.
sub OctreeType.GetIndexedChildren(inList as LinkedListType ptr, IndexMask as ubyte)
    
    dim i as integer
    
    dim IDBits as ubyte
    
    if this.HasChildren() then
        
        for i = 0 to 7
            if this.Children[i] then
                
                IDBits = 1 SHL this.Children[i]->Index

                if (IDBits AND IndexMask) then
                    this.Children[i]->GetIndexedChildren(inList, IndexMask)
                end if
                
            end if
        next
        
        return
        
    end if
        
    inList->InsertLast(@this)
    
end sub

sub OctreeType.CollapseChildren()

    dim i as integer

    if this.Children = 0 then
        return
    end if
    
    for i = 0 to 7
        if this.Children[i] <> 0 then
            delete(this.Children[i])
            
            OctreeTypeCount -= 1
            
        end if
    next

    delete [] this.Children
    this.Children = 0

end sub

function OctreeType.GetNeighbor(xDir as byte, _
                                yDir as byte, _
                                zDir as byte) as OctreeType ptr

    if this.Parent = 0 then
        return 0
    end if
    
    dim retNode as OctreeType ptr
    
    dim as byte xID, yID, zID
    dim as byte xMask, yMask, zMask
    
    dim index as integer
    
    'Extract our positional indicies
    xID = (this.Index AND 4) SHR 2
    yID = (this.Index AND 2) SHR 1
    zID = (this.Index AND 1) SHR 0

    index = ((xID + xDir) AND 1) * 4 + ((yID + yDir) AND 1) * 2 + ((zID + zDir) AND 1)
    
    if xDir then
        'This converts the direction sign to 0 (negative direction) or 1 (positive)
        xMask = ((xDir AND &h80) = 0) AND &h01
    end if
    
    if yDir then
        yMask = ((yDir AND &h80) = 0) AND &h01
    end if
    
    if zDir then
        zMask = ((zDir AND &h80) = 0) AND &h01
    end if
    
    if xDir = 0 then xID = 2 : end if
    if yDir = 0 then yID = 2 : end if
    if zDir = 0 then zID = 2 : end if
  
    retNode = this.Parent
    
    if xID <> xMask then xDir = 0 : end if
    if yID <> yMask then yDir = 0 : end if
    if zID <> zMask then zDir = 0 : end if
    
    if (xID = xMask) OR (yID = yMask) OR (zID = zMask) then
        retNode = retNode->GetNeighbor(xDir, yDir, zDir)
    end if
    
    if retNode = 0 then return 0: end if
    
    if retNode->HasChildren() = 0 then 
        return retNode
    end if
    
    retNode = retNode->GetChild(index)
    
    return retNode

end function

'Builds an octree from a 3D grid of level 0 (finest) resolution octree nodes
function OctreeType.BuildBottomUp(BottomUpOctree as OctreeType ptr ptr, _
                                  BaseSize as integer) as OctreeType ptr
    
    dim Offset(2) as single
    Offset(0) = this.AABBMin[0]
    Offset(1) = this.AABBMin[1]
    Offset(2) = this.AABBMin[2]
    
    return BuildBottomUp(BottomUpOctree, _
                         BaseSize, _
                         (this.AABBMax[0] - this.AABBMin[0]) / BaseSize, _
                         Offset(), _
                         this.CurrentDepth, _
                         this.MaxElements, _
                         cast(any ptr, @this.CompareFunction), _
                         cast(any ptr, @this.GetIndexFunction))
    
end function

static function OctreeType.BuildBottomUp(BottomUpOctree as OctreeType ptr ptr, _
                                         BaseSize as integer, _
                                         inScale as single, _
                                         inOffset() as single, _
                                         inCurrentDepth as ubyte, _
                                         inMaxElements as ushort, _
                                         inCompareFunction as any ptr, _
                                         inGetIndexFunction as any ptr) as OctreeType ptr
    
    dim as integer i,j,x,y,z

    dim Offset(7,2) as integer = {{0,0,0}, {0,0,1}, {0,1,0}, {0,1,1}, _
                                  {1,0,0}, {1,0,1}, {1,1,0}, {1,1,1}}
    
    dim size as integer
    size = BaseSize + 1

    dim createParent as byte
    
    dim OctreeNode as OctreeType ptr
    
    dim AABBMin as Vector3DType
    dim AABBMax as Vector3DType
    
    dim OffsetVec as Vector3DType 
    OffsetVec = @inOffset(0)
    
    dim Scale as Vector3DType
    dim Depth as integer = 0
    
    while BaseSize > (1 SHL Depth)'pow(2, Depth)
        Depth += 1
    wend

    dim index as integer
    dim maxIndex as integer
    maxIndex = size * size * size
    
    'Base scale of the smallest node
    Scale = inScale

    'Loop over each depth in the octree
    for i = 1 to Depth
        
        'Loop over each index in the octree, stepping the size of the depth
        for x = 0 to BaseSize - 1 step (1 SHL i)'pow(2, i)
         for y = 0 to BaseSize - 1 step (1 SHL i)'pow(2, i)
          for z = 0 to BaseSize - 1 step (1 SHL i)'pow(2, i)
            
            createParent = 0

            'Loop over each 2x2x2 block to see
            'if there's a viable node here.
            for j = 0 to 7
                
                index = (x + (Offset(j, 0) * (1 SHL (i-1)))) * size * size + _
                        (y + (Offset(j, 1) * (1 SHL (i-1)))) * size + _
                        (z + (Offset(j, 2) * (1 SHL (i-1))))
                
                'Index checks are for non-power-of-2
                'BaseSize octrees
                if index > maxIndex then
                    continue for
                end if
                
                if BottomUpOctree[index] <> 0 then
                    
                    'if BottomUpOctree[index]->CurrentDepth = i - 1 then
                    
                        'If we've found a node, create a parent node
                        'for this block of 2x2x2 cells
                        createParent = 1
                        
                        exit for
                    
                    'end if
                    
                end if
                
            next
            
            OctreeNode = 0

            'If there's a node to create, make one
            if createParent then

                'Create the bounding box at this level
                AABBMin = OffsetVec + (Vector3DType(x,y,z) * Scale)
                
                AABBMax = AABBMin + (Scale * (1 SHL i))

                OctreeNode = new OctreeType(AABBMin, _
                                            AABBMax, _
                                            i + (inCurrentDepth - Depth), _
                                            inMaxElements, _
                                            inCompareFunction)
                
                OctreeTypeCount += 1
                
                OctreeNode->SetGetIndexFunction(inGetIndexFunction)
                
                'Set each child to the newly created octree node
                for j = 0 to 7
                    
                    index = (x + (Offset(j, 0) * (1 SHL (i-1)))) * size * size + _
                            (y + (Offset(j, 1) * (1 SHL (i-1)))) * size + _
                            (z + (Offset(j, 2) * (1 SHL (i-1))))
                    
                    if index >= MaxIndex then
                        continue for
                    end if

                    OctreeNode->SetChild(BottomUpOctree[index], j)
                    
                    if BottomUpOctree[index] <> 0 then
                        'If this is a real child, set it's parent
                        'to the newly created node
                        BottomUpOctree[index]->SetParent(OctreeNode)
                        BottomUpOctree[index]->Index = j
                    end if
                    
                    BottomUpOctree[index] = 0
                    
                next
                
                'Set this part of the octree to the newly created node and use
                'it in the next iteration to generate the next octree level
                BottomUpOctree[x*size*size + y*size + z] = OctreeNode
                
            end if
            
          next
         next
        next
        
    next
    
    'if OctreeNode then
    '    OctreeNode->Parent = this.Parent
    'end if
    
    'We're returning the pointer to the root node
    'of the newly created octree.
    'The user is expected to delete the octree that
    'called this function afterwards, and set it to this.
    return OctreeNode
    
end function

sub OctreeType.SetComparisonFunction(comparisonFunction as any ptr)
    this.CompareFunction = comparisonFunction
end sub

sub OctreeType.SetGetIndexFunction(getIndexFunction as any ptr)
    this.GetIndexFunction = getIndexFunction
end sub

function OctreeType.GetItemCount() as integer
    
    if this.MaxElements <> 1 then
        if this.BufferData then
            return this.BufferData->GetItemCount()
        else
            return 0
        end if
    else
        if this.Data then
            return 1
        else
            return 0
        end if
    end if
    
end function

function OctreeType.GetIndex() as ubyte
    return this.Index
end function

sub OctreeType.PrintOctree()
    
    dim i as integer
    
    static IndentCount as integer
    
    if this.HasChildren() = 0 then
        
        for i = 0 to IndentCount - 1
            print "+";
        next
        
        print "Leaf node ("; this.Index;") "; @this;" with parent ";this.Parent;":"
        for i = 0 to IndentCount - 1
            print " ";
        next
        if this.MaxElements <> 1 then
            print " Has ";this.BufferData->GetItemCount();" element(s)"
        else
            print " Has ";(1-abs(this.Data = 0));" element"
        end if
    end if
    
    if this.HasChildren() then
        
        for i = 0 to IndentCount - 1
            print "+";
        next
        
        IndentCount += 2
        
        if this.IsRoot() then
            print "From root:"; @this
        else
            print "Child node(";this.Index;") "; @this;" with parent ";this.Parent;":"
        end if
        
        for i = 0 to 7
            if this.Children[i] then
                this.Children[i]->PrintOctree()
            end if
        next
        IndentCount -= 2
        print
        
    end if
    
end sub

#ifdef __gl_h_

sub OctreeType.Render3DRecursive(ShaderHandle as GLuint, LeafNodesOnly as integer)

    dim i as integer
    
    dim scale(2) as single

    if this.HasChildren() then
        
        for i = 0 to 7
            if this.Children[i] then
                this.Children[i]->Render3DRecursive(ShaderHandle, LeafNodesOnly)
            end if
        next

    end if
    
    if LeafNodesOnly <> 0 then
        if this.MaxElements <> 1 then
            if this.BufferData = 0 then
                return
            end if
        else
            if this.Data = 0 then
                return
            end if
        end if
    end if
    
    scale(0) = this.AABBMax[0] - this.AABBMin[0]
    scale(1) = this.AABBMax[1] - this.AABBMin[1]
    scale(2) = this.AABBMax[2] - this.AABBMin[2]
    
    dim Offset(2) as single
    Offset(0) = this.AABBMin[0]
    Offset(1) = this.AABBMin[1]
    Offset(2) = this.AABBMin[2]
    
    SetUniformVector3(ShaderHandle, "offset", Offset())
    SetUniformVector3(ShaderHandle, "scale",  scale())
    SetUniformInt(ShaderHandle, "color",  this.GetDepth())
    
    glLineWidth(this.GetDepth())
    
    glDrawElements(GL_LINES, 24, GL_UNSIGNED_BYTE, 0)
    
end sub

sub OctreeType.Render3D(ShaderHandle as GLuint, ViewMat() as single, ProjMat() as single, LeafNodesOnly as integer)
    
    'The in place, one cut, no strings attached octree renderer.
    'It's not exactly "efficient".
    
    dim VAO as GLuint
    dim VBO as GLuint
    dim IBO as GLuint
    
    dim pVerticies(8*3-1) as GLfloat = _
        {0,0,0, 1,0,0, 1,1,0, 0,1,0, 0,0,1, 1,0,1, 1,1,1, 0,1,1}
        
    dim pIndicies(12*2-1) as GLubyte = _
        {0,1, 1,2, 2,3, 3,0, 0,4, 1,5, 2,6, 3,7, 4,5, 5,6, 6,7, 7,4}
    
    glGenBuffers(1, @IBO)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte)*24, @pIndicies(0), GL_STATIC_DRAW)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    
    glGenVertexArrays(1, @VAO)
    glBindVertexArray(VAO)
    
    glGenBuffers(1, @VBO)
    
    dim attributeName as zString * 12 = "in_position"
    
    dim attributeLocation as GLuint = glGetAttribLocation(ShaderHandle, @attributeName)
    glBindBuffer(GL_ARRAY_BUFFER, VBO)
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*24, @pVerticies(0), GL_STATIC_DRAW)
    glEnableVertexAttribArray(attributeLocation)
    glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, 0)

    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
    
    glUseProgram(ShaderHandle)
    
    SetUniformMatrix4(ShaderHandle, "viewMatrix", ViewMat())
    SetUniformMatrix4(ShaderHandle, "projMatrix", ProjMat())
    
    glBindVertexArray(VAO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO)

    this.Render3DRecursive(ShaderHandle, LeafNodesOnly)
    
    glLineWidth(1)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
    
    glUseProgram(0)
    
    glDeleteBuffers(1, @IBO)
    glDeleteBuffers(1, @VBO)
    
    glDeleteVertexArrays(1, @VAO)

end sub

#endif

Destructor OctreeType()

    dim i as integer
    
    if this.HasChildren() then
        for i = 0 to 7
            if this.Children[i] <> 0 then
                delete(this.Children[i])
                
                OctreeTypeCount -= 1
                
            end if
        next

        delete [] this.Children
        this.Children = 0

    end if
    
    'We're only deleting the node here (not the stored data)
    if this.MaxElements <> 1 then
        if this.BufferData then
            delete(this.BufferData)
        end if
    end if

end Destructor

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
/'
type stuff
    
    dim position(2) as single
    
    dim myName as string
    
end type

function Compare(ByRef inData as any ptr, AABBMin() as single, AABBMax() as single) as integer

    dim myNode as stuff ptr = cast(stuff ptr, inData)

    if (myNode->position(0) >= AABBMin(0)) AND (myNode->position(0) < AABBMax(0)) then
    if (myNode->position(1) >= AABBMin(1)) AND (myNode->position(1) < AABBMax(1)) then
    if (myNode->position(2) >= AABBMin(2)) AND (myNode->position(2) < AABBMax(2)) then

        return 1
       
    end if
    end if
    end if

    return 0

end function

function GetAABBIndex(ByRef inData as any ptr, _
                      AABBMin() as single, _
                      AABBMax() as single) as integer

    dim inNode as stuff ptr = cast(stuff ptr, inData)

    dim retIndex as uinteger
    
    dim as single xDiff, yDiff, zDiff
    
    xDiff = (AABBMax(0) - AABBMin(0))
    yDiff = (AABBMax(1) - AABBMin(1))
    zDiff = (AABBMax(2) - AABBMin(2))
    
    xDiff = (inNode->Position(0) - AABBMin(0)) / xDiff
    yDiff = (inNode->Position(1) - AABBMin(1)) / yDiff
    zDiff = (inNode->Position(2) - AABBMin(2)) / zDiff
    
    if (xDiff < 0) OR (yDiff < 0) OR (zDiff < 0) then
        return -1
    end if
    
    if (xDiff > 1) OR (yDiff > 1) OR (zDiff > 1) then
        return -1
    end if
    
    retIndex = retIndex OR (cast(uinteger, xDiff) SHL 2)
    retIndex = retIndex OR (cast(uinteger, yDiff) SHL 1)
    retIndex = retIndex OR (cast(uinteger, zDiff) SHL 0)

    return retIndex
    
end function

dim test as OctreeType ptr

dim leaf as OctreeType ptr

dim min(2) as single = {0,0,0}
dim max(2) as single = {10,10,10}

test = new OctreeType(min(), max(), 3, 1, @Compare)

test->SetGetIndexFunction(@GetAABBIndex)

dim inNode as stuff ptr

dim i as integer

dim as integer x,y,z,j

dim vert(2) as single

dim OctreeNode as OctreeType ptr

for i = 0 to 25

    
    vert(0) = rnd() * 9
    vert(1) = rnd() * 9
    vert(2) = rnd() * 9
    
    inNode = new stuff
    inNode->position(0) = vert(0)
    inNode->position(1) = vert(1)
    inNode->position(2) = vert(2)
    
    inNode->myName = "I'm node" + STR(i)
    
    test->Insert(inNode)

next

OctreeNode = test->GetChild(3)->GetChild(4)->GetChild(5)

test->PrintOctree()

print "OctreeNode = ";OctreeNode
print "Neighbor = "; OctreeNode->GetNeighbor(1,0,0)


print "Size of OctreeType: ";sizeof(OctreeType)
sleep

delete(test)
'/
#endif