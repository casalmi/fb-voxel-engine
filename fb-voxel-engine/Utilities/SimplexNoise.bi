/' Copyright (c) 2007-2012 Eliot Eshelman
 '
 ' This program is free software: you can redistribute it and/or modify
 ' it under the terms of the GNU General Public License as published by
 ' the Free Software Foundation, either version 3 of the License, or
 ' (at your option) any later version.
 '
 ' This program is distributed in the hope that it will be useful,
 ' but WITHOUT ANY WARRANTY; without even the implied warranty of
 ' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 ' GNU General Public License for more details.
 '
 ' You should have received a copy of the GNU General Public License
 ' along with this program. If not, see <http://www.gnu.org/licenses/>.
 '
 '/

'Converted to FreeBasic by Shadow008: 1/27/2014


#ifndef SimplexNoise_bi
#define SimplexNoise_bi

#include once "NoiseHeader.bi"

#define SIMPLEX_F2 .3660254037844
#define SIMPLEX_G2 .2113248654051

#define SIMPLEX_F3 .3333333333333
#define SIMPLEX_G3 .1666666666667

dim shared SimplexPerm(512) as integer
dim shared SimplexPermMOD12(512) as integer

sub InitSimplexPermMap()
    
    dim i as integer
    
    for i = 0 to 255
        SimplexPerm(i) = noise(i*GLOBAL_SEED, i*GLOBAL_SEED) MOD 255
        SimplexPerm(i + 256) = SimplexPerm(i)
    next
    
    for i = 0 to 511
        SimplexPermMOD12(i) = SimplexPerm(i) MOD 12
    next
    
end sub

InitSimplexPermMap()

dim shared GRAD3(11, 2) as byte = {_
{1,1,0},{-1,1,0},{1,-1,0},{-1,-1,0},{1,0,1},_
{-1,0,1},{1,0,-1},{-1,0,-1},{0,1,1},{0,-1,1},_
{0,1,-1},{0,-1,-1} }
 
function SimplexDot2( gx as integer, gy as integer, x as single, y as single ) as single
    return gx * x + gy * y
end function

function SimplexDot3(gx as integer, gy as integer, gz as integer, _
                     x as single, y as single, z as single) as single
    return gx * x + gy * y + gz * z
end function

function fastFloor(value as integer) as integer
    if value > 0 then
        return int(value)
    else
        return int(value) - 1
    endif
end function

function SimplexNoise2D(ByVal x as single, ByVal y as single) as single
    ' Noise contributions from the three corners
    dim as single n0, n1, n2

    ' Skew the input space to determine which simplex cell we're in
    'SIMPLEX_F2 = 0.5 * (sqrtf(3.0) - 1.0);
    
    ' Hairy factor for 2D
    dim s as single  = (x + y) * SIMPLEX_F2
    dim i as integer = int( x + s )
    dim j as integer = int( y + s )

    'float SIMPLEX_G2 = (3.0 - sqrtf(3.0)) / 6.0;
    dim t as single = (i + j) * SIMPLEX_G2
    
    ' Unskew the cell origin back to (x,y) space
    dim A0 as single = i-t
    dim B0 as single = j-t
    
    ' The x,y distances from the cell origin
    dim x0 as single = x-A0
    dim y0 as single = y-B0

    ' For the 2D case, the simplex shape is an equilateral triangle.
    ' Determine which simplex we are in.
    dim as integer i1, j1 ' Offsets for second (middle) corner of simplex in (i,j) coords
    if x0 > y0 then
        ' lower triangle, XY order: (0,0)->(1,0)->(1,1)
        i1=1
        j1=0
    else 
        ' upper triangle, YX order: (0,0)->(0,1)->(1,1)
        i1=0
        j1=1
    endif

    ' A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
    ' a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
    ' c = (3-sqrt(3))/6
    dim x1 as single = x0 - i1 + SIMPLEX_G2 ' Offsets for middle corner in (x,y) unskewed coords
    dim y1 as single = y0 - j1 + SIMPLEX_G2
    dim x2 as single = x0 - 1.0 + 2.0 * SIMPLEX_G2 ' Offsets for last corner in (x,y) unskewed coords
    dim y2 as single = y0 - 1.0 + 2.0 * SIMPLEX_G2

    ' Work out the hashed gradient indices of the three simplex corners
    dim ii as integer = i and 255
    dim jj as integer = j and 255
    dim gi0 as integer = SimplexPermMOD12(ii+SimplexPerm(jj))
    dim gi1 as integer = SimplexPermMOD12(ii+i1+SimplexPerm(jj+j1))
    dim gi2 as integer = SimplexPermMOD12(ii+1+SimplexPerm(jj+1))

    ' Calculate the contribution from the three corners
    dim t0 as single = 0.5 - x0*x0-y0*y0
    if t0 < 0 then
        n0 = 0.0
    else
        t0 *= t0
        n0 = t0 * t0 * (grad3(gi0, 0)* x0 + grad3(gi0, 1) * y0) ' (x,y) of grad3 used for 2D gradient
    endif

    dim t1 as single = 0.5 - x1*x1-y1*y1
    if t1 < 0 then
        n1 = 0.0
    else
        t1 *= t1
        n1 = t1 * t1 * (grad3(gi1, 0)* x1 + grad3(gi1, 1) * y1)
    endif

    dim t2 as single = 0.5 - x2*x2-y2*y2
    if t2 < 0 then
        n2 = 0.0
    else
        t2 *= t2
        n2 = t2 * t2 * (grad3(gi2, 0)* x2 + grad3(gi2, 1) * y2)
    endif

    ' Add contributions from each corner to get the final noise value.
    ' The result is scaled to return values in the interval [-1,1].
    return 70.0 * (n0 + n1 + n2)

end function


function SimplexNoise3D(ByVal x as single, ByVal y as single, ByVal z as single) as single
    
    dim as single n0, n1, n2, n3

    ' Skew the input space to determine which simplex cell we're in
    dim s as single = (x+y+z)*SIMPLEX_F3
    dim i as integer = int( x + s )
    dim j as integer = int( y + s )
    dim k as integer = int( z + s )

    dim t  as single = (i+j+k)*SIMPLEX_G3
    
    'Unskew the cell origin back to (x,y,z) space
    dim A0 as single = i-t
    dim B0 as single = j-t
    dim C0 as single = k-t
    
    'The x,y,z distances from the cell origin
    dim x0 as single = x-A0
    dim y0 as single = y-B0
    dim z0 as single = z-C0

    ' For the 3D case, the simplex shape is a slightly irregular tetrahedron.
    ' Determine which simplex we are in.
    dim as integer i1, j1, k1 ' Offsets for second corner of simplex in (i,j,k) coords
    dim as integer i2, j2, k2 ' Offsets for third corner of simplex in (i,j,k) coords

    if x0 >= y0 then
        
        if y0 >= z0 then
            i1=1: j1=0: k1=0: i2=1: j2=1: k2=0 ' X Y Z order
        elseif x0 >= z0 then
            i1=1: j1=0: k1=0: i2=1: j2=0: k2=1 ' X Z Y order
        else 
            i1=0: j1=0: k1=1: i2=1: j2=0: k2=1 ' Z X Y order
        end if
        
    else ' x0<y0
        
        if y0 < z0  then
            i1=0: j1=0: k1=1: i2=0: j2=1: k2=1 ' Z Y X order
        elseif x0 < z0 then
            i1=0: j1=1: k1=0: i2=0: j2=1: k2=1 ' Y Z X order
        else 
            i1=0: j1=1: k1=0: i2=1: j2=1: k2=0 ' Y X Z order
        end if
        
    end if

    ' A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
    ' a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
    ' a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
    ' c = 1/6.
    dim x1 as single = x0 - i1 + SIMPLEX_G3 ' Offsets for second corner in (x,y,z) coords
    dim y1 as single = y0 - j1 + SIMPLEX_G3
    dim z1 as single = z0 - k1 + SIMPLEX_G3
    dim x2 as single = x0 - i2 + 2.0*SIMPLEX_G3 ' Offsets for third corner in (x,y,z) coords
    dim y2 as single = y0 - j2 + 2.0*SIMPLEX_G3
    dim z2 as single = z0 - k2 + 2.0*SIMPLEX_G3
    dim x3 as single = x0 - 1.0 + 3.0*SIMPLEX_G3 ' Offsets for last corner in (x,y,z) coords
    dim y3 as single = y0 - 1.0 + 3.0*SIMPLEX_G3
    dim z3 as single = z0 - 1.0 + 3.0*SIMPLEX_G3

    ' Work out the hashed gradient indices of the four simplex corners
    dim ii as integer = i AND 255
    dim jj as integer = j AND 255
    dim kk as integer = k AND 255
    dim gi0 as integer = SimplexPermMOD12(ii+SimplexPerm(jj+SimplexPerm(kk)))
    dim gi1 as integer = SimplexPermMOD12(ii+i1+SimplexPerm(jj+j1+SimplexPerm(kk+k1)))
    dim gi2 as integer = SimplexPermMOD12(ii+i2+SimplexPerm(jj+j2+SimplexPerm(kk+k2)))
    dim gi3 as integer = SimplexPermMOD12(ii+1+SimplexPerm(jj+1+SimplexPerm(kk+1)))

    ' Calculate the contribution from the four corners
    dim t0 as single = 0.6 - x0*x0 - y0*y0 - z0*z0
    if t0 < 0 then
        n0 = 0.0
    else
        t0 *= t0
        n0 = t0 * t0 * (grad3(gi0, 0)* x0 + grad3(gi0, 1)* y0 + grad3(gi0, 2)* z0)
    end if

    dim t1 as single = 0.6 - x1*x1 - y1*y1 - z1*z1
    if t1 < 0 then
        n1 = 0.0
    else
        t1 *= t1
        n1 = t1 * t1 * (grad3(gi1, 0)* x1 + grad3(gi1, 1)* y1 + grad3(gi1, 2)* z1)
        'n1 = t1 * t1 * dot(grad3[gi1], x1, y1, z1)
    end if

    dim t2 as single = 0.6 - x2*x2 - y2*y2 - z2*z2
    if t2 < 0 then
        n2 = 0.0
    else
        t2 *= t2
        n2 = t2 * t2 * (grad3(gi2, 0)* x2 + grad3(gi2, 1)* y2 + grad3(gi2, 2)* z2)
        'n2 = t2 * t2 * dot(grad3[gi2], x2, y2, z2)
    end if

    dim t3 as single = 0.6 - x3*x3 - y3*y3 - z3*z3
    if t3 < 0 then
        n3 = 0.0
    else
        t3 *= t3
        n3 = t3 * t3 * (grad3(gi3, 0)* x3 + grad3(gi3, 1)* y3 + grad3(gi3, 2)* z3)
        'n3 = t3 * t3 * dot(grad3[gi3], x3, y3, z3)
    end if

    ' Add contributions from each corner to get the final noise value.
    ' The result is scaled to stay just inside [-1,1]
    return 32.0*(n0 + n1 + n2 + n3)

end function


function SimplexNoise(ByVal x as single, ByVal y as single) as single', ByVal per as single, ByVal iterations as integer, ByVal decStart as single) as single
    
    dim total as single = 0.0
    dim decimator as single = 16000.0'= decStart
    dim amplitude as single = 1.0
    dim i as integer
    dim ampSum as single = 0

    'with GlobalNoise.NC
    /'for i = 0 to iterations - 1                
    
        '.PerNoise = .FuncPtr[i]
        'total = total + (.PerNoise(SimplexNoise2D((x / decimator), (y / decimator))) * amplitude)
        total = total + (SimplexNoise2D((x / decimator), (y / decimator)) * amplitude)

        ampSum = ampSum + amplitude
        amplitude = amplitude * per

        decimator = decimator / 2.0
        
    next
    'end with

    total = total / ampSum
    '/
    
    for i = 0 to 20
        total = total + SimplexNoise2D(x / decimator, y / decimator) * amplitude
        decimator /= 2.0
        amplitude /= 2.0
    next

    return total
    
end function

#endif