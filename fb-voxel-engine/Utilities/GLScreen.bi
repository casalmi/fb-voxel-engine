    #ifndef __GLSCREEN_BI__
    #define __GLSCREEN_BI__

    #include once "fbgfx.bi"
    #include once "GL/gl.bi"
    #include once "GL/glext.bi"

    #define DEBUG_MODE 1

    #ifdef USE_DPRINT
    #define dprint(msg) if DEBUG_MODE = 1 then open err for output as #99 : print  #99,msg : close #99 : endif:
    #else
    #define dprint(msg) :
    #endif


    type VIDEOMODE
      as integer w,h,b,s,f ' width,height,bits,size,fullscreen
    end type
    type PVIDEOMODE  as VIDEOMODE  ptr

    type VideoModes
      declare constructor
      declare destructor
      declare sub Fit(byref w as integer, _
                      byref h as integer, _
                      byref b as integer, _
                      byref f as integer)
      as VIDEOMODE      Desktop
      as VIDEOMODE      Current
      as PVIDEOMODE ptr p15,p16,p24,p32
      as integer        n15,n16,n24,n32
    end type

    #macro GETMODES(X)
      Mode = ScreenList(X)
      if Mode then
        dprint("modes with " & #X & " bits:")
        While (Mode <> 0)
          ' expand the list for one member
          p##X = reallocate(p##X,(n##X+1)*sizeof(PVIDEOMODE))
          ' allocate item
          p##X[n##X] = allocate(sizeof(VIDEOMODE))
          p##X[n##X]->w = HiWord(Mode)
          p##X[n##X]->h = LoWord(Mode)
          p##X[n##X]->s = p##X[n##X]->w*p##X[n##X]->h
          p##X[n##X]->b = X
          dprint("mode: " & p##X[n##X]->w & " x " & p##X[n##X]->h & " x " & p##X[n##X]->b)
          n##X+=1
          Mode = ScreenList()
        wend
        dprint("")
      end if 
    #endmacro 

    #macro FREEMODES(X)
      if p##X then
        while n##X
          n##X-=1: if p##X[n##X] then deallocate p##X[n##X]
        wend
        deallocate p##X 
      end if
    #endmacro

    constructor VideoModes
      dim as integer Mode
      dprint("VideoModes()")
      With DeskTop
        dprint("")
        ScreenInfo .w,.h,.b
        .s = .w * .h
        dprint("Desktop " & .w & " x " & .h  & " x " & .b)
        dprint("")
      end with 
       
      GETMODES(15)
      GETMODES(16)
      GETMODES(24)
      GETMODES(32)
    end constructor

    destructor VideoModes
      dprint("~VideoModes")
      FREEMODES(15)
      FREEMODES(16)
      FREEMODES(24)
      FREEMODES(32)
    end destructor

    ' get nearest video mode params
    sub VideoModes.Fit(byref w as integer, _
                       byref h as integer, _
                       byref b as integer, _
                       byref f as integer)
      dim as integer size = w*h

      if b=32 andalso p32=0 then b=24
      if b=24 andalso p24=0 then b=16
      if b=16 andalso p16=0 then b=15
     
      if size>Desktop.s then f=1
      ' no fullscreen
      if f=0 then return
     
      dim as PVIDEOMODE ptr pList
      dim as integer        nModes
      select case b
        case 15 : pList = p15 : nModes = n15
        case 16 : pList = p16 : nModes = n16
        case 24 : pList = p24 : nModes = n24
        case 32 : pList = p32 : nModes = n32
      end select 
     
      ' search a fullscreen mode
      dim as integer diff = size
      dim as integer indx = -1
      for i as integer = 0 to nModes-1
        if abs(pList[i]->s - size)<diff then
          indx=i:diff=abs(pList[i]->s - size)
        end if
      next
      w=pList[indx]->w
      h=pList[indx]->h
    end sub                     

    dim shared as VideoModes Modes

    sub glScreen(w as integer=800, h as integer=600, b as integer=24, d as integer=16, s as integer=8, f as integer=0)
      if ScreenPtr() then screen 0
     
      dprint("wish: " & w & " x " & h & " x " & b  & " fullscreen " & f)
     
      dim as integer minW = iif(f<>0,640,320)
      dim as integer minH = iif(f<>0,480,200)
     
      if w<minW then w=minW
      if h<minH then h=minH
       
      if b<16 then
        b=15
      elseif b<24 then
        b=16
      elseif b<32 then
        b=24
      else
        b=32
      end if     
     
      if d<8 then
        d=0
      elseif d<16 then
        d=8
      elseif d<24 then
        d=16
      elseif d<32 then
        d=24
      else
        d=32
      end if

      if s<1 then
        s=0
      elseif s<8 then
        s=1
      elseif s<16 then
        s=8   
      else
        s=16
      end if
     
      Modes.Fit(w,h,b,f)
     
      dprint("fit: " & w & " x " & h & " x " & b & " fullscreen " & f)
     
      if s then ScreenControl FB.SET_GL_STENCIL_BITS,s
      if d then ScreenControl FB.SET_GL_DEPTH_BITS,d
      ScreenRes w,h,b,,FB.GFX_OPENGL or iif(f<>0,FB.GFX_FULLSCREEN,0)
      flip
      with Modes.Current
        ScreenInfo .w,.h,.b
        .f = iif(f<>0,1,0)
        dprint("final: " & .w & " x " & .h & " x " & .b & " fullscreen " & .f)
      end with 

    end sub
     
    #endif ' __GLSCREEN_BI__
