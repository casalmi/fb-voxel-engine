#ifndef Dynamic_Buffer_bi
#define Dynamic_Buffer_bi

#define RESIZE_DOUBLE -1

#macro CREATE_DYNAMIC_BUFFER_TYPE(THIS_DATA_TYPE)

type DynamicBuffer##THIS_DATA_TYPE##
    
    declare sub SetResizeStride(newResizeStride as integer)
    
    declare sub Resize(newSize as integer)
    declare function Push(inData as ##THIS_DATA_TYPE##) as integer
    declare function FastPush(inData as ##THIS_DATA_TYPE##) as integer
    declare sub Swp(indexA as integer, indexB as integer)
    declare sub ShrinkToSize()
    declare sub EmptyData()
    
    declare function Pop() as ##THIS_DATA_TYPE##
    declare function GetData(index as integer) as ##THIS_DATA_TYPE##
    declare sub      SetData(index as integer, inData as ##THIS_DATA_TYPE##)
    declare function GetDataSizeInBytes() as integer
    declare function GetArraySizeInBytes() as integer

    dim BufferData as ##THIS_DATA_TYPE## ptr
    dim Size as integer
    dim Count as integer
    dim ResizeStride as integer
    
    declare Constructor()
    declare Constructor(InitSize as integer)
    
    declare Destructor()
    
end type

Constructor DynamicBuffer##THIS_DATA_TYPE##(InitSize as integer)

    this.BufferData = new ##THIS_DATA_TYPE##[InitSize]
    this.ResizeStride = RESIZE_DOUBLE
    this.Size = InitSize
    this.Count = 0

end Constructor

Constructor DynamicBuffer##THIS_DATA_TYPE##()

    this.Constructor(64)

end Constructor

sub DynamicBuffer##THIS_DATA_TYPE##.SetResizeStride(newResizeStride as integer)
    
    this.ResizeStride = newResizeStride
    
end sub

sub DynamicBuffer##THIS_DATA_TYPE##.Resize(newSize as integer)

    if newSize < 0 then
        this.EmptyData()
        return
    end if

    dim i as integer
    dim upperBound as integer = this.Size
    
    if newSize <= this.Size then
        upperBound = newSize
    end if

    dim newArrayPtr as ##THIS_DATA_TYPE## ptr
    newArrayPtr = new ##THIS_DATA_TYPE##[newSize]

    for i = 0 to upperBound - 1
        newArrayPtr[i] = this.BufferData[i]
    next

    Delete [] this.BufferData
    this.BufferData = newArrayPtr
    
    this.Size = newSize

end sub

function DynamicBuffer##THIS_DATA_TYPE##.Push(inData as ##THIS_DATA_TYPE##) as integer

    
    if this.Count > this.Size - 1 then
        if this.ResizeStride = RESIZE_DOUBLE then
            this.Resize(this.Size * 2)
        else
            this.Resize(this.Size + this.ResizeStride)
        end if
    end if
    
    this.BufferData[this.Count] = inData

    this.Count += 1
    
    return (this.Count-1)
    
end function

'ASSUMES THE ARRAY IS LARGE ENOUGH FOR THE DATA
'DOES NO OUT OF BOUNDS CHECKING
function DynamicBuffer##THIS_DATA_TYPE##.FastPush(inData as ##THIS_DATA_TYPE##) as integer

    this.BufferData[this.Count] = inData
    
    this.Count += 1
    
    return (this.Count-1)
    
end function

sub DynamicBuffer##THIS_DATA_TYPE##.Swp(indexA as integer, indexB as integer)

    if indexA > this.Size - 1 OR indexA < 0 OR _
       indexB > this.Size - 1 OR indexB < 0 then
       
       return
       
    end if

    dim tempItem as ##THIS_DATA_TYPE##
    
    tempItem = this.BufferData[indexA]
    
    this.BufferData[indexA] = this.BufferData[indexB]
    this.BufferData[indexB] = tempItem

end sub

sub DynamicBuffer##THIS_DATA_TYPE##.ShrinkToSize()

    this.Resize(this.Count)

end sub

sub DynamicBuffer##THIS_DATA_TYPE##.EmptyData()

    'This "essentially" nullifies everything in the array
    this.Count = 0

end sub

function DynamicBuffer##THIS_DATA_TYPE##.Pop() as ##THIS_DATA_TYPE##

    if this.Count = 0 then
        return 0
    end if
    
    this.Count -= 1 '0 indexed arrays
    return this.BufferData[this.Count]

end function

function DynamicBuffer##THIS_DATA_TYPE##.GetData(index as integer) as ##THIS_DATA_TYPE##

    if index < 0 OR index > this.Size - 1 then
        return 0
    end if
    
    return this.BufferData[index]

end function

sub DynamicBuffer##THIS_DATA_TYPE##.SetData(index as integer, inData as ##THIS_DATA_TYPE##)

    if index < 0 OR index > this.Size - 1 then
        return
    end if
    
    this.BufferData[index] = inData

    if index > this.Count - 1 then
        this.Count += 1
    end if

end sub

function DynamicBuffer##THIS_DATA_TYPE##.GetDataSizeInBytes() as integer
    
    return this.Count * sizeof(##THIS_DATA_TYPE##)
    
end function

function DynamicBuffer##THIS_DATA_TYPE##.GetArraySizeInBytes() as integer
    
    return this.Size * sizeof(##THIS_DATA_TYPE##)
    
end function

Destructor DynamicBuffer##THIS_DATA_TYPE##()

    Delete [] this.BufferData

end Destructor

#endmacro

CREATE_DYNAMIC_BUFFER_TYPE(Single)

CREATE_DYNAMIC_BUFFER_TYPE(uByte)
CREATE_DYNAMIC_BUFFER_TYPE(Byte)
CREATE_DYNAMIC_BUFFER_TYPE(uShort)
CREATE_DYNAMIC_BUFFER_TYPE(Short)
CREATE_DYNAMIC_BUFFER_TYPE(Long)
CREATE_DYNAMIC_BUFFER_TYPE(uLong)
CREATE_DYNAMIC_BUFFER_TYPE(Integer)
CREATE_DYNAMIC_BUFFER_TYPE(uInteger)
CREATE_DYNAMIC_BUFFER_TYPE(GLuint)

#endif