'Returns a byte containing in fixed point decimal notation
'Max values -1 to 1, 6 bits decimal precision
'1 1.111111
'(sign 1)(integer 1).(decimal 6)
function compressFloatToByte(inVal as single) as ubyte
    
    dim i as integer
    dim retVal as ubyte
    
    'The sign
    if inVal < 0 then
        retVal = &h80
    end if
    
    'Guarantees the value is positive from this point on
    inVal = inVal * sgn(inVal)
    
    'We represent MAX_VALUE as one (or MIN_VALUE as -1)
    if inVal > 1 then
        return retVal OR &h40
    end if

    inVal = inVal - cast(integer, inVal)
    
    for i = 5 to 0 step -1
        inVal *= 2
        if inVal > 1 then
            retVal = retVal OR (&h01 SHL i)
            inVal -= 1
        end if
    next
    
    return retVal

end function

function decompressByteToFloat(inVal as ubyte) as single
	
	dim retVal as single = 0.0
	
    if inVal AND &h20 then retVal += 0.500000: end if
	if inVal AND &h10 then retVal += 0.250000: end if
    if inVal AND &h08 then retVal += 0.125000: end if
    if inVal AND &h04 then retVal += 0.062500: end if
    if inVal AND &h02 then retVal += 0.031250: end if
    if inVal AND &h01 then retVal += 0.015625: end if
	
	if inVal AND &h40 then retVal += 1: end if
			
	if inVal AND &h80 then retVal *= -1: end if
	
	return retVal

end function

'Returns a byte containing in fixed point decimal notation
'Max values -1 to 1, 6 bits decimal precision
'+-(1) (1).(111111111)(11111)

'(sign 1)(integer 1)(decimal 9)(undecided 5)
function compressFloatToShort(inVal as single) as ushort

	dim retVal as ushort = &h0000
	
    dim i as integer
    
	if inVal < 0 then
		retVal = &h8000
	end if
	
    if inVal < 0 then
        inVal *= -1
    end if

	if inVal > 1 then
		return retVal OR &h4000
	end if

	inVal = inVal - cast(integer, inVal)
	
	for i = 13 to 5 step -1
		inVal *= 2
		if inVal > 1 then
			retVal = retVal OR (1 SHL i)
			inVal -= 1
		end if
	next
	
	return retVal

end function

'Returns a value between -1 and 1
'with 9 bits of floating precision
function decompressShortToFloat(inVal as ushort) as single
	
	dim retVal as single = 0.0
	
	if inVal AND &h2000 then retVal += 0.500000: end if
	if inVal AND &h1000 then retVal += 0.250000: end if
	if inVal AND &h0800 then retVal += 0.125000: end if
	if inVal AND &h0400 then retVal += 0.062500: end if
	if inVal AND &h0200 then retVal += 0.031250: end if
	if inVal AND &h0100 then retVal += 0.015625: end if
	if inVal AND &h0080 then retVal += 0.0078125: end if
	if inVal AND &h0040 then retVal += 0.00390625: end if
	if inVal AND &h0020 then retVal += 0.001953125: end if
	
	'retVal += 1 * (inVal & &h4000)
	
	if inVal AND &h4000 then retVal += 1: end if
	
	'retVal *= (-1 * (inVal & &h8000)) + (inVal * !(inVal & &h8000))
	
	if inVal AND &h8000 then retVal *= -1: end if
	
	return retVal

end function 