#ifndef QuadraticErrorMetric_bi
#define QuadraticErrorMetric_bi

#define QRALGORITHM_N 3

type Plane3
    
    A as single
    B as single
    C as single
    D as single
    
end type

type DualContourQuadricErrorMetric
    
    dim as single aa, ab, ac, ad
    dim as single     bb, bc, bd
    dim as single         cc, cd
    dim as single             dd
    
    declare constructor()
    
    Declare sub Compute(planes() as Plane3, n as integer)
    Declare sub Optimize(massPoint() as single, v() as single)
    
    Declare function Evaluate(v() as single) as single

end type

constructor DualContourQuadricErrorMetric()

    aa = ab = ac = ad = bb = bc = bd = cc = cd = dd = 0.0
    
end constructor

function ScalarSign(a as single, b as single) as single
    
    if b < 0 then
        return a * -1
    else
        return a
    end if
    
end function

function pythag(a as single, b as single) as single

    dim as single absa, absb
    dim d as single
    absa = abs(a)
    absb = abs(b)

    if (absa > absb) then
        
        d = absb / absa
        d *= d
        return absa * sqr(1.0 + d)
        
    elseif(absb = 0.0) then
        
        return 0.0
        
    else

        d = absa / absb
        d *= d
        return absb * sqr(1.0 + d)
    end if
    
end function

sub QRAlgorithmTridiagonalize(a() as single, _
                              d() as single, _
                              e() as single)
/' Householder reduction of a real, symmetric matrix a[1..n][1..n].

 'On output, a is replaced by the orthogonal matrix Q effecting the 
 'transformation.
 'd[1..n] returns the diagonal elements of the tridiagonal matrix, 
 'and e[1..n] the off-diagonal elements, with e[1]=0. 
 
 'Several statements, as noted in comments, can be omitted if only eigenvalues 
 'are to be found, in which case a contains no useful information on output.
 'otherwise they are to be included.
 '/
    
    dim as integer l, k, j, i
    dim as single scale, hh, h , g, f

    ' i iterates all the rows in reverse order
    for i = QRALGORITHM_N - 1 to 1 step -1' (i = QRALGORITHM_N - 1; i >= 1; --i) 

        l = i-1
        h = 0.0
        scale = 0.0
        if l > 0 then

            for k = 0 to l'(k = 0; k <= l; ++k)

                scale += Abs(a(i,k))
                
            next
            
            ' Skip transformation.
            if scale = 0.0 then
                
                e(i) = a(i,l)
                
            else 

                for k = 0 to l'(k = 0; k <= l; ++k) 

                    ' Use scaled a's for transformation.
                    a(i,k) /= scale 
                    ' Form s in h.
                    h += a(i,k) * a(i,k) 
                
                next
                
                f = a(i,l)
                if f < 0 then
                    g = sqr(h)
                else
                    g = sqr(h) * -1
                end if
                'g = Scalar::IsNegative(f) ? sqrt(h) : -sqrt(h)
                
                e(i) = scale * g
                ' Now h is equation (11.2.4).
                h -= f * g
                ' Store u in the ith row of a.
                a(i,l) = f - g
                f = 0.0
                
                for j = 0 to l'(j=0; j <= l; ++j) 

                    ' Next statement can be omitted if eigenvectors not wanted
                    ' Store u/H in ith column of a.
                    a(j,i) = a(i,j) / h
                    ' Form an element of A ? u in g.
                    g=0.0
                    for k = 0 to j'( k= 0; k <= j; ++k)

                        g += a(j,k) * a(i,k)
                    next
                    
                    for k = j+1 to l'(k = j + 1; k <= l; ++k)

                        g += a(k,j) * a(i,k)
                    next
                    
                    ' Form element of p in temporarily unused element of e. 
                    e(j)=g/h

                    f += e(j) * a(i,j)
                next
                
                ' Form K, equation (11.2.11). 
                hh = f /(h + h)
                ' Form q and store in e overwriting p. 
                for j = 0 to l'(j = 0;j <= l; ++j) 
                
                    f=a(i,j)
                    g = e(j) - (hh * f)
                    e(j) = g
                    'e(j) = g = e(j) - (hh * f)
                    ' Reduce a, equation (11.2.13). 
                    for k = 0 to j'(k = 0; k <= j; ++k)
                    
                        a(j,k) -= (f * e(k) + g * a(i,k))
                    
                    next
                next
                
            end if

        else

            e(i) = a(i,l)
            
        end if
        
        d(i) = h
    next
    
    ' Next statement can be omitted if eigenvectors not wanted 
    d(0) = 0.0
    e(0) = 0.0
    ' Contents of this loop can be omitted if eigenvectors not
    ' wanted except for statement d[i]=a(i][i]; 
    ' Begin accumulation of transformation matrices. 
    for i = 0 to QRALGORITHM_N - 1'(i = 0; i < QRALGORITHM_N; ++i) 

        l=i-1
        ' This block skipped when i=0.
        if d(i) then
            for j = 0 to l'(j = 0; j <= l; ++j) 

                g = 0.0
                'Use u and u/H stored in a to form P?Q.
                for k = 0 to l'(k = 0; k <= l; ++k) 

                    g += a(i,k) * a(k,j)
                next
                
                for k = 0 to l'(k = 0; k <= l; ++k)
                
                    a(k,j) -= g * a(k,i)
                next
            next
        end if
        ' This statement remains. 
        d(i) = a(i,i)
        ' Reset row and column of a to identity 
        a(i,i) = 1.0
        'matrix for next iteration.  
        for j = 0 to l'(j = 0; j <= l; ++j) 

            a(j,i) = a(i,j) = 0.0
            
        next
    next
end sub

sub QRAlgorithmDiagonalize (d() as single, _
                            e() as single, _
                            z() as single)
/'
 'QL algorithm with implicit shifts, to determine the eigenvalues 
 'and eigenvectors of a real, symmetric, tridiagonal matrix, or of a real, 
 'symmetric matrix previously reduced by tred2 11.2. 
 '
 'On input, d[1..n] contains the diagonal elements of the tridiagonal matrix. 
 'On output, it returns the eigenvalues. 
 '
 'The vector e[1..n] inputs the subdiagonal elements of the tridiagonal matrix,
 'with e[1] arbitrary. On output e is destroyed. 
 '
 'When finding only the eigenvalues, several lines
 'may be omitted, as noted in the comments. 
 '
 'If the eigenvectors of a tridiagonal matrix are desired,
 'the matrix z[1..n][1..n] is input as the identity matrix. 
 'If the eigenvectors of a matrix
 'that has been reduced by tred2 are required, 
 'then z is input as the matrix output by tred2.
 'In either case, the kth column of z returns the normalized 
 'eigenvector corresponding to d[k]. 
 '/

    dim as integer m,l,iter,i,k

    dim as single s,r,p,g,f,dd,c,b

    for i = 1 to QRALGORITHM_N - 1'(i = 1; i < QRALGORITHM_N; ++i)

        e(i-1)=e(i)
    next
    ' Convenient to renumber the elements of e. 
    e(QRALGORITHM_N - 1)=0.0
    
    for l = 0 to QRALGORITHM_N - 1'(l = 0; l < QRALGORITHM_N; ++l)

        iter=0
        do
            for m = l to QRALGORITHM_N - 1'(m = l; m < QRALGORITHM_N-1; ++m)

                ' Look for a single small subdiagonal 
                ' element to split the matrix. 
                dd = Abs(d(m)) + Abs(d(m+1))
                if dd = (Abs(e(m)) + dd) then
                    exit do 'Break
                end if
            next
            
            if m <> l then
                
                iter += 1
                
                if (iter >= 30) then

                    'assert(0) ' Too many iterations
                    dprint("Too many iterations")
                    exit do
                    
                end if
                
                g=(d(l+1)-d(l))/(2.0*e(l)) ' Form shift. 
                r=pythag(g,1.0)
                g=d(m)-d(l)+e(l)/(g+ScalarSign(r,g)) ' This is dm - ks. 
                s=1.0
                c=1.0
                p=0.0

                for i = m - 1 to l step -1'(i = m - 1; i >= l; --i) 
                    
                    /'A plane rotation as in the original QL, 
                     'followed by Givens rotations to restore 
                     'tridiagonal form. 
                     '/
                    
                    f=s*e(i)
                    b=c*e(i)
                    'e[i + 1] = (r = pythag(f, g))
                    r = pythag(f,g)
                    e(i + 1) = r
                    
                    if r = 0.0 then 
                        ' Recover from underflow. 
                        d(i+1) -= p
                        e(m)=0.0
                        exit do
                    end if
                    
                    s=f/r
                    c=g/r
                    g=d(i+1)-p
                    r=(d(i)-g)*s+2.0*c*b
                    'd[i+1]=g+(p=s*r)
                    p=s*r
                    d(i+1)=g+p
                    g=c*r-b
                     
                    'Next loop can be omitted if eigenvectors not wanted 
                    
                    for k = 0 to QRALGORITHM_N - 1'(k = 0; k < QRALGORITHM_N; ++k)

                        'Form eigenvectors. 
                        f = z(k,i+1)
                        z(k,i + 1) = (s * z(k,i)) + (c * f)
                        z(k,i)     = (c * z(k,i)) - (s * f)
                    next
                
                next
                
                if (r = 0.0) AND (i >= l) then
                    continue do
                end if
                
                d(l) -= p
                e(l) = g
                e(m) = 0.0
            end if
            
        loop while (m <> l)
    next
end sub

/'
 'Solve A * x = b 
 'using SVD: A = U * D * VT
 'U and V are orthogonal and are inverted by transposing their values
 'D is a diagonal matrix and is inverted by inverting the diagonal values
 'If A is singular, then one or more of the diagonal values will be zero.
 'A * x = b = U * D * VT * x
 'x = V * inv(D) * UT * b
 '@param u The input m x n svd matrix U
 '@param v The input m x n svd matrix V
 '@param d The diagonals of the svd
 '@param b The [1..m] vector b in the equation A * x = b
 '@param x The [1..n] solution vector that stores the result
 '/
sub QRAlgorithmSolve(u() as single, _
                     v() as single, _
                     d() as single, _
                     b() as single, _
                     x() as single)

    dim as integer j, jj ' j iterates columns 
    dim as integer i     ' i iterates rows 
    dim as single s, tmp(QRALGORITHM_N - 1)
    
    for j = 0 to QRALGORITHM_N - 1'(j = 0; j < QRALGORITHM_N; ++j)

        ' Calculate UTB. 
        s = 0.0
        if Abs(d(j)) > 0.001 then'!Scalar::IsZero(d(j])) 

            ' Nonzero result only if wj is nonzero. 
            for i = 0 to QRALGORITHM_N - 1'(i = 0; i < QRALGORITHM_N; ++i)
                
                s += u(i,j) * b(i)
            next
            
            s /= d(j) ' Divide by wj . 
        end if
        
        tmp(j) = s
    next

    for j = 0 to QRALGORITHM_N - 1'(j = 0; j < QRALGORITHM_N; ++j) 

        ' Matrix multiply by V to get answer. 
        s = 0.0
        for jj = 0 to QRALGORITHM_N - 1'(jj = 0 ; jj < QRALGORITHM_N; ++jj)

            s += v(j,jj) * tmp(jj)
        next
        
        x(j) = s
    next
end sub

sub DualContourQuadricErrorMetricCompute(qem as DualContourQuadricErrorMetric ptr, _
                                         planes() as Plane3, _
                                         n as integer)

    /'
     ' 0 = ni.(v - vi)
     ' 0 = n.v - n.vi
     ' nv = n.vi
     '
     ' (a, b, c).(x, y, z) - (a, b, c).(xi, yi, zi) = 0
     '
     '***********************************************************************
     '
     ' A = |  a  b  c |   x = | x  y  z |   B = | (a,  b,  c).(xi, yi, zi) | 
     '     |  a  b  c |                         | (a,  b,  c).(xi, yi, zi) |
     '     |  a  b  c |                         | (a,  b,  c).(xi, yi, zi) | 
     '     |  .  .  . |                         |  .                       |          
     ' 
     ' Ax = B
     '
     ' 0 = Ax - B
     ' 
     ' Q = (Ax - B)T(Ax - B)
     ' 
     ' Q = xATAx - 2xAB + B.B
     ' 
     '/
    
    qem->aa = qem->ab = qem->ac = qem->ad = _
              qem->bb = qem->bc = qem->bd = _
                        qem->cc = qem->cd = _
                                  qem->dd = 0.0
    dim as single a, b, c, d
    dim i as integer
    
    for i = 0 to n - 1'(int i = 0; i < n; ++i)
        
        a = planes(i).A
        b = planes(i).B
        c = planes(i).C
        d = planes(i).D

        qem->aa += a*a: qem->ab += a*b: qem->ac += a*c: qem->ad += a*d
        qem->bb += b*b: qem->bc += b*c: qem->bd += b*d:
        qem->cc += c*c: qem->cd += c*d:
        qem->dd += d*d
        
        'dprint("Compute: ";qem->aa;" ";qem->ab;" ";qem->ac;" ";qem->ad)
    
    next
    
end sub

function DualContourQuadricErrorMetricEvaluate(qem as DualContourQuadricErrorMetric ptr, _
                                               v() as single) as single

    /' 
     ' A = |  a  b  c |   x = | x  y  z |   B = | (a,  b,  c).(xi, yi, zi) | 
     '     |  a  b  c |                         | (a,  b,  c).(xi, yi, zi) |
     '     |  a  b  c |                         | (a,  b,  c).(xi, yi, zi) | 
     '     |  .  .  . |                         |  .                       |          
     ' 
     ' Ax = B
     '
     ' 0 = Ax - B
     ' 
     ' Q = (Ax - B)T(Ax - B)
     ' 
     ' Q = xATAx - 2xAB + B.B
     ' 
     '/
    return  (v(0) * v(0) *        qem->aa) + _
            (2.0f * v(0) * v(1) * qem->ab) + _
            (2.0f * v(0) * v(2) * qem->ac) + _
            (2.0f * v(0) *        qem->ad) + _
            (v(1) * v(1) *        qem->bb) + _
            (2.0f * v(1) * v(2) * qem->bc) + _
            (2.0f * v(1) *        qem->bd) + _
            (v(2) * v(2) *        qem->cc) + _
            (2.0f * v(2) *        qem->cd) + _
                                 (qem->dd)

end function

sub DualContourQuadricErrorMetricOptimize(qem as DualContourQuadricErrorMetric ptr, _
                                          massPoint() as single, v() as single)

    /' 
     ' A = |  a  b  c |   x = | x  y  z |   B = | (a,  b,  c).(xi, yi, zi) | 
     '     |  a  b  c |                         | (a,  b,  c).(xi, yi, zi) |
     '     |  a  b  c |                         | (a,  b,  c).(xi, yi, zi) | 
     '     |  .  .  . |                         |  .                       |          
     ' 
     ' Ax = B
     ' Ax = B
     '
     ' 0 = Ax - B
     ' Q = (Ax - B)T(Ax - B)
     ' Q = xATAx - 2xAB + B.B
     ' 
     '***********************************************************************
     '
     ' d f(x)/dx = 2ATAx - 2AB
     ' d f(x)/dx = ATAx - AB
     ' 0 = ATAx - AB
     ' AB = ATAx
     ' x = inv(ATA)AB
     '/
    /' DECOMPOSITION USING EIGEN CALCULATIONS THROUGH QR ALGORITHM
     ' ROUGHLY 1/2 THE RUNNING TIME OF GENERALIZED SVD 
     '/

    dim as single a(2,2), b(2)
    a(0, 0) = qem->aa: a(0, 1) = qem->ab: a(0, 2) = qem->ac
    a(1, 0) = qem->ab: a(1, 1) = qem->bb: a(1, 2) = qem->bc
    a(2, 0) = qem->ac: a(2, 1) = qem->bc: a(2, 2) = qem->cc
    b(0) = -qem->ad: b(1) = -qem->bd: b(2) = -qem->cd
    dim as single d(2), e(2)
    dim as single p(2)

    p(0) = massPoint(0)*qem->aa + massPoint(1)*qem->ab + massPoint(2)*qem->ac
    p(1) = massPoint(0)*qem->ab + massPoint(1)*qem->bb + massPoint(2)*qem->bc
    p(2) = massPoint(0)*qem->ac + massPoint(1)*qem->bc + massPoint(2)*qem->cc

    b(0) -= p(0)
    b(1) -= p(1)
    b(2) -= p(2)
    
    'dprint("running tri diagonalize")
    QRAlgorithmTridiagonalize(a(), d(), e())
    'dprint("a = ";a(0,0);" ";a(1,0);" ";a(2,0))
    'dprint("running diagonalize")
    QRAlgorithmDiagonalize(d(), e(), a())
    'dprint("running solve")
    QRAlgorithmSolve(a(), a(), d(), b(), v())
    'dprint("done")

    v(0) += massPoint(0)
    v(1) += massPoint(1)
    v(2) += massPoint(2)

end sub

sub DualContourQuadricErrorMetric.Compute(planes() as Plane3, n as integer)
    
    DualContourQuadricErrorMetricCompute(@this, planes(), n)
    
end sub

sub DualContourQuadricErrorMetric.Optimize(massPoint() as single, v() as single)
    
    DualContourQuadricErrorMetricOptimize(@this, massPoint(), v())
    
end sub

function DualContourQuadricErrorMetric.Evaluate(v() as single) as single
    return DualContourQuadricErrorMetricEvaluate(@this, v())
end function

#endif