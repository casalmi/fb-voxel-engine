#ifndef WaitTimer_bi
#define WaitTimer_bi

dim shared G_WaitTimerCount as double

G_WaitTimerCount = Timer

sub WaitTimer(Delay as double)

    if Delay < 0 then Delay = 0:endif
    if Delay > 5000 then Delay = 5000:endif
    
    G_WaitTimerCount += Delay / 1000.0

    dim TickCount as double = Timer
    if TickCount > G_WaitTimerCount then
        G_WaitTimerCount = TickCount
    end if

    dim Diff as integer = (G_WaitTimerCount  - TickCount) * 1000

    if Diff < 0 then Diff = 0:endif

    Sleep(Diff, 1)
    
    G_WaitTimerCount = Timer
    
end sub

#endif