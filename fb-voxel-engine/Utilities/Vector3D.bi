#ifndef Vector3DType_bi
#define Vector3DType_bi

type Vector3DType
    
    public:
        
        declare Constructor(initX as single, initY as single, initZ as single)
        declare Constructor(vector() as single)
        declare Constructor(value as single)
        declare Constructor()
        
        declare Destructor()
        
        declare sub Normalize()
        
        declare function Dot overload () as single
        declare function Dot          (otherVec as Vector3DType) as single
        
        declare function CrossProduct(otherVec as Vector3DType) as Vector3DType
        
        declare function Length() as single
        
        declare function X() as single
        declare function Y() as single
        declare function Z() as single
        
        declare sub SetX(inX as single)
        declare sub SetY(inY as single)
        declare sub SetZ(inZ as single)
        
        declare Operator += (ByRef rightSide as Vector3DType)
        declare Operator -= (ByRef rightSide as Vector3DType)
        
        declare Operator *= (ByRef rightSide as Vector3DType)
        declare Operator /= (ByRef rightSide as Vector3DType)
        
        declare Operator Let (ByRef rightSide as Vector3DType)
        declare Operator Let (value as single)
        declare Operator Let (value as single ptr)
        declare Operator Let (value as integer)
        declare Operator Let (value as uinteger)
        
        declare Operator [] (index as integer) ByRef as single
        
        declare Operator Cast() as String
        
    private:
        
        declare function FastInvSqrt(inVal as single) as single
        
        dim Vec(2) as single
    
end type

Constructor Vector3DType(initX as single, initY as single, initZ as single)

    Vec(0) = initX : Vec(1) = initY : Vec(2) = initZ
    
end constructor

Constructor Vector3DType(vector() as single)
    
    this.Constructor(vector(0), vector(1), vector(2))

end constructor

Constructor Vector3DType(value as single)
    
    this.Constructor(value, value, value)
    
end constructor

Constructor Vector3DType()
    this.Constructor(0.0, 0.0, 0.0)
end constructor

function Vector3DType.FastInvSqrt(inVal as single) as single
    
    dim valHalf as single = .5 * inVal
    dim i as long = *cast(long ptr, @inVal)
    
    i = &h5f3759df - (i SHR 1)
    inVal = *cast(single ptr, @i)
    
    inVal *= 1.5 - valHalf*inVal*inVal
    
    return inVal
    
end function

sub Vector3DType.Normalize()
    
    'This way is significantly faster but produces more error on larger values
    dim normal as single = FastInvSqrt(Vec(0) * Vec(0) + _
                                       Vec(1) * Vec(1) + _
                                       Vec(2) * Vec(2))
    
    
    Vec(0) *= normal
    Vec(1) *= normal
    Vec(2) *= normal
    
    
    /'dim normal as single = sqr(Vec(0) * Vec(0) + _
                                Vec(1) * Vec(1) + _
                                Vec(2) * Vec(2))
    
    if normal > 0.00001 then
        Vec(0) /= normal
        Vec(1) /= normal
        Vec(2) /= normal
    end if
    '/
end sub

function Vector3DType.Dot() as single
    
    return (Vec(0)*Vec(0)) + (Vec(1)*Vec(1)) + (Vec(2)*Vec(2))
    
end function

function Vector3DType.Dot(otherVec as Vector3DType) as single
    
    return (Vec(0)*otherVec.Vec(0)) + (Vec(1)*otherVec.Vec(1)) + (Vec(2)*otherVec.Vec(2))
    
end function

function Vector3DType.CrossProduct(otherVec as Vector3DType) as Vector3DType
    
    dim result as Vector3DType

    result.Vec(0) = this.Vec(1) * otherVec.Vec(2) - otherVec.Vec(1) * this.Vec(2)
    result.Vec(1) = this.Vec(2) * otherVec.Vec(0) - otherVec.Vec(2) * this.Vec(0)
    result.Vec(2) = this.Vec(0) * otherVec.Vec(1) - otherVec.Vec(0) * this.Vec(1)

    return result
    
end function

function Vector3DType.Length() as single
    
    return sqr(this.Dot())
    
end function

function Vector3DType.X() as single
    return this.Vec(0)
end function

function Vector3DType.Y() as single
    return this.Vec(1)
end function

function Vector3DType.Z() as single
    return this.Vec(2)
end function

sub Vector3DType.SetX(inX as single)
    this.Vec(0) = inX
end sub

sub Vector3DType.SetY(inY as single)
    this.Vec(1) = inY
end sub

sub Vector3DType.SetZ(inZ as single)
    this.Vec(2) = inZ
end sub

Operator + (ByRef leftSide as Vector3DType, ByRef rightSide as Vector3DType) as Vector3DType
    return Vector3DType(leftSide.X()+rightSide.X(), leftSide.Y()+rightSide.Y(), leftSide.Z()+rightSide.Z())
end operator

Operator + (ByRef leftSide as Vector3DType, Scalar as single) as Vector3DType
    return Vector3DType(leftSide.X()+Scalar, leftSide.Y()+Scalar, leftSide.Z()+Scalar)
end operator

Operator + (ByRef leftSide as Vector3DType, Scalar as integer) as Vector3DType
    return Vector3DType(leftSide.X()+Scalar, leftSide.Y()+Scalar, leftSide.Z()+Scalar)
end operator

Operator Vector3DType.+=(ByRef rightSide as Vector3DType)
    this.Vec(0) += rightSide.Vec(0) : this.Vec(1) += rightSide.Vec(1) : this.Vec(2) += rightSide.Vec(2)
end operator

Operator - (ByRef leftSide as Vector3DType, ByRef rightSide as Vector3DType) as Vector3DType
    return Vector3DType(leftSide.X()-rightSide.X(), leftSide.Y()-rightSide.Y(), leftSide.Z()-rightSide.Z())
end operator

Operator - (ByRef leftSide as Vector3DType, Scalar as single) as Vector3DType
    return Vector3DType(leftSide.X()-Scalar, leftSide.Y()-Scalar, leftSide.Z()-Scalar)
end operator

Operator - (ByRef leftSide as Vector3DType, Scalar as integer) as Vector3DType
    return Vector3DType(leftSide.X()-Scalar, leftSide.Y()-Scalar, leftSide.Z()-Scalar)
end operator

'Negate
Operator - (ByRef leftSide as Vector3DType) as Vector3DType
    return Vector3DType(leftSide.X() * -1, leftSide.Y() * -1, leftSide.Z() * -1)
end operator

Operator Vector3DType.-=(ByRef rightSide as Vector3DType)
    this.Vec(0) -= rightSide.Vec(0) : this.Vec(1) -= rightSide.Vec(1) : this.Vec(2) -= rightSide.Vec(2)
end operator

Operator * (ByRef leftSide as Vector3DType, ByRef rightSide as Vector3DType) as Vector3DType
    return Vector3DType(leftSide.X()*rightSide.X(), leftSide.Y()*rightSide.Y(), leftSide.Z()*rightSide.Z())
end operator

Operator * (ByRef leftSide as Vector3DType, Scalar as single) as Vector3DType
    return Vector3DType(leftSide.X()*Scalar, leftSide.Y()*Scalar, leftSide.Z()*Scalar)
end operator

Operator * (ByRef leftSide as Vector3DType, Scalar as integer) as Vector3DType
    return Vector3DType(leftSide.X()*Scalar, leftSide.Y()*Scalar, leftSide.Z()*Scalar)
end operator

Operator Vector3DType.*=(ByRef rightSide as Vector3DType)
    this.Vec(0) *= rightSide.Vec(0) : this.Vec(1) *= rightSide.Vec(1) : this.Vec(2) *= rightSide.Vec(2)
end operator

Operator / (ByRef leftSide as Vector3DType, ByRef rightSide as Vector3DType) as Vector3DType
    
    dim divisor(2) as single = {1.0, 1.0, 1.0}
    
    if (abs(rightSide.X()) > .000001) then
        divisor(0) = rightSide.X()
    end if
    if (abs(rightSide.Y()) > .000001) then
        divisor(1) = rightSide.Y()
    end if
    if (abs(rightSide.Z()) > .000001) then
        divisor(2) = rightSide.Z()
    end if
    
    return Vector3DType(leftSide.X()/divisor(0), leftSide.Y()/divisor(1), leftSide.Z()/divisor(2))

end operator

Operator / (ByRef leftSide as Vector3DType, Scalar as single) as Vector3DType

    if abs(Scalar) > 0.000001 then
        return Vector3DType(leftSide.X()/Scalar, leftSide.Y()/Scalar, leftSide.Z()/Scalar)
    else
        return leftSide
    end if

end operator

Operator / (ByRef leftSide as Vector3DType, Scalar as integer) as Vector3DType
    
    if Scalar <> 0 then
        return Vector3DType(leftSide.X()/Scalar, leftSide.Y()/Scalar, leftSide.Z()/Scalar)
    else
        return leftSide
    end if

end operator

Operator Vector3DType./=(ByRef rightSide as Vector3DType)
    if (abs(rightSide.Vec(0)) > .000001) then
        this.Vec(0) /= rightSide.Vec(0)
    end if
    if (abs(rightSide.Vec(1)) > .000001) then
        this.Vec(1) /= rightSide.Vec(1)
    end if
    if (abs(rightSide.Vec(2)) > .000001) then
        this.Vec(2) /= rightSide.Vec(2)
    end if
end operator

Operator = (ByRef leftSide as Vector3DType, ByRef rightSide as Vector3DType) as integer
    return (leftSide.X() = rightSide.X()) AND (leftSide.Y() = rightSide.Y()) AND (leftSide.Z() = rightSide.Z())
end operator

Operator = (ByRef leftSide as Vector3DType, Value as single) as integer
    return (leftSide.X() = Value) AND (leftSide.Y() = Value) AND (leftSide.Z() = Value)
end operator

Operator Vector3DType.Let(ByRef rightSide as Vector3DType)
    this.Vec(0) = rightSide.X() : this.Vec(1) = rightSide.Y() : this.Vec(2) = rightSide.Z()
end operator

Operator Vector3DType.Let(value as single)
    this.Vec(0) = value : this.Vec(1) = value : this.Vec(2) = value
end operator

Operator Vector3DType.Let(value as single ptr)
    this.Vec(0) = value[0] : this.Vec(1) = value[1] : this.Vec(2) = value[2]
end operator

Operator Vector3DType.Let(value as integer)
    this.Vec(0) = value : this.Vec(1) = value : this.Vec(2) = value
end operator

Operator Vector3DType.Let(value as uinteger)
    this.Vec(0) = value : this.Vec(1) = value : this.Vec(2) = value
end operator

Operator Vector3DType.[] (index as integer) ByRef as single

    return Vec(index)
    
end operator

Operator Vector3DType.cast() as String
    return "("&Vec(0)&", "&Vec(1)&", "&Vec(2)&")"
end operator

Destructor Vector3DType()
end destructor

#endif