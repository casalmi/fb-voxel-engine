

type BitArrayType

    public:
        declare Constructor(size as integer)
        declare Destructor()
        
        declare Operator [] (ByVal index as uinteger) as ubyte
        
        declare sub Set overload (inBit as ubyte, index as integer)
        
        declare sub Set          (inBits as ubyte ptr, StartIndex as integer, BitCount as integer)
        
        declare function Get(Index as integer) as ubyte
        declare sub      Get(ByteArray as ubyte ptr, StartIndex as integer, BitCount as integer)
        
        dim Array as ubyte ptr
    private:
    
        
        dim ArraySize as integer
        dim BitCount as integer
    
end type

Constructor BitArrayType(size as integer)
    
    ArraySize = int(size / 8) + 1
    BitCount = size
    Array = new ubyte[int(size / 8) + 1]
    
end Constructor

Operator BitArrayType.[] (ByVal index as uinteger) as ubyte

    if (index > BitCount) then
        return 0
    end if

    return Sgn(Array[index SHR 3] AND (1 SHL (index AND 7)))
    
end Operator

sub BitArrayType.Set(inBit as ubyte, index as integer)
        
    dim ByteIndex as integer = index SHR 3

    if (ByteIndex < 0) OR (index >= BitCount) then
        return
    end if
    
    dim ShiftAmt as integer = (index AND 7)

    inBit = sgn(inBit)
    
    Array[ByteIndex] = Array[ByteIndex] AND (NOT (inBit SHL ShiftAmt))
    Array[ByteIndex] = Array[ByteIndex] OR (inBit SHL ShiftAmt)
    
end sub

function BitArrayType.Get(Index as integer) as ubyte
    
    return this[index]
    
end function

sub BitArrayType.Get(ByteArray as ubyte ptr, StartIndex as integer, BitCount as integer)
    
    dim ByteIndex as integer = StartIndex SHR 3

    if (StartIndex < 0) OR (StartIndex + BitCount >= this.BitCount) then
        return
    end if
    
    dim RetByte as ubyte
    dim RetIndex as integer = 0
    dim Count as byte = 0
    
    dim i as integer
    
    for i = 0 to BitCount - 1
        RetByte = RetByte OR this[i + StartIndex] SHL Count
        Count += 1
        
        if Count = 8 then
            ByteArray[RetIndex] = RetByte
            Count = 0
            RetByte = 0
            RetIndex += 1
        end if
    next

    if Count <> 0 then
        ByteArray[RetIndex] = RetByte
    end if
    
end sub

Destructor BitArrayType()

    delete [] Array
    
end Destructor
/'
dim test as BitArrayType = BitArrayType(27)

dim ByteArray(3) as ubyte

dim as integer i,j

for i = 0 to 5*8-1
    'if i MOD 2 then
        test.Set(1, i)
    'end if
next

for i = 0 to 4
    for j = 0 to 7
        print test.get(i*8+j);
    next
    print
next

test.Get(@ByteArray(0), 0, 5)
print
for i = 0 to 3
    print Bin(ByteArray(i),8)
next

sleep'/