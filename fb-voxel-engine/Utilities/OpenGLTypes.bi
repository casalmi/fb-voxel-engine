
#ifndef OpenGLTypes_bi
#define OpenGLTypes_bi

type OpenGLObjectType
    
    'All public, this is more of a container

    dim VAO as GLuint
    
    dim IndexIBO  as GLuint
    dim VertexVBO as GLuint
    dim NormalVBO as GLuint
    
    declare Constructor()
    
    declare Destructor()
    
end type

Constructor OpenGLObjectType()

    VAO = 0
    IndexIBO = 0
    VertexVBO = 0
    NormalVBO = 0

end Constructor

Destructor OpenGLObjectType()

    if this.IndexIBO then
        glDeleteBuffers(1, @this.IndexIBO)
        this.IndexIBO = 0
    end if
    
    if this.VertexVBO then
        glDeleteBuffers(1, @this.VertexVBO)
        this.VertexVBO = 0
    end if
    
    if this.NormalVBO then
        glDeleteBuffers(1, @this.NormalVBO)
        this.NormalVBO = 0
    end if
    
    if this.VAO then
        glDeleteVertexArrays(1, @this.VAO)
        this.VAO = 0
    end if

end Destructor

#endif