#ifndef Skybox_bi
#define Skybox_bi

#include once "GL/gl.bi"
#include once "GL/glu.bi"
#include once "GL/glext.bi"

#include once "../VoxelEngine.bi"

type SkyboxType
    
    dim Textures(5) as GLuint
    
    dim IBO    as GLuint
    dim VBO(1) as GLuint
    dim VAO    as GLuint
    
    dim ShaderProgram as OpenGL_Shader_TYPE ptr
    
    declare sub Init(FrontTex  as String, _
                     BackTex   as String, _
                     RightTex  as String, _
                     LeftTex   as String, _
                     TopTex    as String, _
                     BottomTex as String)
    
    declare sub Render(ViewMat() as single, ProjMat() as single)
    
    declare Constructor()
    declare Constructor(FrontTex   as String, _
                        BackTex    as String, _
                        RightTex   as String, _
                        LeftTex    as String, _
                        TopTex     as String, _
                        BottomTex  as String, _
                        VertShader as String, _
                        FragShader as String)
                        
    declare Destructor()
    
end type

sub SkyboxType.Init(BackTex   as String, _
                    FrontTex  as String, _
                    BottomTex as String, _
                    TopTex    as String, _
                    LeftTex   as String, _
                    RightTex  as String)

    this.Textures(0) = load_texture(BackTex, 512, 512, GL_CLAMP_TO_EDGE)
    this.Textures(1) = load_texture(FrontTex, 512, 512, GL_CLAMP_TO_EDGE)
    this.Textures(2) = load_texture(BottomTex, 512, 512, GL_CLAMP_TO_EDGE)
    this.Textures(3) = load_texture(TopTex, 512, 512, GL_CLAMP_TO_EDGE)
    this.Textures(4) = load_texture(LeftTex, 512, 512, GL_CLAMP_TO_EDGE)
    this.Textures(5) = load_texture(RightTex, 512, 512, GL_CLAMP_TO_EDGE)
    
    dim pVerticies(71) as GLfloat = { _
         1,-1, 1,   1, 1, 1,   -1, 1, 1,   -1,-1, 1, _ 'Front
         1,-1,-1,   1, 1,-1,   -1, 1,-1,   -1,-1,-1, _ 'Back
         1,-1, 1,   1,-1,-1,   -1,-1,-1,   -1,-1, 1, _ 'Bottom
         1, 1, 1,   1, 1,-1,   -1, 1,-1,   -1, 1, 1, _ 'Top
        -1,-1,-1,  -1, 1,-1,   -1, 1, 1,   -1,-1, 1, _ 'Left
         1,-1,-1,   1, 1,-1,    1, 1, 1,    1,-1, 1}   'Right
    
    dim pTexCoords(47) as GLfloat = { _
        1.0,0.0,   1.0,1.0,   0.0,1.0,   0.0,0.0, _
        0.0,0.0,   0.0,1.0,   1.0,1.0,   1.0,0.0, _
        1.0,1.0,   1.0,0.0,   0.0,0.0,   0.0,1.0, _
        1.0,0.0,   1.0,1.0,   0.0,1.0,   0.0,0.0, _
        0.0,0.0,   0.0,1.0,   1.0,1.0,   1.0,0.0, _
        1.0,0.0,   1.0,1.0,   0.0,1.0,   0.0,0.0}
    
    dim pIndicies(35) as GLubyte = { _
         2, 1, 0,   3, 2, 0, _
         4, 5, 6,   4, 6, 7, _
         8, 9,10,   8,10,11, _
        14,13,12,  15,14,12, _
        16,17,18,  16,18,19, _
        22,21,20,  23,22,20}
    
    glGenBuffers(1, @this.IBO)
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.IBO)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte)*36, @pIndicies(0), GL_STATIC_DRAW)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    
    glGenVertexArrays(1, @this.VAO)
    glBindVertexArray(this.VAO)
    
    glGenBuffers(1, @this.VBO(0))
    glGenBuffers(1, @this.VBO(1))
    
    dim attributeName as zString * 12 = "in_position"
    
    dim attributeLocation as GLuint = glGetAttribLocation(this.ShaderProgram->ShaderHandle, @attributeName)
    glBindBuffer(GL_ARRAY_BUFFER, this.VBO(0))
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*72, @pVerticies(0), GL_STATIC_DRAW)
    glEnableVertexAttribArray(attributeLocation)
    glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, 0)

    attributeName = "in_texCoord"
    attributeLocation = glGetAttribLocation(this.ShaderProgram->ShaderHandle, @attributeName)
    glBindBuffer(GL_ARRAY_BUFFER, this.VBO(1))
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*48, @pTexCoords(0), GL_STATIC_DRAW)
    glEnableVertexAttribArray(attributeLocation)
    glVertexAttribPointer(attributeLocation, 2, GL_FLOAT, GL_FALSE, 0, 0)

    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glBindVertexArray(0)

    glFinish()
    
end sub

sub SkyboxType.Render(ViewMat() as single, ProjMat() as single)
    
    dim i as integer = 0
    
    glUseProgram(this.ShaderProgram->ShaderHandle)
    
    dim ModifiedViewMat(15) as single
    
    ModifiedViewMat(0) = ViewMat(0)
    ModifiedViewMat(1) = ViewMat(1)
    ModifiedViewMat(2) = ViewMat(2)
    
    ModifiedViewMat(4) = ViewMat(4)
    ModifiedViewMat(5) = ViewMat(5)
    ModifiedViewMat(6) = ViewMat(6)
    
    ModifiedViewMat(8) = ViewMat(8)
    ModifiedViewMat(9) = ViewMat(9)
    ModifiedViewMat(10) = ViewMat(10)
    
    ModifiedViewMat(15) = ViewMat(15)
    
    SetUniformMatrix4(this.ShaderProgram->ShaderHandle, "viewMatrix", ModifiedViewMat())
    SetUniformMatrix4(this.ShaderProgram->ShaderHandle, "projMatrix", ProjMat())
    
    glDisable(GL_DEPTH_TEST)
    glDepthMask(0)
    
    glBindVertexArray(this.VAO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.IBO)
    
    glEnable(GL_TEXTURE_2D)
    
    glActiveTexture(GL_TEXTURE0)
    
    for i = 0 to 5
        glBindTexture(GL_TEXTURE_2D, this.Textures(i))
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, cast(ubyte ptr, i * 6))
    next
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
    glBindVertexArray(0)
    
    glDepthMask(1)
    
    glEnable(GL_DEPTH_TEST)
    
    glUseProgram(0)
    
end sub

Constructor SkyboxType()

end Constructor

Constructor SkyboxType(BackTex    as String, _
                       FrontTex   as String, _
                       BottomTex  as String, _
                       TopTex     as String, _
                       LeftTex    as String, _
                       RightTex   as String, _
                       VertShader as String, _
                       FragShader as String)
    
    this.ShaderProgram = new OpenGL_Shader_TYPE(VertShader, FragShader)
    
    this.Init(BackTex, FrontTex, BottomTex, TopTex, LeftTex, RightTex)

end Constructor

Destructor SkyboxType()
    
    dim i as integer
    
    if this.VBO(0) then
        glDeleteBuffers(1, @this.VBO(0))
    end if
    if this.VBO(1) then
        glDeleteBuffers(1, @this.VBO(1))
    end if
    if this.IBO then
        glDeleteBuffers(1, @this.IBO)
    end if
    if this.VAO then
        glDeleteVertexArrays(1, @this.VAO)
    end if
    
    if this.Textures(5) then
        'If the last one was allocated, we assume they all were
        glDeleteTextures(1, @this.Textures(0))
        glDeleteTextures(1, @this.Textures(1))
        glDeleteTextures(1, @this.Textures(2))
        glDeleteTextures(1, @this.Textures(3))
        glDeleteTextures(1, @this.Textures(4))
        glDeleteTextures(1, @this.Textures(5))
        
        for i = 0 to 5
            this.Textures(i) = 0
        next
    end if
    
    if this.ShaderProgram then
        delete(this.ShaderProgram)
    end if
    
end Destructor

#endif