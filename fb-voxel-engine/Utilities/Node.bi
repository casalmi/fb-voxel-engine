#ifndef Node_bi
#define Node_bi

type NodeType
    
    public:
        
        declare Constructor()
        declare Constructor(inData as any ptr)
        declare Constructor(inPreviousNode as NodeType ptr, _
                            inNextNode     as NodeType ptr)
        declare Constructor(inPreviousNode as NodeType ptr, _
                            inNextNode     as NodeType ptr, _
                            inData as any ptr)
        
        declare function GetPrev() as NodeType ptr
        declare function GetNext() as NodeType ptr
        
        declare sub SetPrev(inNode as NodeType ptr)
        declare sub SetNext(inNode as NodeType ptr)
        
        declare sub SetData(inData as any ptr)
        declare function GetData() as any ptr
        
        'User is expected to call "delete" on this pointer
        'after calling "remove"
        declare sub Remove()
        
    private:
    
        dim PrevNode as NodeType ptr
        dim NextNode as NodeType ptr
        
        dim Data as any ptr
        
end type

Constructor NodeType()
    
    this.Constructor(0, 0, 0)
    
end Constructor

Constructor NodeType(inData as any ptr)
    
    this.Constructor(0, 0, inData)

end Constructor

Constructor NodeType(inPreviousNode as NodeType ptr, _
                     inNextNode     as NodeType ptr)
    
    this.Constructor(inPreviousNode, inNextNode, 0)
    
end Constructor

Constructor NodeType(inPreviousNode as NodeType ptr, _
                     inNextNode     as NodeType ptr, _
                     inData as any ptr)

    if inPreviousNode then
        this.PrevNode = inPreviousNode
    end if
    
    if inNextNode then
        this.NextNode = inNextNode
    end if
    
    if inData then
        this.Data = inData
    end if
    
end Constructor

function NodeType.GetPrev() as NodeType ptr
    return this.PrevNode
end function

function NodeType.GetNext() as NodeType ptr
    return this.NextNode
end function

sub NodeType.SetData(inData as any ptr)
    this.Data = inData
end sub

function NodeType.GetData() as any ptr
    return this.Data
end function

sub NodeType.Remove()

    if (this.PrevNode <> 0) AND (this.NextNode <> 0) then
        this.PrevNode->SetNext(this.NextNode)
        this.NextNode->SetPrev(this.PrevNode)
    else
        if this.PrevNode then
            this.PrevNode->SetNext(0)
        end if
        
        if this.NextNode then
            this.NextNode->SetPrev(0)
        end if
    end if
    
    delete(@this)
    
    NodeTypeCount -= 1    

end sub

sub NodeType.SetPrev(inNode as NodeType ptr)
    this.PrevNode = inNode
end sub

sub NodeType.SetNext(inNode as NodeType ptr)
    this.NextNode = inNode
end sub

#endif